<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'Views\PanelController@login');
Route::post('/login', 'Views\PanelController@loguear');
Route::post('olvidar-contrasenia', 'Views\PanelController@olvidarContrasenia');
Route::get('/cambiar-contrasenia/{token}', 'Views\PanelController@cambiarContrasenia');
Route::post('/cambiar-contrasenia/{token}', 'Views\PanelController@cambioContrasenia');

Route::group(['middleware' => ['login', 'volverLink']], function() {

    Route::get('/', 'Views\PanelController@index');
    Route::get('/cerrarSesion', 'Views\PanelController@cerrarSesion');

    Route::group(['prefix' => 'clientes'], function () {

        Route::get('/crear', 'ClienteController@create');
        Route::get('/editar', 'ClienteController@index');
        Route::get('/editar/{id}', 'ClienteController@edit');
        Route::get('/borrar/{id}', 'ClienteController@destroy');

        Route::post('/crear', 'ClienteController@store');
        Route::post('/editar/{id}', 'ClienteController@update');
    });

    Route::group(['prefix' => 'presupuestos'], function() {
        Route::get('/crear', 'PresupuestoController@create');
        Route::get('/editar', 'PresupuestoController@index');
        Route::get('/editar/{id}', 'PresupuestoController@edit');
        Route::get('/borrar/{id}', 'PresupuestoController@destroy');
        Route::get('/finalizados', 'PresupuestoController@finalizados');

        Route::post('/crear', 'PresupuestoController@store');
        Route::post('/editar', 'PresupuestoController@buscar');
        Route::post('/editar/{id}', 'PresupuestoController@update');

        // HEREDADOS DE PRESUPUESTO
        Route::group(['prefix' => '{presupuesto}'], function () {

            // PAGOS
            Route::group(['prefix' => 'direcciones'], function () {

                Route::get('/crear', 'DireccionController@create');
                Route::get('/editar', 'DireccionController@index');
                Route::get('/editar/{id}', 'DireccionController@edit');
                Route::get('/borrar/{id}', 'DireccionController@destroy');

                Route::post('/crear', 'DireccionController@store');
                Route::post('/editar/{id}', 'DireccionController@update');
            });

            // OTROS ADICIONALES
            Route::group(['prefix' => 'otros_adicionales'], function () {

                Route::get('/crear', 'OtroAdicionalController@create');
                Route::get('/editar', 'OtroAdicionalController@index');
                Route::get('/editar/{id}', 'OtroAdicionalController@edit');
                Route::get('/borrar/{id}', 'OtroAdicionalController@destroy');

                Route::post('/crear', 'OtroAdicionalController@store');
                Route::post('/editar/{id}', 'OtroAdicionalController@update');
            });

            // VISITAS
            Route::group(['prefix' => 'visitas'], function () {

                Route::get('/crear', 'VisitaController@create');
                Route::get('/editar/{id}', 'VisitaController@edit');
                Route::get('/borrar/{id}', 'VisitaController@destroy');

                Route::post('/crear', 'VisitaController@store');
                Route::post('/editar/{id}', 'VisitaController@update');
            });

            // PAGOS
            Route::group(['prefix' => 'pagos'], function () {

                Route::get('/crear', 'PagoController@create');
                Route::get('/editar', 'PagoController@index');
                Route::get('/editar/{id}', 'PagoController@edit');
                Route::get('/borrar/{id}', 'PagoController@destroy');

                Route::post('/crear', 'PagoController@store');
                Route::post('/editar/{id}', 'PagoController@update');
            });

            // HISTORIAL DE ESTADOS
            Route::group(['prefix' => 'historiales'], function() {

                Route::get('/editar/{id}', 'HistorialEstadoController@edit');
                Route::get('/borrar/{id}', 'HistorialEstadoController@destroy');

                Route::post('/editar/{id}', 'HistorialEstadoController@update');
            });
        });
    });

    // HEREDADOS DE VISITA
    Route::group(['prefix' => 'visita/{visita}'], function () {

        // DETALLE DE VISITA
//        Route::group(['prefix' => 'detalle'], function () {
//
//            Route::get('/editar/{id}', 'DetalleVisitaController@edit');
//
//            Route::post('/editar/{id}', 'DetalleVisitaController@update');
//        });

        // OBSERVACIONES
        Route::group(['prefix' => 'observaciones'], function () {

            Route::get('/crear', 'ObservacionVisitaController@create');
            Route::get('/editar', 'ObservacionVisitaController@index');
            Route::get('/editar/{id}', 'ObservacionVisitaController@edit');
            Route::get('/borrar/{id}', 'ObservacionVisitaController@destroy');

            Route::post('/crear', 'ObservacionVisitaController@store');
            Route::post('/editar/{id}', 'ObservacionVisitaController@update');
        });
    });

    Route::group(['prefix' => 'llamados'], function () {

        Route::get('/prospectos', 'LlamadosController@prospectos');
        Route::get('/visitas', 'LlamadosController@visitas');
        Route::get('/material', 'LlamadosController@material');
        Route::get('/mudanzas', 'LlamadosController@mudanzas');
        Route::get('/confirmar', 'LlamadosController@confirmar');
    });

    Route::group(['prefix' => 'listados'], function () {

        Route::get('/mudanzas', 'ListadoController@mudanzas');
        Route::get('/visitas', 'ListadoController@visitas');
//        Route::get('/entrega', 'ListadoController@entrega');
    });

    Route::group(['prefix' => 'reportes'], function () {

        Route::get('/vendedores', 'ReportesController@vendedores');
        Route::get('/vendedores/{id}', 'ReportesController@vendedor');
        Route::get('/general', 'ReportesController@general');
        Route::get('/formas_contacto', 'ReportesController@formasDeContacto');
        Route::post('/formas_contacto', 'ReportesController@buscarFormasDeContacto');
        Route::any('/presupuestos_excel', 'ReportesController@presupuestosExcel');
        Route::get('/presupuestos_excel/{nombreArchivo}', 'ReportesController@descargarPresupuestoExcel');
//        Route::post('/presupuestos_excel', 'ReportesController@buscarPresupuestosExcel');
    });

    Route::get('/agenda', 'EventoController@show');

    /** ADMIN */

    Route::group(['prefix' => 'adicionales'], function () {

        Route::get('/crear', 'AdicionalController@create');
        Route::get('/editar', 'AdicionalController@index');
        Route::get('/editar/{id}', 'AdicionalController@edit');
        Route::get('/borrar/{id}', 'AdicionalController@destroy');

        Route::post('/crear', 'AdicionalController@store');
        Route::post('/editar/{id}', 'AdicionalController@update');
    });

    Route::group(['prefix' => 'estados_presupuesto'], function () {

        Route::get('/crear', 'EstadoPresupuestoController@create');
        Route::get('/editar', 'EstadoPresupuestoController@index');
        Route::get('/editar/{id}', 'EstadoPresupuestoController@edit');
        Route::get('/borrar/{id}', 'EstadoPresupuestoController@destroy');

        Route::post('/crear', 'EstadoPresupuestoController@store');
        Route::post('/editar/{id}', 'EstadoPresupuestoController@update');
    });

    Route::group(['prefix' => 'formas_contacto'], function () {

        Route::get('/crear', 'FormaContactoController@create');
        Route::get('/editar', 'FormaContactoController@index');
        Route::get('/editar/{id}', 'FormaContactoController@edit');
        Route::get('/borrar/{id}', 'FormaContactoController@destroy');

        Route::post('/crear', 'FormaContactoController@store');
        Route::post('/editar/{id}', 'FormaContactoController@update');
    });

    Route::group(['prefix' => 'formas_pago'], function () {

        Route::get('/crear', 'FormaPagoController@create');
        Route::get('/editar', 'FormaPagoController@index');
        Route::get('/editar/{id}', 'FormaPagoController@edit');
        Route::get('/borrar/{id}', 'FormaPagoController@destroy');

        Route::post('/crear', 'FormaPagoController@store');
        Route::post('/editar/{id}', 'FormaPagoController@update');
    });

    Route::group(['prefix' => 'servicios'], function () {

        Route::get('/crear', 'ServicioController@create');
        Route::get('/editar', 'ServicioController@index');
        Route::get('/editar/{id}', 'ServicioController@edit');
        Route::get('/borrar/{id}', 'ServicioController@destroy');
        Route::get('/desactivar/{id}', 'ServicioController@desactivar');

        Route::post('/crear', 'ServicioController@store');
        Route::post('/editar/{id}', 'ServicioController@update');
    });

    Route::group(['prefix' => 'vehiculos'], function () {

        Route::get('/crear', 'VehiculoController@create');
        Route::get('/editar', 'VehiculoController@index');
        Route::get('/editar/{id}', 'VehiculoController@edit');
        Route::get('/borrar/{id}', 'VehiculoController@destroy');
        Route::get('/desactivar/{id}', 'VehiculoController@desactivar');

        Route::post('/crear', 'VehiculoController@store');
        Route::post('/editar/{id}', 'VehiculoController@update');
    });

    Route::group(['prefix' => 'usuarios'], function () {

        Route::get('/crear', 'UserController@create');
        Route::get('/editar', 'UserController@index');
        Route::get('/editar/{id}', 'UserController@edit');
        Route::get('/borrar/{id}', 'UserController@destroy');

        Route::post('/crear', 'UserController@store');
        Route::post('/editar/{id}', 'UserController@update');
    });

    Route::group(['prefix' => 'rol_usuarios'], function () {

        Route::get('/crear', 'RolUsuarioController@create');
        Route::get('/editar', 'RolUsuarioController@index');
        Route::get('/editar/{id}', 'RolUsuarioController@edit');
        Route::get('/borrar/{id}', 'RolUsuarioController@destroy');

        Route::post('/crear', 'RolUsuarioController@store');
        Route::post('/editar/{id}', 'RolUsuarioController@update');
    });

    Route::group(['prefix' => 'localidades'], function () {

        Route::get('/crear', 'LocalidadController@create');
        Route::get('/editar', 'LocalidadController@index');
        Route::get('/editar/{id}', 'LocalidadController@edit');
        Route::get('/borrar/{id}', 'LocalidadController@destroy');

        Route::post('/crear', 'LocalidadController@store');
        Route::post('/editar/{id}', 'LocalidadController@update');
    });

    Route::group(['prefix' => 'items_adicionales'], function () {

        Route::get('/crear', 'ItemAdicionalController@create');
        Route::get('/editar', 'ItemAdicionalController@index');
        Route::get('/editar/{id}', 'ItemAdicionalController@edit');
        Route::get('/borrar/{id}', 'ItemAdicionalController@destroy');
        Route::get('/ver', 'ItemAdicionalController@show');

        Route::post('/crear', 'ItemAdicionalController@store');
        Route::post('/editar/{id}', 'ItemAdicionalController@update');
    });

    Route::group(['prefix' => 'items_observaciones'], function () {

        Route::get('/crear', 'ItemObservacionController@create');
        Route::get('/editar', 'ItemObservacionController@index');
        Route::get('/editar/{id}', 'ItemObservacionController@edit');
        Route::get('/borrar/{id}', 'ItemObservacionController@destroy');
        Route::get('/ver', 'ItemObservacionController@show');

        Route::post('/crear', 'ItemObservacionController@store');
        Route::post('/editar/{id}', 'ItemObservacionController@update');
    });

    /** END ADMIN */

    Route::group(['prefix' => 'excel'], function() {

        Route::get('visitasDelDia', 'ExcelController@visitasDelDia');

    });

});

Route::group(['prefix' => 'pdf'], function() {

    // VISTAS DE LOS PDF
    Route::group(['prefix' => 'vista'], function() {
        Route::get('presupuesto/{id}', 'PdfController@presupuestoVista');
        Route::get('resumenDeServicio/{id}', 'PdfController@resumenDeServicioVista');
        Route::get('resumenDeServicioConObservaciones/{id}', 'PdfController@resumenDeServicioConObservacionesVista');
        Route::get('visitasDelDia/{fecha}', 'PdfController@visitasDelDiaVista');
        Route::get('detalleDeVisita/{id}', 'PdfController@detalleDeVisitaVista');
//        Route::get('planillaDeVisita/{id}', 'PdfController@planillaDeVisitaVista');
        Route::get('planillaDeVisitaPorEstado/{estado}/{id}', 'PdfController@planillaDeVisitaPorEstadoVista');

    });

    // PARA DESCARGAR
    Route::get('presupuesto/{id}', 'PdfController@presupuesto');
    Route::get('resumenDeServicio/{id}', 'PdfController@resumenDeServicio');
    Route::post('visitasDelDia', 'PdfController@visitasDelDia');

    // PARA ENVIAR
    Route::group(['prefix' => 'enviar'], function() {
        Route::get('presupuesto/{id}', 'PdfController@enviarPresupuesto');
        Route::get('resumenDeServicio/{id}', 'PdfController@enviarResumenDeServicio');
        Route::get('detalleDeVisita/{id}', 'PdfController@enviarDetalleDeVisita');
        Route::get('materialDeEmbalaje/{id}', 'PdfController@enviarMaterialDeEmbalaje');
        Route::get('mudanza/{id}', 'PdfController@enviarMudanza');
        Route::get('retiroDeSenia/{id}', 'PdfController@enviarRetiroDeSenia');
    });

    // PARA VER
    Route::group(['prefix' => 'ver'], function() {
        Route::get('presupuesto/{id}', 'PdfController@presupuestoVer');
        Route::get('resumenDeServicio/{id}', 'PdfController@resumenDeServicioVer');
        Route::get('resumenDeServicioConObservaciones/{id}', 'PdfController@resumenDeServicioConObservacionesVer');
//        Route::get('planillaDeVisita/{id}', 'PdfController@planillaDeVisitaVer');
        Route::get('planillaDeVisitaPorEstado/{estado}/{id}', 'PdfController@planillaDeVisitaPorEstadoVer');
    });

    Route::get('header/{id}', 'PdfController@header');
    Route::get('headerPlanilla/{id}', 'PdfController@headerPlanilla');

});

Route::group(['prefix' => 'domPdf'], function() {

    // VISTAS DE LOS PDF
    Route::group(['prefix' => 'vista'], function() {
        Route::get('presupuesto/{id}', 'DomPdfController@presupuestoVista');
        Route::get('resumenDeServicio/{id}', 'DomPdfController@resumenDeServicioVista');
        Route::get('resumenDeServicioConObservaciones/{id}', 'DomPdfController@resumenDeServicioConObservacionesVista');
        Route::get('visitasDelDia/{fecha}', 'DomPdfController@visitasDelDiaVista');
        Route::get('detalleDeVisita/{id}', 'DomPdfController@detalleDeVisitaVista');
        Route::get('planillaDeVisita/{id}', 'DomPdfController@planillaDeVisitaVista');
    });

    // PARA DESCARGAR
    Route::get('presupuesto/{id}', 'DomPdfController@presupuesto');
    Route::get('resumenDeServicio/{id}', 'DomPdfController@resumenDeServicio');
    Route::post('visitasDelDia', 'DomPdfController@visitasDelDia');

    // PARA ENVIAR
    Route::group(['prefix' => 'enviar'], function() {
        Route::get('presupuesto/{id}', 'DomPdfController@enviarPresupuesto');
        Route::get('resumenDeServicio/{id}', 'DomPdfController@enviarResumenDeServicio');
        Route::get('detalleDeVisita/{id}', 'DomPdfController@enviarDetalleDeVisita');
        Route::get('materialDeEmbalaje/{id}', 'DomPdfController@enviarMaterialDeEmbalaje');
        Route::get('mudanza/{id}', 'DomPdfController@enviarMudanza');
        Route::get('retiroDeSenia/{id}', 'DomPdfController@enviarRetiroDeSenia');
    });

    // PARA VER
    Route::group(['prefix' => 'ver'], function() {
        Route::get('presupuesto/{id}', 'DomPdfController@presupuestoVer');
        Route::get('resumenDeServicio/{id}', 'DomPdfController@resumenDeServicioVer');
        Route::get('resumenDeServicioConObservaciones/{id}', 'DomPdfController@resumenDeServicioConObservacionesVer');
        Route::get('planillaDeVisita/{id}', 'DomPdfController@planillaDeVisitaVer');
    });

    Route::get('header/{id}', 'DomPdfController@header');
    Route::get('headerPlanilla/{id}', 'DomPdfController@headerPlanilla');

});

Route::group(['prefix' => 'ajax'], function() {
    Route::get('eventos', 'EventoController@index');
    Route::get('vehiculo/{id}', 'VehiculoController@show');

    Route::post('cotizar', 'PresupuestoController@cotizar');
    Route::post('valorAproximado', 'PresupuestoController@valorAproximado');

    Route::get('localidades/{provincia}', 'LocalidadController@obtenerLocalidadesPorProvincia');

    Route::group(['prefix' => 'reportes'], function() {

        Route::get('presupuestos', 'ReportesController@presupuestos');
        Route::get('prospectos', 'ReportesController@prospectos');

        Route::group(['prefix' => 'vendedor/{id}'], function() {
            Route::get('presupuestos', 'ReportesController@vendedorPresupuestoAjax');
            Route::get('prospectos', 'ReportesController@vendedorProspectosAjax');
        });

    });

});

Route::get('/test', function() {

//    $user = new \App\User;
//    $user->nombre = "Rodrigo";
//    $user->apellido = "Rio";
//    $user->email = "rodrigo@eawebagency.com";
//    $user->telefono = "11114444";
//    $user->password = \Illuminate\Support\Facades\Hash::make('123');
//    $user->rol = 1;
//    $user->save();

});

Route::get('test2', function() {

//    $localidades = explode(', ', 'Abasto, Agronomía, Almagro, Balvanera, Barracas, Barrio Norte, Belgrano, Boedo, Buenos Aires, Caba, Caballito, Capital Federal, Chacarita, Ciudad Autónoma De Buenos Aires, Coghlan, Colegiales, Constitución, Flores, Floresta, La Boca, Liniers, Mataderos, Microcentro, Monte Castro, Montserrat, Nueva Pompeya, Núñez, Palermo, Palermo Viejo, Parque Avellaneda, Parque Chacabuco, Parque Patricios, Paternal, Puerto Madero, Recoleta, Retiro, Saavedra, San Cristobal, San Nicolás, San Telmo, Velez Sarsfield, Versalles, Villa Crespo, Villa del Parque, Villa Devoto, Villa General Mitre, Villa Lugano, Villa Luro, Villa Ortúzar, Villa Pueyrredón, Villa Real, Villa Riachuelo, Villa Santa Rita, Villa Soldati, Villa Urquiza ');
//
//    foreach($localidades as $localidad) {
//        \Illuminate\Support\Facades\DB::table('localidades')
//            ->insert([
//                'provincia' => 'Ciudad Autonoma de Buenos Aires',
//                'nombre' => $localidad,
//            ]);
//    }

});