<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtroAdicionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otro_adicionals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->float('valor');
            $table->date('fecha')->nullable();
            $table->time('hora_inicio')->nullable();
            $table->time('hora_fin')->nullable();
            $table->integer('personal')->nullable();
            $table->float('duracion')->nullable();
            $table->text('observaciones')->nullable();
            $table->timestamps();
        });

        Schema::table('otro_adicionals', function (Blueprint $table) {
            $table->unsignedInteger('presupuesto');

            $table->foreign('presupuesto')->references('id')->on('presupuestos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otro_adicionals');
    }
}
