<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleVisitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_visitas', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('embalaje');
            $table->integer('percheros_moviles');
            $table->integer('cajas');
            $table->integer('horas');
            $table->date('fecha')->nullable();
            $table->time('hora_desde')->nullable();
            $table->time('hora_hasta')->nullable();
            $table->timestamps();
        });

        Schema::table('detalle_visitas', function (Blueprint $table) {
            $table->unsignedInteger('visita');

            $table->foreign('visita')->references('id')->on('visitas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_visitas');
    }
}
