<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha')->nullable();
            $table->time('hora_inicio')->nullable();
            $table->time('hora_fin')->nullable();
            $table->text('observaciones')->nullable();
            $table->text('observaciones_carga')->nullable();
            $table->text('observaciones_descarga')->nullable();
            $table->timestamps();
        });

        Schema::table('visitas', function (Blueprint $table) {
            $table->unsignedInteger('presupuesto');

            $table->foreign('presupuesto')->references('id')->on('presupuestos')->onDelete('cascade');
        });

        Schema::table('visitas', function (Blueprint $table) {
            $table->unsignedInteger('visitador')->nullable();

            $table->foreign('visitador')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitas');
    }
}
