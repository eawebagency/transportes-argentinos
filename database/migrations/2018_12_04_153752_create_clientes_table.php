<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('nombre')->nullable();
            $table->text('apellido')->nullable();
            $table->text('email')->nullable();
            $table->text('telefono')->nullable();
            $table->boolean('wpp')->default(false);
            $table->text('telefono_alternativo')->nullable();
            $table->text('observaciones')->nullable();
            $table->boolean('prospecto')->default(false);
            $table->timestamps();
        });

        Schema::table('clientes', function (Blueprint $table) {
            $table->unsignedInteger('usuario');

            $table->foreign('usuario')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('clientes', function (Blueprint $table) {
            $table->unsignedInteger('contacto')->nullable();

            $table->foreign('contacto')->references('id')->on('forma_contactos')->onDelete('cascade');
        });

        Schema::table('clientes', function (Blueprint $table) {
            $table->unsignedInteger('ultimoUsuario');

            $table->foreign('ultimoUsuario')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('clientes', function (Blueprint $table) {
            $table->unsignedInteger('estado')->nullable();

            $table->foreign('estado')->references('id')->on('estado_clientes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
