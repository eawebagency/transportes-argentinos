<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_estados', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha')->nullable();
            $table->time('desde')->nullable();
            $table->time('hasta')->nullable();
            $table->text('observaciones')->nullable();
            $table->timestamps();
        });

        Schema::table('historial_estados', function (Blueprint $table) {
            $table->unsignedInteger('presupuesto');

            $table->foreign('presupuesto')->references('id')->on('presupuestos')->onDelete('cascade');
        });

        Schema::table('historial_estados', function (Blueprint $table) {
            $table->unsignedInteger('usuario');

            $table->foreign('usuario')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('historial_estados', function (Blueprint $table) {
            $table->unsignedInteger('estado');

            $table->foreign('estado')->references('id')->on('estado_presupuestos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_estados');
    }
}
