<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDireccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direccions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('direccion')->nullable();
            $table->text('departamento')->nullable();
            $table->text('piso')->nullable();
            $table->text('entrecalles')->nullable();
            $table->text('provincia')->nullable();
            $table->text('localidad')->nullable();
            $table->text('observaciones')->nullable();
            $table->boolean('carga');
            $table->timestamps();
        });

        Schema::table('direccions', function (Blueprint $table) {
            $table->unsignedInteger('presupuesto');

            $table->foreign('presupuesto')->references('id')->on('presupuestos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direccions');
    }
}
