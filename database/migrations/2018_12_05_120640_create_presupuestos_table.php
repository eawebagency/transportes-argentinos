<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresupuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuestos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->time('hora')->nullable();
            $table->time('hora_fin')->nullable();
            $table->integer('ambientes')->nullable();
            $table->float('kms')->nullable();
            $table->integer('personal')->nullable();
            $table->float('valor_aproximado');
            $table->float('cotizacion');
            $table->float('total');
            $table->date('vencimiento');
            $table->boolean('larga_distancia');
            $table->text('observaciones')->nullable();
            $table->date('fecha_estado')->nullable();
            $table->time('hora_desde')->nullable();
            $table->time('hora_hasta')->nullable();
            $table->boolean('enviarDetalle')->default(0);
            $table->boolean('enviarPresupuesto')->default(0);
            $table->boolean('enviarMaterial')->default(0);
            $table->boolean('enviarMudanza')->default(0);
            $table->boolean('enviarRetiroDeSenia')->default(0);
            $table->timestamps();
        });

        Schema::table('presupuestos', function (Blueprint $table) {
            $table->unsignedInteger('vehiculo');

            $table->foreign('vehiculo')->references('id')->on('vehiculos')->onDelete('cascade');
        });

        Schema::table('presupuestos', function (Blueprint $table) {
            $table->unsignedInteger('servicio');

            $table->foreign('servicio')->references('id')->on('servicios')->onDelete('cascade');
        });

        Schema::table('presupuestos', function (Blueprint $table) {
            $table->unsignedInteger('estado');

            $table->foreign('estado')->references('id')->on('estado_presupuestos')->onDelete('cascade');
        });

        Schema::table('presupuestos', function (Blueprint $table) {
            $table->unsignedInteger('cliente')->nullable();

            $table->foreign('cliente')->references('id')->on('clientes')->onDelete('cascade');
        });

        Schema::table('presupuestos', function (Blueprint $table) {
            $table->unsignedInteger('vendedor');

            $table->foreign('vendedor')->references('id')->on('users')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presupuestos');
    }
}
