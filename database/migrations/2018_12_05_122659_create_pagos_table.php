<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->float('monto');
            $table->date('fecha');
            $table->boolean('pagorecibido'); // TRUE LO RECIBIO ADMINISTRACION, FALSE LO RECIBIO LOGISTICA
            $table->text('observaciones')->nullable();
            $table->text('comprobante')->nullable();
            $table->timestamps();
        });

        Schema::table('pagos', function (Blueprint $table) {
            $table->unsignedInteger('presupuesto');

            $table->foreign('presupuesto')->references('id')->on('presupuestos')->onDelete('cascade');
        });

        Schema::table('pagos', function (Blueprint $table) {
            $table->unsignedInteger('usuario');

            $table->foreign('usuario')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('pagos', function (Blueprint $table) {
            $table->unsignedInteger('forma');

            $table->foreign('forma')->references('id')->on('forma_pagos')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
