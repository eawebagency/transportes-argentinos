<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresupuestoAdicionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuesto_adicionals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::table('presupuesto_adicionals', function (Blueprint $table) {
            $table->unsignedInteger('presupuesto');

            $table->foreign('presupuesto')->references('id')->on('presupuestos')->onDelete('cascade');
        });

        Schema::table('presupuesto_adicionals', function (Blueprint $table) {
            $table->unsignedInteger('adicional')->nullable();

            $table->foreign('adicional')->references('id')->on('adicionals')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presupuesto_adicionals');
    }
}
