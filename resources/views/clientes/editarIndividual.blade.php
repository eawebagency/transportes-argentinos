@extends('master', ['seccionActiva' => 'Clientes'])

@section('titulo', 'Editar Cliente')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    <div class="row">
        {{ Form::open(['class' => 'form', 'files' => true]) }}
        <div class="m-portlet">

            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-12 col-sm-3">
                        <div class="form-group">
                            {{ Form::label('nombre', 'Nombre') }}
                            {{ Form::text('nombre', $elemento->nombre, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="form-group">
                            {{ Form::label('apellido', 'Apellido') }}
                            {{ Form::text('apellido', $elemento->apellido, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="form-group">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::email('email', $elemento->email, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="form-group">
                            {{ Form::label('telefono', 'Telefono') }}
                            {{ Form::text('telefono', $elemento->telefono, ['class' => 'form-control input-telefono', 'required' => 'required']) }}
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="form-group">
                            {{ Form::label('wpp', 'Whatsapp?') }}
                            {{ Form::select('wpp', ['1' => 'Si', '0' => 'No'], $elemento->wpp, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="form-group">
                            {{ Form::label('telefono_alternativo', 'Teléfono alternativo') }}
                            {{ Form::text('telefono_alternativo', $elemento->telefono_alternativo, ['class' => 'form-control input-telefono']) }}
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="form-group">
                            {{ Form::label('contacto', 'Contacto') }}
                            {{ Form::select('contacto', $formas, $elemento->contacto, ['class' => 'form-control', 'placeholder' => 'Seleccionar forma de contacto']) }}
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="form-group">
                            {{ Form::label('estado', 'Estado') }}
                            {{ Form::select('estado', $estados, $elemento->estado, ['class' => 'form-control', 'placeholder' => 'Seleccionar estado']) }}
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="form-group">
                            {{ Form::label('usuario', 'Usuario') }}
                            {{ Form::text('usuario', $elemento->ultimoUsuario()->nombre.' '.$elemento->ultimoUsuario()->apellido, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            {{ Form::label('observaciones', 'Observaciones') }}
                            {{ Form::textarea('observaciones', $elemento->observaciones, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

            </div>

            <div class="m-portlet__foot">
                <div class="m-form__actions">
                    {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
                    <a href="{{ url('presupuestos/crear?cliente='.$elemento->id) }}" class="btn btn-success" title="Crear Presupuesto">
                        <i class="la la-plus"></i> Crear presupuesto
                    </a>
                    <a href="{{ Session::get('urlPrevia') }}" class="btn btn-metal" title="Volver">
                        < Volver
                    </a>
                </div>
            </div>

        </div>

        {{ Form::close() }}
    </div>

@endsection