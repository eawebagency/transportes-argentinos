@extends('master', ['seccionActiva' => 'Clientes'])

@section('titulo', 'Editar Cliente')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    <div class="m-portlet m-portlet--mobile  m-portlet--unair">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Clientes
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Forma de contacto</th>
                            <th>Apellido y Nombre</th>
                            <th>Teléfono</th>
                            <th>Operador</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($elementos as $elemento)
                        <tr>
                            <td>{{ \Carbon\Carbon::parse($elemento->created_at)->format('d/m/Y') }}</td>
                            <td>{{ $elemento->contacto()->nombre }}</td>
                            <td>
                                {{ $elemento->apellido }} {{ $elemento->nombre }}
                            </td>
                            <td>
                                {{ $elemento->telefono }} @if($elemento->wpp == 1 && !is_null($elemento->telefono)) (Tiene Whatsapp) @endif
                            </td>
                            <td>
                                {{ $elemento->ultimoUsuario()->nombre }} {{ $elemento->ultimoUsuario()->apellido }}
                            </td>
                            <td>
                                <a href="{{ url('clientes/editar/'.$elemento->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">
                                    <i class="la la-edit"></i>
                                </a>
                                {{--<a href="{{ url('clientes/borrar/'.$elemento->id) }}" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')">--}}
                                    {{--<i class="la la-trash"></i>--}}
                                {{--</a>--}}

                                <a href="{{ url('presupuestos/crear?cliente='.$elemento->id) }}" class="btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Crear Presupuesto">
                                    <i class="la la-plus"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection