@extends('master', ['seccionActiva' => 'Home'])

@section('titulo', 'Home')

@section('contenido')

    <h5>Bienvenido al panel de administración</h5>

    <div class="row">

        <!-- CANTIDADES -->

        <div class="col-12 col-sm-6">
            <div class="m-portlet m-portlet--bordered-semi">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Actividad en el mes
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">

                    <div class="m-widget4">
                        <div class="m-widget4__item">
                            <div class="m-widget4__info">
                        <span class="m-widget4__title">
                            Consultas del mes
                        </span>
                                <br>
                            </div>
                            <span class="m-widget4__ext">
                        <span class="m-widget4__number m--font-danger">{{ $consultasDelMes }}</span>
                    </span>
                        </div>
                        <div class="m-widget4__item">
                            <div class="m-widget4__info">
                        <span class="m-widget4__title">
                            Presupuestos enviados
                        </span>
                                <br>
                            </div>
                            <span class="m-widget4__ext">
                        <span class="m-widget4__number m--font-danger">{{ $presupuestosEnviados }}</span>
                    </span>
                        </div>
                        <div class="m-widget4__item">
                            <div class="m-widget4__info">
                        <span class="m-widget4__title">
                            Visitas Confirmadas
                        </span>
                                <br>
                            </div>
                            <span class="m-widget4__ext">
                        <span class="m-widget4__number m--font-danger">{{ $visitasConfirmadas }}</span>
                    </span>
                        </div>
                        <div class="m-widget4__item">
                            <div class="m-widget4__info">
                        <span class="m-widget4__title">
                            Mudanzas Confirmadas
                        </span>
                                <br>
                            </div>
                            <span class="m-widget4__ext">
                        <span class="m-widget4__number m--font-danger">{{ $mudanzasConfirmadas }}</span>
                    </span>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6">

            <div class="m-portlet m-portlet--bordered-semi">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Llamados a Prospectos
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Apellido y Nombre</th>
                                <th>Forma de contacto</th>
                                <th>Teléfono</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($llamadosAProspectos as $elemento)
                                <tr>
                                    <td>
                                        {{ $elemento->apellido }} {{ $elemento->nombre }}
                                    </td>
                                    <td>
                                        {{ $elemento->contacto()->nombre }}
                                    </td>
                                    <td>
                                        {{ $elemento->telefono }} @if($elemento->wpp == 1 && !is_null($elemento->telefono)) (Tiene Whatsapp) @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('clientes/editar/'.$elemento->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">
                                            <i class="la la-edit"></i>
                                        </a>
                                        <a href="{{ url('clientes/borrar/'.$elemento->id) }}" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6">

            <div class="m-portlet m-portlet--bordered-semi">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Mudanzas de Hoy
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nro. Presupuesto</th>
                                <th>Cliente</th>
                                <th>Fecha de presupuesto</th>
                                <th>Fecha de mudanza</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($mudanzasDeHoy as $elemento)

                                <tr>
                                    <td>
                                        {{ $elemento->id }}
                                    </td>
                                    <td>
                                        {{ $elemento->cliente()->apellido }} {{ $elemento->cliente()->nombre }}
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($elemento->created_at)->format('d/m/Y') }}
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($elemento->fecha)->format('d/m/Y') }}
                                    </td>
                                    <td>
                                <span class="m-badge m-badge--brand m-badge--wide" style="background: #{{ $elemento->estado()->color }}">
                                    {{ $elemento->estado()->nombre }}
                                </span>
                                    </td>
                                    <td>
                                        <a href="{{ url('presupuestos/editar/'.$elemento->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">
                                            <i class="la la-edit"></i>
                                        </a>
                                        <a href="{{ url('presupuestos/borrar/'.$elemento->id) }}" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6">

            <div class="m-portlet m-portlet--bordered-semi">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Visitas de Hoy
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nro. Presupuesto</th>
                                <th>Cliente</th>
                                <th>Fecha de presupuesto</th>
                                <th>Fecha de visita</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($visitasDeHoy as $elemento)

                                <tr>
                                    <td>
                                        {{ $elemento->presupuesto()->id }}
                                    </td>
                                    <td>
                                        {{ $elemento->presupuesto()->cliente()->apellido }} {{ $elemento->presupuesto()->cliente()->nombre }}
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($elemento->presupuesto()->created_at)->format('d/m/Y') }}
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($elemento->fecha)->format('d/m/Y') }}
                                    </td>
                                    <td>
                                <span class="m-badge m-badge--brand m-badge--wide" style="background: #{{ $elemento->estado()->color }}">
                                    {{ $elemento->presupuesto()->estado()->nombre }}
                                </span>
                                    </td>
                                    <td>
                                        <a href="{{ url('presupuestos/editar/'.$elemento->presupuesto) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">
                                            <i class="la la-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')

@endsection