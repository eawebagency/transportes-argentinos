@extends('master', ['seccionActiva' => 'Agenda'])

@section('titulo', 'Agenda')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif
    <div class="m-portlet">

        <div class="m-portlet__body">

            <div id='calendario'></div>

        </div>

        <div class="m-portlet__foot">
            <div class="m-form__actions">
                {{--{{ Form::submit('Editar', ['class' => 'btn btn-primary']) }}--}}
            </div>
        </div>

    </div>

    @foreach($elementos as $index => $elemento)
        <div class="modal fade" id="evento-{{ $index }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ $elemento->titulo }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {!! $elemento->descripcion !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@endsection

@section('scripts')

    <script src='{{ asset('js/fullcalendar/lib/moment.min.js') }}'></script>
    <script src='{{ asset('js/fullcalendar/fullcalendar.min.js') }}'></script>
    <script src="{{ asset('js/fullcalendar/locale-all.js') }}" type="text/javascript"></script>

    <script>
        $(document).ready(function() {

            $.get('/ajax/eventos', function(eventosJSON) {

                var eventos = [];

                $.each(eventosJSON, function(index, evento) {

                    eventos.push({
                        'title': evento.titulo,
                        'description': evento.descripcion,
                        'start': evento.fecha_inicio + " " + evento.hora_inicio,
                        'end': evento.fecha_fin + " " + evento.hora_fin,
                        'color': evento.color,
                        'textColor': evento.colorTexto,
                        'evento-id': index,
                        'orden': evento.orden,
                    });
                });

                $('#calendario').fullCalendar({
                    lang: 'es',
                    locale: 'es',
                    eventSources: [
                        {
                            events: eventos,
                        },
                    ],
                    timeFormat: 'H:mm',
                    eventClick: function(calEvent, jsEvent, view) {

                        $('#evento-' + calEvent['evento-id']).modal('show');

                    },
                });
            });
        });
    </script>

@endsection