@extends('master', ['seccionActiva' => 'Presupuestos'])

@section('titulo', 'Editar Dirección')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    {{ Form::open(['class' => 'form', 'files' => true]) }}

    <div class="m-portlet">

        <div class="m-portlet__body">

            <div class="row">

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        {{ Form::label('direccion', 'Dirección') }}
                        {{ Form::text('direccion', $elemento->direccion, ['class' => 'form-control', 'required' => 'required']) }}
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        {{ Form::label('piso', 'Piso') }}
                        {{ Form::text('piso', $elemento->piso, ['class' => 'form-control', 'required' => 'required']) }}
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        {{ Form::label('departamento', 'Departamento') }}
                        {{ Form::text('departamento', $elemento->departamento, ['class' => 'form-control', 'required' => 'required']) }}
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        {{ Form::label('entrecalles', 'Entrecalles') }}
                        {{ Form::text('entrecalles', $elemento->entrecalles, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        {{ Form::label('provincia', 'Provincia') }}
                        {{ Form::select('provincia', ['Ciudad Autonoma de Buenos Aires' => 'Ciudad Autonoma de Buenos Aires', 'Buenos Aires' => 'Buenos Aires', 'Catamarca' => 'Catamarca', 'Chaco' => 'Chaco', 'Chubut' => 'Chubut', 'Cordoba' => 'Cordoba', 'Corrientes' => 'Corrientes', 'Entre Rios' => 'Entre Rios', 'Formosa' => 'Formosa', 'Jujuy' => 'Jujuy', 'La Pampa' => 'La Pampa', 'La Rioja' => 'La Rioja', 'Mendoza' => 'Mendoza', 'Misiones' => 'Misiones', 'Neuquen' => 'Neuquen', 'Rio Negro' => 'Rio Negro', 'Salta' => 'Salta', 'San Juan' => 'San Juan', 'San Luis' => 'San Luis', 'Santa Cruz' => 'Santa Cruz', 'Santa Fe' => 'Santa Fe', 'Santiago del Estero' => 'Santiago del Estero', 'Tierra del Fuego' => 'Tierra del Fuego', 'Tucuman' => 'Tucuman'], $elemento->provincia, ['class' => 'form-control', 'required' => 'required']) }}
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        {{ Form::label('localidad', 'Localidad') }}
                        {{ Form::select('localidad', [], $elemento->localidad, ['class' => 'form-control', 'required' => 'required']) }}
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group">
                        {{ Form::label('observaciones', 'Observaciones') }}
                        {{ Form::textarea('observaciones', $elemento->observaciones, ['class' => 'form-control']) }}
                    </div>
                </div>


            </div>

        </div>

        <div class="m-portlet__foot">
            <div class="m-form__actions">
                {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
                <a href="{{ url('presupuestos/editar/'.$elemento->presupuesto) }}" class="btn btn-info">
                    < Volver
                </a>
            </div>
        </div>

    </div>

    {{ Form::close() }}

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {

            function actualizarProvincia() {

                $select = $('select[name="provincia"]');

                if($($select).val() != '' && $($select).val() != null) {

                    $.get('/ajax/localidades/' + $($select).val(), function (data) {

                        $options = "";

                        $.each(data, function(index, localidad) {
                            $options += '<option value="' + localidad.nombre + '">' + localidad.nombre + '</option>';
                        });

                        // ACTUALIZAMOS LA LOCALIDAD
                        $localidad = $('select[name="localidad"]');
                        $localidad.attr('data-live-search', true);
                        $localidad.empty().append($options);
                        $localidad.selectpicker('refresh');
                    });
                }
            }

            $('select[name="provincia"]').change(function() {

                actualizarProvincia();

            });

            actualizarProvincia();

        });

    </script>

@endsection