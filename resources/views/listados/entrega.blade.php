@extends('master', ['seccionActiva' => 'Listados'])

@section('titulo', 'Entrega de embalaje')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    <div class="m-portlet m-portlet--mobile  m-portlet--unair">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Entregas de material
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Nro. Presupuesto</th>
                        <th>Fecha</th>
                        <th>Hora</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($elementos as $elemento)
                        <tr>
                            <td>
                                {{ $elemento->presupuesto }}
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($elemento->fecha)->format('d/m/Y') }}
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($elemento->desde)->format('H:i') }} - {{ \Carbon\Carbon::parse($elemento->hasta)->format('H:i') }}
                            </td>
                            <td>
                                <a href="{{ url('presupuestos/editar/'.$elemento->presupuesto) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">
                                    <i class="la la-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection