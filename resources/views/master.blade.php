<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <title>{{ config('app.name') }} | Administrador</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/dataTable.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/admin.css") }}" rel="stylesheet" >

    <link href="{{ asset('adminAssets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('adminAssets/demo/demo12/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('adminAssets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{ asset('js/awesomplete/awesomplete.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">

    {{--<link rel="shortcut icon" href="{{ asset('adminAssets/demo/demo12/media/img/logo/favicon.ico') }}" />--}}
    <link rel="shortcut icon" href="{{ asset('img/logos/logo.png') }}" />
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<div class="m-grid m-grid--hor m-grid--root m-page">

    <header id="m_header" class="m-grid__item m-header" m-minimize-offset="200" m-minimize-mobile-offset="200">
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">

                <div class="m-stack__item m-brand  m-brand--skin-dark ">
                    <div class="m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-stack__item--middle m-brand__logo">
                            <a href="{{ url('/') }}" class="m-brand__logo-wrapper">
                                <img alt="" src="{{ asset('img/logos/logo.png') }}" style="max-width: 50px !important; max-height: 150px !important;" />
                            </a>
                        </div>
                        <div class="m-stack__item m-stack__item--middle m-brand__tools">

                            {{--<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">--}}
                                {{--<span></span>--}}
                            {{--</a>--}}

                            <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            {{--<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">--}}
                                {{--<span></span>--}}
                            {{--</a>--}}

                            {{--<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">--}}
                                {{--<i class="flaticon-more"></i>--}}
                            {{--</a>--}}

                        </div>
                    </div>
                </div>

                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                    <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
                        <ul class="m-menu__nav">
                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
                                <a href="javascript:;" class="m-menu__link m-menu__toggle" title="General">
                                    <i class="m-menu__link-icon flaticon-more"></i>
                                    <span class="m-menu__link-text">General</span>
                                    <i class="m-menu__hor-arrow la la-angle-down"></i>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
                                    <ul class="m-menu__subnav">

                                        <li class="m-menu__item  m-menu__item--submenu" m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle" title="Usuarios">
                                                <i class="m-menu__link-icon flaticon-user"></i>
                                                <span class="m-menu__link-text">Usuarios</span>
                                                <i class="m-menu__hor-arrow la la-angle-right"></i>
                                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                                            </a>

                                            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right"><span class="m-menu__arrow "></span>
                                                <ul class="m-menu__subnav">
                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                        <a href="{{ url('admins/usuarios/crear') }}" class="m-menu__link ">
                                                            <span class="m-menu__link-text">Crear</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                        <a href="{{ url('admins/usuarios/editar') }}" class="m-menu__link ">
                                                            <span class="m-menu__link-text">Listar</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-menu__item  m-menu__item--submenu" m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle" title="Redes">
                                                <i class="m-menu__link-icon flaticon-facebook-logo-button"></i>
                                                <span class="m-menu__link-text">Redes</span>
                                                <i class="m-menu__hor-arrow la la-angle-right"></i>
                                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                                            </a>

                                            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right"><span class="m-menu__arrow "></span>
                                                <ul class="m-menu__subnav">
                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                        <a href="{{ url('admins/redes/crear') }}" class="m-menu__link ">
                                                            <span class="m-menu__link-text">Crear</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                        <a href="{{ url('admins/redes/editar') }}" class="m-menu__link ">
                                                            <span class="m-menu__link-text">Listar</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-menu__item  m-menu__item--submenu" m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle" title="Datos">
                                                <i class="m-menu__link-icon flaticon-notes"></i>
                                                <span class="m-menu__link-text">Datos</span>
                                                <i class="m-menu__hor-arrow la la-angle-right"></i>
                                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                                            </a>

                                            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right"><span class="m-menu__arrow "></span>
                                                <ul class="m-menu__subnav">
                                                    <li class="m-menu__item" m-menu-link-redirect="1" aria-haspopup="true">
                                                        <a href="{{ url('admins/datos/editar') }}" class="m-menu__link ">
                                                            <span class="m-menu__link-text">Editar</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-menu__item  m-menu__item--submenu" m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle" title="Datos">
                                                <i class="m-menu__link-icon flaticon-earth-globe"></i>
                                                <span class="m-menu__link-text">Metadatos</span>
                                                <i class="m-menu__hor-arrow la la-angle-right"></i>
                                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                                            </a>

                                            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right"><span class="m-menu__arrow "></span>
                                                <ul class="m-menu__subnav">
                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                        <a href="{{ url('admins/metadatos/crear') }}" class="m-menu__link ">
                                                            <span class="m-menu__link-text">Crear</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                        <a href="{{ url('admins/metadatos/editar') }}" class="m-menu__link ">
                                                            <span class="m-menu__link-text">Listar</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                        </ul>
                    </div>

                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item m-topbar__notifications m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                    <a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
                                        <span class="m-nav__link-icon">
                                            <span class="m-nav__link-icon-wrapper"><i class="flaticon-alarm"></i></span>
                                            <span class="m-nav__link-badge m-badge m-badge--danger">{{ count($notificacion->get()) }}</span>
                                        </span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center">
                                                <span class="m-dropdown__header-title">{{ count($notificacion->get()) }} nuevas</span>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
                                                        <div class="m-list-timeline m-list-timeline--skin-light">
                                                            <div class="m-list-timeline__items">
                                                                @foreach($notificacion->get() as $notiIndividual)
                                                                <div class="m-list-timeline__item">
                                                                    <span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
                                                                    <span class="m-list-timeline__text">{{ $notiIndividual->titulo }}</span>
                                                                    <span class="m-list-timeline__time">{{ $notiIndividual->obtenerTiempo() }}</span>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">

                                    <a href="#" class="m-nav__link m-dropdown__toggle">

                                        <span class="m-nav__link-icon">
                                            <span class="m-nav__link-icon-wrapper">
                                                <i class="flaticon-settings-1"></i>
                                            </span>
                                        </span>
                                        <span class="m-topbar__username m--hide">Nick</span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center">
                                                <div class="m-card-user m-card-user--skin-light">
                                                    <div class="m-card-user__pic">
                                                        {{--<img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt="" />--}}
                                                    </div>
                                                    <div class="m-card-user__details">
                                                        <span class="m-card-user__name m--font-weight-500">{{ $usuario->nombre }}</span>
                                                        <a href="" class="m-card-user__email m--font-weight-300 m-link">{{ $usuario->email }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav m-nav--skin-light">
                                                        <li class="m-nav__item">
                                                            <a href="{{ url('cerrarSesion') }}" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </header>

    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

            <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
                <ul class="m-menu__nav ">

                    <li class="m-menu__item  m-menu__item{{ ($seccionActiva == 'Home') ? "--active" : "" }}" aria-haspopup="true">
                        <a href="{{ url('/') }}" class="">
                            <span class="m-menu__item-here"></span>
                            {{--<i class="m-menu__link-icon flaticon-home"></i>--}}
                            <div class="text-center ">
                                <img src="{{ asset('img/iconos/casita-low.png') }}" class="m-imagen img-fluid" style="width: 50px; height: 50px;">
                            </div>
                            <span class="m-menu__link-text">Home</span>
                        </a>
                    </li>

                    <li class="m-menu__item  m-menu__item--submenu m-menu__item{{ ($seccionActiva == 'Clientes') ? "--active m-menu__item--open" : "" }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class=" m-menu__toggle">
                            <span class="m-menu__item-here"></span>
                            {{--<i class="m-menu__link-icon flaticon-users"></i>--}}
                            <div class="text-center ">
                                <img src="{{ asset('img/iconos/hombres-low.png') }}" class="m-imagen img-fluid" style="width: 50px; height: 50px;">
                            </div>
                            <span class="m-menu__link-text">Clientes</span>
                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                        </a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">Clientes</span></span>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="{{ url('clientes/crear') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-crear.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Crear</span>
                                    </a>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ url('clientes/editar') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Listar</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item  m-menu__item--submenu m-menu__item{{ ($seccionActiva == 'Presupuestos') ? "--active m-menu__item--open" : "" }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__toggle">
                            <span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-piggy-bank"></i>
                            <span class="m-menu__link-text">Presupuestos</span>
                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                        </a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">Presupuestos</span></span>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="{{ url('presupuestos/crear') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-crear.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Crear</span>
                                    </a>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ url('presupuestos/editar') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Listar</span>
                                    </a>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ url('presupuestos/finalizados') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-apoin.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Finalizados</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item  m-menu__item--submenu m-menu__item{{ ($seccionActiva == 'Llamados') ? "--active m-menu__item--open" : "" }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__toggle">
                            <span class="m-menu__item-here"></span>
                            {{--<i class="m-menu__link-icon flaticon-support"></i>--}}
                            <div class="text-center ">
                                <img src="{{ asset('img/iconos/llamados-low.png') }}" class="m-imagen img-fluid" style="width: 50px; height: 50px;">
                            </div>
                            <span class="m-menu__link-text">Actividades del día</span>
                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                        </a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">Actividades del día</span></span>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="{{ url('llamados/prospectos') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-prospectos.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Nuevos clientes</span>
                                    </a>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ url('llamados/visitas') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-visita.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Visitas y entregas</span>
                                    </a>
                                </li>

                                {{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
                                    {{--<a href="{{ url('llamados/material') }}" class="m-menu__link ">--}}
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        {{--<img src="{{ asset('img/iconos/sub-mudanza.png') }}" class="img-fluid">--}}
                                        {{--<span class="m-menu__link-text">Material de embalaje</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ url('llamados/mudanzas') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-mudanza-domani.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Mudanzas de mañana</span>
                                    </a>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ url('llamados/confirmar') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-apoin.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Llamados</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item  m-menu__item--submenu m-menu__item{{ ($seccionActiva == 'Listados') ? "--active m-menu__item--open" : "" }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__toggle">
                            <span class="m-menu__item-here"></span>
                            {{--<i class="m-menu__link-icon flaticon-list"></i>--}}
                            <div class="text-center ">
                                <img src="{{ asset('img/iconos/listados-low.png') }}" class="m-imagen img-fluid" style="width: 50px; height: 50px;">
                            </div>
                            <span class="m-menu__link-text">Listados</span>
                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                        </a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">Listados</span></span>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="{{ url('listados/mudanzas') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-mudanza.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Mudanzas</span>
                                    </a>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                    <a href="{{ url('listados/visitas') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-visita.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Visitas y entregas</span>
                                    </a>
                                </li>

                                {{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
                                    {{--<a href="{{ url('listados/entrega') }}" class="m-menu__link ">--}}
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        {{--<img src="{{ asset('img/iconos/sub-mudanza-domani.png') }}" class="img-fluid">--}}
                                        {{--<span class="m-menu__link-text">Material de embalaje</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item  m-menu__item--submenu m-menu__item{{ ($seccionActiva == 'Reportes') ? "--active m-menu__item--open" : "" }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__toggle">
                            <span class="m-menu__item-here"></span>
                            {{--<i class="m-menu__link-icon flaticon-list"></i>--}}
                            <i class="m-menu__link-icon flaticon-graphic"></i>
                            <span class="m-menu__link-text">Reportes</span>
                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                        </a>

                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                    <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">Reportes</span></span>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="{{ url('reportes/vendedores') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Vendedores</span>
                                    </a>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="{{ url('reportes/general') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">General</span>
                                    </a>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="{{ url('reportes/formas_contacto') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Formas de contacto</span>
                                    </a>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="{{ url('reportes/presupuestos_excel') }}" class="m-menu__link ">
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                        <span class="m-menu__link-text">Excel Presupuestos</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item  m-menu__item{{ ($seccionActiva == 'Agenda') ? "--active" : "" }}" aria-haspopup="true">
                        <a href="{{ url('/agenda') }}" class=" ">
                            <span class="m-menu__item-here"></span>
                            <i class="m-menu__link-icon flaticon-event-calendar-symbol"></i>
                            <span class="m-menu__link-text">Agenda</span>
                        </a>
                    </li>

                    @if($usuario->permisos([1]))

                        <li class="m-menu__item  m-menu__item--submenu m-menu__item{{ ($seccionActiva == 'Usuarios') ? "--active m-menu__item--open" : "" }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class=" m-menu__toggle">
                                <span class="m-menu__item-here"></span>
                                {{--<i class="m-menu__link-icon flaticon-user-settings"></i>--}}
                                <div class="text-center ">
                                    <img src="{{ asset('img/iconos/clientes-low.png') }}" class="m-imagen img-fluid" style="width: 50px; height: 50px;">
                                </div>
                                <span class="m-menu__link-text">Usuarios</span>
                                {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                            </a>

                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                        <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">Usuarios</span></span>
                                    </li>

                                    <li class="m-menu__item " aria-haspopup="true">
                                        <a href="{{ url('usuarios/crear') }}" class="m-menu__link ">
                                            {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                            <img src="{{ asset('img/iconos/sub-crear.png') }}" class="img-fluid">
                                            <span class="m-menu__link-text">Crear</span>
                                        </a>
                                    </li>

                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="{{ url('usuarios/editar') }}" class="m-menu__link ">
                                            {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                            <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                            <span class="m-menu__link-text">Listar</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="m-menu__item  m-menu__item--submenu m-menu__item{{ ($seccionActiva == 'Administracion') ? "--active m-menu__item--open" : "" }}" aria-haspopup="true" m-menu-submenu-toggle="hover" style="background: darkgrey !important;">
                            <a href="javascript:;" class="m-menu__toggle">
                                <span class="m-menu__item-here"></span>
                                {{--<i class="m-menu__link-icon flaticon-settings-1"></i>--}}
                                <div class="text-center ">
                                    <img src="{{ asset('img/iconos/config-low.png') }}" class="m-imagen img-fluid" style="width: 50px; height: 50px;">
                                </div>
                                <span class="m-menu__link-text">Administración</span>
                                {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                            </a>

                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                        <span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">Administración</span></span>
                                    </li>

                                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-icon flaticon-truck"></i>
                                            <span class="m-menu__link-text">Vehículos</span>
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        </a>

                                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                                            <ul class="m-menu__subnav">
                                                <li class="m-menu__item " aria-haspopup="true">
                                                    <a href="{{ url('vehiculos/crear') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-crear.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Crear</span>
                                                    </a>
                                                </li>

                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="{{ url('vehiculos/editar') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Listar</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-icon flaticon-tabs"></i>
                                            <span class="m-menu__link-text">Servicios</span>
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        </a>

                                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                                            <ul class="m-menu__subnav">
                                                <li class="m-menu__item " aria-haspopup="true">
                                                    <a href="{{ url('servicios/crear') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-crear.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Crear</span>
                                                    </a>
                                                </li>

                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="{{ url('servicios/editar') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Listar</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-icon flaticon-notes"></i>
                                            <span class="m-menu__link-text">Adicionales</span>
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        </a>

                                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                                            <ul class="m-menu__subnav">
                                                <li class="m-menu__item " aria-haspopup="true">
                                                    <a href="{{ url('adicionales/crear') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-crear.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Crear</span>
                                                    </a>
                                                </li>

                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="{{ url('adicionales/editar') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Listar</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-icon flaticon-information"></i>
                                            <span class="m-menu__link-text">Formas de contacto</span>
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        </a>

                                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                                            <ul class="m-menu__subnav">
                                                <li class="m-menu__item " aria-haspopup="true">
                                                    <a href="{{ url('formas_contacto/crear') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-crear.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Crear</span>
                                                    </a>
                                                </li>

                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="{{ url('formas_contacto/editar') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Listar</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    {{--<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">--}}
                                        {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
                                            {{--<i class="m-menu__link-icon flaticon-alert"></i>--}}
                                            {{--<span class="m-menu__link-text">Estado de presupuestos</span>--}}
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        {{--</a>--}}

                                        {{--<div class="m-menu__submenu "><span class="m-menu__arrow"></span>--}}
                                            {{--<ul class="m-menu__subnav">--}}
                                                {{--<li class="m-menu__item " aria-haspopup="true">--}}
                                                    {{--<a href="{{ url('estados_presupuesto/crear') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<img src="{{ asset('img/iconos/sub-crear.png') }}" class="img-fluid">--}}
                                                        {{--<span class="m-menu__link-text">Crear</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}

                                                {{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
                                                    {{--<a href="{{ url('estados_presupuesto/editar') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">--}}
                                                        {{--<span class="m-menu__link-text">Listar</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}

                                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-icon flaticon-security"></i>
                                            <span class="m-menu__link-text">Formas de pago</span>
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        </a>

                                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                                            <ul class="m-menu__subnav">
                                                <li class="m-menu__item " aria-haspopup="true">
                                                    <a href="{{ url('formas_pago/crear') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-crear.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Crear</span>
                                                    </a>
                                                </li>

                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="{{ url('formas_pago/editar') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Listar</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-icon flaticon-map-location"></i>
                                            <span class="m-menu__link-text">Localidades</span>
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        </a>

                                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                                            <ul class="m-menu__subnav">
                                                <li class="m-menu__item " aria-haspopup="true">
                                                    <a href="{{ url('localidades/crear') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-crear.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Crear</span>
                                                    </a>
                                                </li>

                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="{{ url('localidades/editar') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Listar</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-icon flaticon-notes"></i>
                                            <span class="m-menu__link-text">Items Adicionales</span>
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        </a>

                                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                                            <ul class="m-menu__subnav">
                                                <li class="m-menu__item " aria-haspopup="true">
                                                    <a href="{{ url('items_adicionales/crear') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-crear.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Crear</span>
                                                    </a>
                                                </li>

                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="{{ url('items_adicionales/editar') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Listar</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-icon flaticon-notes"></i>
                                            <span class="m-menu__link-text">Items Observaciones</span>
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        </a>

                                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                                            <ul class="m-menu__subnav">
                                                <li class="m-menu__item " aria-haspopup="true">
                                                    <a href="{{ url('items_observaciones/crear') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-crear.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Crear</span>
                                                    </a>
                                                </li>

                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="{{ url('items_observaciones/editar') }}" class="m-menu__link ">
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        <img src="{{ asset('img/iconos/sub-listar.png') }}" class="img-fluid">
                                                        <span class="m-menu__link-text">Listar</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>

                    @endif

                    {{--<li class="m-menu__item  m-menu__item{{ ($seccionActiva == 'Home') ? "--active" : "" }}" aria-haspopup="true">--}}
                        {{--<a href="{{ url('/') }}" class="m-menu__link ">--}}
                            {{--<span class="m-menu__item-here"></span>--}}
                            {{--<i class="m-menu__link-icon flaticon-home"></i>--}}
                            {{--<span class="m-menu__link-text">Home</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}

                    {{--<li class="m-menu__item  m-menu__item--submenu m-menu__item{{ ($seccionActiva == 'Presupuestos') ? "--active m-menu__item--open" : "" }}" aria-haspopup="true" m-menu-submenu-toggle="hover">--}}
                        {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
                            {{--<span class="m-menu__item-here"></span>--}}
                            {{--<i class="m-menu__link-icon flaticon-piggy-bank"></i>--}}
                            {{--<span class="m-menu__link-text">Presupuestos</span>--}}
                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                        {{--</a>--}}

                        {{--<div class="m-menu__submenu ">--}}
                            {{--<span class="m-menu__arrow"></span>--}}
                            {{--<ul class="m-menu__subnav">--}}
                                {{--<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">--}}
                                    {{--<span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">Presupuestos</span></span>--}}
                                {{--</li>--}}

                                {{--<li class="m-menu__item " aria-haspopup="true">--}}
                                    {{--<a href="{{ url('presupuestos/crear') }}" class="m-menu__link ">--}}
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        {{--<span class="m-menu__link-text">Crear</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}

                                {{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
                                    {{--<a href="{{ url('presupuestos/editar') }}" class="m-menu__link ">--}}
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        {{--<span class="m-menu__link-text">Listar</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</li>--}}

                    {{--<li class="m-menu__item  m-menu__item--submenu m-menu__item{{ ($seccionActiva == 'Clientes') ? "--active m-menu__item--open" : "" }}" aria-haspopup="true" m-menu-submenu-toggle="hover">--}}
                        {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
                            {{--<span class="m-menu__item-here"></span>--}}
                            {{--<i class="m-menu__link-icon flaticon-users"></i>--}}
                            {{--<span class="m-menu__link-text">Clientes</span>--}}
                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                        {{--</a>--}}

                        {{--<div class="m-menu__submenu ">--}}
                            {{--<span class="m-menu__arrow"></span>--}}
                            {{--<ul class="m-menu__subnav">--}}
                                {{--<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">--}}
                                    {{--<span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">Clientes</span></span>--}}
                                {{--</li>--}}

                                {{--<li class="m-menu__item " aria-haspopup="true">--}}
                                    {{--<a href="{{ url('clientes/crear') }}" class="m-menu__link ">--}}
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        {{--<span class="m-menu__link-text">Crear</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}

                                {{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
                                    {{--<a href="{{ url('clientes/editar') }}" class="m-menu__link ">--}}
                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                        {{--<span class="m-menu__link-text">Listar</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</li>--}}

                    {{--<li class="m-menu__item  m-menu__item{{ ($seccionActiva == 'Agenda') ? "--active" : "" }}" aria-haspopup="true">--}}
                        {{--<a href="{{ url('/agenda') }}" class="m-menu__link ">--}}
                            {{--<span class="m-menu__item-here"></span>--}}
                            {{--<i class="m-menu__link-icon flaticon-event-calendar-symbol"></i>--}}
                            {{--<span class="m-menu__link-text">Agenda</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}

                    {{--@if($usuario->permisos([1]))--}}

                        {{--<li class="m-menu__item  m-menu__item--submenu m-menu__item{{ ($seccionActiva == 'Usuarios') ? "--active m-menu__item--open" : "" }}" aria-haspopup="true" m-menu-submenu-toggle="hover">--}}
                            {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
                                {{--<span class="m-menu__item-here"></span>--}}
                                {{--<i class="m-menu__link-icon flaticon-user-settings"></i>--}}
                                {{--<span class="m-menu__link-text">Usuarios</span>--}}
                                {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                            {{--</a>--}}

                            {{--<div class="m-menu__submenu ">--}}
                                {{--<span class="m-menu__arrow"></span>--}}
                                {{--<ul class="m-menu__subnav">--}}
                                    {{--<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">--}}
                                        {{--<span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">Usuarios</span></span>--}}
                                    {{--</li>--}}

                                    {{--<li class="m-menu__item " aria-haspopup="true">--}}
                                        {{--<a href="{{ url('usuarios/crear') }}" class="m-menu__link ">--}}
                                            {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                            {{--<span class="m-menu__link-text">Crear</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}

                                    {{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
                                        {{--<a href="{{ url('usuarios/editar') }}" class="m-menu__link ">--}}
                                            {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                            {{--<span class="m-menu__link-text">Listar</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</li>--}}

                        {{--<li class="m-menu__item  m-menu__item--submenu m-menu__item{{ ($seccionActiva == 'Administracion') ? "--active m-menu__item--open" : "" }}" aria-haspopup="true" m-menu-submenu-toggle="hover">--}}
                            {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
                                {{--<span class="m-menu__item-here"></span>--}}
                                {{--<i class="m-menu__link-icon flaticon-settings-1"></i>--}}
                                {{--<span class="m-menu__link-text">Administración</span>--}}
                                {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                            {{--</a>--}}

                            {{--<div class="m-menu__submenu ">--}}
                                {{--<span class="m-menu__arrow"></span>--}}
                                {{--<ul class="m-menu__subnav">--}}
                                    {{--<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">--}}
                                        {{--<span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">Administración</span></span>--}}
                                    {{--</li>--}}

                                    {{--<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">--}}
                                        {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
                                            {{--<i class="m-menu__link-icon flaticon-truck"></i>--}}
                                            {{--<span class="m-menu__link-text">Vehículos</span>--}}
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        {{--</a>--}}

                                        {{--<div class="m-menu__submenu "><span class="m-menu__arrow"></span>--}}
                                            {{--<ul class="m-menu__subnav">--}}
                                                {{--<li class="m-menu__item " aria-haspopup="true">--}}
                                                    {{--<a href="{{ url('vehiculos/crear') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<span class="m-menu__link-text">Crear</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}

                                                {{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
                                                    {{--<a href="{{ url('vehiculos/editar') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<span class="m-menu__link-text">Listar</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}

                                    {{--<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">--}}
                                        {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
                                            {{--<i class="m-menu__link-icon flaticon-tabs"></i>--}}
                                            {{--<span class="m-menu__link-text">Servicios</span>--}}
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        {{--</a>--}}

                                        {{--<div class="m-menu__submenu "><span class="m-menu__arrow"></span>--}}
                                            {{--<ul class="m-menu__subnav">--}}
                                                {{--<li class="m-menu__item " aria-haspopup="true">--}}
                                                    {{--<a href="{{ url('servicios/crear') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<span class="m-menu__link-text">Crear</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}

                                                {{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
                                                    {{--<a href="{{ url('servicios/editar') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<span class="m-menu__link-text">Listar</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}

                                    {{--<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">--}}
                                        {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
                                            {{--<i class="m-menu__link-icon flaticon-notes"></i>--}}
                                            {{--<span class="m-menu__link-text">Adicionales</span>--}}
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        {{--</a>--}}

                                        {{--<div class="m-menu__submenu "><span class="m-menu__arrow"></span>--}}
                                            {{--<ul class="m-menu__subnav">--}}
                                                {{--<li class="m-menu__item " aria-haspopup="true">--}}
                                                    {{--<a href="{{ url('adicionales/crear') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<span class="m-menu__link-text">Crear</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}

                                                {{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
                                                    {{--<a href="{{ url('adicionales/editar') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<span class="m-menu__link-text">Listar</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}

                                    {{--<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">--}}
                                        {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
                                            {{--<i class="m-menu__link-icon flaticon-information"></i>--}}
                                            {{--<span class="m-menu__link-text">Formas de contacto</span>--}}
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        {{--</a>--}}

                                        {{--<div class="m-menu__submenu "><span class="m-menu__arrow"></span>--}}
                                            {{--<ul class="m-menu__subnav">--}}
                                                {{--<li class="m-menu__item " aria-haspopup="true">--}}
                                                    {{--<a href="{{ url('formas_contacto/crear') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<span class="m-menu__link-text">Crear</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}

                                                {{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
                                                    {{--<a href="{{ url('formas_contacto/editar') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<span class="m-menu__link-text">Listar</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}

                                    {{--<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">--}}
                                        {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
                                            {{--<i class="m-menu__link-icon flaticon-alert"></i>--}}
                                            {{--<span class="m-menu__link-text">Estado de presupuestos</span>--}}
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        {{--</a>--}}

                                        {{--<div class="m-menu__submenu "><span class="m-menu__arrow"></span>--}}
                                            {{--<ul class="m-menu__subnav">--}}
                                                {{--<li class="m-menu__item " aria-haspopup="true">--}}
                                                    {{--<a href="{{ url('estado_presupuestos/crear') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<span class="m-menu__link-text">Crear</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}

                                                {{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
                                                    {{--<a href="{{ url('estado_presupuestos/editar') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<span class="m-menu__link-text">Listar</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}

                                    {{--<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">--}}
                                        {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
                                            {{--<i class="m-menu__link-icon flaticon-security"></i>--}}
                                            {{--<span class="m-menu__link-text">Formas de pago</span>--}}
                                            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                                        {{--</a>--}}

                                        {{--<div class="m-menu__submenu "><span class="m-menu__arrow"></span>--}}
                                            {{--<ul class="m-menu__subnav">--}}
                                                {{--<li class="m-menu__item " aria-haspopup="true">--}}
                                                    {{--<a href="{{ url('formas_pago/crear') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<span class="m-menu__link-text">Crear</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}

                                                {{--<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">--}}
                                                    {{--<a href="{{ url('formas_pago/editar') }}" class="m-menu__link ">--}}
                                                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                                        {{--<span class="m-menu__link-text">Listar</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</li>--}}

                    {{--@endif--}}

                </ul>
            </div>

        </div>

        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title ">@yield('titulo')</h3>
                    </div>
                </div>
            </div>

            <div class="m-content">
                @yield('contenido')
            </div>
        </div>
    </div>

    <footer class="m-grid__item	m-footer">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								{{ date('Y') }} &copy; <a href="https://eawebagency.com" class="m-link">EA Web Agency</a>
							</span>
                </div>

            </div>
        </div>
    </footer>

</div>

<div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">
    <div class="m-quick-sidebar__content m--hide">
        <span id="m_quick_sidebar_close" class="m-quick-sidebar__close"><i class="la la-close"></i></span>
        <ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_messenger" role="tab">Messages</a>
            </li>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_settings" role="tab">Settings</a>
            </li>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_logs" role="tab">Logs</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="m_quick_sidebar_tabs_messenger" role="tabpanel">
                <div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
                    <div class="m-messenger__messages m-scrollable">
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--in">
                                <div class="m-messenger__message-pic">
                                    <img src="{{ asset('adminAssets/app/media/img/users/user3.jpg') }}" alt="" />
                                </div>
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-username">
                                            Megan wrote
                                        </div>
                                        <div class="m-messenger__message-text">
                                            Hi Bob. What time will be the meeting ?
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--out">
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-text">
                                            Hi Megan. It's at 2.30PM
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--in">
                                <div class="m-messenger__message-pic">
                                    <img src="{{ asset('adminAssets/app/media/img/users/user3.jpg') }}" alt="" />
                                </div>
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-username">
                                            Megan wrote
                                        </div>
                                        <div class="m-messenger__message-text">
                                            Will the development team be joining ?
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--out">
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-text">
                                            Yes sure. I invited them as well
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__datetime">2:30PM</div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--in">
                                <div class="m-messenger__message-pic">
                                    <img src="{{ asset('adminAssets/app/media/img/users/user3.jpg') }}" alt="" />
                                </div>
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-username">
                                            Megan wrote
                                        </div>
                                        <div class="m-messenger__message-text">
                                            Noted. For the Coca-Cola Mobile App project as well ?
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--out">
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-text">
                                            Yes, sure.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--out">
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-text">
                                            Please also prepare the quotation for the Loop CRM project as well.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__datetime">3:15PM</div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--in">
                                <div class="m-messenger__message-no-pic m--bg-fill-danger">
                                    <span>M</span>
                                </div>
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-username">
                                            Megan wrote
                                        </div>
                                        <div class="m-messenger__message-text">
                                            Noted. I will prepare it.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--out">
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-text">
                                            Thanks Megan. I will see you later.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-messenger__wrapper">
                            <div class="m-messenger__message m-messenger__message--in">
                                <div class="m-messenger__message-pic">
                                    <img src="{{ asset('adminAssets/app/media/img/users/user3.jpg') }}" alt="" />
                                </div>
                                <div class="m-messenger__message-body">
                                    <div class="m-messenger__message-arrow"></div>
                                    <div class="m-messenger__message-content">
                                        <div class="m-messenger__message-username">
                                            Megan wrote
                                        </div>
                                        <div class="m-messenger__message-text">
                                            Sure. See you in the meeting soon.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-messenger__seperator"></div>
                    <div class="m-messenger__form">
                        <div class="m-messenger__form-controls">
                            <input type="text" name="" placeholder="Type here..." class="m-messenger__form-input">
                        </div>
                        <div class="m-messenger__form-tools">
                            <a href="" class="m-messenger__form-attachment">
                                <i class="la la-paperclip"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="m_quick_sidebar_tabs_settings" role="tabpanel">
                <div class="m-list-settings m-scrollable">
                    <div class="m-list-settings__group">
                        <div class="m-list-settings__heading">General Settings</div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Email Notifications</span>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" checked="checked" name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Site Tracking</span>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">SMS Alerts</span>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Backup Storage</span>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Audit Logs</span>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" checked="checked" name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                    </div>
                    <div class="m-list-settings__group">
                        <div class="m-list-settings__heading">System Settings</div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">System Logs</span>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Error Reporting</span>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Applications Logs</span>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Backup Servers</span>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" checked="checked" name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                        <div class="m-list-settings__item">
                            <span class="m-list-settings__item-label">Audit Logs</span>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="m_quick_sidebar_tabs_logs" role="tabpanel">
                <div class="m-list-timeline m-scrollable">
                    <div class="m-list-timeline__group">
                        <div class="m-list-timeline__heading">
                            System Logs
                        </div>
                        <div class="m-list-timeline__items">
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">12 new users registered <span class="m-badge m-badge--warning m-badge--wide">important</span></a>
                                <span class="m-list-timeline__time">Just now</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">System shutdown</a>
                                <span class="m-list-timeline__time">11 mins</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                                <a href="" class="m-list-timeline__text">New invoice received</a>
                                <span class="m-list-timeline__time">20 mins</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                                <a href="" class="m-list-timeline__text">Database overloaded 89% <span class="m-badge m-badge--success m-badge--wide">resolved</span></a>
                                <span class="m-list-timeline__time">1 hr</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">System error</a>
                                <span class="m-list-timeline__time">2 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">Production server down <span class="m-badge m-badge--danger m-badge--wide">pending</span></a>
                                <span class="m-list-timeline__time">3 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">Production server up</a>
                                <span class="m-list-timeline__time">5 hrs</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-list-timeline__group">
                        <div class="m-list-timeline__heading">
                            Applications Logs
                        </div>
                        <div class="m-list-timeline__items">
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">New order received <span class="m-badge m-badge--info m-badge--wide">urgent</span></a>
                                <span class="m-list-timeline__time">7 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">12 new users registered</a>
                                <span class="m-list-timeline__time">Just now</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">System shutdown</a>
                                <span class="m-list-timeline__time">11 mins</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                                <a href="" class="m-list-timeline__text">New invoices received</a>
                                <span class="m-list-timeline__time">20 mins</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                                <a href="" class="m-list-timeline__text">Database overloaded 89%</a>
                                <span class="m-list-timeline__time">1 hr</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">System error <span class="m-badge m-badge--info m-badge--wide">pending</span></a>
                                <span class="m-list-timeline__time">2 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">Production server down</a>
                                <span class="m-list-timeline__time">3 hrs</span>
                            </div>
                        </div>
                    </div>
                    <div class="m-list-timeline__group">
                        <div class="m-list-timeline__heading">
                            Server Logs
                        </div>
                        <div class="m-list-timeline__items">
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">Production server up</a>
                                <span class="m-list-timeline__time">5 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">New order received</a>
                                <span class="m-list-timeline__time">7 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">12 new users registered</a>
                                <span class="m-list-timeline__time">Just now</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">System shutdown</a>
                                <span class="m-list-timeline__time">11 mins</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                                <a href="" class="m-list-timeline__text">New invoice received</a>
                                <span class="m-list-timeline__time">20 mins</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                                <a href="" class="m-list-timeline__text">Database overloaded 89%</a>
                                <span class="m-list-timeline__time">1 hr</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">System error</a>
                                <span class="m-list-timeline__time">2 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">Production server down</a>
                                <span class="m-list-timeline__time">3 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                <a href="" class="m-list-timeline__text">Production server up</a>
                                <span class="m-list-timeline__time">5 hrs</span>
                            </div>
                            <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                <a href="" class="m-list-timeline__text">New order received</a>
                                <span class="m-list-timeline__time">1117 hrs</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>

<script src="{{ asset('adminAssets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('adminAssets/demo/demo12/base/scripts.bundle.js') }}" type="text/javascript"></script>

<script src="{{ asset('adminAssets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/cleave/dist/cleave.min.js') }}"></script>
<script src="{{ asset('js/cleave/dist/addons/cleave-phone.ar.js') }}"></script>
<script src="{{ asset('js/mask/dist/jquery.mask.min.js') }}"></script>

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('js/awesomplete/awesomplete.min.js') }}"></script>
<script src="https://cdn.ckeditor.com/4.11.2/full/ckeditor.js"></script>
<script src="{{ asset('js/ckeditor/adapters/jquery.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>

<!-- MIS SCRIPTS -->
{{--<script src="{{ asset('js/direcciones/direcciones.js') }}"></script>--}}
<!-- END MIS SCRIPTS -->

<script>

    $(document).ready( function () {

        // DATATABLE

        $('table').DataTable({
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            bSort: false,
            ordering: false,
        });

        // CLEAVE
        function inputTelefono() {
            if($('.input-telefono').length) {

                $('.input-telefono').each(function() {
                    // var cleaveInputTelefono = new Cleave($(this), {
                    //     phone: true,
                    //     phoneRegionCode: 'AR'
                    // });

                    var masks = ['(000) 0000-00000', '(00) 0000-00000', '(0000) 0000-00000', '(0000) 0000-000000000000'];

                    var options = {
                        onKeyPress: function (cep, e, field, options) {
                            var mask = "";

                            if(cep.length <= 14) {
                                mask = masks[1];
                            } else if(cep.length == 15) {
                                mask = masks[0];
                            } else if(cep.length == 16) {
                                mask = masks[2];
                            } else if(cep.length > 16) {
                                mask = masks[3];
                            }

                            $(field).mask(mask, options);
                        },
                        placeholder: "",
                    };

                    var mask = "";

                    if($(this).val().length <= 14) {
                        mask = masks[3];
                    } else if($(this).val().length == 15) {
                        mask = masks[0];
                    } else if($(this).val().length == 16) {
                        mask = masks[2];
                    } else if($(this).val().length > 16) {
                        mask = masks[3];
                    }

                    $(this).mask(mask, options)
                });
            }
        }

        function inputHora() {
            if($('.input-hora').length) {

                $('.input-hora').each(function() {
                    var cleaveInputHora = new Cleave($(this), {
                        time: true,
                        timePattern: ['h', 'm']
                    });
                });
            }
        }

        // CKEDITOR
        $("textarea").each(function() {

            $(this).ckeditor({
                customConfig: '/js/ckeditor/config.js'
            });

            // CKEDITOR.replace($(this).attr('name'), {
            //     customConfig: '/js/ckeditor/config.js'
            // });

        });

        /** EJECUTAR FUNCIONES */

        // inputTelefono();
        inputHora();

        /** END EJECUTAR FUNCIONES */

    });

</script>

@yield('scripts')

</body>

</html>