@extends('master', ['seccionActiva' => 'Reportes'])

@section('titulo', $elemento->nombre.' '.$elemento->apellido)

@section('contenido')

    {{ Form::hidden('idVendedor', $elemento->id) }}

    <div class="row">
        <div class="col-12">
            <div class="m-portlet">

                <div class="m-portlet__body">
                    <ul class="nav nav-pills" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabPresupuestos">Presupuestos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabProspectos">Prospectos</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabPresupuestos" role="tabpanel">
                            <div class="row">
                                <div class="col-12">
                                    <div id="presupuestos" style="width: 100%; height: 500px"></div>
                                </div>

                                <div class="col-12">
                                    <div id="presupuestosLinea" style="width: 100%; height: 500px"></div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabProspectos" role="tabpanel">
                            <div class="row">
                                <div class="col-12">
                                    <div id="prospectos" style="width: 1200px; height: 500px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('js/echarts/echarts.min.js') }}"></script>

    <script>

        $(document).ready(function() {

            // PRESUPUESTOS
            var chartPresupuestos = echarts.init(document.getElementById('presupuestos'));
            var chartPresupuestosLinea = echarts.init(document.getElementById('presupuestosLinea'));

            $.get( "/ajax/reportes/vendedor/" + $('input[name="idVendedor"]').val() + "/presupuestos", function( data ) { }).done(function(data) {

                var options = {
                    title : {
                        text: 'Presupuestos',
                        subtext: 'Generados / Confirmados'
                    },
                    tooltip : {
                        trigger: 'axis'
                    },
                    legend: {
                        data:['Generados','Confirmados']
                    },
                    toolbox: {
                        show : false,
                    },
                    calculable : false,
                    xAxis : [
                        {
                            type : 'category',
                            data : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value'
                        }
                    ],
                    series : [
                        {
                            name:'Generados',
                            type:'bar',
                            data:data.generados.valores,
                        },
                        {
                            name:'Confirmados',
                            type:'bar',
                            data:data.confirmados.valores,
                        }
                    ]
                };

                chartPresupuestos.setOption(options);
                chartPresupuestos.resize();

                var optionsLinea = {
                    xAxis: {
                        type: 'category',
                        data: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [{
                        data: data.confirmados.valores,
                        type: 'line'
                    }]
                };

                chartPresupuestosLinea.setOption(optionsLinea);
                chartPresupuestosLinea.resize();
            });

            // PROSPECTOS
            var chartProspectos = echarts.init(document.getElementById('prospectos'));

            $.get( "/ajax/reportes/vendedor/" + $('input[name="idVendedor"]').val() + "/prospectos/", function( data ) { }).done(function(data) {

                var options = {
                    title : {
                        text: 'Prospectos',
                        subtext: 'Contactados / Clientes'
                    },
                    tooltip : {
                        trigger: 'axis'
                    },
                    legend: {
                        data:['Prospectos','Clientes']
                    },
                    toolbox: {
                        show : false,
                    },
                    calculable : false,
                    xAxis : [
                        {
                            type : 'category',
                            data : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value'
                        }
                    ],
                    series : [
                        {
                            name:'Prospectos',
                            type:'bar',
                            data:data.prospectos.valores,
                        },
                        {
                            name:'Clientes',
                            type:'bar',
                            data:data.clientes.valores,
                        }
                    ]
                };

                chartProspectos.setOption(options);
                chartProspectos.resize();

            });

        });

    </script>

@endsection