@extends('master', ['seccionActiva' => 'Reportes'])

@section('titulo', 'Presupuestos x Forma de Contacto')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    <div class="m-portlet m-portlet--mobile  m-portlet--unair">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Cantidad de presupuestos por forma decontacto
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-portlet__body">

            {{ Form::open(['url' => 'reportes/formas_contacto']) }}

                <div class="form-group">
                    {{ Form::label('desde', 'Desde') }}
                    {{ Form::date('desde', $desde, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('hasta', 'Hasta') }}
                    {{ Form::date('hasta', $hasta, ['class' => 'form-control']) }}
                </div>

            <div class="form-group">
                    {{ Form::label('confirmados', 'Sólo Confirmados') }}
                    {{ Form::select('confirmados', [0 => 'No', 1 => 'Sí'], $confirmados, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('BUSCAR', ['class' => 'btn btn-info']) }}
                </div>

            {{ Form::close() }}

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Forma de contacto</th>
                        <th>Cantidad</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($elementos as $elemento)
                        <tr>
                            <td>
                                {{ $elemento->nombre }}
                            </td>
                            <td>
                                {{ $elemento->cantidad }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection