@extends('master', ['seccionActiva' => 'Reportes'])

@section('titulo', 'Presupuestos')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    <div class="m-portlet m-portlet--mobile  m-portlet--unair">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Presupuestos
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-portlet__body">

            {{ Form::open(['url' => 'reportes/presupuestos_excel']) }}

            <div class="form-group">
                {{ Form::label('desde', 'Desde') }}
                {{ Form::date('desde', null, ['class' => 'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('hasta', 'Hasta') }}
                {{ Form::date('hasta', null, ['class' => 'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('estado', 'Estado') }}
                {{ Form::select('estado', $estados, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar estado']) }}
            </div>

            <div class="form-group">
                {{ Form::label('vendedor', 'Vendedor') }}
                {{ Form::select('vendedor', $vendedores, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar vendedor']) }}
            </div>

            <div class="form-group">
                {{ Form::submit('BUSCAR', ['class' => 'btn btn-info']) }}
            </div>

            <div class="form-group">
                <a href="{{ url('reportes/presupuestos_excel/'.$nombreArchivo) }}" class="btn btn-info" target="_blank">DESCARGAR EXCEL</a>
            </div>

            {{ Form::close() }}

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Nro. de presupuestos</th>
                        <th>Cliente</th>
                        <th>Forma de contacto</th>
                        <th>Vendedor</th>
                        <th>Valor final</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($elementos as $elemento)
                        <tr>
                            <td>
                                {{ \Carbon\Carbon::parse($elemento->created_at)->format('d/m/Y') }}
                            </td>
                            <td>
                                {{ $elemento->id }}
                            </td>
                            <td>
                                {{ $elemento->cliente()->nombre." ".$elemento->cliente()->apellido }}
                            </td>
                            <td>
                                {{ $elemento->cliente()->contacto()->nombre }}
                            </td>
                            <td>
                                {{ $elemento->vendedor()->nombre." ".$elemento->vendedor()->apellido }}
                            </td>
                            <td>
                                {{ $elemento->cotizacion }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection