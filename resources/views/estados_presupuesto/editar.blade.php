@extends('master', ['seccionActiva' => 'Administracion'])

@section('titulo', 'Editar Estados')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    <div class="m-portlet m-portlet--mobile  m-portlet--unair">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Estados
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Color</th>
                            {{--<th>Acciones</th>--}}
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($elementos as $elemento)
                        <tr>
                            <td>
                                {{ $elemento->nombre }}
                            </td>
                            <td>
                                <span class="m-badge" style="background: #{{ $elemento->color }}">{{ $elemento->color }}</span>
                            </td>
                            {{--<td>--}}
                                {{--<a href="{{ url('estados_presupuesto/editar/'.$elemento->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">--}}
                                    {{--<i class="la la-edit"></i>--}}
                                {{--</a>--}}
                                {{--<a href="{{ url('estados_presupuesto/borrar/'.$elemento->id) }}" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')">--}}
                                    {{--<i class="la la-trash"></i>--}}
                                {{--</a>--}}
                            {{--</td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection