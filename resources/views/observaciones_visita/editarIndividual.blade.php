@extends('master', ['seccionActiva' => 'Presupuestos'])

@section('titulo', 'Editar Observación')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    {{ Form::open(['class' => 'form', 'files' => true]) }}

    <div class="m-portlet">

        <div class="m-portlet__body">

            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('nombre', 'Nombre') }}
                        {{ Form::text('nombre', $elemento->nombre, ['class' => 'form-control awesomplete', 'required' => 'required']) }}
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('descripcion', 'Descripción') }}
                        {{ Form::textarea('descripcion', $elemento->descripcion, ['class' => 'form-control', 'required' => 'required']) }}
                    </div>
                </div>

            </div>

        </div>

        <div class="m-portlet__foot">
            <div class="m-form__actions">
                {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
                <a href="{{ url('presupuestos/editar/'.$visita->presupuesto) }}" class="btn btn-info">
                    < Volver
                </a>
            </div>
        </div>

    </div>

    {{ Form::close() }}

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            function cargarAutocompletado() {

                $.ajax({
                    method: "GET",
                    url: "/items_observaciones/ver",
                }).done(function(data) {

                    var adicionales = [];

                    data.forEach(function(valor, index) {
                        adicionales.push(valor.nombre);
                    });

                    new Awesomplete('input[name="nombre"]', {
                        list: adicionales
                    });

                    $('input[name="nombre"]').on('awesomplete-selectcomplete', function() {

                        var nombreAdicional = this.value;

                        data.forEach(function(valor, index) {

                            if(valor.nombre == nombreAdicional) {
                                CKEDITOR.instances['descripcion'].setData(valor.descripcion);
                            }
                        });
                    });

                }).fail(function() {

                    $.notify({
                        message: '<strong>¡Error!</strong> No se pudo obtener los adicionales'
                    },{
                        type: 'danger'
                    });

                });
            }

            cargarAutocompletado();
        });
    </script>

@endsection