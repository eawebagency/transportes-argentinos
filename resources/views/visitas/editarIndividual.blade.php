@extends('master', ['seccionActiva' => 'Presupuestos'])

@section('titulo', 'Editar Visita')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    {{ Form::open(['class' => 'form', 'files' => true]) }}

    <div class="row">
        <div class="col-12 col-sm-6">
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('fecha', 'Fecha') }}
                                {{ Form::date('fecha', $elemento->fecha, ['class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('visitador', 'Visitador') }}
                                {{ Form::select('visitador', $visitadores, $elemento->visitador, ['class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('hora_inicio', 'Hora de inicio') }}
                                {{ Form::text('hora_inicio', \Carbon\Carbon::parse($elemento->hora_inicio)->format('H:i'), ['class' => 'form-control input-hora', 'required' => 'required', 'placeholder' => 'HH:MM (Ejemplo: 16:15)']) }}
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('hora_fin', 'Hora de fin') }}
                                {{ Form::text('hora_fin', \Carbon\Carbon::parse($elemento->hora_fin)->format('H:i'), ['class' => 'form-control input-hora', 'required' => 'required', 'placeholder' => 'HH:MM (Ejemplo: 16:15)']) }}
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                {{ Form::label('observaciones', 'Observaciones') }}
                                {{ Form::textarea('observaciones', $elemento->observaciones, ['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('observaciones_carga', 'Observaciones de carga') }}
                                {{ Form::textarea('observaciones_carga', $elemento->observaciones_carga, ['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('observaciones_descarga', 'Observaciones de descarga') }}
                                {{ Form::textarea('observaciones_descarga', $elemento->observaciones_descarga, ['class' => 'form-control']) }}
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>

        <div class="col-12 col-sm-6">
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <div class="row">
                        <div class="col-12 col-sm-3">
                            <div class="form-group">
                                {{ Form::label('embalaje', 'Embalaje enviado?') }}
                                {{ Form::select('embalaje', ['1' => 'Sí', '0' => 'No'], $detalle->embalaje, ['class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                        <div class="col-12 col-sm-3">
                            <div class="form-group">
                                {{ Form::label('cajas', 'Cantidad de Cajas') }}
                                {{ Form::number('cajas', $detalle->cajas, ['class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                        <div class="col-12 col-sm-3">
                            <div class="form-group">
                                {{ Form::label('percheros_moviles', 'Percheros móviles') }}
                                {{ Form::number('percheros_moviles', $detalle->percheros_moviles, ['class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                        <div class="col-12 col-sm-3">
                            <div class="form-group">
                                {{ Form::label('horas', 'Horas de trabajo') }}
                                {{ Form::number('horas', $detalle->horas, ['class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('fecha_embalaje', 'Fecha') }}
                                {{ Form::date('fecha_embalaje', $detalle->fecha, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-3">
                            <div class="form-group">
                                {{ Form::label('hora_desde', 'Desde') }}
                                {{ Form::text('hora_desde', $detalle->hora_desde, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ej.: 10:30)', 'disabled' => 'disabled' ]) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-3">
                            <div class="form-group">
                                {{ Form::label('hora_hasta', 'Hasta') }}
                                {{ Form::text('hora_hasta', $detalle->hora_hasta, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ej.: 10:30)', 'disabled' => 'disabled' ]) }}
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="m-portlet__foot">
        <div class="m-form__actions">
            {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
            <a href="{{ url('presupuestos/editar/'.$elemento->presupuesto) }}" class="btn btn-info">
                < Volver
            </a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-12">
            <div class="m-portlet">

                <div class="m-portlet__body">
                    <h5>Observaciones</h5>

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($elemento->observaciones() as $observacion)

                                <tr>
                                    <td>
                                        {{ $observacion->nombre }}
                                    </td>
                                    <td>
                                        {{ $observacion->descripcion }}
                                    </td>
                                    <td>
                                        <a href="{{ url('visita/'.$elemento->id.'/observaciones/editar/'.$observacion->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">
                                            <i class="la la-edit"></i>
                                        </a>
                                        <a href="{{ url('visita/'.$elemento->id.'/observaciones/borrar/'.$observacion->id) }}" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <a href="{{ url('visita/'.$elemento->id.'/observaciones/crear/') }}"><button type="button" class="btn btn-primary">Agregar observación</button></a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    {{ Form::close() }}

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {

            function setearAtributoDisabled(estado) {
                $('input[name="fecha_embalaje"]').attr('disabled', estado);
                $('input[name="hora_desde"]').attr('disabled', estado);
                $('input[name="hora_hasta"]').attr('disabled', estado);
            }

            function valorEmbalaje() {
                var embalaje = $('select[name="embalaje"]').val();

                if(embalaje == 1) {
                    setearAtributoDisabled(true);
                } else {
                    setearAtributoDisabled(false);
                }
            }

            valorEmbalaje();

            $('select[name="embalaje"]').change(function() {
                valorEmbalaje();
            });

        });
    </script>
@endsection