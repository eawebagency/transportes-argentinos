@extends('master', ['seccionActiva' => 'Presupuestos'])

@section('titulo', 'Crear Visita')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    {{ Form::open(['class' => 'form', 'files' => true]) }}

    <div class="m-portlet">

        <div class="m-portlet__body">

            <div class="row">
                <div class="col-12 col-sm-3">
                    <div class="form-group">
                        {{ Form::label('fecha', 'Fecha') }}
                        {{ Form::date('fecha', null, ['class' => 'form-control', 'required' => 'required']) }}
                    </div>
                </div>
                <div class="col-12 col-sm-3">
                    <div class="form-group">
                        {{ Form::label('hora_inicio', 'Hora de inicio') }}
                        {{ Form::text('hora_inicio', null, ['class' => 'form-control input-hora', 'required' => 'required', 'placeholder' => 'HH:MM (Ejemplo: 16:15)']) }}
                    </div>
                </div>
                <div class="col-12 col-sm-3">
                    <div class="form-group">
                        {{ Form::label('hora_fin', 'Hora de fin') }}
                        {{ Form::text('hora_fin', null, ['class' => 'form-control input-hora', 'required' => 'required', 'placeholder' => 'HH:MM (Ejemplo: 16:15)']) }}
                    </div>
                </div>
                <div class="col-12 col-sm-3">
                    <div class="form-group">
                        {{ Form::label('visitador', 'Visitador') }}
                        {{ Form::select('visitador', $visitadores, null, ['class' => 'form-control', 'required' => 'required']) }}
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        {{ Form::label('observaciones', 'Observaciones') }}
                        {{ Form::textarea('observaciones', null, ['class' => 'form-control']) }}
                    </div>
                </div>

            </div>

        </div>

        <div class="m-portlet__foot">
            <div class="m-form__actions">
                {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
                <a href="{{ url('presupuestos/editar/'.$presupuesto) }}" class="btn btn-info">
                    < Volver
                </a>
            </div>
        </div>

    </div>

    {{ Form::close() }}

@endsection