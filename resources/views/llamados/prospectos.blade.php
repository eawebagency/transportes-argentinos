@extends('master', ['seccionActiva' => 'Llamados'])

@section('titulo', 'Prospectos')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    <div class="m-portlet m-portlet--mobile  m-portlet--unair">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Prospectos
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Fecha de carga</th>
                        <th>Apellido y Nombre</th>
                        <th>Forma de contacto</th>
                        <th>Estado</th>
                        <th>Teléfono</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($elementos as $elemento)
                        <tr>
                            <td>{{ \Carbon\Carbon::parse($elemento->created_at)->format('d/m/Y H:i') }}</td>
                            <td>
                                {{ $elemento->apellido }} {{ $elemento->nombre }}
                            </td>
                            <td>
                                {{ $elemento->contacto()->nombre }}
                            </td>
                            <td>
                                @if(!empty($elemento->estado()))
                                    <span class="m-badge m-badge--brand m-badge--wide" style="background: #{{ $elemento->estado()->color }}">
                                        {{ $elemento->estado()->nombre }}
                                    </span>
                                @else
                                    Sin estado
                                @endif
                            </td>
                            <td>
                                {{ $elemento->telefono }} @if($elemento->wpp == 1 && !is_null($elemento->telefono)) (Tiene Whatsapp) @endif
                            </td>
                            <td>
                                <a href="{{ url('clientes/editar/'.$elemento->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">
                                    <i class="la la-edit"></i>
                                </a>
                                {{--<a href="{{ url('clientes/borrar/'.$elemento->id) }}" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')">--}}
                                    {{--<i class="la la-trash"></i>--}}
                                {{--</a>--}}

                                @if($elemento->estado != 3)
                                    <a href="{{ url('presupuestos/crear?cliente='.$elemento->id) }}" class="btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Crear Presupuesto">
                                        <i class="la la-plus"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection