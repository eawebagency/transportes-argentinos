@extends('master', ['seccionActiva' => 'Llamados'])

@section('titulo', 'Mudanzas')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    <div class="m-portlet m-portlet--mobile  m-portlet--unair">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Mudanzas de mañana
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Nro. presupuesto</th>
                        <th>Cliente</th>
                        <th>Fecha de mudanza</th>
                        <th>Localidad carga</th>
                        <th>Estado</th>
                        <th>Hora</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($elementos as $elemento)

                        <tr>
                            <td>
                                {{ $elemento->id }}
                            </td>
                            <td>
                                {{ $elemento->cliente()->apellido }} {{ $elemento->cliente()->nombre }}
                            </td>
                            <td>
                                @if($elemento->fecha != '')
                                    {{ \Carbon\Carbon::parse($elemento->fecha)->format('d/m/Y') }}
                                @else
                                    A confirmar
                                @endif
                            </td>
                            <td>
                                {{ implode(', ', $elemento->cargas()->pluck('localidad')->toArray()) }}
                            </td>
                            <td>
                                <span class="m-badge m-badge--brand m-badge--wide" style="background: #{{ $elemento->estado()->color }}">
                                    {{ $elemento->estado()->nombre }}
                                </span>
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($elemento->hora)->format('H:i') }}
                            </td>
                            <td>
                                <a href="{{ url('presupuestos/editar/'.$elemento->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">
                                    <i class="la la-edit"></i>
                                </a>
                                <!-- <a href="{{ url('presupuestos/borrar/'.$elemento->id) }}" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')">
                                    <i class="la la-trash"></i>
                                </a> -->
                            </td>
                            <td>

                                @if(!$elemento->enviarMudanza)
                                    <span class="badge badge-danger" title="No enviado">
                                        	<i class="la la-close"></i>
                                    </span>
                                @else
                                    <span class="badge badge-success" title="Enviado">
                                        	<i class="la la-check"></i>
                                    </span>
                                @endif

                                {{--<a href="{{ url('pdf/ver/resumenDeServicio/'.$elemento->id) }}" class="btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Ver resumen de servicio" target="_blank">--}}
                                {{--<i class="la la-eye"></i>--}}
                                {{--</a>--}}

                                <a href="{{ url('pdf/enviar/mudanza/'.$elemento->id) }}" class="btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill">
                                    <i class="la la-send-o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection