<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>{{ config('app.name') }} - Login</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <link href="{{ asset('adminAssets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('adminAssets/demo/demo12/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="{{ asset('adminAssets/demo12/default/media/img/logo/favicon.ico') }}" />
</head>

<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style="background-image: url({{ asset('adminAssets/app/media/img/bg/bg-3.jpg') }});">
        <div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="#">
                        <img src="{{ asset('img/logos/logo.png') }}" style="max-width: 150px">
                    </a>
                </div>
                <div class="m-login__signin">
                    <div class="m-login__head">
                        <h3 class="m-login__title">Ingresar al panel</h3>
                    </div>

                    @if(isset($success))
                        <div class="alert alert-success" role="alert">
                            {{ $success }}
                        </div>
                    @else


                        {{ Form::open(['class' => 'm-login__form m-form']) }}

                        @if($errors->any())
                            <div class="alert alert-danger" role="alert">
                                <ul class="list-unstyled">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                            <div class="form-group m-form__group">
                                {{ Form::email('email', null, ['class' => 'form-control m-input', 'placeholder' => 'Email', 'autocomplete' => 'off', 'required' => 'required']) }}
                            </div>
                            <div class="form-group m-form__group">
                                {{ Form::password('password', ['class' => 'form-control m-input m-login__form-input--last', 'placeholder' => 'Contraseña', 'required' => 'required']) }}
                            </div>
                            <div class="row m-login__form-sub">
                                <div class="col m--align-left m-login__form-left">
                                    <label class="m-checkbox  m-checkbox--focus">
                                        {{ Form::checkbox('recordarme', true, null) }} Recordarme
                                        <span></span>
                                    </label>
                                </div>
                                <div class="col m--align-right m-login__form-right">
                                    <a href="javascript:;" id="m_login_forget_password" class="m-link">Olvidó la contraseña ?</a>
                                </div>
                            </div>
                            <div class="m-login__form-action">
                                {{ Form::submit('Ingresar', ['class' => 'btn btn-focus m-btn m-btn--pill m-btn--custom m-login__btn m-login__btn--primary', 'style' => 'background-color: #40c8f4; border-color: #40c8f4']) }}
                            </div>
                        {{ Form::close() }}

                    @endif
                </div>

                <div class="m-login__forget-password">
                    <div class="m-login__head">
                        <h3 class="m-login__title">Olvidó la contraseña ?</h3>
                        <div class="m-login__desc">Ingresa tu email para resetear la contraseña</div>
                    </div>
                    {{ Form::open(['url' => 'olvidar-contrasenia', 'class' => 'm-login__form m-form']) }}
                        <div class="form-group m-form__group">
                            {{ Form::email('email', null, ['class' => 'form-control m-input', 'placeholder' => 'Email', 'autocomplete' => 'off', 'required' => 'required']) }}
                        </div>
                        <div class="m-login__form-action">
                            {{ Form::submit('Cambiar contraseña', ['class' => 'btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primaryr']) }}
                            <button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom m-login__btn">Cancelar</button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('adminAssets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('adminAssets/demo/demo12/base/scripts.bundle.js') }}" type="text/javascript"></script>

<script src="{{ asset('adminAssets/snippets/custom/pages/user/login.js') }}" type="text/javascript"></script>

</body>

</html>