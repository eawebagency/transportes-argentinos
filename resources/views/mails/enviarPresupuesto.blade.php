@extends('mails.master')

@section('titulo', '')

@section('contenido')

    <tr>
        <td align="center">
            <table width="100%" style="min-width:100%">
                <tbody>
                <tr>
                    <td align="center">
                        <table width="100%" style="background-color:rgb(255,255,255);padding-left:20px;padding-right:20px;border-radius:0px;border-bottom:0px none rgb(200,200,200)">
                            <tbody>
                            <tr>
                                <td height="20" style="font-size:1px;line-height:0px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <table width="100%">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <table width="100%" align="left">
                                                    <tbody>
                                                    <tr>
                                                        <td style="font-size:14px;font-family:Arial,Helvetica,sans-serif,sans-serif;color:rgb(60,72,88);line-height:21px">
                                                            <div style="margin:0px"><span style="margin:0px">Hola {{ $presupuesto->cliente()->nombre }}, mi nombre es {{ \Illuminate\Support\Facades\Auth::user()->nombre }}, me contacto de la empresa de mudanzas TRANSPORTES ARGENTINOS.</span></div>
                                                            <div style="margin:0px"><br>
                                                                Adjunto a continuación el detalle del presupuesto conversado en el día de la fecha y un breve video para que pueda visualizar cuales son las principales actividades que se llevarán a cabo en su domicilio.</div>
                                                            <div style="margin:0px">&nbsp;</div>
                                                            <div style="margin:0px"><span style="margin:0px;font-size:11pt;font-family:Arial,Helvetica,sans-serif,serif,EmojiFont">Desde ya quedo a su disposición ante cualquier inquietud o consulta.</span><br>
                                                            </div>
                                                            <div style="margin:0px"><span style="margin:0px;font-size:11pt;font-family:Arial,Helvetica,sans-serif,serif,EmojiFont">Saludos,</span></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="1" style="font-size:14px;font-family:Arial,Helvetica,sans-serif,sans-serif;color:rgb(60,72,88);line-height:21px">
                                                            <br>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px;line-height:0px"><span style="margin:0px;font-size:11pt">&nbsp;</span></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table width="100%" style="min-width:100%">
                <tbody>
                <tr>
                    <td align="center">
                        <table width="100%" style="max-width:100%;min-width:100%;table-layout:fixed;background-color:rgb(255,255,255);border-radius:0px;padding-left:20px;padding-right:20px">
                            <tbody>
                            <tr>
                                <td height="20" style="font-size:1px;line-height:0px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <table width="100%">
                                        <tbody>
                                        <tr>
                                            <td width="265" style="padding-right:20px">
                                                <table align="left" width="265">
                                                    <tbody>
                                                    <tr>
                                                        <td width="100%" align="left">
                                                            <table width="100%">
                                                                <tbody>
                                                                <tr>
                                                                    <td width="100%" align="left">
                                                                        <table style="display:inline-block">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="margin:0px;border:0px none rgb(0,0,0);display:inline-block">
                                                                                        <div style="margin:0px"><a href="http://3khep.r.a.d.sendibm1.com/mk/cl/LsBMiSdx4mDqpzyZVeZMh3jVG5d898z2-elwIh_d-jGsXmE8zvczWYhGO02sN-X7kGmbaPIIBmxfEgT_XnA1sVikHY5myMlYbZ_ldBSD1y20AjBvUEY" rel="noopener noreferrer" style="margin:0px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://3khep.r.a.d.sendibm1.com/mk/cl/LsBMiSdx4mDqpzyZVeZMh3jVG5d898z2-elwIh_d-jGsXmE8zvczWYhGO02sN-X7kGmbaPIIBmxfEgT_XnA1sVikHY5myMlYbZ_ldBSD1y20AjBvUEY&amp;source=gmail&amp;ust=1553180748788000&amp;usg=AFQjCNEZAgS1VJOT_UkyMXI6qWo43Qufyw"><img width="265" alt="" style="margin:0px;vertical-align:top;max-width:605px;float:left" src="https://ci4.googleusercontent.com/proxy/hEAE8lztV7eVHWWtFk3JwI1-Rxp1BPNSsr5gQOrirlkDhDyX_O4UBc8BxLzBaVPy7I13tT6gJnjSykONjOUol_vyUsOVNUELeHJuzv8UQ78kug73abha59K4RZeYnUzYno6bqmY=s0-d-e1-ft#http://img.mailinblue.com/2011081/images/rnb/original/5b3be3339d579c4ddf1d929f.gif" class="CToWUd"></a></div>
                                                                                        <div style="margin:0px;clear:both"></div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="font-size:1px;line-height:0px">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size:24px;font-family:Arial,Helvetica,sans-serif;color:rgb(60,72,88)">
                                                            <strong><span style="margin:0px;font-size:18px">¿Cómo acondicionamos los muebles para el traslado?</span></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="font-size:1px;line-height:0px">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size:14px;font-family:Arial,Helvetica,sans-serif,sans-serif;color:rgb(60,72,88);line-height:21px">
                                                            Todos los elementos del hogar son protegidos con los mejores insumos del mercado. Conocé nuestro procedimiento de embalaje haciendo clic aquí.</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="font-size:1px;line-height:0px">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table align="left" style="margin:0px auto">
                                                                <tbody>
                                                                <tr>
                                                                    <td width="auto" align="left" height="32" style="font-size:14px;font-family:Arial,Helvetica,sans-serif;text-align:center;color:rgb(255,255,255);padding-left:18px;padding-right:18px;background-color:rgb(52,153,219);border-radius:4px;border-width:0px;border-style:none;border-color:rgb(0,0,0)">
                                                                        <a href="http://3khep.r.a.d.sendibm1.com/mk/cl/Y58uZ0D1b3izUtmcQl4D9RVJaLQsfhq51DimpAKpvqT9RefzjQFeSDN1j1TK9_OwUzTAdIw0mylzQD75KHS8GxcL9GGJqKFHpOkf4eEVooaBCB7Ox5s" rel="noopener noreferrer" style="margin:0px;text-decoration-line:none" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://3khep.r.a.d.sendibm1.com/mk/cl/Y58uZ0D1b3izUtmcQl4D9RVJaLQsfhq51DimpAKpvqT9RefzjQFeSDN1j1TK9_OwUzTAdIw0mylzQD75KHS8GxcL9GGJqKFHpOkf4eEVooaBCB7Ox5s&amp;source=gmail&amp;ust=1553180748788000&amp;usg=AFQjCNG75QwDwnFu1R0EfoGSLuVwyU-6kA">Mirá
                                                                            el video</a></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="265">
                                                <table align="left" width="265">
                                                    <tbody>
                                                    <tr>
                                                        <td width="100%" align="left">
                                                            <table width="100%">
                                                                <tbody>
                                                                <tr>
                                                                    <td width="100%" align="left">
                                                                        <table style="display:inline-block">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div style="margin:0px;border:0px none rgb(0,0,0);display:inline-block">
                                                                                        <div style="margin:0px"><a href="http://3khep.r.a.d.sendibm1.com/mk/cl/z6QfFC4_cm-iWgmpiPXfojLUzAHZr7bv5L5OqMBpAhMw1VcIG1eyurBlmPUQ806wPfJO6-q8cRSGCDZFqmoQMwi5KFhqReGXWQ7-EjfgZnOzZue-qsA" rel="noopener noreferrer" style="margin:0px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://3khep.r.a.d.sendibm1.com/mk/cl/z6QfFC4_cm-iWgmpiPXfojLUzAHZr7bv5L5OqMBpAhMw1VcIG1eyurBlmPUQ806wPfJO6-q8cRSGCDZFqmoQMwi5KFhqReGXWQ7-EjfgZnOzZue-qsA&amp;source=gmail&amp;ust=1553180748788000&amp;usg=AFQjCNEWcxYZ0HJkwPdhW-ooqt_fZyhA2Q"><img width="265" alt="" style="margin:0px;vertical-align:top;max-width:1027px;float:left" src="https://ci4.googleusercontent.com/proxy/Vv_b-ZvbI12lHzVmGT1-E0OWKxkU3jt-m6ibHE7KEb4NgZDvWBzh4mm6b7cyDMhKquYMQL-HRF_8iScXtJ4qWngvW1HotZkxvXTT264SfrEiOgNNO0NFnBb4ioLxIF5zM9Gsm2I=s0-d-e1-ft#http://img.mailinblue.com/2011081/images/rnb/original/5b3be3439d579c2c9a285bdd.gif" class="CToWUd"></a></div>
                                                                                        <div style="margin:0px;clear:both"></div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="font-size:1px;line-height:0px">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size:24px;font-family:Arial,Helvetica,sans-serif;color:rgb(60,72,88)">
                                                            <strong><span style="margin:0px;font-size:18px">¿Que incluye mi mudanza tradicional?</span></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="font-size:1px;line-height:0px">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size:14px;font-family:Arial,Helvetica,sans-serif,sans-serif;color:rgb(60,72,88);line-height:21px">
                                                            Nuestro servicio de mudanza tradicional incluye todos los aspectos básicos de una mudanza. Para conocer más sobre los servicios incluidos hace clic aquí.</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="font-size:1px;line-height:0px">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table align="left" style="margin:0px auto">
                                                                <tbody>
                                                                <tr>
                                                                    <td width="auto" align="left" height="32" style="font-size:14px;font-family:Arial,Helvetica,sans-serif;text-align:center;color:rgb(255,255,255);padding-left:18px;padding-right:18px;background-color:rgb(52,153,219);border-radius:4px;border-width:0px;border-style:none;border-color:rgb(0,0,0)">
                                                                        <a href="http://3khep.r.a.d.sendibm1.com/mk/cl/eGkkZtOzuFltM-ZxyIwK2JQvUsU_Lmb3yVHm5vBBPJh1me3_7ToU059dBgajF7aFq4S_NR0qB39PSKIE0ed_c7DU0ZMf-M3v7e6ZZb0y29OsNkqbDqo" rel="noopener noreferrer" style="margin:0px;text-decoration-line:none" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://3khep.r.a.d.sendibm1.com/mk/cl/eGkkZtOzuFltM-ZxyIwK2JQvUsU_Lmb3yVHm5vBBPJh1me3_7ToU059dBgajF7aFq4S_NR0qB39PSKIE0ed_c7DU0ZMf-M3v7e6ZZb0y29OsNkqbDqo&amp;source=gmail&amp;ust=1553180748788000&amp;usg=AFQjCNEaEyEuOLnDBTGK372PSq2EoUeZ0A">Mirá
                                                                            el video</a></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px;line-height:0px">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table width="100%" style="min-width:100%">
                <tbody>
                <tr>
                    <td align="center">
                        <table width="100%" style="max-width:100%;min-width:100%;table-layout:fixed;background-color:rgb(255,255,255);border-radius:0px;padding-left:20px;padding-right:20px">
                            <tbody>
                            <tr>
                                <td height="20" style="font-size:1px;line-height:0px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <table width="100%">
                                        <tbody>
                                        <tr>
                                            <td width="180" style="padding-right:20px">
                                                <table align="left" width="180">
                                                    <tbody>
                                                    <tr>
                                                        <td width="100%" align="left" style="line-height:0px">
                                                            <div style="margin:0px;border:0px none rgb(0,0,0);display:inline-block">
                                                                <div style="margin:0px"><img alt="" width="180" style="margin:0px;vertical-align:top;float:left;width:180px;max-width:512px!important" src="https://ci5.googleusercontent.com/proxy/JBPLKkW0phbfEvtQLw0HOFB7BWflBb4KzQ5Ntxl2jHTOHhopmidJEUpc2p4II9VhFH85RWMYLitlwltvR_UTwUpK1nCKlg-acJ8SNUYtEKuLqa8493YoesO8w9m8VbSBs91k-Vc=s0-d-e1-ft#http://img.mailinblue.com/2011081/images/rnb/original/5b476e9a9d579c56017c6eac.png" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01;"><div id=":2d4" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Descargar" role="button" tabindex="0" aria-label="Descargar el archivo adjunto " data-tooltip-class="a1V"><div class="aSK J-J5-Ji aYr"></div></div></div></div>
                                                                <div style="margin:0px;clear:both"></div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td>
                                                <table width="350" align="left">
                                                    <tbody>
                                                    <tr>
                                                        <td style="font-size:24px;font-family:Arial,Helvetica,sans-serif;color:rgb(60,72,88)">
                                                            <strong><span style="margin:0px;font-size:18px">Por qué es importante tener una entrevista con el cotizador?</span></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="font-size:1px;line-height:0px">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size:14px;font-family:Arial,Helvetica,sans-serif;color:rgb(60,72,88);float:right;width:350px;line-height:21px">
                                                            <div style="margin:0px">Al momento de la visita el cotizador releva todas las actividades a realizar en la mudanza, por ejemplo:</div>
                                                            <ul>
                                                                <li>Embalaje de todos los muebles.</li><li>Desinstalación e instalación de luminarias.</li><li>Amurado/desamurado de TVs, alacenas y otros artefactos.</li><li>Entrega gratuita de cajas, cintas de embalar y papel blanco (para preparar la vajilla y adorno).</li><li>Asesoramiento personalizado.</li></ul>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="font-size:1px;line-height:0px">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table align="left">
                                                                <tbody>
                                                                <tr>
                                                                    <td width="auto" align="center" height="32" style="font-size:14px;font-family:Arial,Helvetica,sans-serif;text-align:center;color:rgb(255,255,255);padding-left:14px;padding-right:14px;background-color:rgb(52,153,219);border-radius:4px;border-width:0px;border-style:none;border-color:rgb(0,0,0);border-collapse:separate">
                                                                        <a href="https://www.transportesargentinos.com.ar/nosotros/">Tenes dudas? Consultanos</a></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px;line-height:0px">
                                    <div style="visibility: hidden; color: #f9fafc; opacity: 0">
                                        <a href="https://www.youtube.com/watch?v=e3kBf1YcFDI">https://www.youtube.com/watch?v=e3kBf1YcFDI</a>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table width="100%" style="min-width:590px">
                <tbody>
                <tr>
                    <td align="center" style="min-width:590px">
                        <table width="100%" height="30">
                            <tbody>
                            <tr>
                                <td height="30"><img width="20" height="30" alt="" style="margin:0px;display:block;max-height:30px;max-width:20px" src="https://ci4.googleusercontent.com/proxy/-OuY2U8zgmzJ0_cR_GkMPZkIOKfo4MNsRDY9_ONriyCOrEDlqB5t5MuOta8zVOjOk7-vtUq6p-s4B5COQ6VHEtfLzO7i5GmpVQ=s0-d-e1-ft#http://img.mailinblue.com/new_images/rnb/rnb_space.gif" class="CToWUd"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>

@endsection