<head>
    <style>
        .gs .lista-items li {
            margin-bottom: 1rem;
        }
    </style>
</head>

<body>

<div style="margin:0px;font-size:15px;font-family:wf_segoe-ui_normal,&quot;Segoe UI&quot;,&quot;Segoe WP&quot;,Tahoma,Arial,sans-serif,serif,EmojiFont;color:rgb(33,33,33)">
    <table align="center" width="100%" style="background-color:rgb(249,250,252)">
        <tbody>
        <tr>
            <td align="center">
                <!--[if (gte mso 9)|(IE)]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                    <tr>
                        <td align="center" valign="top" width="600">
                <![endif]-->
                <table width="100%" style="width:590px;max-width:590px!important">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table width="100%" style="min-width:590px">
                                <tbody>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table width="100%" style="min-width:590px">
                                <tbody>
                                <tr>
                                    <td align="center" style="min-width:590px">
                                        <table width="100%" style="border-radius:0px;padding-left:20px;padding-right:20px">
                                            <tbody>
                                            <tr>
                                                <td height="20" style="font-size:1px;line-height:0px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <table width="100%" align="center">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center">
                                                                <table align="center">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align="center" style="line-height:0px">
                                                                            <div style="margin:0px;border:0px none rgb(0,0,0);display:inline-block"><img width="550" alt="TRANSPORTES ARGENTINOS" style="margin:0px;float:left;max-width:550px;display:block" src="https://ci5.googleusercontent.com/proxy/yvhsZi1hyoShHDz8HaLm16luY0CAzxRtppb1lYjYKWPCYS40ItQ7ePPH_TqH3bI3Oa2q9aWGbMVbzITrYrBb_VBB9e2S5J_lUzLNmL2rTr5lJq3z0WyFO603SchSJ59Q-VnZdK0=s0-d-e1-ft#http://img.mailinblue.com/2011081/images/rnb/original/5b9fc5c0e694aa37245d786f.png" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01;"><div id=":2d3" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Descargar" role="button" tabindex="0" aria-label="Descargar el archivo adjunto " data-tooltip-class="a1V"><div class="aSK J-J5-Ji aYr"></div></div></div></div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" style="font-size:1px;line-height:0px">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    @yield('contenido')

                    <tr style="background: #40c8f4;">
                        <td align="center">
                            <!--[if (gte mso 9)|(IE)]>
                            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                                <tr>
                                    <td align="center" valign="top" width="600">
                            <![endif]-->
                            <table width="100%" style="min-width:590px">
                                <tbody>
                                <tr>
                                    <td align="center" style="min-width:590px">
                                        <table width="590" align="center">
                                            <tbody>
                                            <tr>
                                                <td align="left" style="font-size:14px;font-family:Arial,Helvetica,sans-serif;color:rgb(136,136,136)">
                                                    <!--[if (gte mso 9)|(IE)]>
                                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                                                        <tr>
                                                            <td align="center" valign="top" width="600">
                                                    <![endif]-->
                                                    <table width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td style="padding-right:20px;padding-left:20px">
                                                                <h2 style="text-align: center; color: white; margin-bottom: 0">Para más información ingresá a</h2>
                                                                <a href="http://www.transportesargentinos.com.ar" style="color: white; text-decoration: none">
                                                                    <h1 style="text-align: center; margin-top: 0;">
                                                                        www.transportesargentinos.com.ar
                                                                    </h1>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: center">
                                                                <table align="left" style="width: 100%; padding: 0 5rem;">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="http://3khep.r.a.d.sendibm1.com/mk/cl/fEYElWFGm6asctho7Yo0g9u1YllQ2Fz-ec8GHeC6-f2oLvMWIeYv3BIntNv3rrXIWWfUsoBWYqWHVPMIb2jwj6qVrQSEUJqTGFkIKyCy94CqJYZkk08" rel="noopener noreferrer" style="margin:0px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://3khep.r.a.d.sendibm1.com/mk/cl/fEYElWFGm6asctho7Yo0g9u1YllQ2Fz-ec8GHeC6-f2oLvMWIeYv3BIntNv3rrXIWWfUsoBWYqWHVPMIb2jwj6qVrQSEUJqTGFkIKyCy94CqJYZkk08&amp;source=gmail&amp;ust=1553180748789000&amp;usg=AFQjCNGYpfW5b5tR51-VMk4qvyX9dWnHhA">
                                                                                <img alt="Facebook" style="margin:0px;vertical-align:top; max-width: 36px; max-height: 36px;" src="{{ asset('img/pdf/icono_facebook.png') }}" class="CToWUd">
                                                                            </a>
                                                                        </td>

                                                                        <td>
                                                                            <a href="http://3khep.r.a.d.sendibm1.com/mk/cl/dX_UY06SbL1JB_WK1G-Uu5a2FMt8NZ7JsMzSfpl5SnOfIoXq_c9v3hQbvZhkE4HCfMmokJsKF7SBP3yVZ4D0sqCfCRe8VYEHAywqCFl9r5mCOBDQuxU" rel="noopener noreferrer" style="margin:0px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://3khep.r.a.d.sendibm1.com/mk/cl/dX_UY06SbL1JB_WK1G-Uu5a2FMt8NZ7JsMzSfpl5SnOfIoXq_c9v3hQbvZhkE4HCfMmokJsKF7SBP3yVZ4D0sqCfCRe8VYEHAywqCFl9r5mCOBDQuxU&amp;source=gmail&amp;ust=1553180748789000&amp;usg=AFQjCNGzTmFJAk7uKPMlJRei7vc13gMp8w">
                                                                                <img alt="Facebook" style="margin:0px;vertical-align:top; max-width: 36px; max-height: 36px;" src="{{ asset('img/pdf/icono_twitter.png') }}" class="CToWUd">
                                                                            </a>
                                                                        </td>

                                                                        <td>
                                                                            <a href="http://3khep.r.a.d.sendibm1.com/mk/cl/sarGikunSueZmdZ2iFsBJXCDIHm4omIyC0MMOBTpw04RGp4Tcl4vNyIIHIzfAigq08V8MTMrfVOTa_38Cg6nEgzR19jYg2Itiuoa1xtCaSfXp9Gzy38" rel="noopener noreferrer" style="margin:0px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://3khep.r.a.d.sendibm1.com/mk/cl/sarGikunSueZmdZ2iFsBJXCDIHm4omIyC0MMOBTpw04RGp4Tcl4vNyIIHIzfAigq08V8MTMrfVOTa_38Cg6nEgzR19jYg2Itiuoa1xtCaSfXp9Gzy38&amp;source=gmail&amp;ust=1553180748789000&amp;usg=AFQjCNEBur4tDG6HwOYONUuleE9D0GiopA">
                                                                                <img alt="Facebook" style="margin:0px;vertical-align:top; max-width: 36px; max-height: 36px;" src="{{ asset('img/pdf/icono_linkedin.png') }}" class="CToWUd">
                                                                            </a>
                                                                        </td>

                                                                        <td>
                                                                            <a href="#">
                                                                                <img alt="Facebook" style="margin:0px;vertical-align:top; max-width: 36px; max-height: 36px;" src="{{ asset('img/pdf/icono_instagram.png') }}" class="CToWUd">
                                                                            </a>
                                                                        </td>

                                                                        <td>
                                                                            <a href="http://3khep.r.a.d.sendibm1.com/mk/cl/VKJznNbVmxsv01-Mmps1JyopJSjDdqjfXiMhtZ5llzugve9o3v37G2UB0pHfXoGZX6GGAz7lZe_NQCU7EA0GUGtxZtVbw1PXhUDpdbBzWKqi7wVpss0" rel="noopener noreferrer" style="margin:0px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://3khep.r.a.d.sendibm1.com/mk/cl/VKJznNbVmxsv01-Mmps1JyopJSjDdqjfXiMhtZ5llzugve9o3v37G2UB0pHfXoGZX6GGAz7lZe_NQCU7EA0GUGtxZtVbw1PXhUDpdbBzWKqi7wVpss0&amp;source=gmail&amp;ust=1553180748789000&amp;usg=AFQjCNHOwWIMYJ0zMNKbfBl30cgE_OBnLw">
                                                                                <img alt="Facebook" style="margin:0px;vertical-align:top; max-width: 36px; max-height: 36px;" src="{{ asset('img/pdf/icono_youtube.png') }}" class="CToWUd">
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="text-align: center;">
                                                            <td style="padding-top: 1.5rem;">
                                                                <a href="tel: 01139746822" style="color: white; text-decoration: none">011-3974-6822</a> / <a href="https://wa.me/549114179342" style="color: white; text-decoration: none">15-4179-342</a>
                                                            </td>
                                                        </tr>
                                                        <tr style="text-align: center; color: white">
                                                            <td>
                                                                <a href="mailto:info@transportesargentinos.com.ar" style="color: white; text-decoration: none">info@transportesargentinos.com.ar</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <hr style="border-color: white; width: 75%">
                                                            </td>
                                                        </tr>
                                                        <tr style="text-align: center; color: white">
                                                            <td>
                                                                © {{ date('Y') }} TRANSPORTES ARGENTINOS
                                                            </td>
                                                        </tr>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--[if (gte mso 9)|(IE)]>
                                                    </td>
                                                    </tr>
                                                    </table>
                                                    <![endif]-->
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" style="font-size:1px;line-height:0px">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!--[if (gte mso 9)|(IE)]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--[if (gte mso 9)|(IE)]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
</div>

</body>