@extends('mails.master')

@section('titulo', 'Resumen de servicio #'.$presupuesto->id)

@section('contenido')

    <p style="text-align: center !important;margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #ffffff;font-family: Helvetica;font-size: 16px;line-height: 150%;">
        A continuación los detalles del resumen de servicio #{{ $presupuesto->id }}
    </p>

@endsection