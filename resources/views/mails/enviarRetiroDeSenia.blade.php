@extends('mails.master')

@section('titulo', '')

@section('contenido')

    <style>
        .gs .lista-items li {
            margin-bottom: 1rem;
        }
    </style>

    <tr style="border-collapse:collapse">
        <td align="center" style="border-collapse:collapse">
            <div style="margin:0px">
                <table width="100%" style="border-collapse:collapse;min-width:0px">
                    <tbody>
                    <tr style="border-collapse:collapse">
                        <td align="center" style="border-collapse:collapse;min-width:0px!important">
                            <table width="100%" style="border-collapse:separate;background-color:rgb(255,255,255);padding-left:20px;padding-right:20px;border-radius:0px;border-bottom:0px none rgb(200,200,200)">
                                <tbody>
                                <tr style="border-collapse:collapse">
                                    <td height="20" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                        &nbsp;</td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td align="left" style="border-collapse:collapse">
                                        <table width="100%" style="border-collapse:collapse">
                                            <tbody>
                                            <tr style="border-collapse:collapse">
                                                <td style="border-collapse:collapse">
                                                    <table width="100%" align="left" style="border-collapse:collapse">
                                                        <tbody>
                                                        <tr style="border-collapse:collapse">
                                                            <td style="border-collapse:collapse;font-size:14px;font-family:Arial,Helvetica,sans-serif,sans-serif;color:rgb(60,72,88);line-height:21px">
                                                                <div style="margin:0px">
                                                                    <div style="margin:0px">
                                                                        <div style="margin:0px;text-align:justify">

                                                                            <div style="margin:0px; text-align: center">
                                                                                <span style="font-weight: 600">
                                                                                    <span>Hola {{ $presupuesto->cliente()->nombre }} le informamos que:</span>

                                                                                    <br>

                                                                                    <ul style="list-style-type: none; text-align: left; margin-left: 50px;">
                                                                                        <li style="margin-bottom: 0.5rem">
                                                                                            <img src="{{ asset('img/pdf/icono_tilde_redondeado.png') }}" style="max-height: 20px; max-width: 20px; vertical-align: middle">
                                                                                            <span style="vertical-align: middle">Hemos retirado exitosamente la seña por el servicio solicitado.</span>
                                                                                        </li>
                                                                                    </ul>

                                                                                    <br>

                                                                                    Esperamos que disfrutes esta experiencia y desde ya nos ponemos a su disposición
                                                                                    en caso de dudas o inquietudes acerca del servicio.

                                                                                </span>

                                                                            </div>
                                                                        </div>
                                                                        <div style="margin:0px">&nbsp;<span style="margin:0px">&nbsp;</span></div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td height="20" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                        &nbsp;</td>
                                </tr>
                                </tbody>
                            </table>

                            <table width="100%" style="border-collapse:separate;background-color:#40c8f4;padding-left:20px;padding-right:20px;border-radius:0px;border-bottom:0px none rgb(200,200,200)">
                                <tbody>
                                <tr style="border-collapse:collapse">
                                    <td height="20" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                        &nbsp;</td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td align="left" style="border-collapse:collapse">
                                        <table width="100%" style="border-collapse:collapse">
                                            <tbody>
                                            <tr style="border-collapse:collapse">
                                                <td style="border-collapse:collapse">
                                                    <table width="100%" align="left" style="border-collapse:collapse">
                                                        <tbody>
                                                        <tr style="border-collapse:collapse">
                                                            <td style="border-collapse:collapse;font-size:14px;font-family:Arial,Helvetica,sans-serif,sans-serif;color:rgb(60,72,88);line-height:21px">
                                                                <div style="margin:0px">
                                                                    <div style="margin:0px">
                                                                        <div style="margin:0px;text-align:justify">

                                                                            <div style="margin:0px">

                                                                                <h2 style="text-align: center; color: white; font-weight: 600"><strong>¿PREPARÁNDOTE PARA LA MUDANZA?</strong></h2>

                                                                                <ul style="list-style-type: none; text-align: center; color: white; font-weight: 600; padding-left: 0;">
                                                                                    <li style="margin-left: 0 !important">Podes ver nuestras RECOMENDACIONES Y PREGUNTAS
                                                                                        FRECUENTES, <a href="https://www.transportesargentinos.com.ar/mudate-facil/" style="">AQUÍ</a>.</li>
                                                                                    <li style="margin-left: 0 !important">IMPRIMI TÚ LISTA DE CONTROL MOVIL <a href="https://transportesargentinos.com.ar/wp-content/uploads/2019/03/Lista-de-
Chequeo.pdf">AQUÍ</a>.</li>
                                                                                </ul>

                                                                            </div>
                                                                        </div>
                                                                        <div style="margin:0px">&nbsp;<span style="margin:0px">&nbsp;</span></div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td height="20" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                        &nbsp;</td>
                                </tr>
                                </tbody>
                            </table>

                            <table width="100%" style="border-collapse:separate;background-color:rgb(255,255,255);padding-left:20px;padding-right:20px;border-radius:0px;border-bottom:0px none rgb(200,200,200)">
                                <tbody>
                                <tr style="border-collapse:collapse">
                                    <td height="20" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                        &nbsp;</td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td align="left" style="border-collapse:collapse">
                                        <table width="100%" style="border-collapse:collapse">
                                            <tbody>
                                            <tr style="border-collapse:collapse">
                                                <td style="border-collapse:collapse">
                                                    <table width="100%" align="left" style="border-collapse:collapse">
                                                        <tbody>
                                                        <tr style="border-collapse:collapse">
                                                            <td style="border-collapse:collapse;font-size:14px;font-family:Arial,Helvetica,sans-serif,sans-serif;color:rgb(60,72,88);line-height:21px">
                                                                <div style="margin:0px">
                                                                    <div style="margin:0px">
                                                                        <div style="margin:0px;text-align:justify">

                                                                            <div style="margin:0px">

                                                                                <h2 style="color: #40c8f4; font-weight: 600; padding-left: 40px;"><strong>RECORDÁ</strong></h2>

                                                                                <ul class="lista-items" style="list-style-type: none; font-weight: 400; padding-right: 40px;">
                                                                                    <li style="margin-left: 0; !important; margin: 0 0 3px 0;">
                                                                                        <img src="{{ asset('img/pdf/icono_tilde.png') }}" style="max-height: 20px; max-width: 20px">
                                                                                        <span>
                                                                                            Si optaste por realizar vos mismo el embalaje de cajas, recordá guardar todos los objetos pequeños: vajilla, libros, ropa, adornos, juguetes, en las cajas que proveemos. <strong>EL DIA DE MUDANZA NO PREPARAMOS O ARMAMOS CAJAS</strong>.
                                                                                        </span>
                                                                                        <br><br>
                                                                                    </li>
                                                                                    <li style="margin-left: 0; !important; margin: 0 0 3px 0;">
                                                                                        <img src="{{ asset('img/pdf/icono_tilde.png') }}" style="max-height: 20px; max-width: 20px">
                                                                                        <span>
                                                                                        Reservar el lugar para el camión tanto en la carga como en descarga. En caso de que no tengamos lugar para estacionar o tengamos un recorrido superior al relevado en la visita, debemos contemplar un costo adicional a la mudanza.
                                                                                    </span>
                                                                                        <br><br>
                                                                                    </li>
                                                                                    <li style="margin-left: 0; !important; mmargin: 0 0 3px 0;">
                                                                                        <img src="{{ asset('img/pdf/icono_tilde.png') }}" style="max-height: 20px; max-width: 20px">
                                                                                        <span>
                                                                                        Es muy importante que puedas dar aviso a la administración del edificio o condominio sobre fecha y hora de tu mudanza. De esta forma evitaras costos adicionales o multas. Generalmente la administración reserva un ascensor para que podamos realizar la carga o la descarga con acceso fluido, evitando demoras.
                                                                                    </span>
                                                                                        <br><br>
                                                                                    </li>
                                                                                    <li style="margin-left: 0; !important; margin: 0 0 3px 0;">
                                                                                        <img src="{{ asset('img/pdf/icono_tilde.png') }}" style="max-height: 20px; max-width: 20px">
                                                                                        <span>
                                                                                        Para proteger la integridad de los muebles los mismos se deben encontrar vacíos al momento del traslado ya que de esta forma los podremos embalar y manipular de forma apropiada.
                                                                                    </span>
                                                                                        <br><br>
                                                                                    </li>
                                                                                    <li style="margin-left: 0; !important; margin: 0 0 3px 0;">
                                                                                        <img src="{{ asset('img/pdf/icono_tilde.png') }}" style="max-height: 20px; max-width: 20px">
                                                                                        <span>
                                                                                        Es importante que la heladera se encuentre vacía y descongelada de no ser así corremos riesgos de que se derramen líquidos poniendo en peligro el resto de la carga. También tener en cuenta que la heladera debe ser enchufada al menos 6 horas posteriores al traslado.
                                                                                    </span>
                                                                                        <br><br>
                                                                                    </li>
                                                                                    <li style="margin-left: 0; !important; margin: 0 0 3px 0;">
                                                                                        <img src="{{ asset('img/pdf/icono_tilde.png') }}" style="max-height: 20px; max-width: 20px">
                                                                                        <span>
                                                                                        Planifica la ubicación de los elementos en el destino. El equipo de trabajo colocara los muebles donde se les indique haciendo el movimiento solo una vez.
                                                                                    </span>
                                                                                        <br><br>
                                                                                    </li>
                                                                                    <li style="margin-left: 0; !important; margin: 0 0 3px 0;">
                                                                                        <img src="{{ asset('img/pdf/icono_tilde.png') }}" style="max-height: 20px; max-width: 20px">
                                                                                        <span>
                                                                                        Toma el recaudo de desenchufar y enrollar los cables de equipos de audio, computadoras, televisores, equipo de video, etc. Recomendamos tomar fotos de las conexiones para su posterior instalación.
                                                                                    </span>
                                                                                        <br><br>
                                                                                    </li>
                                                                                    <li style="margin-left: 0; !important; margin: 0 0 3px 0;">
                                                                                        <img src="{{ asset('img/pdf/icono_tilde.png') }}" style="max-height: 20px; max-width: 20px">
                                                                                        <span>
                                                                                        Los días de lluvia es muy probable que los barrios y condominios nos restrinjan el traslado con vehículos de gran porte. Por lo que para este traslado realizaremos los movimientos en vehículos pequeños, este servicio tendrá un costo adicional del 20%.
                                                                                    </span>
                                                                                        <br><br>
                                                                                    </li>
                                                                                    <li style="margin-left: 0; !important; margin: 0 0 3px 0;">
                                                                                        <img src="{{ asset('img/pdf/icono_tilde.png') }}" style="max-height: 20px; max-width: 20px">
                                                                                        <span>
                                                                                        Los roperos móviles para la ropa en perchas serán entregados y retirados vacíos el mismo día del traslado.
                                                                                    </span>
                                                                                        <br><br>
                                                                                    </li>
                                                                                    <li style="margin-left: 0; !important; margin-bottom: 1rem">
                                                                                        <img src="{{ asset('img/pdf/icono_tilde.png') }}" style="max-height: 20px; max-width: 20px">
                                                                                        <span>
                                                                                        Confirmada la fecha de  traslado podrá ser cambiada por el cliente hasta 7 días previos al servicio sin costo adicional. Posterior a este periodo deberá  abonar un 20% adicional.
                                                                                    </span>
                                                                                        <br><br>
                                                                                    </li>
                                                                                </ul>

                                                                                <br><br>

                                                                            </div>
                                                                        </div>
                                                                        <div style="margin:0px">&nbsp;<span style="margin:0px">&nbsp;</span></div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td height="20" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                        <div style="visibility: hidden; color: #f9fafc; opacity: 0">
                                            <a href="https://www.youtube.com/watch?v=e3kBf1YcFDI">https://www.youtube.com/watch?v=e3kBf1YcFDI</a>
                                        </div>
                                        &nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </td>
    </tr>

@endsection