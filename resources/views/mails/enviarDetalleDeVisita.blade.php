@extends('mails.master')

@section('titulo', '')

@section('contenido')

    <tr style="border-collapse:collapse">
        <td align="center" style="border-collapse:collapse">
            <div style="margin:0px">
                <table width="100%" style="border-collapse:collapse;min-width:0px">
                    <tbody>
                    <tr style="border-collapse:collapse">
                        <td align="center" style="border-collapse:collapse;min-width:0px!important">
                            <table width="100%" style="border-collapse:separate;background-color:rgb(255,255,255);padding-left:20px;padding-right:20px;border-radius:0px;border-bottom:0px none rgb(200,200,200)">
                                <tbody>
                                <tr style="border-collapse:collapse">
                                    <td height="20" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                        &nbsp;</td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td align="left" style="border-collapse:collapse">
                                        <table width="100%" style="border-collapse:collapse">
                                            <tbody>
                                            <tr style="border-collapse:collapse">
                                                <td style="border-collapse:collapse">
                                                    <table width="100%" align="left" style="border-collapse:collapse">
                                                        <tbody>
                                                        <tr style="border-collapse:collapse">
                                                            <td style="border-collapse:collapse;font-size:14px;font-family:Arial,Helvetica,sans-serif,sans-serif;color:rgb(60,72,88);line-height:21px">
                                                                <div style="margin:0px">
                                                                    <div style="margin:0px">
                                                                        <div style="margin:0px;text-align:justify">

                                                                            <div style="margin:0px">Hola {{ $presupuesto->cliente()->nombre }}, mi nombre es {{ \Illuminate\Support\Facades\Auth::user()->nombre }}, me contacto de la empresa de mudanzas Transportes Argentinos para brindarle el detalle de la visita realizada el día de hoy {{ (!empty($presupuesto->visita()->visitador())) ? "por el cotizador ".$presupuesto->visita()->visitador()->nombre : "" }}.<br>
                                                                                <br>


                                                                            <div style="margin:0px">&nbsp;</div>
                                                                            <div style="margin:0px">Adjunto presupuesto y detalle de la visita. Las actividades que se encuentran en el presente documento fueron señaladas como prioritarias al momento del servicio&nbsp;&nbsp;</div>
                                                                            <div style="margin:0px">&nbsp;</div>
                                                                            <div style="margin:0px">Quedo atento a sus comentarios,</div>
                                                                            <div style="margin:0px"><br>
                                                                                <br>
                                                                                Saludos.</div>
                                                                        </div>
                                                                        <div style="margin:0px">&nbsp;<span style="margin:0px">&nbsp;</span></div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td height="20" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                        &nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </td>
    </tr>

    <tr style="border-collapse:collapse">
        <td align="center" style="border-collapse:collapse">
            <div style="margin:0px">
                <table width="100%" style="border-collapse:collapse;min-width:0px">
                    <tbody>
                    <tr style="border-collapse:collapse">
                        <td align="center" style="border-collapse:collapse;min-width:0px!important">
                            <table width="100%" style="border-collapse:separate;max-width:100%;min-width:100%;table-layout:fixed;background-color:rgb(255,255,255);border-radius:0px;padding-left:20px;padding-right:20px">
                                <tbody>
                                <tr style="border-collapse:collapse">
                                    <td height="20" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                        &nbsp;</td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td align="left" style="border-collapse:collapse">
                                        <table width="100%" style="border-collapse:collapse">
                                            <tbody>
                                            <tr style="border-collapse:collapse">
                                                <td width="265" style="border-collapse:collapse;padding-right:20px; padding-top: 20px;">
                                                    <table align="left" width="265" style="border-collapse:collapse">
                                                        <tbody>
                                                        <tr style="border-collapse:collapse">
                                                            <td width="100%" align="left" style="border-collapse:collapse">
                                                                <table width="100%" style="border-collapse:collapse">
                                                                    <tbody>
                                                                    <tr style="border-collapse:collapse">
                                                                        <td width="100%" align="left" style="border-collapse:collapse">
                                                                            <table style="border-collapse:collapse;display:inline-block">
                                                                                <tbody>
                                                                                <tr style="border-collapse:collapse">
                                                                                    <td style="border-collapse:collapse">
                                                                                        <div style="margin:0px;border:0px none rgb(0,0,0);display:inline-block">
                                                                                            <div style="margin:0px"><a href="http://3khep.r.a.d.sendibm1.com/mk/cl/f/iY0KYxK7xftNRNOV6l9JakhevFRxbP0OpPZPk8PQXx_JPWC_wdNB1C6O7tG81V3WyovBAcm-xLALUcXMobu5IyRNU0u34d0ICAbCGgPduxjxkoAu0zO_fYA1RXNkod8sWvV7VEflrz9IH-q4zVIQV3_xWYyH7NyMwE5KoTOrpbqnHtzA-mJj" rel="noopener noreferrer" style="margin:0px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://3khep.r.a.d.sendibm1.com/mk/cl/f/iY0KYxK7xftNRNOV6l9JakhevFRxbP0OpPZPk8PQXx_JPWC_wdNB1C6O7tG81V3WyovBAcm-xLALUcXMobu5IyRNU0u34d0ICAbCGgPduxjxkoAu0zO_fYA1RXNkod8sWvV7VEflrz9IH-q4zVIQV3_xWYyH7NyMwE5KoTOrpbqnHtzA-mJj&amp;source=gmail&amp;ust=1553260084033000&amp;usg=AFQjCNFtmwxZXYHbV4KjnKREk3kacJVakw"><img width="264" alt="" style="margin:0px;vertical-align:top;max-width:264px;float:left" src="https://ci3.googleusercontent.com/proxy/wnjlu8Nrpvvko1ZSNRHXDJ4ULWEh7LoKeRGNZifZJMk35-P5xzXjSfipgVoArIY4fhJA3ioCjwCtaK4c5Xd5Vwd3MyKd=s0-d-e1-ft#http://3khep.img.a.d.sendibm1.com/2my8iublxmve.png" class="CToWUd"></a></div>
                                                                                            <div style="margin:0px;clear:both"></div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td height="10" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td style="border-collapse:collapse;font-size:24px;font-family:Arial,Helvetica,sans-serif;color:rgb(60,72,88);text-align:left">
                                                                <span style="margin:0px"><strong><span style="margin:0px;font-size:18px">¿Qué incluye mi servicio de mudanza premium?</span></strong></span></td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td height="10" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td style="border-collapse:collapse;font-size:14px;font-family:Arial,Helvetica,sans-serif,sans-serif;color:rgb(60,72,88);line-height:21px">
                                                                <div style="margin:0px">
                                                                    <div style="margin:0px;text-align:justify">Trabajamos para que tu día de mudanza sea mas fácil y libre de estrés...&nbsp;</div>
                                                                    <div style="margin:0px;text-align:justify"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ¡<span style="margin:0px">NOS OCUPAMOS DE TODO!</span></strong></div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td height="10" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td style="border-collapse:collapse">
                                                                <table align="left" style="border-collapse:separate;margin:0px auto">
                                                                    <tbody>
                                                                    <tr style="border-collapse:collapse">
                                                                        <td width="auto" align="left" height="32" style="border-collapse:collapse;font-size:14px;font-family:Arial,Helvetica,sans-serif;text-align:center;color:rgb(255,255,255);font-weight:normal;padding-left:18px;padding-right:18px;background-color:rgb(52,153,219);border-radius:4px;border-width:0px;border-style:none;border-color:rgb(0,0,0)">
<span style="margin:0px"><a href="https://vimeo.com/275431775/97f4efd89b" style="color: white">Mirá el video</a></span></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="265" style="border-collapse:collapse">
                                                    <table align="left" width="265" style="border-collapse:collapse">
                                                        <tbody>
                                                        <tr style="border-collapse:collapse">
                                                            <td width="100%" align="left" style="border-collapse:collapse">
                                                                <table width="100%" style="border-collapse:collapse">
                                                                    <tbody>
                                                                    <tr style="border-collapse:collapse">
                                                                        <td width="100%" align="left" style="border-collapse:collapse">
                                                                            <table style="border-collapse:collapse;display:inline-block">
                                                                                <tbody>
                                                                                <tr style="border-collapse:collapse">
                                                                                    <td style="border-collapse:collapse">
                                                                                        <div style="margin:0px;border:0px none rgb(0,0,0);display:inline-block">
                                                                                            <div style="margin:0px"><a href="http://3khep.r.a.d.sendibm1.com/mk/cl/f/tq0MDqomOyxaSuVHwnFB0hRMb-7w6FyF5EBv_--b2G-5pnJj6IDMr48sL_ahjqrxHb7DY-TwwTw9Cs9xveEfMZY322R5AilsauD2HelIWQyv7DIhHrTTTkMNHiMr69LPhyhHtrNld_HHj-2vMM8VMn7uvphstAY1m59gQQiv7jJ9BCi0QZ3c" rel="noopener noreferrer" style="margin:0px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://3khep.r.a.d.sendibm1.com/mk/cl/f/tq0MDqomOyxaSuVHwnFB0hRMb-7w6FyF5EBv_--b2G-5pnJj6IDMr48sL_ahjqrxHb7DY-TwwTw9Cs9xveEfMZY322R5AilsauD2HelIWQyv7DIhHrTTTkMNHiMr69LPhyhHtrNld_HHj-2vMM8VMn7uvphstAY1m59gQQiv7jJ9BCi0QZ3c&amp;source=gmail&amp;ust=1553260084033000&amp;usg=AFQjCNGvR6bKcGVuIX47fYWFWQNpkhBimA"><img width="264" alt="" style="margin:0px;vertical-align:top;max-width:264px;float:left" src="https://ci6.googleusercontent.com/proxy/3PtE-NUGuUv8B496l0CIkQtYU1WRaQ6jFzXOGRsuoqiJiYS0_-g-T7dGCloEDqcMKZ7dblCOUEIcgCSvH2lId_6f3R2u=s0-d-e1-ft#http://3khep.img.a.d.sendibm1.com/2my8jmrlxmve.png" class="CToWUd"></a></div>
                                                                                            <div style="margin:0px;clear:both"></div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td height="10" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td style="border-collapse:collapse;font-size:24px;font-family:Arial,Helvetica,sans-serif;color:rgb(60,72,88);text-align:left">
                                                                <span style="margin:0px"><strong><span style="margin:0px;font-size:18px">¿Qué incluye mi mudanza de tradicional?</span></strong></span></td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td height="10" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td style="border-collapse:collapse;font-size:14px;font-family:Arial,Helvetica,sans-serif,sans-serif;color:rgb(60,72,88);line-height:21px">
                                                                <div style="margin:0px">
                                                                    <div style="margin:0px;text-align:justify">Nuestro servicio de mudanza incluye todos los puntos fundamentales del traslado.&nbsp;&nbsp;</div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td height="10" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr style="border-collapse:collapse">
                                                            <td style="border-collapse:collapse">
                                                                <table align="left" style="border-collapse:separate;margin:0px auto">
                                                                    <tbody>
                                                                    <tr style="border-collapse:collapse">
                                                                        <td width="auto" align="left" height="32" style="border-collapse:collapse;font-size:14px;font-family:Arial,Helvetica,sans-serif;text-align:center;color:rgb(255,255,255);font-weight:normal;padding-left:18px;padding-right:18px;background-color:rgb(52,153,219);border-radius:4px;border-width:0px;border-style:none;border-color:rgb(0,0,0)">
<span style="margin:0px"><a href="http://3khep.r.a.d.sendibm1.com/mk/cl/f/b0f3jGkUExO80ALvGHOMHdz7MBBzv5QaGCnk8s1dmYURcoTB5mwh3OZ4Uh9-cLOacLT3EG8-eIUNdUIOCsx9jeaUQvXQK_EoMDG142izf9bfFw241WKvVw93Oc9hhzoyFYnO6eiPBn7IHduktcLqy7MJDeoMTyXx9r08Tm3onXw78ME96uAI" rel="noopener noreferrer" style="margin:0px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://3khep.r.a.d.sendibm1.com/mk/cl/f/b0f3jGkUExO80ALvGHOMHdz7MBBzv5QaGCnk8s1dmYURcoTB5mwh3OZ4Uh9-cLOacLT3EG8-eIUNdUIOCsx9jeaUQvXQK_EoMDG142izf9bfFw241WKvVw93Oc9hhzoyFYnO6eiPBn7IHduktcLqy7MJDeoMTyXx9r08Tm3onXw78ME96uAI&amp;source=gmail&amp;ust=1553260084033000&amp;usg=AFQjCNEqNcKfhhPWT2E9IAfaoJc8u_X1Bw" style="color: white">Conocé
 mas mirando este video!</a></span></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                    <td height="20" style="border-collapse:collapse;font-size:1px;line-height:0px">
                                        <div style="visibility: hidden; color: #f9fafc; opacity: 0">
                                            <a href="https://www.youtube.com/watch?v=e3kBf1YcFDI">https://www.youtube.com/watch?v=e3kBf1YcFDI</a>
                                        </div>
                                        &nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </td>
    </tr>

@endsection