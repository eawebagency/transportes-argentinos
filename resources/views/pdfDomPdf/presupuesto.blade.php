<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Presupuesto</title>
    <link href="https://fonts.googleapis.com/css?family=Gothic+A1:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/pdfs/presupuesto.css') }}" />

</head>

<body>

<table class="header" width="100%">
    <tbody>
    <tr>
        <td class="logo" width="44%" valign="top">
            <table>
                <tbody>
                <tr>
                    <td class="imagen">
                        <img src="{{ asset('img/pdf/logotransportes.png') }}">
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul>
                            <li>CUIT: 30-71628105-8</li>
                            <li>TAMOVES S.A.S</li>
                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td class="separador" width="10%" valign="top">
            <table>
                <tbody>
                <tr>
                    <td>
                        <div class="x">
                            X
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" height="65">
                        <div class="linea"></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td class="datos" width="40%" valign="top">
            <ul>
                <li>FECHA DE EMISIÓN <span>{{ \Carbon\Carbon::parse($presupuesto->created_at)->format('d/m/Y') }}</span></li>
                <li>PRESUPUESTO <span>{{ $presupuesto->id }}</span></li>
                <li>(011) 15-4179-3432 (011) 3974-6822</li>
                <li>info@transportesargentinos.com.ar</li>
                <li>www.transportesargentinos.com.ar</li>
            </ul>
        </td>
    </tr>
    </tbody>
</table>

<table class="informacion" width="100%">
    <tbody>
    <tr>
        <td class="columna1" width="50%" valign="middle">
            <ul>
                <li><strong>Sr./Sra:</strong> <span>{{ $presupuesto->cliente()->nombre }} {{ $presupuesto->cliente()->apellido }}</span></li>
                <li><strong>Teléfono:</strong> <span>{{ $presupuesto->cliente()->telefono }}</span></li>
                <li><strong>Teléfono alternativo:</strong> <span>{{ $presupuesto->cliente()->telefono_alternativo }}</span></li>
                <li><strong>Fecha de Servicio:</strong> <span>
                        @if($presupuesto->fecha != '')
                            {{ \Carbon\Carbon::parse($presupuesto->fecha)->format('d/m/Y') }}
                        @else
                            A confirmar
                        @endif
                    </span></li>
                <li><strong>Hora de Servicio:</strong> <span>
                        @if($presupuesto->hora != '')
                            {{ \Carbon\Carbon::parse($presupuesto->hora)->format('H:i') }}
                        @else
                            A confirmar
                        @endif
                    </span></li>
            </ul>
        </td>
        <td class="columna2" width="50%" valign="middle">
            <ul>
                <li><strong>Validéz del presupuesto:</strong> <span>{{ \Carbon\Carbon::parse($presupuesto->vencimiento)->format('d/m/Y') }}</span></li>
                <li><strong>Vehículo:</strong> <span>{{ $presupuesto->vehiculo()->patente }}</span></li>
                <li><strong>Operarios:</strong> <span>{{ $presupuesto->personal }}</span></li>
                {{--<li><strong>Forma de pago:</strong> <span>Efectivo</span></li>--}}
            </ul>
        </td>
    </tr>
    </tbody>
</table>

<table class="direcciones">
    <thead>
    <tr class="titulos">
        <th width="50%">
            <h2>CARGAS</h2>
        </th>
        <th width="50%">
            <h2>DESCARGAS</h2>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td class="cargas">
            <table class="direccion">
                <tbody>
                <?php $primera = true; ?>

                @foreach($presupuesto->cargas() as $direccion)

                    @if(!$primera)
                        <tr>
                            <hr>
                        </tr>
                    @endif

                    <tr>
                        <td>
                            <ul>
                                <li><strong>Dirección:</strong> <span>{{ $direccion->direccion }}</span>
                                <li><strong>Piso:</strong> {{ $direccion->piso }} <strong>Dpto.:</strong> {{ $direccion->departamento }}</li>
                                <li><strong>Entre calles:</strong> <span>{{ $direccion->entrecalles }}</span></li>
                                <li><strong>Localidad:</strong> <span>{{ $direccion->localidad }}</span></li>
                                <li><strong>Provincia:</strong> <span>{{ $direccion->provincia }}</span></li>
                            </ul>
                        </td>
                        <td class="imagen">
                            @if($direccion->piso == "PB")
                                <img src="{{ asset('img/pdf/1018528.png') }}">
                            @else
                                <img src="{{ asset('img/pdf/1018550.png') }}">
                            @endif
                        </td>
                    </tr>
                    <?php $primera = false; ?>
                @endforeach
                </tbody>
            </table>
        </td>

        <td class="descargas">
            <table class="direccion">
                <tbody>
                <?php $primera = true; ?>

                @foreach($presupuesto->descargas() as $direccion)

                    @if(!$primera)
                        <tr>
                            <hr>
                        </tr>
                    @endif

                    <tr>
                        <td>
                            <ul>
                                <li><strong>Dirección:</strong> <span>{{ $direccion->direccion }}</span>
                                <li><strong>Piso:</strong> {{ $direccion->piso }} <strong>Dpto.:</strong> {{ $direccion->departamento }}</li>
                                <li><strong>Entre calles:</strong> <span>{{ $direccion->entrecalles }}</span></li>
                                <li><strong>Localidad:</strong> <span>{{ $direccion->localidad }}</span></li>
                                <li><strong>Provincia:</strong> <span>{{ $direccion->provincia }}</span></li>
                            </ul>
                        </td>
                        <td class="imagen">
                            @if($direccion->piso == "PB")
                                <img src="{{ asset('img/pdf/1018528.png') }}">
                            @else
                                <img src="{{ asset('img/pdf/1018550.png') }}">
                            @endif
                        </td>
                    </tr>
                    <?php $primera = false; ?>
                @endforeach
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<table class="conceptos" width="100%">
    <thead>
    <tr>
        <th class="concepto" width="70">CONCEPTO</th>
        <th class="importe" width="30">IMPORTE</th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>{{ strtoupper($presupuesto->servicio()->nombre) }}</td>
        <td>
            {{--@if($presupuesto->servicio()->id == 3 || $presupuesto->servicio()->id == 4)--}}
            {{--${{ $presupuesto->vehiculo()->valor }}--}}
            {{--@else--}}
            {{--{!! $presupuesto->cotizacion !!}--}}
            {{--@endif--}}

            ${!! number_format($presupuesto->valor_aproximado, 0, ",", ".") !!}
        </td>
    </tr>
    @foreach($presupuesto->adicionales() as $adicional)
        <tr>
            <td>{{ strtoupper($adicional->nombre) }}</td>
            <td>{{ ($adicional->valor == 0) ? "Incluído" : "$".number_format($adicional->valor, 0, ",", ".") }}</td>
        </tr>
    @endforeach

    @foreach($presupuesto->otros_adicionales() as $otroAdicional)
        <tr>
            <td>{{ strtoupper($otroAdicional->nombre) }}</td>
            <td>${{ number_format($otroAdicional->valor, 0, ",", ".") }}</td>
        </tr>
    @endforeach
    <tr>
        <td>ASCENSO Y DESCENSO POR ESCALERA</td>
        <td>Bonificado</td>
    </tr>
    <tr>
        <td>DESARME Y ARMADO DE MUEBLES</td>
        <td>Bonificado</td>
    </tr>
    <tr>
        <td>DESINSTALACIÓN E INSTALACIÓN DE LUMINARIAS</td>
        <td>Bonificado</td>
    </tr>
    <tr>
        <td>KIT DE EMBALAJE</td>
        <td>Bonificado</td>
    </tr>
    <tr>
        <td>ASCENSO Y DESCENSO POR POLEAS/SOGAS</td>
        <td>Consultar presupuesto</td>
    </tr>
    <tr>
        <td>DESEMBALAJE PREMIUM</td>
        <td>Consultar presupuesto</td>
    </tr>
    <tr>
        <td>EMBALAJE PREMIUM</td>
        <td>Consultar presupuesto</td>
    </tr>
    {{--<tr>--}}
    {{--<td>OTROS</td>--}}
    {{--<td>$.-</td>--}}
    {{--</tr>--}}
    </tbody>
</table>

<table class="visita" width="100%">
    <tbody>
    <tr>
        @if($presupuesto->servicio()->id != 3 && $presupuesto->servicio()->id != 4)
            <td valign="middle" width="50%">
                <table class="programada" width="100%">
                    <tbody>
                    <tr>
                        <td width="100%">
                            <h4>Visita Programada</h4>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 1rem">
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        @if(!empty($ultimaVisita))
                                            Fecha:
                                            @if($ultimaVisita->fecha != '')
                                                {{ \Carbon\Carbon::parse($ultimaVisita->fecha)->format('d/m/Y') }}
                                            @else
                                                A confirmar
                                            @endif
                                        @else
                                            Fecha: A confirmar
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @if(!empty($ultimaVisita))
                                            Franja Horaria:
                                            @if($ultimaVisita->desde != '' && $ultimaVisita->hasta != '')
                                                {{ \Carbon\Carbon::parse($ultimaVisita->desde)->format('H:i') }} a {{ \Carbon\Carbon::parse($ultimaVisita->hasta)->format('H:i') }}
                                            @else
                                                A confirmar
                                            @endif
                                        @else
                                            Franja Horaria: A confirmar
                                        @endif
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        @endif

        <td valign="top" style="padding-left: 3rem;">
            <table width="100%">
                <tbody>
                @if($presupuesto->impuesto != 0)
                    <tr>
                        <td width="100%">
                            <table class="presupuesto" width="100%">
                                <tr>
                                    <td class="nombre" width="50%">
                                        TOTAL SIN IMPUESTO
                                    </td>

                                    <td class="total" width="50%">
                                        ${{ number_format($presupuesto->total, 0, ",", ".") }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <table class="presupuesto" width="100%">
                                <tr>
                                    <td class="nombre" width="50%">
                                        IMPUESTO {{ $presupuesto->impuesto }}%
                                    </td>

                                    <td class="total" width="50%">
                                        ${{ number_format(($presupuesto->impuesto * $presupuesto->total) / 100, 0, ",", ".") }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <table class="presupuesto" width="100%">
                                <tr>
                                    <td class="nombre" width="50%">
                                        TOTAL CON IMPUESTO
                                    </td>

                                    <td class="total" width="50%">
                                        ${{ number_format($presupuesto->cotizacion, 0, ",", ".") }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    @if($presupuesto->pagado() > 0)
                        <tr>
                            <td width="100%">
                                <table class="presupuesto" width="100%">
                                    <tr>
                                        <td class="nombre" width="50%">
                                            ABONO
                                        </td>

                                        <td class="total" width="50%">
                                            ${{ number_format($presupuesto->pagado(), 0, ",", ".") }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <table class="presupuesto" width="100%">
                                    <tr>
                                        <td class="nombre" width="50%">
                                            SALDO
                                        </td>

                                        <td class="total" width="50%">
                                            ${{ number_format($presupuesto->faltaPagar(), 0, ",", ".") }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    @endif
                @else
                    <tr>
                        <td width="100%">
                            <table class="presupuesto" width="100%">
                                <tr>
                                    <td class="nombre" width="50%">
                                        TOTAL
                                    </td>

                                    <td class="total" width="50%">
                                        ${{ number_format($presupuesto->cotizacion, 0, ",", ".") }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    @if($presupuesto->pagado() > 0)
                        <tr>
                            <td width="100%">
                                <table class="presupuesto" width="100%">
                                    <tr>
                                        <td class="nombre" width="50%">
                                            ABONO
                                        </td>

                                        <td class="total" width="50%">
                                            ${{ number_format($presupuesto->pagado(), 0, ",", ".") }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <table class="presupuesto" width="100%">
                                    <tr>
                                        <td class="nombre" width="50%">
                                            SALDO
                                        </td>

                                        <td class="total" width="50%">
                                            ${{ number_format($presupuesto->faltaPagar(), 0, ",", ".") }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    @endif
                @endif
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<table class="alerta" width="100%">
    <tbody>
    <tr>
        <td>
            @if($presupuesto->pagado() > 0)
                <p>El saldo podrá ser abonado en efectivo al finalizar el servicio o en caso de pago con transferencia bancaria 48hs antes de finalizar el servicio</p>
            @else
                <p>Los valores del siguiente presupuesto quedan sujetos a validación en el domicilio</p>
            @endif
        </td>
    </tr>
    </tbody>
</table>

<table class="bases" width="100%">
    <tbody>
    <tr>
        <td>
            <h4>Bases y condiciones</h4>

            {!! $presupuesto->servicio()->basesycondiciones !!}
        </td>
    </tr>
    </tbody>
</table>

<footer>


</footer>

</body>

</html>