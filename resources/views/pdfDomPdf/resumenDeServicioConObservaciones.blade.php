<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Gothic+A1:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/pdfs/resumenDeServicio.css') }}" />
    <title>Resumen de Servicio</title>

</head>

<body>

<header>
    <table width="100%">
        <tr>
            <td width="50%">
                <img src="{{ asset('img/pdf/logotransportes.png') }}" style="max-height: 50px">
            </td>
            <td width="50%" align="right">
                <h5>Presupuesto: {{ $presupuesto->id }}</h5>
            </td>
        </tr>
    </table>
</header>

<main>
    <table class="resumenDeServicio" width="100%">
        <thead>
        <tr>
            <th>
                <h3>DETALLE DE VISITA</h3>
            </th>
        </tr>

        </thead>

        <tbody>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <ul>
                                <li>Fecha de Servicio:
                                    @if($presupuesto->fecha != '')
                                        {{ \Carbon\Carbon::parse($presupuesto->fecha)->format('d/m/Y') }}
                                    @else
                                        A confirmar
                                    @endif
                                </li>
                                <li>Hora de Servicio:
                                    @if($presupuesto->hora != '' && $presupuesto->hora_fin != '')
                                        {{ \Carbon\Carbon::parse($presupuesto->hora)->format('H:i') }} - {{ \Carbon\Carbon::parse($presupuesto->hora_fin)->format('H:i') }}
                                    @else
                                        A confirmar
                                    @endif
                                </li>
                                @if(!empty($presupuesto->vehiculo()))
                                    <li>Vehiculo: {{ $presupuesto->vehiculo()->patente }}</li>
                                @endif
                                <li>Operarios: {{ $presupuesto->personal }}</li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li>Tipo de Servicio: {{ $presupuesto->servicio()->nombre }}</li>
                                <li>Sr./Sra.: {{ $presupuesto->cliente()->nombre }} {{ $presupuesto->cliente()->apellido }}</li>
                                <li>Teléfono: {{ $presupuesto->cliente()->telefono }}</li>

                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li>Teléfono Alternativo: {{ $presupuesto->cliente()->telefono_alternativo }}</li>
                                <li>Horas de servicio estimado: {{ $presupuesto->visita()->detalle()->horas }}</li>
                                @if($presupuesto->larga_distancia == 1)
                                    <li>Kilómetros a recorrer: {{ $presupuesto->kms }}</li>
                                @endif
                            </ul>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        </tbody>
    </table>

    <table class="direcciones" width="100%">
        <tr>
            <td class="desde">
                <table width="100%">
                    @foreach($presupuesto->cargas() as $direccion)
                        <tr class="direccion">
                            <td class="imagen">
                                @if($direccion->piso == "PB")
                                    <img src="{{ asset('img/pdf/1018528.png') }}">
                                @else
                                    <img src="{{ asset('img/pdf/1018550.png') }}">
                                @endif
                            </td>
                            <td class="informacion">
                                <h4>DESDE</h4>
                                <ul>
                                    <li>{{ $direccion->direccion }}</li>
                                    <li>{{ $direccion->entrecalles }}</li>
                                    <li>{{ $direccion->localidad }}</li>
                                    <li>{{ $direccion->provincia }}</li>
                                    <li>Piso: {{ $direccion->piso }}</li>
                                    <li>Departamento: {{ $direccion->departamento }}</li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </td>
            <td class="hasta">
                <table width="100%">
                    @foreach($presupuesto->descargas() as $direccion)
                        <tr class="direccion">
                            <td class="imagen">
                                @if($direccion->piso == "PB")
                                    <img src="{{ asset('img/pdf/1018528.png') }}">
                                @else
                                    <img src="{{ asset('img/pdf/1018550.png') }}">
                                @endif
                            </td>
                            <td class="informacion">
                                <h4>DESDE</h4>
                                <ul>
                                    <li>{{ $direccion->direccion }}</li>
                                    <li>{{ $direccion->entrecalles }}</li>
                                    <li>{{ $direccion->localidad }}</li>
                                    <li>{{ $direccion->provincia }}</li>
                                    <li>Piso: {{ $direccion->piso }}</li>
                                    <li>Departamento: {{ $direccion->departamento }}</li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </td>
        </tr>
    </table>

    <table class="visita" width="100%">
        <thead>
        <tr>
            <td>
                <h3>
                    <span class="visitador">
                        Visitado por:
                        @if(!empty($presupuesto->visita()->visitador()))
                            {{ $presupuesto->visita()->visitador()->nombre }} {{ $presupuesto->visita()->visitador()->apellido }}
                        @endif
                    </span>
                    <span class="linea">|</span>
                    <span class="confirmado">Visita confirmada por: {{ $presupuesto->vendedor()->nombre }} {{ $presupuesto->vendedor()->apellido }}</span>
                </h3>
            </td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <table class="adicionales" width="100%">
                    @foreach($presupuesto->otros_adicionales() as $otroAdicional)
                        <tr class="elemento">
                            <td class="nombre" width="30%">
                                @if(file_exists('img/pdf/' . \App\Helpers\ImagenPdf::generarNombre($otroAdicional->nombre) . '.png'))
                                    <img src="{{ asset('img/pdf/'.\App\Helpers\ImagenPdf::generarNombre($otroAdicional->nombre).'.png') }}">
                                @endif
                                <h4>{{ $otroAdicional->nombre }}</h4>
                            </td>
                            <td class="descripcion">
                                {!! $otroAdicional->observaciones !!}
                                <br>
                                Operarios: {{ $otroAdicional->personal }} <br>
                                Duración: {{ $otroAdicional->duracion }}
                                @if($otroAdicional->fecha != '')
                                    Fecha: {{ \Carbon\Carbon::parse($otroAdicional->fecha)->format('d/m/Y') }} <br>
                                    Hora:
                                    @if($otroAdicional->hora_inicio != '' && $otroAdicional->hora_fin != '')
                                        {{ \Carbon\Carbon::parse($otroAdicional->hora_inicio)->format('H:i') }} - {{ \Carbon\Carbon::parse($otroAdicional->hora_fin)->format('H:i') }}
                                    @else
                                        A confirmar
                                    @endif
                                @endif
                                <hr>
                            </td>
                        </tr>
                    @endforeach
                </table>
                <table class="observaciones" width="100%">
                    @foreach($presupuesto->visita()->observaciones() as $observacion)
                        <tr class="elemento">
                            <td class="nombre" width="30%">
                                @if(file_exists('img/pdf/' . \App\Helpers\ImagenPdf::generarNombre($observacion->nombre) . '.png'))
                                    <img src="{{ asset('img/pdf/'.\App\Helpers\ImagenPdf::generarNombre($observacion->nombre).'.png') }}">
                                @endif
                                <h4>{{ $observacion->nombre }}</h4>
                            </td>
                            <td class="descripcion">
                                {!! $observacion->descripcion !!}
                                <hr>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="generales" width="100%">
        <thead>
            <tr>
                <th>
                    <h2>Observaciones generales</h2>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="texto">
                    {!! $presupuesto->observaciones !!}
                </td>
            </tr>
        </tbody>
    </table>
</main>

</body>

</html>