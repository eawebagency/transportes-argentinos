<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Gothic+A1:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/pdfs/planillaDeVisita.css') }}" >
    <title>Resumen de Servicio</title>

</head>

<body>

<header>
    <table width="100%">
        <tr>
            <td class="logo">
                <img src="{{ asset('img/pdf/logotransportes.png') }}" style="max-height: 50px;">
            </td>

            <td class="presupuesto">
                <h4>DETALLE DE VISITA #{{ $presupuesto->id }}</h4>
                <h4>{{ $presupuesto->cliente()->nombre }} {{ $presupuesto->cliente()->apellido }}</h4>
            </td>
        </tr>
    </table>
</header>

<main>

    <table width="100%" class="resumenDeServicio">
        <tr>
            <td>
                <ul>
                    <li><strong>Presupuesto:</strong> #{{ $presupuesto->id }}</li>
                    <li><strong>Cliente:</strong> {{ $presupuesto->cliente()->nombre }} {{ $presupuesto->cliente()->apellido }}</li>
                    <li><strong>Teléfono:</strong> {{ $presupuesto->cliente()->telefono }}</li>
                    <li><strong>Teléfono Alternativo:</strong> {{ $presupuesto->cliente()->telefono_alternativo }}</li>
                    <li><strong>Mail:</strong> {{ $presupuesto->cliente()->email }}</li>
                </ul>
            </td>

            <td>
                <ul>
                    @if(!empty($presupuesto->vehiculo()))
                        <li><strong>Vehiculo cotizado:</strong> {{ $presupuesto->vehiculo()->patente }}</li>
                    @endif
                    <li><strong>Operarios:</strong> {{ $presupuesto->personal }}</li>
                    @if($presupuesto->larga_distancia == 1)
                        <li><strong>KM Cotizados:</strong> {{ $presupuesto->kms }}</li>
                    @endif
                    <li><strong>Cotización Total:</strong> {{ $presupuesto->cotizacion }}</li>
                    <li><strong>Fecha y hora de mudanza:</strong> @if($presupuesto->fecha != '') {{ \Carbon\Carbon::parse($presupuesto->fecha)->format('d/m/Y') }} - {{ \Carbon\Carbon::parse($presupuesto->hora)->format('H:i') }} hs @else A confirmar @endif </li>
                    {{--<li><strong>Tipo de Servicio:</strong> {{ $presupuesto->servicio()->nombre }}</li>--}}
                    {{--<li><strong>Horas de servicio estimado:</strong> {{ $presupuesto->visita()->detalle()->horas }}</li>--}}
                </ul>
            </td>
        </tr>
    </table>

    <table width="100%" class="direcciones">
        <thead>
            <tr class="titulos">
                <th>
                    <h3>DATOS DE ORIGEN</h3>
                </th>
                <th>
                    <h3>DATOS DE DESTINO</h3>
                </th>
            </tr>
        </thead>

        <tbody>
            <tr>
               <td width="50%" class="cargas">
                   <table width="100%">
                       <?php $primera = true; ?>

                       @foreach($presupuesto->cargas() as $direccion)
                           @if(!$primera)
                               <tr>
                                   <td colspan="2">
                                       <hr>
                                   </td>
                               </tr>
                           @endif

                           <tr class="direccion">
                               <td width="35%">
                                   <ul>
                                       <li><strong>Dirección:</strong> <span>{{ $direccion->direccion }}</span>
                                       <li><strong>Piso:</strong> {{ $direccion->piso }} <strong>Dpto.:</strong> {{ $direccion->departamento }}</li>
                                       <li><strong>Entre calles:</strong> <span>{{ $direccion->entrecalles }}</span></li>
                                       <li><strong>Localidad:</strong> <span>{{ $direccion->localidad }}</span></li>
                                       <li><strong>Provincia:</strong> <span>{{ $direccion->provincia }}</span></li>
                                       <li><strong>¿Tiene recorrido?</strong></li>
                                       <li><strong>¿Tiene restricción?</strong></li>

                                   </ul>
                               </td>

                               <td width="65%" class="imagen">
                                   @if($direccion->piso == "PB")
                                       <img src="{{ asset('img/pdf/1018528.png') }}">
                                   @else
                                       <img src="{{ asset('img/pdf/1018550.png') }}">
                                   @endif
                               </td>
                           </tr>
                           <tr class="ascensor">
                               <td>
                                   <table width="100%">
                                       <tr>
                                           <td class="titulo" width="60%">
                                               <strong>¿Ascensor?</strong>
                                           </td>
                                           <td class="opciones" width="40%">
                                               <table width="100%">
                                                   <tr>
                                                       <td>
                                                           <span>Chico</span>
                                                       </td>
                                                       <td>
                                                           <span>Mediano</span>
                                                       </td>
                                                       <td>
                                                           <span>Grande</span>
                                                       </td>
                                                   </tr>
                                               </table>
                                           </td>
                                       </tr>
                                   </table>
                               </td>
                           </tr>
                           <tr class="observaciones">
                               <td colspan="2">
                                   {!! $direccion->observaciones !!}
                               </td>
                           </tr>

                           <?php $primero = false; ?>
                       @endforeach
                   </table>
               </td>

                <td width="50%" class="descargas">
                    <table width="100%">
                        <?php $primera = true; ?>

                        @foreach($presupuesto->descargas() as $direccion)
                            @if(!$primera)
                                <tr>
                                    <td colspan="2">
                                        <hr>
                                    </td>
                                </tr>
                            @endif

                            <tr class="direccion">
                                <td width="35%">
                                    <ul>
                                        <li><strong>Dirección:</strong> <span>{{ $direccion->direccion }}</span>
                                        <li><strong>Piso:</strong> {{ $direccion->piso }} <strong>Dpto.:</strong> {{ $direccion->departamento }}</li>
                                        <li><strong>Entre calles:</strong> <span>{{ $direccion->entrecalles }}</span></li>
                                        <li><strong>Localidad:</strong> <span>{{ $direccion->localidad }}</span></li>
                                        <li><strong>Provincia:</strong> <span>{{ $direccion->provincia }}</span></li>
                                        <li><strong>¿Tiene recorrido?</strong></li>
                                        <li><strong>¿Tiene restricción?</strong></li>

                                    </ul>
                                </td>

                                <td width="65%" class="imagen">
                                    @if($direccion->piso == "PB")
                                        <img src="{{ asset('img/pdf/1018528.png') }}">
                                    @else
                                        <img src="{{ asset('img/pdf/1018550.png') }}">
                                    @endif
                                </td>
                            </tr>
                            <tr class="ascensor">
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td class="titulo" width="60%">
                                                <strong>¿Ascensor?</strong>
                                            </td>
                                            <td class="opciones" width="40%">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <span>Chico</span>
                                                        </td>
                                                        <td>
                                                            <span>Mediano</span>
                                                        </td>
                                                        <td>
                                                            <span>Grande</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="observaciones">
                                <td colspan="2">
                                    {!! $direccion->observaciones !!}
                                </td>
                            </tr>

                            <?php $primero = false; ?>
                        @endforeach
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <table width="100%" class="visita">
        <tr>
            <td>
                <textarea>
CAJAS:                                                   ROPEROS:&#13;&#10;
                    ILUMINARIAS:                                                   VENT. DE TECHO:                 AA:&#13;&#10;&#13;&#10;


DESARME Y ARMADO DE MUEBLES:&#13;&#10;&#13;&#10;&#13;&#10;&#13;&#10;
OBJETOS DELICADOS Y/O RELEVANTES:&#13;&#10;&#13;&#10;&#13;&#10;&#13;&#10;
OBSERVACIONES:&#13;&#10;&#13;&#10;&#13;&#10;&#13;&#10;
        </textarea>
            </td>
        </tr>
    </table>

    <table width="100%" class="mudanzas">
        <thead>
        <tr>
            <th></th>
            <th>VEHÍCULO</th>
            <th>HORAS DE SERVICIO</th>
            <th>PERSONAL AFECTADO</th>
            <th>PRESUPUESTO</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>Mudanza</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Embalaje Premium</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Desembalaje Premium</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>

    <table width="100%" class="detalle">
        <tr>
            <td>
                <h4><strong>Detalle del presupuesto</strong></h4>
                <ul>
                    <li><strong>Vehículo:</strong> <span>{{ $presupuesto->vehiculo()->patente }}</span></li>
                    <li><strong>Operarios:</strong> <span>{{ $presupuesto->personal }}</span></li>
                </ul>

                <ul>
                    @foreach($presupuesto->adicionales() as $adicional)
                        <li><strong>{{ strtoupper($adicional->nombre) }}:</strong> <span>${{ number_format($adicional->valor, 0, ",", ".") }}</span></li>
                    @endforeach
                </ul>

                <ul>
                    @foreach($presupuesto->otros_adicionales() as $otroAdicional)
                        <li><strong>{{ strtoupper($otroAdicional->nombre) }}:</strong> <span>${{ number_format($otroAdicional->valor, 0, ",", ".") }}</span></li>
                    @endforeach
                </ul>

                <ul>
                    @if($presupuesto->impuesto != 0)
                        <li><strong>TOTAL SIN IMPUESTO:</strong> <span>${{ number_format($presupuesto->total, 0, ",", ".") }}</span></li>
                        <li><strong>IMPUESTO {{ $presupuesto->impuesto }}%:</strong> <span>${{ number_format(($presupuesto->impuesto * $presupuesto->total) / 100, 0, ",", ".") }}</span></li>
                        <li><strong>TOTAL CON IMPUESTO:</strong> <span>${{ number_format($presupuesto->cotizacion, 0, ",", ".") }}</span></li>
                        @if($presupuesto->pagado() > 0)
                            <li><strong>ABONO:</strong> <span>${{ number_format($presupuesto->pagado(), 0, ",", ".") }}</span></li>
                            <li><strong>SALDO:</strong> <span>${{ number_format($presupuesto->faltaPagar(), 0, ",", ".") }}</span></li>
                        @endif

                    @else

                        <li><strong>TOTAL:</strong> <span>${{ number_format($presupuesto->cotizacion, 0, ",", ".") }}</span></li>
                        @if($presupuesto->pagado() > 0)
                            <li><strong>ABONO:</strong> <span>${{ number_format($presupuesto->pagado(), 0, ",", ".") }}</span></li>
                            <li><strong>SALDO:</strong> <span>${{ number_format($presupuesto->faltaPagar(), 0, ",", ".") }}</span></li>
                        @endif
                    @endif
                </ul>
            </td>
        </tr>
    </table>

    <table width="100%" class="fechas">
        <tr>
            <td width="100%">
                <hr>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <h4><strong>Observaciones de la Visita:</strong></h4>
                        </td>
                        <td>
                            <h4><strong>Fecha de la visita:</strong> {{ \Carbon\Carbon::parse($presupuesto->ultimaVisita()->fecha)->format('d/m/Y') }}</h4>
                            <h4><strong>Franja Horaria de la Visita:</strong> {{ \Carbon\Carbon::parse($presupuesto->ultimaVisita()->desde)->format('H:i') }} - {{ \Carbon\Carbon::parse($presupuesto->ultimaVisita()->hasta)->format('H:i') }}</h4>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table width="100%" class="generales">
        <tr>
            <td class="texto">
                {!! $presupuesto->ultimaVisita()->observaciones !!}
            </td>
        </tr>
    </table>

</main>

<footer>

</footer>
</body>

</html>