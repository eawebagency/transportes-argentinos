<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Presupuesto</title>

    <style>
        @page { margin: 0; }

        div {
            page-break-inside: avoid;
        }

        body {
            width: 23cm;
            height: 29.7cm;
            margin: 0;
            font-size: 14px;
        }

        header {
            width: 100%;
            border: 2px solid #3ec6f1;
            border-radius: 10px;
            padding: 0.7rem;
            position: relative;
        }

        header .logo {
            width: 40%;
            display: inline-block;
            vertical-align: middle;
        }

        header .logo .imagen {
            width: 100%;
        }

        header .logo .imagen img {
            max-width: 300px;
        }

        header .logo ul {
            list-style-type: none;
            padding-left: 0;
        }

        header .separador {
            position: absolute;
            top: 0;
            left: 47%;
            height: 100%;
            width: 5%;
            overflow: hidden;
        }

        header .separador .x {
            background: #3ec6f1;
            padding: 0.5rem;
            text-align: center;
            font-size: 1.5rem;
            color: black;
            height: 25%;
            font-weight: 700;
            border-bottom-left-radius: 10px;
            border-bottom-right-radius: 10px;
        }

        header .separador .linea {
            border-left: 2px solid #3ec6f1;
            height: 75%;
            margin-left: 50%;
        }

        header .datos {
            width: 40%;
            display: inline-block;
            vertical-align: middle;
            margin-left: 15%;
        }

        header .datos ul {
            list-style-type: none;
        }

        header .datos ul li {
            text-align: left;
        }

        header .datos ul li span {
            float: right;
        }

        main .informacion {
            margin-top: 1rem;
            width: 100%;
            position: relative;
            padding: 1rem 0.7rem;
            border: 2px solid #3ec6f1;
            border-radius: 10px;
        }

        main .informacion .columna1,
        main .informacion .columna2 {
            width: 35%;
            display: inline-block;
            vertical-align: middle;
        }

        main .informacion .columna2 {
            margin-left: 20%;
        }

        main .informacion .columna1 ul,
        main .informacion .columna2 ul {
            list-style-type: none;
            padding-left: 0;
            margin-top: 0;
            margin-bottom: 0;
        }

        main .informacion .columna1 ul li,
        main .informacion .columna2 ul li {
            padding-bottom: 0.5rem;
        }

        main .informacion .separador {
            position: absolute;
            top: 0;
            height: 100%;
            left: 50%;
            border-left: 2px solid #3ec6f1;
        }

        main .direcciones {
            margin-top: 1rem;
            width: 100%;
            position: relative;
            padding: 0.7rem;
            border: 2px solid #3ec6f1;
            border-radius: 10px;
        }

        main .direcciones .titulos {
            background: #3ec6f1;
            margin: -0.7rem -0.7rem 0 -0.7rem;
        }

        main .direcciones .titulos h2 {
            width: 49%;
            display: inline-block;
            text-align: center;
            margin-bottom: 0.2rem;
            margin-top: 0.2rem;
        }

        main .direcciones .cargas,
        main .direcciones .descargas {
            width: 48%;
            display: inline-block;
            vertical-align: middle;
        }

        main .direcciones .descargas {
            margin-left: 3%;
        }

        main .direcciones .cargas .direccion,
        main .direcciones .descargas .direccion {
            width: 100%;
        }

        main .direcciones .cargas .direccion ul,
        main .direcciones .descargas .direccion ul {
            list-style-type: none;
            padding-left: 0;
            width: 60%;
            display: inline-block;
            vertical-align: middle;
        }

        main .direcciones .cargas .direccion ul li,
        main .direcciones .descargas .direccion  ul li {
            margin-bottom: 0.3rem;
        }

        main .direcciones .cargas .direccion .imagen,
        main .direcciones .descargas .direccion .imagen {
            width: 30%;
            display: inline-block;
            vertical-align: middle;
            margin-left: 9%;
        }

        main .direcciones .cargas .direccion .imagen img,
        main .direcciones .descargas .direccion .imagen img {
            max-width: 100px;
        }

        main .direcciones .cargas hr,
        main .direcciones .descargas hr {
            border-color: #3ec6f1;
        }

        main .direcciones .separador {
            position: absolute;
            top: 0;
            height: 100%;
            left: 50%;
            border-left: 2px solid #3ec6f1;
        }

        main .conceptos {
            width: 100%;
            margin-top: 1rem;
            position: relative;
            padding: 0.7rem;
        }

        main .conceptos table {
            table-layout: fixed;
            width: 100%;
        }

        main .conceptos table thead tr th {
            background: #3ec6f1;
            font-weight: 700;
        }

        main .conceptos table thead tr th:first-child {
            border-top-left-radius: 20px;
            width: 75%;
        }

        main .conceptos table thead tr th:last-child {
            border-top-right-radius: 20px;
            width: 25%;
        }

        main .conceptos table tbody tr td {
            border-bottom: 1px solid #ddd;
        }

        main .visita {
            width: 100%;
            position: relative;
            padding: 0.7rem;
        }

        main .visita .contenedor,
        main .visita .presupuesto {
            width: 45%;
            display: inline-block;
            vertical-align: middle;
        }

        main .visita .contenedor .programada {
            border: 2px solid #3ec6f1;
            border-radius: 15px;
        }

        main .visita .contenedor .programada h4{
            background: #3ec6f1;
            text-align: center;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
            margin: -0.3rem 0;
            padding: 0.2rem;
        }

        main .visita .contenedor .programada .horario {
            width: 100%;
            padding: 1rem 0.3rem;
            overflow: hidden;
        }

        main .visita .contenedor .programada .horario span {
            width: 49%;
            display: inline-block;
            vertical-align: middle;
        }

        main .visita .contenedor .programada .horario span:last-child {
            float: right;
        }

        main .visita .presupuesto {
            margin-left: 9%;
        }

        main .visita .presupuesto .contenedor {
            width: 100%;
            border: 2px solid #3ec6f1;
            border-radius: 15px;
            margin-bottom: 1rem;
        }

        main .visita .presupuesto .contenedor .nombre,
        main .visita .presupuesto .contenedor .total {
            width: 47%;
            vertical-align: middle;
            display: inline-block;
            font-size: 1.5rem;
            text-align: center;
        }

        main .visita .presupuesto .contenedor .nombre {
            background: #3ec6f1;
            font-weight: 600;
            border-top-left-radius: 15px;
            border-bottom-left-radius: 15px;
            padding: 0.5rem;
            margin-left: -0.3rem;
        }

        main .visita .presupuesto .contenedor .total {
            background: transparent;
            font-weight: 600;
            border-top-right-radius: 15px;
            border-bottom-right-radius: 15px;
        }

        /*main .visita .presupuesto ul {*/
            /*list-style-type: none;*/
            /*padding: 1rem 0.3rem;*/
            /*margin-top: 0;*/
            /*margin-bottom: 0;*/
        /*}*/

        /*main .visita .presupuesto ul li {*/
            /*font-size: 1rem;*/
        /*}*/

        /*main .visita .presupuesto ul li span {*/
            /*float: right;*/
        /*}*/

        main .alerta {
            width: 100%;
            margin-top: 1rem;
            position: relative;
            text-align: center;
            font-size: 1rem;
        }

        main .bases {
            width: 100%;
            margin-top: 1rem;
            padding: 0.7rem;
            border: 2px solid #3ec6f1;
            border-radius: 15px;
        }

        main .bases h4 {
            background: #3ec6f1;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
            margin: -0.8rem;
            padding: 0.3rem;
        }

        main .bases p {
            margin-top: 1.5rem;
            font-size: 1rem;
        }

        main .bases p span {
            font-size: 1rem !important;
        }

        footer {
            position: fixed;
            bottom: 5px;
            width: 21cm;
        }
    </style>

</head>

<body>

<header>

    <div class="logo">
        <div class="imagen">
            <img src="{{ asset('img/pdf/logotransportes.png') }}">
        </div>

        <ul>
            <li>CUIT: 30-71628105-8</li>
            <li>TAMOVES S.A.S</li>
        </ul>
    </div>

    <div class="separador">
        <div class="x">
            X
        </div>

        <div class="linea"></div>
    </div>

    <div class="datos">
        <ul>
            <li>FECHA DE EMISIÓN <span>{{ \Carbon\Carbon::parse($presupuesto->created_at)->format('d/m/Y') }}</span></li>
            <li>PRESUPUESTO <span>{{ $presupuesto->id }}</span></li>
            <li>(011) 15-4179-3432 (011) 3974-6822</li>
            <li>info@transportesargentinos.com.ar</li>
            <li>www.transportesargentinos.com.ar</li>
        </ul>
    </div>

</header>

<main>

    <div class="informacion">
        <div class="columna1">
            <ul>
                <li><strong>Sr./Sra:</strong> <span>{{ $presupuesto->cliente()->nombre }} {{ $presupuesto->cliente()->apellido }}</span></li>
                <li><strong>Teléfono:</strong> <span>{{ $presupuesto->cliente()->telefono }}</span></li>
                <li><strong>Teléfono alternativo:</strong> <span>{{ $presupuesto->cliente()->telefono_alternativo }}</span></li>
                <li><strong>Fecha de Servicio:</strong> <span>
                        @if($presupuesto->fecha != '')
                            {{ \Carbon\Carbon::parse($presupuesto->fecha)->format('d/m/Y') }}
                        @else
                            A confirmar
                        @endif
                    </span></li>
                <li><strong>Hora de Servicio:</strong> <span>
                        @if($presupuesto->hora != '')
                            {{ \Carbon\Carbon::parse($presupuesto->hora)->format('H:i') }}
                        @else
                            A confirmar
                        @endif
                    </span></li>
            </ul>
        </div>

        <div class="separador"></div>

        <div class="columna2">
            <ul>
                <li><strong>Validéz del presupuesto:</strong> <span>{{ \Carbon\Carbon::parse($presupuesto->vencimiento)->format('d/m/Y') }}</span></li>
                <li><strong>Vehículo:</strong> <span>{{ $presupuesto->vehiculo()->patente }}</span></li>
                <li><strong>Operarios:</strong> <span>{{ $presupuesto->personal }}</span></li>
                {{--<li><strong>Forma de pago:</strong> <span>Efectivo</span></li>--}}
            </ul>
        </div>
    </div>

    <div class="direcciones">

        <div class="titulos">
            <h2>CARGAS</h2>
            <h2>DESCARGAS</h2>
        </div>

        <div class="cargas">

            <?php $primera = true; ?>

            @foreach($presupuesto->cargas() as $direccion)

                @if(!$primera)
                    <hr>
                @endif

                <div class="direccion">
                    <ul>
                        <li><strong>Dirección:</strong> <span>{{ $direccion->direccion }}</span>
                        <li><strong>Piso:</strong> {{ $direccion->piso }} <strong>Dpto.:</strong> {{ $direccion->departamento }}</li>
                        <li><strong>Entre calles:</strong> <span>{{ $direccion->entrecalles }}</span></li>
                        <li><strong>Localidad:</strong> <span>{{ $direccion->localidad }}</span></li>
                        <li><strong>Provincia:</strong> <span>{{ $direccion->provincia }}</span></li>
                    </ul>

                    <div class="imagen">
                        @if($direccion->piso == "PB")
                            <img src="{{ asset('img/pdf/1018528.png') }}">
                        @else
                            <img src="{{ asset('img/pdf/1018550.png') }}">
                        @endif
                    </div>
                </div>

                    <?php $primero = false; ?>
            @endforeach
        </div>

        <div class="separador"></div>

        <div class="descargas">

            <?php $primera = true; ?>

            @foreach($presupuesto->descargas() as $direccion)

                @if(!$primera)
                    <hr>
                @endif

                <div class="direccion">
                    <ul>
                        <li><strong>Dirección:</strong> <span>{{ $direccion->direccion }}</span>
                        <li><strong>Piso:</strong> {{ $direccion->piso }} <strong>Dpto.:</strong> {{ $direccion->departamento }}</li>
                        <li><strong>Entre calles:</strong> <span>{{ $direccion->entrecalles }}</span></li>
                        <li><strong>Localidad:</strong> <span>{{ $direccion->localidad }}</span></li>
                        <li><strong>Provincia:</strong> <span>{{ $direccion->provincia }}</span></li>
                    </ul>

                    <div class="imagen">
                        @if($direccion->piso == "PB")
                            <img src="{{ asset('img/pdf/1018528.png') }}">
                        @else
                            <img src="{{ asset('img/pdf/1018550.png') }}">
                        @endif
                    </div>
                </div>

                <?php $primero = false; ?>
            @endforeach
        </div>
    </div>

    <div class="conceptos">
        <table>
            <thead>
            <tr>
                <th>CONCEPTO</th>
                <th>IMPORTE</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>{{ strtoupper($presupuesto->servicio()->nombre) }}</td>
                <td>
                    {{--@if($presupuesto->servicio()->id == 3 || $presupuesto->servicio()->id == 4)--}}
                    {{--${{ $presupuesto->vehiculo()->valor }}--}}
                    {{--@else--}}
                    {{--{!! $presupuesto->cotizacion !!}--}}
                    {{--@endif--}}

                    ${!! number_format($presupuesto->valor_aproximado, 0, ",", ".") !!}
                </td>
            </tr>
            @foreach($presupuesto->adicionales() as $adicional)
                <tr>
                    <td>{{ strtoupper($adicional->nombre) }}</td>
                    <td>{{ ($adicional->valor == 0) ? "Incluído" : "$".number_format($adicional->valor, 0, ",", ".") }}</td>
                </tr>
            @endforeach

            @foreach($presupuesto->otros_adicionales() as $otroAdicional)
                <tr>
                    <td>{{ strtoupper($otroAdicional->nombre) }}</td>
                    <td>${{ number_format($otroAdicional->valor, 0, ",", ".") }}</td>
                </tr>
            @endforeach
            <tr>
                <td>ASCENSO Y DESCENSO POR ESCALERA</td>
                <td>Bonificado</td>
            </tr>
            <tr>
                <td>DESARME Y ARMADO DE MUEBLES</td>
                <td>Bonificado</td>
            </tr>
            <tr>
                <td>DESINSTALACIÓN E INSTALACIÓN DE LUMINARIAS</td>
                <td>Bonificado</td>
            </tr>
            <tr>
                <td>KIT DE EMBALAJE</td>
                <td>Bonificado</td>
            </tr>
            <tr>
                <td>ASCENSO Y DESCENSO POR POLEAS/SOGAS</td>
                <td>Consultar presupuesto</td>
            </tr>
            <tr>
                <td>DESEMBALAJE PREMIUM</td>
                <td>Consultar presupuesto</td>
            </tr>
            <tr>
                <td>EMBALAJE PREMIUM</td>
                <td>Consultar presupuesto</td>
            </tr>
            {{--<tr>--}}
            {{--<td>OTROS</td>--}}
            {{--<td>$.-</td>--}}
            {{--</tr>--}}
            </tbody>
        </table>
    </div>

    <div class="visita">

        <div class="contenedor">

            @if($presupuesto->servicio()->id != 3 && $presupuesto->servicio()->id != 4)
                <div class="programada">
                    <h4>Visita Programada</h4>
                    <div class="horario">

                        @if(!empty($ultimaVisita))
                            <span>Fecha:
                                @if($ultimaVisita->fecha != '')
                                    {{ \Carbon\Carbon::parse($ultimaVisita->fecha)->format('d/m/Y') }}
                                @else
                                    A confirmar
                                @endif
                            </span>
                            <span>Franja Horaria:
                                @if($ultimaVisita->desde != '' && $ultimaVisita->hasta != '')
                                    {{ \Carbon\Carbon::parse($ultimaVisita->desde)->format('H:i') }} a {{ \Carbon\Carbon::parse($ultimaVisita->hasta)->format('H:i') }}
                                @else
                                    A confirmar
                                @endif
                            </span>
                        @else
                            <span>Fecha: A confirmar</span>
                            <span>Franja Horaria: A confirmar</span>
                        @endif
                    </div>
                </div>
            @endif

        </div>

        <div class="presupuesto">

            @if($presupuesto->impuesto != 0)
                <div class="contenedor">
                    <div class="nombre">
                        TOTAL SIN IMPUESTO
                    </div>

                    <div class="total">
                        ${{ number_format($presupuesto->total, 0, ",", ".") }}
                    </div>
                </div>

                <div class="contenedor">
                    <div class="nombre">
                        IMPUESTO {{ $presupuesto->impuesto }}%
                    </div>

                    <div class="total">
                        ${{ number_format(($presupuesto->impuesto * $presupuesto->total) / 100, 0, ",", ".") }}
                    </div>
                </div>

                <div class="contenedor">
                    <div class="nombre">
                        TOTAL CON IMPUESTO
                    </div>

                    <div class="total">
                        ${{ number_format($presupuesto->cotizacion, 0, ",", ".") }}
                    </div>
                </div>

                @if($presupuesto->pagado() > 0)
                    <div class="contenedor">
                        <div class="nombre">
                            ABONO
                        </div>

                        <div class="total">
                            ${{ number_format($presupuesto->pagado(), 0, ",", ".") }}
                        </div>
                    </div>

                    <div class="contenedor">
                        <div class="nombre">
                            SALDO
                        </div>

                        <div class="total">
                            ${{ number_format($presupuesto->faltaPagar(), 0, ",", ".") }}
                        </div>
                    </div>
                @endif
            @else
                <div class="contenedor">
                    <div class="nombre">
                        TOTAL
                    </div>

                    <div class="total">
                        ${{ number_format($presupuesto->cotizacion, 0, ",", ".") }}
                    </div>
                </div>

                @if($presupuesto->pagado() > 0)
                    <div class="contenedor">
                        <div class="nombre">
                            ABONO
                        </div>

                        <div class="total">
                            ${{ number_format($presupuesto->pagado(), 0, ",", ".") }}
                        </div>
                    </div>

                    <div class="contenedor">
                        <div class="nombre">
                            SALDO
                        </div>

                        <div class="total">
                            ${{ number_format($presupuesto->faltaPagar(), 0, ",", ".") }}
                        </div>
                    </div>
                @endif
            @endif
        </div>

    </div>

    <div class="alerta">
        @if($presupuesto->pagado() > 0)
            <p>El saldo podrá ser abonado en efectivo al finalizar el servicio o en caso de pago con transferencia bancaria 48hs antes de finalizar el servicio</p>
        @else
            <p>Los valores del siguiente presupuesto quedan sujetos a validación en el domicilio</p>
        @endif
    </div>

    <div class="bases">
        <h4>Bases y condiciones</h4>

        {!! $presupuesto->servicio()->basesycondiciones !!}
    </div>

</main>

<footer>


</footer>

</body>

</html>