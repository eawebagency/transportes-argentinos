<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        header {
            width: 100%;
            margin-bottom: 1rem;
        }

        header .logo {
            width: 70%;
            display: inline-block;
            vertical-align: middle;
        }

        header .presupuesto {
            width: 25%;
            display: inline-block;
            vertical-align: middle;
        }

        header .presupuesto h4 {
            font-weight: 800;
        }
    </style>
</head>
<body>

<header>
    <div class="logo">
        <img src="{{ asset('img/pdf/logotransportes.png') }}" style="max-width: 75%">
    </div>

    <div class="presupuesto">
        <h4>DETALLE DE VISITA #{{ $presupuesto->id }}</h4>
        <h4>{{ $presupuesto->cliente()->nombre }} {{ $presupuesto->cliente()->apellido }}</h4>
    </div>
</header>

</body>
</html>


