<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Resumen de Servicio</title>

    <style>
        @page { margin: 0; }

        body {
            width: 23cm;
            height: 29.7cm;
            margin: 0;
            font-size: 14px;
        }

        main .resumenDeServicio {
            width: 100%;
            border: 2px solid #3ec6f1;
            border-radius: 15px;
            margin-bottom: 1rem;
            padding: 0.3rem 0.7rem;
        }

        main .resumenDeServicio h3 {
            margin-top: 0;
            text-align: center;
            padding: 0.5rem;
            background: #3ec6f1;
            color: black;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
        }

        main .resumenDeServicio .columna1,
        main .resumenDeServicio .columna2 {
            width: 45%;
            display: inline-block;
            vertical-align: middle;
            padding: 0.5rem 0;
        }

        main .resumenDeServicio .columna1 ul,
        main .resumenDeServicio .columna2 ul {
            margin-bottom: 0;
            padding-left: 1rem;
        }

        main .resumenDeServicio .columna1 ul li,
        main .resumenDeServicio .columna2 ul li {
            margin-bottom: 0.5rem;
        }

        main .resumenDeServicio .columna1 {
            border-right: 1px solid #3ec6f1;
        }

        main .resumenDeServicio ul {
            list-style-type: none;
            margin-top: 0;
        }

        main .direcciones {
            margin-top: 1rem;
            width: 100%;
            position: relative;
            padding: 0.7rem;
            border: 2px solid #3ec6f1;
            border-radius: 10px;
            margin-bottom: 1rem;
        }

        main .direcciones .titulos {
            background: #3ec6f1;
            margin: -0.7rem -0.7rem 0 -0.7rem;
        }

        main .direcciones .titulos h3 {
            width: 49%;
            display: inline-block;
            text-align: center;
            margin-bottom: 0.2rem;
            margin-top: 0.2rem;
        }

        main .direcciones .cargas,
        main .direcciones .descargas {
            width: 48%;
            display: inline-block;
            vertical-align: middle;
        }

        main .direcciones .descargas {
            margin-left: 3%;
        }

        main .direcciones .cargas .direccion,
        main .direcciones .descargas .direccion {
            width: 100%;
        }

        main .direcciones .cargas .direccion ul,
        main .direcciones .descargas .direccion ul {
            list-style-type: none;
            padding-left: 0;
            width: 60%;
            display: inline-block;
            vertical-align: middle;
            margin-bottom: 0;
        }

        main .direcciones .cargas .direccion ul li,
        main .direcciones .descargas .direccion  ul li {
            margin-bottom: 0.3rem;
        }

        main .direcciones .cargas .direccion .imagen,
        main .direcciones .descargas .direccion .imagen {
            width: 30%;
            display: inline-block;
            vertical-align: middle;
            margin-left: 9%;
        }

        main .direcciones .cargas .direccion .imagen img,
        main .direcciones .descargas .direccion .imagen img {
            max-width: 100px;
        }

        main .direcciones .cargas .ascensor,
        main .direcciones .descargas .ascensor {
            width: 100%;
        }

        main .direcciones .cargas .ascensor .titulo,
        main .direcciones .descargas .ascensor .titulo {
            width: 34%;
            display: inline-block;
        }

        main .direcciones .cargas .ascensor .opciones,
        main .direcciones .descargas .ascensor .opciones {
            width: 65%;
            display: inline-block;
        }

        main .direcciones .cargas .ascensor .opciones span,
        main .direcciones .descargas .ascensor .opciones span {
            padding: 0.2rem 1rem;
            border: 1px solid #3ec6f1;
            border-radius: 30px;
            margin-right: 0.3rem;
        }

        main .direcciones .cargas .observaciones,
        main .direcciones .descargas .observaciones {
            width: 100%;
        }

        main .direcciones .cargas hr,
        main .direcciones .descargas hr {
            border-color: #3ec6f1;
        }

        main .direcciones .separador {
            position: absolute;
            top: 0;
            height: 100%;
            left: 50%;
            border-left: 2px solid #3ec6f1;
        }

        main .visita {
            page-break-after: always;
            width: 100%;
            height: auto;
            border: 1px solid #3ec6f1;
            border-radius: 15px;
            margin-bottom: 1rem;
            padding: 0.7rem;
        }

        main .visita textarea {
            background-image: linear-gradient(transparent, transparent 24px, #E7EFF8 0px);
            background-size: 100% 25px;
            border:none;
            height:670px;
            width: 100%;
            overflow:hidden;
            line-height:25px;
            resize: none;
        }

        main .mudanzas {
            width: 100%;
            margin-bottom: 1rem;
            padding: 0.7rem;
        }

        main .mudanzas table {
            border-spacing: 0;
            width: 100%;
            border-collapse: collapse;
            table-layout: fixed;
        }

        main .mudanzas table thead {
            background: #3ec6f1;
        }

        main .mudanzas table thead tr {
            color: black;
            font-weight: 800;
        }

        main .mudanzas table thead tr th {
            border: none;
            padding: 0.3rem 0.8rem;
            text-align: center;
            font-size: 0.8rem;
        }

        main .mudanzas table thead tr th:first-child {
            border-top-left-radius: 15px;
        }

        main .mudanzas table thead tr th:last-child {
            border-top-right-radius: 15px;
        }

        main .mudanzas table tbody {

        }

        main .mudanzas table tbody tr {
            color: black;
        }

        main .mudanzas table tbody tr td {
            padding: 0.5rem 1rem;
            border: 2px solid #3ec6f1;
        }

        main .mudanzas table tbody tr td:first-child {
            font-weight: 800;
        }

        main .fechas {
            width: 100%;
            margin-bottom: 1rem;
            padding: 0 1rem;
        }

        main .fechas hr {
            border: 1px dashed black;
        }

        main .fechas h4 {
            font-weight: 300;
        }

        main .fechas .fila {
            width: 100%;
        }

        main .fechas .fila h4 {
            display: inline-block;
            vertical-align: middle;
        }

        main .fechas .columna1 {
            width: 30%;
            display: inline-block;
        }

        main .fechas .columna2 {
            width: 69%;
            display: inline-block;
        }

        main .fechas .columna2 h4 {
            display: inline-block;
            margin-left: 0.5rem;
        }

        main .generales {
            width: 100%;
            margin-top: 1rem;
        }

        main .generales h2 {
            font-weight: 800;
        }

        main .generales .texto {
            border-radius: 15px;
            padding: 0 1rem;
        }

        main .generales .texto p {
            font-size: 1rem;
        }

    </style>

</head>

<body>

<main>

    <div class="resumenDeServicio">
        <div class="columna1">
            <ul>
                <li><strong>Presupuesto:</strong> #{{ $presupuesto->id }}</li>
                <li><strong>Cliente:</strong> {{ $presupuesto->cliente()->nombre }} {{ $presupuesto->cliente()->apellido }}</li>
                <li><strong>Teléfono:</strong> {{ $presupuesto->cliente()->telefono }}</li>
                <li><strong>Teléfono Alternativo:</strong> {{ $presupuesto->cliente()->telefono_alternativo }}</li>
                <li><strong>Mail:</strong> {{ $presupuesto->cliente()->email }}</li>
                {{--<li><strong>Fecha de Servicio:</strong>--}}
                {{--@if($presupuesto->fecha != '')--}}
                {{--{{ \Carbon\Carbon::parse($presupuesto->fecha)->format('d/m/Y') }}--}}
                {{--@else--}}
                {{--A confirmar--}}
                {{--@endif--}}
                {{--</li>--}}
                {{--<li><strong>Hora de Servicio:</strong>--}}
                {{--@if($presupuesto->hora != '' && $presupuesto->hora_fin != '')--}}
                {{--{{ \Carbon\Carbon::parse($presupuesto->hora)->format('H:i') }} - {{ \Carbon\Carbon::parse($presupuesto->hora_fin)->format('H:i') }}--}}
                {{--@else--}}
                {{--A confirmar--}}
                {{--@endif--}}
                {{--</li>--}}
            </ul>
        </div>

        <div class="columna2">
            <ul>
                @if(!empty($presupuesto->vehiculo()))
                    <li><strong>Vehiculo cotizado:</strong> {{ $presupuesto->vehiculo()->patente }}</li>
                @endif
                <li><strong>Operarios:</strong> {{ $presupuesto->personal }}</li>
                @if($presupuesto->larga_distancia == 1)
                    <li><strong>KM Cotizados:</strong> {{ $presupuesto->kms }}</li>
                @endif
                <li><strong>Cotización Total:</strong> {{ $presupuesto->cotizacion }}</li>
                <li><strong>Fecha y hora de mudanza:</strong> @if($presupuesto->fecha != '') {{ \Carbon\Carbon::parse($presupuesto->fecha)->format('d/m/Y') }} - {{ \Carbon\Carbon::parse($presupuesto->hora)->format('H:i') }} hs @endif </li>
                {{--<li><strong>Tipo de Servicio:</strong> {{ $presupuesto->servicio()->nombre }}</li>--}}
                {{--<li><strong>Horas de servicio estimado:</strong> {{ $presupuesto->visita()->detalle()->horas }}</li>--}}
            </ul>
        </div>
    </div>

    <div class="direcciones">

        <div class="titulos">
            <h3>DATOS DE ORIGEN</h3>
            <h3>DATOS DE DESTINO</h3>
        </div>

        <div class="cargas">

            <?php $primera = true; ?>

            @foreach($presupuesto->cargas() as $direccion)

                @if(!$primera)
                    <hr>
                @endif

                <div class="direccion">
                    <ul>
                        <li><strong>Dirección:</strong> <span>{{ $direccion->direccion }}</span>
                        <li><strong>Piso:</strong> {{ $direccion->piso }} <strong>Dpto.:</strong> {{ $direccion->departamento }}</li>
                        <li><strong>Entre calles:</strong> <span>{{ $direccion->entrecalles }}</span></li>
                        <li><strong>Localidad:</strong> <span>{{ $direccion->localidad }}</span></li>
                        <li><strong>Provincia:</strong> <span>{{ $direccion->provincia }}</span></li>
                        <li><strong>¿Tiene recorrido?</strong></li>
                        <li><strong>¿Tiene restricción?</strong></li>

                    </ul>

                    <div class="imagen">
                        @if($direccion->piso == "PB")
                            <img src="{{ asset('img/pdf/1018528.png') }}">
                        @else
                            <img src="{{ asset('img/pdf/1018550.png') }}">
                        @endif
                    </div>
                </div>

                <div class="ascensor">
                    <div class="titulo">
                        <strong>¿Ascensor?</strong>
                    </div>
                    <div class="opciones">
                        <span>Chico</span>
                        <span>Mediano</span>
                        <span>Grande</span>
                    </div>
                </div>

                @if($direccion->observaciones != '')
                    <div class="observaciones">
                        {!! $direccion->observaciones !!}
                    </div>
                @endif

                <?php $primero = false; ?>
            @endforeach
        </div>

        <div class="separador"></div>

        <div class="descargas">

            <?php $primera = true; ?>

            @foreach($presupuesto->descargas() as $direccion)

                @if(!$primera)
                    <hr>
                @endif

                <div class="direccion">
                    <ul>
                        <li><strong>Dirección:</strong> <span>{{ $direccion->direccion }}</span>
                        <li><strong>Piso:</strong> {{ $direccion->piso }} <strong>Dpto.:</strong> {{ $direccion->departamento }}</li>
                        <li><strong>Entre calles:</strong> <span>{{ $direccion->entrecalles }}</span></li>
                        <li><strong>Localidad:</strong> <span>{{ $direccion->localidad }}</span></li>
                        <li><strong>Provincia:</strong> <span>{{ $direccion->provincia }}</span></li>
                        <li><strong>¿Tiene recorrido?</strong></li>
                        <li><strong>¿Tiene restricción?</strong></li>
                    </ul>

                    <div class="imagen">
                        @if($direccion->piso == "PB")
                            <img src="{{ asset('img/pdf/1018528.png') }}">
                        @else
                            <img src="{{ asset('img/pdf/1018550.png') }}">
                        @endif
                    </div>

                    <div class="ascensor">
                        <div class="titulo">
                            <strong>¿Ascensor?</strong>
                        </div>
                        <div class="opciones">
                            <span>Chico</span>
                            <span>Mediano</span>
                            <span>Grande</span>
                        </div>
                    </div>

                    @if($direccion->observaciones != '')
                        <div class="observaciones">
                            {!! $direccion->observaciones !!}
                        </div>
                    @endif
                </div>

                <?php $primero = false; ?>
            @endforeach
        </div>
    </div>

    <div class="visita">

        <textarea>
CAJAS:                                                   ROPEROS:
ILUMINARIAS:                                             VENT. DE TECHO:                                     AA:


DESARME Y ARMADO DE MUEBLES:






OBJETOS DELICADOS Y/O RELEVANTES:






OBSERVACIONES:







        </textarea>

    </div>

    <div class="mudanzas">
        <table class="table">
            <thead>
            <tr>
                <th></th>
                <th>VEHÍCULO</th>
                <th>HORAS DE SERVICIO</th>
                <th>PERSONAL AFECTADO</th>
                <th>PRESUPUESTO</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>Mudanza</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Embalaje Premium</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Desembalaje Premium</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="detalle">
        <h4><strong>Detalle del presupuesto</strong></h4>
        <ul>
            <li><strong>Vehículo:</strong> <span>{{ $presupuesto->vehiculo()->patente }}</span></li>
            <li><strong>Operarios:</strong> <span>{{ $presupuesto->personal }}</span></li>
        </ul>

        <ul>
            @foreach($presupuesto->adicionales() as $adicional)
                <li><strong>{{ strtoupper($adicional->nombre) }}:</strong> <span>${{ number_format($adicional->valor, 0, ",", ".") }}</span></li>
            @endforeach
        </ul>

        <ul>
            @foreach($presupuesto->otros_adicionales() as $otroAdicional)
                <li><strong>{{ strtoupper($otroAdicional->nombre) }}:</strong> <span>${{ number_format($otroAdicional->valor, 0, ",", ".") }}</span></li>
            @endforeach
        </ul>

        <ul>
            @if($presupuesto->impuesto != 0)
                <li><strong>TOTAL SIN IMPUESTO:</strong> <span>${{ number_format($presupuesto->total, 0, ",", ".") }}</span></li>
                <li><strong>IMPUESTO {{ $presupuesto->impuesto }}%:</strong> <span>${{ number_format(($presupuesto->impuesto * $presupuesto->total) / 100, 0, ",", ".") }}</span></li>
                <li><strong>TOTAL CON IMPUESTO:</strong> <span>${{ number_format($presupuesto->cotizacion, 0, ",", ".") }}</span></li>
                @if($presupuesto->pagado() > 0)
                    <li><strong>ABONO:</strong> <span>${{ number_format($presupuesto->pagado(), 0, ",", ".") }}</span></li>
                    <li><strong>SALDO:</strong> <span>${{ number_format($presupuesto->faltaPagar(), 0, ",", ".") }}</span></li>
                @endif

            @else

                <li><strong>TOTAL:</strong> <span>${{ number_format($presupuesto->cotizacion, 0, ",", ".") }}</span></li>
                @if($presupuesto->pagado() > 0)
                    <li><strong>ABONO:</strong> <span>${{ number_format($presupuesto->pagado(), 0, ",", ".") }}</span></li>
                    <li><strong>SALDO:</strong> <span>${{ number_format($presupuesto->faltaPagar(), 0, ",", ".") }}</span></li>
                @endif
            @endif
        </ul>

    </div>

    <div class="fechas">

        <hr>

        <div class="fila">
            <h4><strong>Fecha de la visita:</strong> {{ \Carbon\Carbon::parse($presupuesto->ultimaVisitaPorEstado($estado->id)->fecha)->format('d/m/Y') }}</h4>
            <h4><strong>Franja Horaria de la Visita:</strong> {{ \Carbon\Carbon::parse($presupuesto->ultimaVisitaPorEstado($estado->id)->desde)->format('H:i') }} - {{ \Carbon\Carbon::parse($presupuesto->ultimaVisitaPorEstado($estado->id)->hasta)->format('H:i') }}</h4>
        </div>

        <hr>

        <div class="fila">
            <h4><strong>Estado:</strong> {{ $estado->nombre }}</h4>
        </div>

        <hr>

        <div class="fila">
            <h4><strong>Observaciones de la Visita:</strong></h4>
        </div>
    </div>

    <div class="generales">
        {{--<h2>Observaciones generales</h2>--}}

        <div class="texto">
            {!! $presupuesto->ultimaVisitaPorEstado($estado->id)->observaciones !!}
        </div>
    </div>

</main>

<footer>

</footer>
</body>

</html>