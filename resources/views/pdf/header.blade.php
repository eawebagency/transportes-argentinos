<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        header {
            width: 100%;
            margin-bottom: 1rem;
        }

        header .logo {
            width: 75%;
            display: inline-block;
            vertical-align: middle;
        }

        header .presupuesto {
            width: 20%;
            display: inline-block;
            vertical-align: middle;
        }

        /*header hr {*/
            /*border: 5px solid #ddd;*/
            /*width: 50%;*/
            /*margin-left: 25%;*/
        /*}*/
    </style>
</head>
<body>

<header>
    <div class="logo">
        <img src="{{ asset('img/pdf/logotransportes.png') }}" style="max-width: 75%">
    </div>

    <div class="presupuesto">
        <h5>Presupuesto: {{$id }}</h5>
    </div>

    {{--<hr>--}}
</header>
</body>
</html>


