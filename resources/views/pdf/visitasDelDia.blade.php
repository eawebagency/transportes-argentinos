<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Visitas del día</title>

    {{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">--}}

    <style>
        @page { margin: 0; }

        div {
            page-break-inside: avoid;
        }

        body {
            height: 20cm;
            margin: 0;
            font-family: 'Open Sans', sans-serif;
            font-size: 14px;
        }

        .imagen,
        .titulo {
            width: 100%;
            margin-bottom: 1rem;
        }

        .imagen img {
            max-height: 150px;
        }

        table { page-break-inside:auto }
        tr    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
    
        table {
            table-layout: fixed;
            width: 100%;
        }

        table thead tr th {
            font-size: 0.8rem;
            font-weight: 800;
            border: 1px solid #ddd;
            padding: 0 0.5rem;
        }

        table thead tr th:last-child,
        table tbody tr td:last-child {
            word-break: break-all;
        }

        table tbody tr td {
            font-size: 0.8rem;
            text-align: center;
            border-bottom: 1px solid #ddd;
            border-right: 1px solid #ddd;
            padding: 0.7rem 0;
        }

    </style>

</head>

<body>

    <div class="imagen">
        <img src="{{ asset('img/logos/logo.png') }}">
    </div>
    <div class="titulo">
        <h1><strong>Planilla de Visitas del {{ \Carbon\Carbon::parse($fecha)->format('d/m/Y') }}</strong></h1>
    </div>
    <table>
        <thead>
            <tr>
                <th width="5%">N°</th>
                <th>Operador</th>
                <th>Localidad</th>
                <th>Horario</th>
                <th width="6%">N° de cliente</th>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Dirección</th>
                <th>Entre calles</th>
                <th width="30%">Observaciones y fecha de servicio</th>
                <th>Cotización total </th>

            </tr>
        </thead>

        <tbody>
            @foreach($visitas as $iVisita => $visita)
                <tr>
                    <td>
                        {{ ($iVisita + 1) }}
                    </td>
                    <td>
                        @if(!empty($visita->presupuesto()->vendedor()))
                            {{ $visita->presupuesto()->vendedor()->nombre.' '.$visita->presupuesto()->vendedor()->apellido }}
                        @endif
                    </td>
                    <td>{{ $visita->presupuesto()->cargas()[0]->localidad }}</td>
                    @if($visita->desde != '' && $visita->hasta != '')
                        <td>{{ \Carbon\Carbon::parse($visita->desde)->format('H:i').' - '.\Carbon\Carbon::parse($visita->hasta)->format('H:i') }}</td>
                    @else
                        <td>A confirmar</td>
                    @endif
                    <td>{{ $visita->presupuesto()->id }}</td>
                    <td>{{ $visita->presupuesto()->cliente()->nombre }}</td>
                    <td>{{ $visita->presupuesto()->cliente()->telefono }}</td>
                    <td>{{ $visita->presupuesto()->cargas()[0]->direccion }} - Departamento: {{ $visita->presupuesto()->cargas()[0]->departamento }} - Piso: {{ $visita->presupuesto()->cargas()[0]->piso }}</td>
                    <td>{{ $visita->presupuesto()->cargas()[0]->entrecalles }}</td>
                    <td>
                        <p>
                            <strong>Estado:</strong> {{ $visita->estado()->nombre }}
                        </p>
                        <p>
                            <strong>Fecha de servicio:</strong>
                            @if($visita->presupuesto()->fecha)
                                {{ \Carbon\Carbon::parse($visita->presupuesto()->fecha)->format('d/m/Y') }}
                            @else
                                A confirmar
                            @endif
                        </p>

                        {!! $visita->observaciones !!}
                    </td>
                    <td>{{ '$'.$visita->presupuesto()->cotizacion }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>