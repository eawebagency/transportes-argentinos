<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Resumen de Servicio</title>

    <style>
        @page { margin: 0; }

        /*div {*/
        /*page-break-inside: avoid;*/
        /*}*/

        .visita .adicionales .elemento,
        .visita .observaciones .elemento {
            page-break-inside: avoid;
        }

        body {
            width: 23cm;
            height: 29.7cm;
            margin: 0;
            font-size: 14px;
        }

        header {
            width: 100%;
            margin-bottom: 1rem;
        }

        header .logo {
            width: 75%;
            display: inline-block;
            vertical-align: middle;
        }

        header .presupuesto {
            width: 20%;
            display: inline-block;
            vertical-align: middle;
        }

        header hr {
            border: 5px solid #ddd;
            width: 50%;
            margin-left: 25%;
        }

        main .resumenDeServicio {
            width: 100%;
            border: 2px solid #3ec6f1;
            border-radius: 15px;
            margin-bottom: 1rem;
        }

        main .resumenDeServicio h3 {
            margin-top: 0;
            text-align: center;
            padding: 0.5rem;
            background: #3ec6f1;
            color: black;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
        }

        main .resumenDeServicio .columna1,
        main .resumenDeServicio .columna2,
        main .resumenDeServicio .columna3 {
            width: 32%;
            display: inline-block;
            vertical-align: middle;
        }

        main .resumenDeServicio ul {
            list-style-type: none;
            margin-top: 0;
        }

        main .direcciones {
            width: 100%;
            position: relative;
            margin-bottom: 1rem;
        }

        main .direcciones .desde,
        main .direcciones .hasta {
            width: 40%;
            display: inline-block;
            vertical-align: top;
        }

        main .direcciones .desde {
            margin-right: 8%;
        }

        main .direcciones .desde .direccion,
        main .direcciones .hasta .direccion {
            width: 100%;
        }

        main .direcciones .desde .direccion .imagen,
        main .direcciones .hasta .direccion .imagen {
            width: 26%;
            display: inline-block;
            vertical-align: top;
            margin-right: 7%;
        }

        main .direcciones .desde .direccion .imagen img,
        main .direcciones .hasta .direccion .imagen img {
            height: 100px;
        }

        main .direcciones .desde .direccion .informacion,
        main .direcciones .hasta .direccion .informacion {
            width: 65%;
            display: inline-block;
            vertical-align: top;
        }

        main .direcciones .desde .direccion .informacion h4,
        main .direcciones .hasta .direccion .informacion h4 {
            font-weight: 800;
        }

        main .direcciones .desde .direccion .informacion ul,
        main .direcciones .hasta .direccion .informacion ul {
            list-style-type: none;
            padding-left: 0;
            font-weight: 600;
        }

        main .direcciones .desde .direccion .informacion hr,
        main .direcciones .hasta .direccion .informacion hr {
            width: 100%;
            border-color: #3ec6f1;
        }

        main .direcciones .hasta .direccion .observaciones {
            font-weight: 400;
            width: 100%;
        }

        main .direcciones .separador {
            border-left: 2px solid #3ec6f1;
            height: 100%;
            width: 1%;
            left: 50%;
            top: 0;
            position: absolute;
        }

        main .direcciones .hasta {
            margin-left: 8%;
        }

        main .visita {
            width: 100%;
            /*border: 2px solid #3ec6f1;*/
            border-radius: 15px;
            margin-bottom: 1rem;
        }

        main .visita h3 {
            margin-top: 0;
            text-align: center;
            padding: 0.5rem;
            background: #3ec6f1;
            color: white;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
        }

        main .visita h3 .linea {
            padding-left: 1rem;
            padding-right: 1rem;
        }

        main .visita .adicionales,
        main .visita .observaciones {
            width: 100%;
            padding: 0.5rem 1rem;
        }

        main .visita .adicionales .elemento,
        main .visita .observaciones .elemento {
            width: 100%;
            margin-bottom: 0.5rem;
        }

        main .visita .adicionales .elemento .nombre,
        main .visita .observaciones .elemento .nombre {
            width: 35%;
            display: inline-block;
            vertical-align: middle;
            text-align: center;
        }

        main .visita .adicionales .elemento .nombre h4,
        main .visita .observaciones .elemento .nombre h4 {
            font-weight: 600;
            margin-top: 0;
        }

        main .visita .adicionales .elemento .nombre img,
        main .visita .observaciones .elemento .nombre img {
            max-height: 100px;
        }

        main .visita .adicionales .elemento .descripcion,
        main .visita .observaciones .elemento .descripcion {
            width: 60%;
            display: inline-block;
            vertical-align: middle;
        }

        main .visita .adicionales .elemento .descripcion hr,
        main .visita .observaciones .elemento .descripcion hr {
            border-color: #3ec6f1;
        }

        main .visita .adicionales .elemento .observaciones hr,
        main .visita .observaciones .elemento .observaciones hr {
            width: 100%;
            border-color: #ddd;
        }

        main .generales {
            width: 100%;
            margin-top: 1rem;
        }

        main .generales h2 {
            font-weight: 800;
        }

        main .generales .texto {
            border-radius: 15px;
            padding: 0 1rem;
        }

        main .generales .texto p {
            font-size: 1rem;
        }

    </style>

</head>

<body>

{{--<header>--}}
    {{--<div class="logo">--}}
        {{--<img src="{{ asset('img/pdf/logotransportes.png') }}" style="max-width: 75%">--}}
    {{--</div>--}}

    {{--<div class="presupuesto">--}}
        {{--<h3>Presupuesto: {{ $presupuesto->id }}</h3>--}}
    {{--</div>--}}

    {{--<hr>--}}
{{--</header>--}}

<main>

    <div class="resumenDeServicio">
        <h3>RESUMEN DE SERVICIO</h3>

        <div class="columna1">
            <ul>
                <li>Fecha de Servicio:
                    @if($presupuesto->fecha != '')
                        {{ \Carbon\Carbon::parse($presupuesto->fecha)->format('d/m/Y') }}
                    @else
                        A confirmar
                    @endif
                </li>
                <li>Hora de Servicio:
                    @if($presupuesto->hora != '' && $presupuesto->hora_fin != '')
                        {{ \Carbon\Carbon::parse($presupuesto->hora)->format('H:i') }} - {{ \Carbon\Carbon::parse($presupuesto->hora_fin)->format('H:i') }}
                    @else
                        A confirmar
                    @endif
                </li>
                @if(!empty($presupuesto->vehiculo()))
                    <li>Vehiculo: {{ $presupuesto->vehiculo()->patente }}</li>
                @endif
                <li>Operarios: {{ $presupuesto->personal }}</li>
            </ul>
        </div>

        <div class="columna2">
            <ul>
                <li>Tipo de Servicio: {{ $presupuesto->servicio()->nombre }}</li>
                <li>Sr./Sra.: {{ $presupuesto->cliente()->nombre }} {{ $presupuesto->cliente()->apellido }}</li>
                <li>Teléfono: {{ $presupuesto->cliente()->telefono }}</li>

            </ul>
        </div>

        <div class="columna3">
            <ul>
                <li>Teléfono Alternativo: {{ $presupuesto->cliente()->telefono_alternativo }}</li>
                <li>Horas de servicio estimado: {{ $presupuesto->visita()->detalle()->horas }}</li>
                @if($presupuesto->larga_distancia == 1)
                    <li>Kilómetros a recorrer: {{ $presupuesto->kms }}</li>
                @endif
            </ul>
        </div>
    </div>

    <div class="direcciones">
        <div class="desde">
            @foreach($presupuesto->cargas() as $direccion)
                <div class="direccion">
                    <div class="imagen">
                        @if($direccion->piso == "PB")
                            <img src="{{ asset('img/pdf/1018528.png') }}">
                        @else
                            <img src="{{ asset('img/pdf/1018550.png') }}">
                        @endif
                    </div>

                    <div class="informacion">
                        <h4>DESDE</h4>
                        <ul>
                            <li>{{ $direccion->direccion }}</li>
                            <li>{{ $direccion->entrecalles }}</li>
                            <li>{{ $direccion->localidad }}</li>
                            <li>{{ $direccion->provincia }}</li>
                            <li>Piso: {{ $direccion->piso }}</li>
                            <li>Departamento: {{ $direccion->departamento }}</li>
                        </ul>

                        <hr>
                    </div>

                    <div class="observaciones">
                        {!! $direccion->observaciones !!}
                    </div>
                </div>
            @endforeach
        </div>

        <div class="separador"></div>

        <div class="hasta">
            @foreach($presupuesto->descargas() as $direccion)
                <div class="direccion">
                    <div class="imagen">
                        @if($direccion->piso == "PB")
                            <img src="{{ asset('img/pdf/1018528.png') }}">
                        @else
                            <img src="{{ asset('img/pdf/1018550.png') }}">
                        @endif
                    </div>

                    <div class="informacion">
                        <h4>HASTA</h4>
                        <ul>
                            <li>{{ $direccion->direccion }}</li>
                            <li>{{ $direccion->entrecalles }}</li>
                            <li>{{ $direccion->localidad }}</li>
                            <li>{{ $direccion->provincia }}</li>
                            <li>Piso: {{ $direccion->piso }}</li>
                            <li>Departamento: {{ $direccion->departamento }}</li>
                        </ul>
                    </div>

                    <div class="observaciones">
                        {!! $direccion->observaciones !!}
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="visita">
        <h3>
                <span class="visitador">
                    Visitado por:
                    @if(!empty($presupuesto->visita()->visitador()))
                        {{ $presupuesto->visita()->visitador()->nombre }} {{ $presupuesto->visita()->visitador()->apellido }}
                    @endif
                </span>
            <span class="linea">|</span>
            <span class="confirmado">Visita confirmada por: {{ $presupuesto->vendedor()->nombre }} {{ $presupuesto->vendedor()->apellido }}</span>
        </h3>

        <div class="adicionales">
            @foreach($presupuesto->otros_adicionales() as $otroAdicional)
                <div class="elemento">
                    <div class="nombre">
                        @if(file_exists('img/pdf/' . \App\Helpers\ImagenPdf::generarNombre($otroAdicional->nombre) . '.png'))
                            <img src="{{ asset('img/pdf/'.\App\Helpers\ImagenPdf::generarNombre($otroAdicional->nombre).'.png') }}">
                        @endif
                        <h4>{{ $otroAdicional->nombre }}</h4>
                    </div>

                    <div class="descripcion">
                        {!! $otroAdicional->observaciones !!}
                        <br>
                        Operarios: {{ $otroAdicional->personal }} <br>
                        Duración: {{ $otroAdicional->duracion }}
                        @if($otroAdicional->fecha != '')
                            Fecha: {{ \Carbon\Carbon::parse($otroAdicional->fecha)->format('d/m/Y') }} <br>
                            Hora:
                            @if($otroAdicional->hora_inicio != '' && $otroAdicional->hora_fin != '')
                                {{ \Carbon\Carbon::parse($otroAdicional->hora_inicio)->format('H:i') }} - {{ \Carbon\Carbon::parse($otroAdicional->hora_fin)->format('H:i') }}
                            @else
                                A confirmar
                            @endif
                        @endif
                        <hr>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="observaciones">
            @foreach($presupuesto->visita()->observaciones() as $observacion)
                <div class="elemento">
                    <div class="nombre">
                        @if(file_exists('img/pdf/' . \App\Helpers\ImagenPdf::generarNombre($observacion->nombre) . '.png'))
                            <img src="{{ asset('img/pdf/'.\App\Helpers\ImagenPdf::generarNombre($observacion->nombre).'.png') }}">
                        @endif
                        <h4>{{ $observacion->nombre }}</h4>
                    </div>

                    <div class="descripcion">
                        {!! $observacion->descripcion !!}
                        <hr>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="generales">
        <h2>Observaciones generales</h2>

        <div class="texto">
            {!! $presupuesto->observaciones !!}
        </div>
    </div>

</main>

<footer>

</footer>
</body>

</html>