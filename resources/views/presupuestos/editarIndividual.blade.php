@extends('master', ['seccionActiva' => 'Presupuestos'])

@section('titulo', 'Editar Presupuesto - '.$elemento->cliente()->nombre.' '.$elemento->cliente()->apellido. ' - Nro Presupuesto: '.$elemento->id)

@section('contenido')

    <style>
        .m-body .m-content {
            position: relative;
        }

        .lds-ring {
            display: inline-block;
            position: relative;
            width: 64px;
            height: 64px;
            position: fixed;
            left: 50%;
            top: 50%;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 51px;
            height: 51px;
            margin: 6px;
            border: 6px solid #1fb7ff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #1fb7ff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        #loaderGif {
            position: absolute;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.3);
            z-index: 1;
            left: 0;
            display: none;
        }
    </style>

    <div id="loaderGif">
        <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
    </div>

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    {{ Form::open(['url' => 'presupuestos/editar/'.$elemento->id, 'class' => 'form', 'files' => true, 'id' => 'formularioPresupuesto']) }}

    {{ Form::hidden('valor_final', $elemento->cotizacion) }}
    {{ Form::hidden('cantidad_direcciones_carga', count($elemento->cargas())) }}
    {{ Form::hidden('cantidad_direcciones_descarga', count($elemento->descargas())) }}

    <div class="row">

        <div class="col-12 mb-3">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-4 text-center">

                    <a href="{{ url('pdf/ver/presupuesto/'.$elemento->id) }}" class="btn m-btn--hover-info m-btn--pill" title="Ver presupuesto" target="_blank">
                        <i class="la la-eye"></i> Ver presupuesto
                    </a>

                    <a href="{{ url('pdf/enviar/presupuesto/'.$elemento->id) }}" class="btn m-btn--hover-info m-btn--pill">
                        <i class="la la-send-o"></i> Enviar presupuesto
                    </a>

                </div>

                <div class="col-12 col-sm-4 text-center">

                    <a href="{{ url('pdf/ver/resumenDeServicio/'.$elemento->id) }}" class="btn m-btn--hover-info m-btn--pill" title="Ver resumen de servicio" target="_blank">
                        <i class="la la-eye"></i> Ver Detalle de visita
                    </a>

                    <a href="{{ url('pdf/enviar/detalleDeVisita/'.$elemento->id) }}" class="btn m-btn--hover-info m-btn--pill">
                        <i class="la la-send-o"></i> Enviar Detalle de visita
                    </a>
                </div>

                <div class="col-12 col-sm-4 text-center">
                    <a href="{{ url('pdf/ver/resumenDeServicioConObservaciones/'.$elemento->id) }}" class="btn m-btn--hover-info m-btn--pill" title="Ver resumen de servicio" target="_blank">
                        <i class="la la-eye"></i> Ver detalle de visita con observaciones
                    </a>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="m-portlet">

                <div class="m-portlet__body">
                    <div class="m-accordion m-accordion--default" role="tablist">
                        <div class="m-accordion__item" style="border: none">
                            <div class="m-accordion__item-head collapsed" role="tab" id="clienteCollapseLarga" data-toggle="collapse" href="#clienteCollapse" aria-expanded="false" style="cursor: pointer">
                                {{--<span class="m-accordion__item-title">Cliente</span>--}}
                                <h3 class="m-accordion__item-title">Cliente</h3>
                                <span class="m-accordion__item-mode"></span>
                            </div>

                            <div class="m-accordion__item-body collapse" id="clienteCollapse">
                                <div class="row mt-3">
                                    <div class="col-12 col-sm-3">
                                        <div class="form-group">
                                            {{ Form::label('cliente_nombre', 'Nombre') }}
                                            {{ Form::text('cliente_nombre', $elemento->cliente()->nombre, ['class' => 'form-control']) }}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-3">
                                        <div class="form-group">
                                            {{ Form::label('cliente_apellido', 'Apellido') }}
                                            {{ Form::text('cliente_apellido', $elemento->cliente()->apellido, ['class' => 'form-control']) }}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-3">
                                        <div class="form-group">
                                            {{ Form::label('cliente_email', 'Email') }}
                                            {{ Form::email('cliente_email', $elemento->cliente()->email, ['class' => 'form-control']) }}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-3">
                                        <div class="form-group">
                                            {{ Form::label('cliente_telefono', 'Telefono') }}
                                            {{ Form::text('cliente_telefono', $elemento->cliente()->telefono, ['class' => 'form-control input-telefono', 'required' => 'required']) }}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-3">
                                        <div class="form-group">
                                            {{ Form::label('cliente_wpp', 'Whatsapp?') }}
                                            {{ Form::select('cliente_wpp', ['1' => 'Si', '0' => 'No'], $elemento->cliente()->wpp, ['class' => 'form-control']) }}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-3">
                                        <div class="form-group">
                                            {{ Form::label('cliente_telefono_alternativo', 'Teléfono alternativo') }}
                                            {{ Form::text('cliente_telefono_alternativo', $elemento->cliente()->telefono_alternativo, ['class' => 'form-control input-telefono']) }}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-3">
                                        <div class="form-group">
                                            {{ Form::label('cliente_contacto', 'Contacto') }}
                                            {{ Form::select('cliente_contacto', $formas, $elemento->cliente()->contacto, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar forma de contacto']) }}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-3">
                                        <div class="form-group">
                                            {{ Form::label('cliente_estado', 'Estado') }}
                                            {{ Form::select('cliente_estado', $estadosCliente, $elemento->cliente()->estado, ['class' => 'form-control', 'placeholder' => 'Seleccionar estado']) }}
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            {{ Form::label('cliente_observaciones', 'Observaciones') }}
                                            {{ Form::textarea('cliente_observaciones', $elemento->cliente()->observaciones, ['class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__foot">
                    <div class="m-form__actions">

                    </div>
                </div>

            </div>
        </div>

        <div class="col-12">
            <div class="m-portlet">

                <div class="m-portlet__body">
                    <div class="m-accordion m-accordion--default" role="tablist">
                        <div class="m-accordion__item" style="border: none">
                            <div class="m-accordion__item-head collapsed mt-3" role="tab" id="agregarEstadoCollapseLarga" data-toggle="collapse" href="#agregarEstadoCollapse" aria-expanded="false" style="cursor: pointer">
                                <h3 class="m-accordion__item-title">Estado</h3>
                                <span class="m-accordion__item-mode"></span>
                            </div>

                            <div class="m-accordion__item-body collapse" id="agregarEstadoCollapse">
                                <div class="row mt-3">

                                    <div class="col-12">
                                        <div class="form-group">
                                            {{ Form::label('estado', 'Estado') }}
                                            {{ Form::select('estado', $estados, $elemento->estado, ['class' => 'form-control', 'required' => 'required']) }}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            {{ Form::label('fecha_estado', 'Fecha') }}
                                            {{--{{ Form::date('fecha_estado', $elemento->fecha_estado, ['class' => 'form-control']) }}--}}
                                            {{ Form::date('fecha_estado', null, ['class' => 'form-control']) }}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-3">
                                        <div class="form-group">
                                            {{ Form::label('estado_hora_desde', 'Desde') }}
                                            {{--{{ Form::text('estado_hora_desde', $elemento->hora_desde, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ej.: 10:30)' ]) }}--}}
                                            {{ Form::text('estado_hora_desde', null, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ej.: 10:30)' ]) }}
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-3">
                                        <div class="form-group">
                                            {{ Form::label('estado_hora_hasta', 'Hasta') }}
                                            {{--{{ Form::text('estado_hora_hasta', $elemento->hora_hasta, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ej.: 10:30)' ]) }}--}}
                                            {{ Form::text('estado_hora_hasta', null, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ej.: 10:30)' ]) }}
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            {{ Form::label('estado_observaciones', 'Observaciones') }}
                                            {{ Form::textarea('estado_observaciones', null, ['class' => 'form-control' ]) }}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <h3>Historial Estados</h3>

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Fecha de carga</th>
                                    <th>Operador</th>
                                    <th>Estado</th>
                                    <th>Fecha de agenda</th>
                                    <th>Hora de agenda</th>
                                    <th>Observaciones</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($elemento->historialEstados() as $historial)

                                <tr>
                                    <td>
                                        {{ \Carbon\Carbon::parse($historial->created_at)->format('d/m/Y') }}
                                    </td>
                                    <td>
                                        {{ $historial->usuario()->nombre }} {{ $historial->usuario()->apellido }}
                                    </td>
                                    <td>
                                        <span class="m-badge m-badge--brand m-badge--wide" style="background: #{{ $historial->estado()->color }}">
                                            {{ $historial->estado()->nombre }}
                                        </span>
                                    </td>
                                    <td>
                                        @if($historial->fecha != '')
                                            {{ \Carbon\Carbon::parse($historial->fecha)->format('d/m/Y')  }}
                                        @endif
                                    </td>
                                    <td>
                                        @if(!empty($historial->desde) && !empty($historial->hasta))
                                            {{ \Carbon\Carbon::parse($historial->desde)->format('H:i') }} - {{ \Carbon\Carbon::parse($historial->hasta)->format('H:i') }}
                                        @endif
                                    </td>
                                    <td>
                                        {!! $historial->observaciones !!}
                                    </td>
                                    <td>
                                        <a href="{{ url('presupuestos/'.$elemento->id.'/historiales/editar/'.$historial->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">
                                            <i class="la la-edit"></i>
                                        </a>
                                        <a href="{{ url('presupuestos/'.$elemento->id.'/historiales/borrar/'.$historial->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar el estado?')">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="m-portlet__foot">
                    <div class="m-form__actions">

                    </div>
                </div>

            </div>
        </div>

        {{--<div class="col-12 col-sm-6">--}}
            {{--<div class="m-portlet">--}}

                {{--<div class="m-portlet__body">--}}

                    {{--<div class="col-12">--}}
                        {{--<h3>Direcciones de carga</h3>--}}

                        {{--<div class="table-responsive">--}}
                            {{--<table class="table table-bordered">--}}
                                {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--<th>Dirección</th>--}}
                                    {{--<th>Acciones</th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody>--}}
                                {{--@foreach($elemento->cargas() as $carga)--}}

                                    {{--<tr>--}}
                                        {{--<td>--}}
                                            {{--{{ $carga->direccion }}--}}
                                        {{--</td>--}}
                                        {{--<td>--}}
                                            {{--<a href="{{ url('presupuestos/'.$elemento->id.'/direcciones/editar/'.$carga->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">--}}
                                                {{--<i class="la la-edit"></i>--}}
                                            {{--</a>--}}
                                            {{--<a href="{{ url('presupuestos/'.$elemento->id.'/direcciones/borrar/'.$carga->id) }}" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')">--}}
                                                {{--<i class="la la-trash"></i>--}}
                                            {{--</a>--}}
                                        {{--</td>--}}
                                    {{--</tr>--}}
                                {{--@endforeach--}}
                                {{--</tbody>--}}
                            {{--</table>--}}
                        {{--</div>--}}

                        {{--<div class="m-portlet__foot">--}}
                            {{--<div class="m-form__actions">--}}
                                {{--<a href="{{ url('presupuestos/'.$elemento->id.'/direcciones/crear?carga=1') }}">--}}
                                    {{--<button type="button" class="btn btn-primary">Agregar dirección de carga</button>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="col-12 col-sm-6">--}}
            {{--<div class="m-portlet">--}}

                {{--<div class="m-portlet__body">--}}

                    {{--<div class="col-12">--}}
                        {{--<h3>Direcciones de descarga</h3>--}}

                        {{--<div class="table-responsive">--}}
                            {{--<table class="table table-bordered">--}}
                                {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--<th>Dirección</th>--}}
                                    {{--<th>Acciones</th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody>--}}
                                {{--@foreach($elemento->descargas() as $descarga)--}}

                                    {{--<tr>--}}
                                        {{--<td>--}}
                                            {{--{{ $descarga->direccion }}--}}
                                        {{--</td>--}}
                                        {{--<td>--}}
                                            {{--<a href="{{ url('presupuestos/'.$elemento->id.'/direcciones/editar/'.$descarga->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">--}}
                                                {{--<i class="la la-edit"></i>--}}
                                            {{--</a>--}}
                                            {{--<a href="{{ url('presupuestos/'.$elemento->id.'/direcciones/borrar/'.$descarga->id) }}" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')">--}}
                                                {{--<i class="la la-trash"></i>--}}
                                            {{--</a>--}}
                                        {{--</td>--}}
                                    {{--</tr>--}}
                                {{--@endforeach--}}
                                {{--</tbody>--}}
                            {{--</table>--}}
                        {{--</div>--}}

                        {{--<div class="m-portlet__foot">--}}
                            {{--<div class="m-form__actions">--}}
                                {{--<a href="{{ url('presupuestos/'.$elemento->id.'/direcciones/crear?carga=0') }}">--}}
                                    {{--<button type="button" class="btn btn-primary">Agregar dirección de descarga</button>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="col-12 col-sm-6">
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <h3>Carga - <button type="button" class="btn btn-sm btn-accent" id="agregarDireccionDeCarga">Agregar dirección de carga</button></h3>

                    <div class="m-accordion m-accordion--default" id="direccionesDeCarga" role="tablist">

                        <?php $contadorCarga = 0; ?>

                        @foreach($elemento->cargas() as $carga)

                            <div class="m-accordion__item">
                                <div class="m-accordion__item-head collapsed" role="tab" id="direccionDeCargaLarga-{{ $contadorCarga }}" data-toggle="collapse" href="#direccionDeCarga-{{ $contadorCarga }}" aria-expanded="false">
                                    <span class="m-accordion__item-title">Dirección {{ $contadorCarga + 1 }}</span>
                                    <span class="m-accordion__item-mode"></span>
                                </div>
                                <div class="m-accordion__item-body collapse show" id="direccionDeCarga-{{ $contadorCarga }}" role="tabpanel" aria-labelledby="direccionDeCargaLarga-{{ $contadorCarga }}">
                                    <div class="m-accordion__item-content">
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="direccion_carga[]">Dirección</label>
                                                    <input type="text" class="form-control" name="direccion_carga[]" value="{{ $carga->direccion }}">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="piso_carga[]">Piso</label>
                                                    <input type="text" class="form-control" name="piso_carga[]" required="required" value="{{ $carga->piso }}">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="departamento_carga[]">Departamento</label>
                                                    <input type="text" class="form-control" name="departamento_carga[]" required="required" value="{{ $carga->departamento }}">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="entrecalles_carga[]">Entrecalles</label>
                                                    <input type="text" class="form-control" name="entrecalles_carga[]" value="{{ $carga->entrecalles }}">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="provincia_carga[]">Provincia</label>
                                                    <select name="provincia_carga[]" class="form-control" required="required">
                                                        <option value="">Selecciona una provincia</option>
                                                        <option value="Ciudad Autonoma de Buenos Aires" @if($carga->provincia == "Ciudad Autonoma de Buenos Aires") selected='selected' @endif>Ciudad Autonoma de Buenos Aires</option>
                                                        <option value="Buenos Aires" @if($carga->provincia == "Buenos Aires") selected='selected' @endif>Buenos Aires</option>
                                                        <option value="Catamarca" @if($carga->provincia == "Catamarca") selected='selected' @endif>Catamarca</option>
                                                        <option value="Chaco" @if($carga->provincia == "Chaco") selected='selected' @endif>Chaco</option>
                                                        <option value="Chubut" @if($carga->provincia == "Chubut") selected='selected' @endif>Chubut</option>
                                                        <option value="Cordoba" @if($carga->provincia == "Cordoba") selected='selected' @endif>Cordoba</option>
                                                        <option value="Corrientes" @if($carga->provincia == "Corrientes") selected='selected' @endif>Corrientes</option>
                                                        <option value="Entre Rios" @if($carga->provincia == "Entre Rios") selected='selected' @endif>Entre Rios</option>
                                                        <option value="Formosa" @if($carga->provincia == "Formosa") selected='selected' @endif>Formosa</option>
                                                        <option value="Jujuy" @if($carga->provincia == "Jujuy") selected='selected' @endif>Jujuy</option>
                                                        <option value="La Pampa" @if($carga->provincia == "La Pampa") selected='selected' @endif>La Pampa</option>
                                                        <option value="La Rioja" @if($carga->provincia == "La Rioja") selected='selected' @endif>La Rioja</option>
                                                        <option value="Mendoza" @if($carga->provincia == "Mendoza") selected='selected' @endif>Mendoza</option>
                                                        <option value="Misiones" @if($carga->provincia == "Misiones") selected='selected' @endif>Misiones</option>
                                                        <option value="Neuquen" @if($carga->provincia == "Neuquen") selected='selected' @endif>Neuquen</option>
                                                        <option value="Rio Negro" @if($carga->provincia == "Rio Negro") selected='selected' @endif>Rio Negro</option>
                                                        <option value="Salta" @if($carga->provincia == "Salta") selected='selected' @endif>Salta</option>
                                                        <option value="San Juan" @if($carga->provincia == "San Juan") selected='selected' @endif>San Juan</option>
                                                        <option value="San Luis" @if($carga->provincia == "San Luis") selected='selected' @endif>San Luis</option>
                                                        <option value="Santa Cruz" @if($carga->provincia == "Santa Cruz") selected='selected' @endif>Santa Cruz</option>
                                                        <option value="Santa Fe" @if($carga->provincia == "Santa Fe") selected='selected' @endif>Santa Fe</option>
                                                        <option value="Santiago del Estero" @if($carga->provincia == "Santiago del Estero") selected='selected' @endif>Santiago del Estero</option>
                                                        <option value="Tierra del Fuego" @if($carga->provincia == "Tierra del Fuego") selected='selected' @endif>Tierra del Fuego</option>
                                                        <option value="Tucuman" @if($carga->provincia == "Tucuman") selected='selected' @endif>Tucuman</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="localidad_carga[]">Localidad</label>
                                                    <select class="form-control" name="localidad_carga[]" required="required" localidad-seleccionada="{{ $carga->localidad }}"></select>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="observaciones_carga[]">Observaciones</label>
                                                    <textarea class="form-control" name="observaciones_carga[]">{!! $carga->observaciones !!}</textarea>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="float-left">
                                                            <button type="button" class="btn btn-outline-danger eliminarDireccion">REMOVER DIRECCION</button>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="float-right">
                                                            <button type="button" class="btn btn-outline-success verDireccionEnMapa">VER MAPA</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php $contadorCarga++; ?>

                            @endforeach

                    </div>

                </div>

            </div>

        </div>

        <div class="col-12 col-sm-6">
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <h3>Descarga - <button type="button" class="btn btn-sm btn-accent" id="agregarDireccionDeDescarga">Agregar dirección de descarga</button></h3>

                    <div class="m-accordion m-accordion--default" id="direccionesDeDescarga" role="tablist">

                        <?php $contadorDescarga = 0; ?>

                        @foreach($elemento->descargas() as $descarga)

                            <div class="m-accordion__item">
                                <div class="m-accordion__item-head collapsed" role="tab" id="direccionDeDescargaLarga-{{ $contadorDescarga }}" data-toggle="collapse" href="#direccionDeDescarga-{{ $contadorDescarga }}" aria-expanded="false">
                                    <span class="m-accordion__item-title">Dirección {{ $contadorDescarga + 1 }}</span>
                                    <span class="m-accordion__item-mode"></span>
                                </div>
                                <div class="m-accordion__item-body collapse show" id="direccionDeDescarga-{{ $contadorDescarga }}" role="tabpanel" aria-labelledby="direccionDeDescargaLarga-{{ $contadorDescarga }}">
                                    <div class="m-accordion__item-content">
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="direccion_descarga[]">Dirección</label>
                                                    <input type="text" class="form-control" name="direccion_descarga[]" value="{{ $descarga->direccion }}">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="piso_descarga[]">Piso</label>
                                                    <input type="text" class="form-control" name="piso_descarga[]" value="{{ $descarga->piso }}" required="required">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="departamento_descarga[]">Departamento</label>
                                                    <input type="text" class="form-control" name="departamento_descarga[]" value="{{ $descarga->departamento }}" required="required">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="entrecalles_descarga[]">Entrecalles</label>
                                                    <input type="text" class="form-control" name="entrecalles_descarga[]" value="{{ $descarga->entrecalles }}">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="provincia_descarga[]">Provincia</label>
                                                    <select name="provincia_descarga[]" class="form-control" required="required" >
                                                        <option value="">Selecciona una provincia</option>
                                                        <option value="Ciudad Autonoma de Buenos Aires" @if($descarga->provincia == "Ciudad Autonoma de Buenos Aires") selected='selected' @endif>Ciudad Autonoma de Buenos Aires</option>
                                                        <option value="Buenos Aires" @if($descarga->provincia == "Buenos Aires") selected='selected' @endif>Buenos Aires</option>
                                                        <option value="Catamarca" @if($descarga->provincia == "Catamarca") selected='selected' @endif>Catamarca</option>
                                                        <option value="Chaco" @if($descarga->provincia == "Chaco") selected='selected' @endif>Chaco</option>
                                                        <option value="Chubut" @if($descarga->provincia == "Chubut") selected='selected' @endif>Chubut</option>
                                                        <option value="Cordoba" @if($descarga->provincia == "Cordoba") selected='selected' @endif>Cordoba</option>
                                                        <option value="Corrientes" @if($descarga->provincia == "Corrientes") selected='selected' @endif>Corrientes</option>
                                                        <option value="Entre Rios" @if($descarga->provincia == "Entre Rios") selected='selected' @endif>Entre Rios</option>
                                                        <option value="Formosa" @if($descarga->provincia == "Formosa") selected='selected' @endif>Formosa</option>
                                                        <option value="Jujuy" @if($descarga->provincia == "Jujuy") selected='selected' @endif>Jujuy</option>
                                                        <option value="La Pampa" @if($descarga->provincia == "La Pampa") selected='selected' @endif>La Pampa</option>
                                                        <option value="La Rioja" @if($descarga->provincia == "La Rioja") selected='selected' @endif>La Rioja</option>
                                                        <option value="Mendoza" @if($descarga->provincia == "Mendoza") selected='selected' @endif>Mendoza</option>
                                                        <option value="Misiones" @if($descarga->provincia == "Misiones") selected='selected' @endif>Misiones</option>
                                                        <option value="Neuquen" @if($descarga->provincia == "Neuquen") selected='selected' @endif>Neuquen</option>
                                                        <option value="Rio Negro" @if($descarga->provincia == "Rio Negro") selected='selected' @endif>Rio Negro</option>
                                                        <option value="Salta" @if($descarga->provincia == "Salta") selected='selected' @endif>Salta</option>
                                                        <option value="San Juan" @if($descarga->provincia == "San Juan") selected='selected' @endif>San Juan</option>
                                                        <option value="San Luis" @if($descarga->provincia == "San Luis") selected='selected' @endif>San Luis</option>
                                                        <option value="Santa Cruz" @if($descarga->provincia == "Santa Cruz") selected='selected' @endif>Santa Cruz</option>
                                                        <option value="Santa Fe" @if($descarga->provincia == "Santa Fe") selected='selected' @endif>Santa Fe</option>
                                                        <option value="Santiago del Estero" @if($descarga->provincia == "Santiago del Estero") selected='selected' @endif>Santiago del Estero</option>
                                                        <option value="Tierra del Fuego" @if($descarga->provincia == "Tierra del Fuego") selected='selected' @endif>Tierra del Fuego</option>
                                                        <option value="Tucuman" @if($descarga->provincia == "Tucuman") selected='selected' @endif>Tucuman</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="localidad_descarga[]">Localidad</label>
                                                    <select class="form-control" name="localidad_descarga[]" localidad-seleccionada="{{ $descarga->localidad }}" required="required"></select>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="observaciones_descarga[]">Observaciones</label>
                                                    <textarea class="form-control" name="observaciones_descarga[]">{!! $descarga->observaciones !!}</textarea>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="float-left">
                                                            <button type="button" class="btn btn-outline-danger eliminarDireccion">REMOVER DIRECCION</button>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="float-right">
                                                            <button type="button" class="btn btn-outline-success verDireccionEnMapa">VER MAPA</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php $contadorDescarga++; ?>

                        @endforeach

                    </div>

                </div>

            </div>

        </div>

        <div class="col-12">
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab_presupuesto">
                                <i class="fa fa-pen-alt"></i> Presupuesto
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_visitas">
                                <i class="fa fa-luggage-cart"></i> Visitas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_pagos">
                                <i class="fa fa-dollar-sign"></i> Pagos
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_presupuesto" role="tabpanel">

                            <div class="row mt-5">
                                <div class="col-12">
                                    <h3>Detalles del presupuesto</h3>

                                    <div class="row justify-content-center align-items-center">

                                        <div class="col-12 col-sm-4">
                                            <div class="form-group">
                                                {{ Form::label('servicio', 'Servicio') }}
                                                {{ Form::select('servicio', $servicios, $elemento->servicio, ['class' => 'form-control', 'required' => 'required']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-4">
                                            <div class="form-group">
                                                {{ Form::label('vehiculo', 'Vehículo') }}
                                                {{ Form::select('vehiculo', $vehiculos, $elemento->vehiculo, ['class' => 'form-control', 'required' => 'required']) }}

                                                <small id="informacionDelVehiculo"></small>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-4">
                                            <div class="form-group">
                                                {{ Form::label('cotizacion', 'Cotización') }}
                                                {{ Form::text('cotizacion', $elemento->valor_aproximado, ['class' => 'form-control', 'required' => 'required']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-4">
                                            <div class="form-group">
                                                {{ Form::checkbox('larga_distancia', true, $elemento->larga_distancia, ['class' => 'form-check-input']) }} {{ Form::label('kms', 'Kms a recorrer') }}
                                                {{ Form::number('kms', $elemento->kms, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-4">
                                            <div class="form-group">
                                                {{ Form::label('ambientes', 'Ambientes') }}
                                                {{ Form::number('ambientes', $elemento->ambientes, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-4">
                                            <div class="form-group">
                                                {{ Form::label('personal', 'Cantidad de personal') }}
                                                {{ Form::number('personal', $elemento->personal, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                {{ Form::label('tipo_impuesto', 'Tipo de Impuesto') }}
                                                {{ Form::select('tipo_impuesto', [
                                                    0 => 'Ninguno',
                                                    21 => '21%',
                                                    'otro' => 'Otro'
                                                ], 0, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                {{ Form::label('otro_impuesto', 'Impuesto') }}
                                                {{ Form::number('otro_impuesto', null, ['class' => 'form-control', 'disabled' => 'disabled', 'required' => 'required']) }}
                                            </div>
                                        </div>

                                        {{ Form::hidden('impuesto', $elemento->impuesto) }}

                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                {{ Form::label('fecha', 'Fecha de Inicio') }}
                                                {{ Form::date('fecha', ($elemento->fecha != '') ? \Carbon\Carbon::parse($elemento->fecha)->format('Y-m-d') : null, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                {{ Form::label('hora', 'Hora de Inicio') }}
                                                {{ Form::text('hora', ($elemento->hora != '') ? \Carbon\Carbon::parse($elemento->hora)->format('H:i') : '', ['class' => 'form-control input-hora', 'placeholder' => 'hh:mm (Ejemplo: 20:30)']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                {{ Form::label('fecha_fin', 'Fecha de Fin') }}
                                                {{ Form::date('fecha_fin', ($elemento->fecha_fin != '') ? \Carbon\Carbon::parse($elemento->fecha_fin)->format('Y-m-d') : null, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                {{ Form::label('hora_fin', 'Hora de Fin') }}
                                                {{ Form::text('hora_fin', ($elemento->hora_fin != '') ? \Carbon\Carbon::parse($elemento->hora_fin)->format('H:i') : '', ['class' => 'form-control input-hora', 'placeholder' => 'hh:mm (Ejemplo: 20:30)']) }}
                                            </div>
                                        </div>

                                        {{--<div class="col-12 col-sm-4">--}}
                                        {{--<div class="form-group">--}}
                                        {{--{{ Form::label('kms', 'Kilómetros a recorrer') }}--}}
                                        {{--{{ Form::number('kms', $elemento->kms, ['class' => 'form-control']) }}--}}
                                        {{--</div>--}}
                                        {{--</div>--}}

                                        {{--<div class="col-12 col-sm-4">--}}
                                        {{--<div class="form-check">--}}
                                        {{--{{ Form::checkbox('larga_distancia', true, $elemento->larga_distancia, ['class' => 'form-check-input']) }}--}}
                                        {{--{{ Form::label('larga_distancia', 'Larga distancia', ['class' => 'form-check-label']) }}--}}
                                        {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="col-12">
                                            <div class="form-group">
                                                {{ Form::label('vencimiento', 'Vencimiento') }}
                                                {{ Form::date('vencimiento', $elemento->vencimiento, ['class' => 'form-control', 'required' => 'required']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">

                                    <h3>Adicionales</h3>

                                    <div class="mb-3" style="cursor: pointer" id="agregarAdicional">
                                        <span class="m-badge m-badge--danger">+</span> Agregar nuevo adicional
                                    </div>

                                    <div id="otrosAdicionales">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Valor</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php $cantidadAdicionales = 0; ?>
                                                @foreach($elemento->otros_adicionales() as $otro_adicional)

                                                    <tr>
                                                        <td>
                                                            {{ $otro_adicional->nombre }}
                                                        </td>
                                                        <td>
                                                            {{ $otro_adicional->valor }}
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar" data-accion="editar" data-id="{{ $cantidadAdicionales }}">
                                                                <i class="la la-edit"></i>
                                                            </button>
                                                            <button type="button" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')" data-accion="eliminar" data-id="{{ $cantidadAdicionales }}">
                                                                <i class="la la-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <?php $cantidadAdicionales++; ?>
                                                @endforeach
                                                {{ Form::hidden('cantidadAdicionales', $cantidadAdicionales) }}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="m-form__actions">

                                    </div>
                                </div>

                                <div class="col-12 mt-3">
                                    <div class="form-group">
                                        {{ Form::label('observaciones', 'Observaciones generales') }}
                                        {{ Form::textarea('observaciones', $elemento->observaciones, ['class' => 'form-control']) }}
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane" id="tab_visitas" role="tabpanel">

                            <div class="row mt-5">
                                <div class="col-12">
                                    <h3>Visitas</h3>

                                    @if(empty($elemento->visita()))

                                    <div class="row justify-content-center align-items-center">

                                        {{--<div class="col-12 col-sm-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--{{ Form::label('visita_fecha', 'Fecha') }}--}}
                                                {{--{{ Form::date('visita_fecha', null, ['class' => 'form-control']) }}--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                {{ Form::label('visita_visitador', 'Visitador') }}
                                                {{ Form::select('visita_visitador', $visitadores, null, ['class' => 'form-control']) }}
                                            </div>
                                        </div>
                                        {{--<div class="col-12 col-sm-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--{{ Form::label('visita_hora_inicio', 'Hora de inicio') }}--}}
                                                {{--{{ Form::text('visita_hora_inicio', null, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ejemplo: 16:15)']) }}--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-12 col-sm-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--{{ Form::label('visita_hora_fin', 'Hora de fin') }}--}}
                                                {{--{{ Form::text('visita_hora_fin', null, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ejemplo: 16:15)']) }}--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-12">--}}
                                            {{--<div class="form-group">--}}
                                                {{--{{ Form::label('visita_observaciones', 'Observaciones') }}--}}
                                                {{--{{ Form::textarea('visita_observaciones', null, ['class' => 'form-control']) }}--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>

                                    @else

                                        <div class="row justify-content-center">

                                            <div class="col-12 col-sm-6">
                                                <div class="row">
                                                    {{--<div class="col-12">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--{{ Form::label('visita_fecha', 'Fecha') }}--}}
                                                            {{--{{ Form::date('visita_fecha', $elemento->visita()->fecha, ['class' => 'form-control']) }}--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            {{ Form::label('visita_visitador', 'Visitador') }}
                                                            {{ Form::select('visita_visitador', $visitadores, $elemento->visita()->visitador, ['class' => 'form-control']) }}
                                                        </div>
                                                    </div>
                                                    {{--<div class="col-12 col-sm-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--{{ Form::label('visita_hora_inicio', 'Hora de inicio') }}--}}
                                                            {{--{{ Form::text('visita_hora_inicio', $elemento->visita()->hora_inicio, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ejemplo: 16:15)']) }}--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-12 col-sm-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--{{ Form::label('visita_hora_fin', 'Hora de fin') }}--}}
                                                            {{--{{ Form::text('visita_hora_fin', $elemento->visita()->hora_fin, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ejemplo: 16:15)']) }}--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-12">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--{{ Form::label('visita_observaciones', 'Observaciones') }}--}}
                                                            {{--{{ Form::textarea('visita_observaciones', $elemento->visita()->observaciones, ['class' => 'form-control']) }}--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            {{ Form::label('detallevisita_embalaje', 'Embalaje enviado?') }}
                                                            {{ Form::select('detallevisita_embalaje', ['1' => 'Sí', '0' => 'No'], $elemento->visita()->detalle()->embalaje, ['class' => 'form-control', 'required' => 'required']) }}
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            {{ Form::label('detallevisita_cajas', 'Cantidad de Cajas') }}
                                                            {{ Form::number('detallevisita_cajas', $elemento->visita()->detalle()->cajas, ['class' => 'form-control', 'required' => 'required']) }}
                                                        </div>
                                                    </div>
                                                    {{--<div class="col-12 col-sm-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--{{ Form::label('detallevisita_percheros_moviles', 'Percheros móviles') }}--}}
                                                            {{--{{ Form::number('detallevisita_percheros_moviles', $elemento->visita()->detalle()->percheros_moviles, ['class' => 'form-control', 'required' => 'required']) }}--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            {{ Form::label('detallevisita_horas', 'Horas de trabajo') }}
                                                            {{ Form::number('detallevisita_horas', $elemento->visita()->detalle()->horas, ['class' => 'form-control', 'required' => 'required']) }}
                                                        </div>
                                                    </div>
                                                    {{--<div class="col-12">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--{{ Form::label('detallevisita_fecha_embalaje', 'Fecha') }}--}}
                                                            {{--{{ Form::date('detallevisita_fecha_embalaje', $elemento->visita()->detalle()->fecha, ['class' => 'form-control', 'disabled' => 'disabled']) }}--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}

                                                    {{--<div class="col-12 col-sm-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--{{ Form::label('detallevisita_hora_desde', 'Desde') }}--}}
                                                            {{--{{ Form::text('detallevisita_hora_desde', $elemento->visita()->detalle()->hora_desde, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ej.: 10:30)', 'disabled' => 'disabled' ]) }}--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}

                                                    {{--<div class="col-12 col-sm-6">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--{{ Form::label('detallevisita_hora_hasta', 'Hasta') }}--}}
                                                            {{--{{ Form::text('detallevisita_hora_hasta', $elemento->visita()->detalle()->hora_hasta, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ej.: 10:30)', 'disabled' => 'disabled' ]) }}--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                            </div>

                                        </div>

                                    @endif

                                    {{--<div class="table-responsive">--}}
                                        {{--<table class="table table-bordered">--}}
                                            {{--<thead>--}}
                                            {{--<tr>--}}
                                                {{--<th>Fecha</th>--}}
                                                {{--<th>Hora</th>--}}
                                                {{--<th>Acciones</th>--}}
                                            {{--</tr>--}}
                                            {{--</thead>--}}
                                            {{--<tbody>--}}
                                            {{--@foreach($elemento->visitas() as $visita)--}}

                                                {{--<tr>--}}
                                                    {{--<td>--}}
                                                        {{--{{ $visita->fecha }}--}}
                                                    {{--</td>--}}
                                                    {{--<td>--}}
                                                        {{--{{ $visita->hora_inicio }} - {{ $visita->hora_fin }}--}}
                                                    {{--</td>--}}
                                                    {{--<td>--}}
                                                        {{--<a href="{{ url('presupuestos/'.$elemento->id.'/visitas/editar/'.$visita->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">--}}
                                                            {{--<i class="la la-edit"></i>--}}
                                                        {{--</a>--}}
                                                        {{--<a href="{{ url('presupuestos/'.$elemento->id.'/visitas/borrar/'.$visita->id) }}" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')">--}}
                                                            {{--<i class="la la-trash"></i>--}}
                                                        {{--</a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--@endforeach--}}
                                            {{--</tbody>--}}
                                        {{--</table>--}}
                                    {{--</div>--}}

                                    {{--<div class="m-form__actions">--}}
                                        {{--<a href="{{ url('presupuestos/'.$elemento->id.'/visitas/crear/') }}"><button type="button" class="btn btn-primary">Agregar visita</button></a>--}}
                                    {{--</div>--}}
                                </div>

                                <div class="col-12">
                                    <h3>Observaciones</h3>

                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Descripción</th>
                                                <th>Acciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($elemento->visita()))
                                                @foreach($elemento->visita()->observaciones() as $observacion)

                                                    <tr>
                                                        <td>
                                                            {{ $observacion->nombre }}
                                                        </td>
                                                        <td>
                                                            {!! $observacion->descripcion !!}
                                                        </td>
                                                        <td>
                                                            <a href="{{ url('visita/'.$elemento->visita()->id.'/observaciones/editar/'.$observacion->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar" onClick="return confirm('Se perderán los datos que no haya guardado, desea continuar?')">
                                                                <i class="la la-edit"></i>
                                                            </a>
                                                            <a href="{{ url('visita/'.$elemento->visita()->id.'/observaciones/borrar/'.$observacion->id) }}" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')">
                                                                <i class="la la-trash"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="m-form__actions">
                                        <a href="{{ url('visita/'.$elemento->visita()->id.'/observaciones/crear/') }}" onClick="return confirm('Se perderán los datos que no haya guardado, desea continuar?')">
                                            <button type="button" class="btn btn-primary">Agregar observación</button>
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="tab-pane" id="tab_pagos" role="tabpanel">

                            <h3>Pagos</h3>

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Operador</th>
                                        <th>Fecha de ingreso de pago</th>
                                        <th>Método de pago</th>
                                        <th>Quién recibió el pago</th>
                                        <th>Monto</th>
                                        <th>Observaciones</th>
                                        <th>Comprobante</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($elemento->pagos() as $pago)

                                        <tr>
                                            <td>
                                                {{ $pago->usuario()->nombre }} {{ $pago->usuario()->apellido }}
                                            </td>
                                            <td>
                                                {{ \Carbon\Carbon::parse($pago->fecha)->format('d/m/Y') }}
                                            </td>
                                            <td>
                                                {{ $pago->forma()->nombre }}
                                            </td
                                            ><td>
                                                @if($pago->pagorecibido)
                                                    Administración
                                                @else
                                                    Logística
                                                @endif
                                            </td>
                                            <td>
                                                ${{ $pago->monto }}
                                            </td>
                                            <td>
                                                {!! $pago->observaciones !!}
                                            </td>
                                            <td>
                                                @if($pago->comprobante != '')
                                                    <span data-comprobante="{{ asset($pago->comprobante) }}" class="btn btn-info" data-toggle="modal" data-target="#modalVerComprobante-{{ $pago->id }}">
                                                        Ver comprobante
                                                    </span>
                                                @else
                                                    No tiene cargado
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ url('presupuestos/'.$elemento->id.'/pagos/editar/'.$pago->id) }}" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar" onClick="return confirm('Se perderán los datos que no haya guardado, desea continuar?')">
                                                    <i class="la la-edit"></i>
                                                </a>
                                                <a href="{{ url('presupuestos/'.$elemento->id.'/pagos/borrar/'.$pago->id) }}" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Está seguro que desea eliminar este elemento?')">
                                                    <i class="la la-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <h3>Resumen de pagos</h3>

                            <ul>
                                <li><strong>Pagado:</strong> ${{ $elemento->pagado() }}</li>
                                <li><strong>Falta pagar:</strong> ${{ $elemento->faltaPagar() }}</li>
                                <li><strong>Monto total a pagar:</strong> ${{ $elemento->cotizacion }}</li>
                            </ul>

                            <div class="m-form__actions">
                                <a href="{{ url('presupuestos/'.$elemento->id.'/pagos/crear/') }}" onClick="return confirm('Se perderán los datos que no haya guardado, desea continuar?')">
                                    <button type="button" class="btn btn-primary">Agregar pago</button>
                                </a>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <h3>Cotización</h3>


                    <div id="cotizacionCompleta">

                    </div>

                </div>

            </div>

        </div>

        <div class="col-12">
            <div class="m-portlet__foot">
                <div class="m-form__actions">
                    {{ Form::submit('Guardar', ['class' => 'col-2 btn btn-primary', 'style' => 'position: fixed;bottom: 0;margin-bottom: 1rem;float: right;left: 80%;z-index:99999', 'id' => 'botonGuardarFormulario']) }}
                </div>
            </div>
        </div>

    </div>

    <div id="adicionales-modals">

        <?php $cantidadAdicionales = 0; ?>
        @foreach($elemento->otros_adicionales() as $otroAdicional)

            <div class="modal fade" id="adicional-{{ $cantidadAdicionales }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Adicional</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        {{ Form::label('adicionales_nombre[]', 'Nombre') }}
                                        {{ Form::text('adicionales_nombre[]', $otroAdicional->nombre, ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        {{ Form::label('adicionales_valor[]', 'Valor') }}
                                        {{ Form::text('adicionales_valor[]', $otroAdicional->valor, ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        {{ Form::label('adicionales_fecha[]', 'Fecha') }}
                                        {{ Form::date('adicionales_fecha[]', $otroAdicional->fecha, ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        {{ Form::label('adicionales_hora_inicio[]', 'Hora de Inicio') }}
                                        {{ Form::text('adicionales_hora_inicio[]', (!empty($otroAdicional->hora_inicio)) ? \Carbon\Carbon::parse($otroAdicional->hora_inicio)->format('H:i') : "", ['class' => 'form-control input-hora']) }}
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        {{ Form::label('adicionales_hora_fin[]', 'Hora de Fin') }}
                                        {{ Form::text('adicionales_hora_fin[]', (!empty($otroAdicional->hora_fin)) ? \Carbon\Carbon::parse($otroAdicional->hora_fin)->format('H:i') : "", ['class' => 'form-control input-hora']) }}
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        {{ Form::label('adicionales_personal[]', 'Personal') }}
                                        {{ Form::number('adicionales_personal[]', $otroAdicional->personal, ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        {{ Form::label('adicionales_duracion[]', 'Duración') }}
                                        {{ Form::text('adicionales_duracion[]', $otroAdicional->duracion, ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="col-12 col-sm-8">
                                    <div class="form-group">
                                        {{ Form::label('adicionales_observaciones[]', 'Observaciones') }}
                                        {{ Form::textarea('adicionales_observaciones[]', $otroAdicional->observaciones, ['class' => 'form-control', 'id' => 'adicionales_observaciones_'.$cantidadAdicionales]) }}
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-actions">
                                        <button type="button" class="btn btn-success">GUARDAR</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

            <?php $cantidadAdicionales++; ?>
        @endforeach

    </div>

    {{ Form::close() }}

    <div class="modal fade" id="modalMapa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Mapa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalAgregarAdicional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar adicional</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ Form::open() }}
                    <div class="row">
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::label('nombre', 'Nombre') }}
                                {{ Form::text('nombre', null, ['class' => 'form-control awesomplete', 'required' => 'required']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::label('valor', 'Valor') }}
                                {{ Form::text('valor', null, ['class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::label('fecha', 'Fecha') }}
                                {{ Form::date('fecha', null, ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::label('hora_inicio', 'Hora de Inicio') }}
                                {{ Form::text('hora_inicio', null, ['class' => 'form-control input-hora']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::label('hora_fin', 'Hora de Fin') }}
                                {{ Form::text('hora_fin', null, ['class' => 'form-control input-hora']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::label('personal', 'Personal') }}
                                {{ Form::number('personal', null, ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::label('duracion', 'Duración') }}
                                {{ Form::text('duracion', null, ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-8">
                            <div class="form-group">
                                {{ Form::label('observaciones', 'Observaciones') }}
                                {{ Form::textarea('observaciones', null, ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-actions">
                                {{ Form::submit('GUARDAR', ['class' => 'btn btn-success']) }}
                            </div>
                        </div>

                    </div>
                    {{ Form::close() }}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    @foreach($elemento->pagos() as $pago)
        <div class="modal fade" id="modalVerComprobante-{{ $pago->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Comprobante</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @if($pago->adjuntoEsPdf())

                            {{--<object data="{{ asset($pago->comprobante) }}" type="application/x-pdf" title="Comprobante" width="100%" height="100%">--}}
                                {{--<a href="{{ asset($pago->comprobante) }}" target="_blank">Su navegador no soporta la visualización de pdfs, haga click aquí para poder verlo</a>--}}
                            {{--</object>--}}

                            <object data="{{ asset( 'comprobantes/pagos/'.rawurlencode(substr($pago->comprobante, strrpos($pago->comprobante, '/' )+1)) ) }}" type="application/pdf" width="100%" height="500">
                                <embed src="{{ asset('comprobantes/pagos/'.rawurlencode(substr($pago->comprobante, strrpos($pago->comprobante, '/' )+1))) }}" width="600px" height="500px" />
                                <p>Su navegador no soporta la visualización de pdfs, haga click aquí para poder verlo:
                                    <a href="{{ asset('comprobantes/pagos/'.rawurlencode(substr($pago->comprobante, strrpos($pago->comprobante, '/' )+1))) }}" target="_blank">PDF</a>.
                                </p>
                            </object>
                        @else
                            <div class="text-center">
                                <img src="{{ asset('comprobantes/pagos/'.rawurlencode(substr($pago->comprobante, strrpos($pago->comprobante, '/' )+1))) }}" class="img-fluid">
                            </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {

            /** PRESUPUESTO **/

            function estadoBotonSubmit(activo) {
                $('#botonGuardarFormulario').attr('disabled', activo);

                if(activo) {
                    $('#loaderGif').css('display', 'block');
                } else {
                    $('#loaderGif').css('display', 'none')
                }
            }

            function cambiarKms(estado) {
                $('input[name="kms"]').attr('readonly', estado);
            }

            function cargarInformacionVehiculo(vehiculo) {

                $.get('/ajax/vehiculo/' + vehiculo, function(data) {

                    $("#informacionDelVehiculo").empty().append(data.descripcion);

                });
            }

            function obtenerServicio() {
                var servicio = $('select[name="servicio"]').val();

                if(servicio != null && servicio != "") {
                    return servicio;
                }

                return null;
            }

            function obtenerVehiculo() {
                var vehiculo = $('select[name="vehiculo"]').val();

                if(vehiculo != null && vehiculo != "") {

                    // if($('select[name="servicio"]').val() == 1 || $('select[name="servicio"]').val() == 2 || $('select[name="servicio"]').val() == 5 || $('select[name="servicio"]').val() == 6) { // VERIFICAMOS SI ES MUDANZA
                    //     cargarInformacionVehiculo(vehiculo);
                    // } else {
                    //     $("#informacionDelVehiculo").empty();
                    // }

                    cargarInformacionVehiculo(vehiculo);

                    return vehiculo;
                }

                return null;
            }

            function obtenerKilometros() {
                var kilometros = $('input[name="kms"]').val();

                if(kilometros != null && kilometros != "") {
                    return kilometros;
                }

                return 0;
            }

            function obtenerAdicionales() {
                var adicionales = [];
                $('input[name="adicionales[]"]:checked').each(function(i){
                    adicionales[i] = $(this).val();
                });

                return adicionales;
            }

            function obtenerOtrosAdicionales() {

                var otros_adicionales = [];
                $('input[name="adicionales_valor[]"]').each(function(i){
                    otros_adicionales[i] = {nombre: $($('input[name="adicionales_nombre[]"]').toArray()[i]).val(), valor: $(this).val()};
                });

                return otros_adicionales;
            }

            function obtenerLargaDistancia() {
                var larga_distancia = 0;
                var estado = true;

                if ($('input[name="larga_distancia"]').is(':checked')) {
                    estado = false;
                    larga_distancia = 1;
                }

                cambiarKms(estado);

                return larga_distancia;
            }

            function obtenerValorAproximado_input() {
                var valor_aproximado = $('input[name="cotizacion"]').val();

                if(valor_aproximado != null && valor_aproximado != "") {
                    return valor_aproximado;
                }

                return 0;
            }

            function obtenerImpuesto() {
                return $('input[name="impuesto"]').val();
            }

            function obtenerCotizacion() {

                estadoBotonSubmit(true);

                var servicio = obtenerServicio();
                var vehiculo = obtenerVehiculo();
                var kilometros = obtenerKilometros();
                var adicionales = obtenerAdicionales();
                var otros_adicionales = obtenerOtrosAdicionales();
                var larga_distancia = obtenerLargaDistancia();
                var valor_aproximado = obtenerValorAproximado_input();
                var impuesto = obtenerImpuesto();

                $.ajax({
                    method: "POST",
                    url: "/ajax/cotizar",
                    data: { servicio: servicio, vehiculo: vehiculo, kilometros: kilometros, adicionales: adicionales, otros_adicionales: otros_adicionales, larga_distancia: larga_distancia, valor_aproximado: valor_aproximado, impuesto: impuesto }
                }).done(function(data) {

                    $("#cotizacionCompleta").empty().append(data.cotizacion); // HTML DE LA COTIZACION
                    $('input[name="valor_final"]').val(data.valor_final); // PONEMOS EL VALOR FINAL
                    estadoBotonSubmit(false);

                }).fail(function() {

                    $.notify({
                        message: '<strong>¡Error!</strong> No se pudo obtener la cotización, revise la información'
                    },{
                        type: 'danger'
                    });

                });
            }

            function obtenerValorAproximado() {

                estadoBotonSubmit(true);

                var servicio = obtenerServicio();
                var vehiculo = obtenerVehiculo();
                var kilometros = obtenerKilometros();
                var larga_distancia = obtenerLargaDistancia();

                $.ajax({
                    method: "POST",
                    url: "/ajax/valorAproximado",
                    data: { servicio: servicio, vehiculo: vehiculo, kilometros: kilometros, larga_distancia: larga_distancia }
                }).done(function(data) {

                    // $('input[name="cotizacion"]').val(data.valor_aproximado); // PONEMOS EL VALOR APROXIMADO
                    obtenerCotizacion();

                }).fail(function() {

                    $.notify({
                        message: '<strong>¡Error!</strong> No se pudo obtener la cotización, revise la información'
                    },{
                        type: 'danger'
                    });

                });
            }

            // EVENTOS

            $('input[name="adicionales[]"]').change(function() {
                obtenerCotizacion();
            });

            $('select[name="servicio"]').change(function() {
                obtenerValorAproximado();
            });

            $('select[name="vehiculo"]').change(function() {
                obtenerValorAproximado();
            });

            $('input[name="kms"]').on('input',function(e){
                obtenerValorAproximado();
            });

            $('input[name="larga_distancia"]').change(function(e){
                obtenerValorAproximado();
            });

            $('input[name="cotizacion"]').change(function(e){
                obtenerCotizacion();
            });

            $('input[name="cotizacion"]').focus(function() {
                $('#botonGuardarFormulario').attr('disabled', true);
            });

            $('input[name="cotizacion"]').focusout(function() {
                $('#botonGuardarFormulario').attr('disabled', false);
            });

            $('#formularioPresupuesto').submit(submitDelFormulario);

            /** END PRESUPUESTO **/

            // AGREGAR ADICIONAL

            var cantidadAdicionales = $('input[name="cantidadAdicionales"]').val();

            $('#agregarAdicional').click(function() {

                $('#modalAgregarAdicional').modal('show');
            });

            $('#modalAgregarAdicional form').submit(function(e) {

                e.preventDefault();

                var nombre = $($(this).find('input[name="nombre"]')[0]).val();
                var valor = $($(this).find('input[name="valor"]')[0]).val();
                var fecha = $($(this).find('input[name="fecha"]')[0]).val();
                var hora_inicio = $($(this).find('input[name="hora_inicio"]')[0]).val();
                var hora_fin = $($(this).find('input[name="hora_fin"]')[0]).val();
                var personal = $($(this).find('input[name="personal"]')[0]).val();
                var duracion = $($(this).find('input[name="duracion"]')[0]).val();
                var observaciones = $($(this).find('textarea[name="observaciones"]')[0]).val();

                $('#adicionales-modals').append(
                    '<div class="modal fade" id="adicional-'+ cantidadAdicionales + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">\n' +
                    '        <div class="modal-dialog modal-lg" role="document">\n' +
                    '            <div class="modal-content">\n' +
                    '                <div class="modal-header">\n' +
                    '                    <h5 class="modal-title" id="exampleModalLabel">Adicional</h5>\n' +
                    '                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
                    '                        <span aria-hidden="true">×</span>\n' +
                    '                    </button>\n' +
                    '                </div>\n' +
                    '                <div class="modal-body">\n' +
                    '                    <div class="row">\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_nombre[]">Nombre</label>\n' +
                    '                                <input class="form-control awesomplete" name="adicionales_nombre[]" type="text" value="' + nombre + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_valor[]">Valor</label>\n' +
                    '                                <input class="form-control" name="adicionales_valor[]" type="text" value="' + valor + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_fecha[]">Fecha</label>\n' +
                    '                                <input class="form-control" name="adicionales_fecha[]" type="date" value="' + fecha + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_hora_inicio[]">Hora de Inicio</label>\n' +
                    '                                <input class="form-control input-hora" name="adicionales_hora_inicio[]" type="text" value="' + hora_inicio + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_hora_fin[]">Hora de Fin</label>\n' +
                    '                                <input class="form-control input-hora" name="adicionales_hora_fin[]" type="text" value="' + hora_fin + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_personal[]">Personal</label>\n' +
                    '                                <input class="form-control" name="adicionales_personal[]" type="number" value="' + personal + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_duracion[]">Duración</label>\n' +
                    '                                <input class="form-control" name="adicionales_duracion[]" type="text" value="' + duracion + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-8">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_observaciones[]">Observaciones</label>\n' +
                    '                                <textarea class="form-control" name="adicionales_observaciones[]" cols="50" rows="10" id="adicionales_observaciones_' + cantidadAdicionales + '">' + observaciones + '</textarea>\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12">\n' +
                    '                            <div class="form-actions">\n' +
                    '                                <button type="button" class="btn btn-success">GUARDAR</button>\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                    </div>\n' +
                    '                </div>\n' +
                    '                <div class="modal-footer">\n' +
                    '                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>\n' +
                    '                </div>\n' +
                    '            </div>\n' +
                    '        </div>\n' +
                    '    </div>'
                );

                var cleaveInputHoraInicio = new Cleave($('#adicional-'+ cantidadAdicionales + ' input[name="adicionales_hora_inicio[]"]'), {
                    time: true,
                    timePattern: ['h', 'm']
                });

                var cleaveInputHoraFin = new Cleave($('#adicional-'+ cantidadAdicionales + ' input[name="adicionales_hora_fin[]"]'), {
                    time: true,
                    timePattern: ['h', 'm']
                });

                cantidadAdicionales++;

                cargarTablaAdicionales();
                cargarSubmitModalAdicional();
                cargarAutocompletado();
                cargarCKEditor();
                inputHora();

                $($(this).find('input[name="nombre"]')[0]).val('');
                $($(this).find('input[name="valor"]')[0]).val('');
                $($(this).find('input[name="fecha"]')[0]).val('');
                $($(this).find('input[name="hora_inicio"]')[0]).val('');
                $($(this).find('input[name="hora_fin"]')[0]).val('');
                $($(this).find('input[name="personal"]')[0]).val('');
                $($(this).find('input[name="duracion"]')[0]).val('');
                $($(this).find('textarea[name="observaciones"]')[0]).val('');

                $('#modalAgregarAdicional').modal('hide');

            });

            function cargarTablaAdicionales() {

                var tabla = $('#otrosAdicionales table').DataTable();
                tabla.clear().draw();

                $('#adicionales-modals .modal').each(function(index, modal) {
                    var id = $(this).attr('id').split('adicional-')[1];
                    var nombre = $($(modal).find('input[name="adicionales_nombre[]"]')[0]).val();
                    var valor = $($(modal).find('input[name="adicionales_valor[]"]')[0]).val();

                    tabla.row.add([
                        nombre,
                        valor,
                        '<button type="button" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar" data-accion="editar" data-id="' + id + '">\n' +
                        '                                                <i class="la la-edit"></i>\n' +
                        '                                            </button>\n' +
                        '                                            <button type="button" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" data-accion="eliminar" data-id="' + id + '">\n' +
                        '                                                <i class="la la-trash"></i>\n' +
                        '                                            </button>\n',
                    ]).draw( false );

                    cargarBotonesTablaAdicional();

                });

                obtenerCotizacion();
            }

            function cargarSubmitModalAdicional() {

                $('#adicionales-modals .modal .form-actions button').click(function() {

                    $(this).parent().parent().parent().parent().parent().parent().parent().modal('hide');

                    cargarTablaAdicionales();

                });

            }

            function cargarBotonesTablaAdicional() {
                $('#otrosAdicionales button').click(function() {

                    var accion = $(this).attr('data-accion');
                    var id = $(this).attr('data-id');

                    if(accion == "editar") {

                        $('#adicional-' + id).modal('show');

                    } else if(accion == "eliminar") {

                        if(confirm('Está seguro que desea eliminar este elemento?')) {
                            $('#adicional-' + id).remove();
                            cargarTablaAdicionales();
                        }

                    }
                });
            }

            function cargarAutocompletado() {

                $.ajax({
                    method: "GET",
                    url: "/items_adicionales/ver",
                }).done(function(data) {

                    var adicionalesNombre = [];

                    data.forEach(function(valor, index) {
                        adicionalesNombre.push(valor.nombre);
                    });

                    if($('input[name="adicionales_nombre[]"]').length) {
                        $.each($('input[name="adicionales_nombre[]"]'), function(indexInput, input) {
                            new Awesomplete($('input[name="adicionales_nombre[]"]')[indexInput], {
                                list: adicionalesNombre
                            });

                            $($('input[name="adicionales_nombre[]"]')[indexInput]).on('awesomplete-selectcomplete', function() {

                                var nombreAdicional = this.value;

                                data.forEach(function(valor, index) {

                                    if(valor.nombre == nombreAdicional) {
                                        CKEDITOR.instances['adicionales_observaciones_' + (indexInput)].setData(valor.descripcion);
                                    }
                                });
                            });
                        });
                    }

                    new Awesomplete('#modalAgregarAdicional input[name="nombre"]', {
                        list: adicionalesNombre
                    });

                    $('#modalAgregarAdicional input[name="nombre"]').on('awesomplete-selectcomplete', function() {
                        var nombreAdicional = this.value;

                        data.forEach(function(valor, index) {

                            if(valor.nombre == nombreAdicional) {
                                CKEDITOR.instances['observaciones'].setData(valor.descripcion);
                            }
                        });
                    });

                }).fail(function() {

                    $.notify({
                        message: '<strong>¡Error!</strong> No se pudo obtener los adicionales'
                    },{
                        type: 'danger'
                    });

                });
            }

            // END AGREGAR ADICIONAL

            // DIRECCIONES
            var direccionesCarga = $('input[name="cantidad_direcciones_carga"]').val();
            var direccionesDescarga = $('input[name="cantidad_direcciones_descarga"]').val();

            $("#agregarDireccionDeCarga").click(function(data) {
                direccionesCarga++;

                $("#direccionesDeCarga").append('<div class="m-accordion__item itemDireccionDeCarga">\n' +
                    '                            <div class="m-accordion__item-head collapsed" role="tab" id="direccionDeCargaLarga-'+ direccionesCarga +'" data-toggle="collapse" href="#direccionDeCarga-'+ direccionesCarga +'" aria-expanded="false">\n' +
                    '                                <span class="m-accordion__item-title">Dirección '+ direccionesCarga +'</span>\n' +
                    '                                <span class="m-accordion__item-mode"></span>\n' +
                    '                            </div>\n' +
                    '                            <div class="m-accordion__item-body collapse" id="direccionDeCarga-'+ direccionesCarga +'" role="tabpanel" aria-labelledby="direccionDeCargaLarga-'+ direccionesCarga +'">\n' +
                    '                                <div class="m-accordion__item-content">\n' +
                    '                                    <div class="row">\n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="direccion_carga[]">Dirección</label>\n' +
                    '                                                <input type="text" class="form-control" name="direccion_carga[]">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="piso_carga[]">Piso</label>\n' +
                    '                                                <input type="text" class="form-control" name="piso_carga[]" required="required">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="departamento_carga[]">Departamento</label>\n' +
                    '                                                <input type="text" class="form-control" name="departamento_carga[]" required="required">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="entrecalles_carga[]">Entrecalles</label>\n' +
                    '                                                <input type="text" class="form-control" name="entrecalles_carga[]">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="provincia_carga[]">Provincia</label>\n' +
                    '                                                <select name="provincia_carga[]" class="form-control" required="required">\n' +
                    '                                                <option selected="selected" value="">Selecciona una provincia</option>\n' +
                    '                                                <option value="Ciudad Autonoma de Buenos Aires">Ciudad Autonoma de Buenos Aires</option>\n' +
                    '                                                <option value="Buenos Aires">Buenos Aires</option>\n' +
                    '                                                <option value="Catamarca">Catamarca</option>\n' +
                    '                                                <option value="Chaco">Chaco</option>\n' +
                    '                                                <option value="Chubut">Chubut</option>\n' +
                    '                                                <option value="Cordoba">Cordoba</option>\n' +
                    '                                                <option value="Corrientes">Corrientes</option>\n' +
                    '                                                <option value="Entre Rios">Entre Rios</option>\n' +
                    '                                                <option value="Formosa">Formosa</option>\n' +
                    '                                                <option value="Jujuy">Jujuy</option>\n' +
                    '                                                <option value="La Pampa">La Pampa</option>\n' +
                    '                                                <option value="La Rioja">La Rioja</option>\n' +
                    '                                                <option value="Mendoza">Mendoza</option>\n' +
                    '                                                <option value="Misiones">Misiones</option>\n' +
                    '                                                <option value="Neuquen">Neuquen</option>\n' +
                    '                                                <option value="Rio Negro">Rio Negro</option>\n' +
                    '                                                <option value="Salta">Salta</option>\n' +
                    '                                                <option value="San Juan">San Juan</option>\n' +
                    '                                                <option value="San Luis">San Luis</option>\n' +
                    '                                                <option value="Santa Cruz">Santa Cruz</option>\n' +
                    '                                                <option value="Santa Fe">Santa Fe</option>\n' +
                    '                                                <option value="Santiago del Estero">Santiago del Estero</option>\n' +
                    '                                                <option value="Tierra del Fuego">Tierra del Fuego</option>\n' +
                    '                                                <option value="Tucuman">Tucuman</option>' +
                    '                                                </select>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="localidad_carga[]">Localidad</label>\n' +
                    '                                                <select class="form-control" name="localidad_carga[]" required="required"></select>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="observaciones_carga[]">Observaciones</label>\n' +
                    '                                                <textarea class="form-control" name="observaciones_carga[]"></textarea>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        <div class="col-12">\n' +
                    '                                             <div class="row">\n' +
                    '                                                    <div class="col-6">\n' +
                    '                                                        <div class="float-left">\n' +
                    '                                                            <button type="button" class="btn btn-outline-danger eliminarDireccion">REMOVER DIRECCION</button>\n' +
                    '                                                        </div>\n' +
                    '                                                    </div>\n' +
                    '\n' +
                    '                                                    <div class="col-6">\n' +
                    '                                                        <div class="float-right">\n' +
                    '                                                            <button type="button" class="btn btn-outline-success verDireccionEnMapa">VER MAPA</button>\n' +
                    '                                                        </div>\n' +
                    '                                                    </div>\n' +
                    '                                                </div>\n' +
                    '                                            </div>\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                        </div>');

                primeraDireccionCarga = "";

                $('input[name="cantidad_direcciones_carga"]').val(direccionesCarga);

                cargarEventoBoton();
                cargarEventoProvincias();
                cargarCKEditor();
            });

            $("#agregarDireccionDeDescarga").click(function(data) {
                direccionesDescarga++;

                $("#direccionesDeDescarga").append('<div class="m-accordion__item itemDireccionDeDescarga">\n' +
                    '                            <div class="m-accordion__item-head collapsed" role="tab" id="direccionDeDescargaLarga-'+ direccionesDescarga +'" data-toggle="collapse" href="#direccionDeDescarga-'+ direccionesDescarga +'" aria-expanded="false">\n' +
                    '                                <span class="m-accordion__item-title">Dirección '+ direccionesDescarga +'</span>\n' +
                    '                                <span class="m-accordion__item-mode"></span>\n' +
                    '                            </div>\n' +
                    '                            <div class="m-accordion__item-body collapse" id="direccionDeDescarga-'+ direccionesDescarga +'" role="tabpanel" aria-labelledby="direccionDeDescargaLarga-'+ direccionesDescarga +'">\n' +
                    '                                <div class="m-accordion__item-content">\n' +
                    '                                    <div class="row">\n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="direccion_descarga[]">Dirección</label>\n' +
                    '                                                <input type="text" class="form-control" name="direccion_descarga[]">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="piso_descarga[]">Piso</label>\n' +
                    '                                                <input type="text" class="form-control" name="piso_descarga[]" required="required">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="departamento_descarga[]">Departamento</label>\n' +
                    '                                                <input type="text" class="form-control" name="departamento_descarga[]" required="required">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="entrecalles_descarga[]">Entrecalles</label>\n' +
                    '                                                <input type="text" class="form-control" name="entrecalles_descarga[]">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="provincia_descarga[]" required="required">Provincia</label>\n' +
                    '                                                <select name="provincia_descarga[]" class="form-control">\n' +
                    '                                                <option selected="selected" value="">Selecciona una provincia</option>\n' +
                    '                                                <option value="Ciudad Autonoma de Buenos Aires">Ciudad Autonoma de Buenos Aires</option>\n' +
                    '                                                <option value="Buenos Aires">Buenos Aires</option>\n' +
                    '                                                <option value="Catamarca">Catamarca</option>\n' +
                    '                                                <option value="Chaco">Chaco</option>\n' +
                    '                                                <option value="Chubut">Chubut</option>\n' +
                    '                                                <option value="Cordoba">Cordoba</option>\n' +
                    '                                                <option value="Corrientes">Corrientes</option>\n' +
                    '                                                <option value="Entre Rios">Entre Rios</option>\n' +
                    '                                                <option value="Formosa">Formosa</option>\n' +
                    '                                                <option value="Jujuy">Jujuy</option>\n' +
                    '                                                <option value="La Pampa">La Pampa</option>\n' +
                    '                                                <option value="La Rioja">La Rioja</option>\n' +
                    '                                                <option value="Mendoza">Mendoza</option>\n' +
                    '                                                <option value="Misiones">Misiones</option>\n' +
                    '                                                <option value="Neuquen">Neuquen</option>\n' +
                    '                                                <option value="Rio Negro">Rio Negro</option>\n' +
                    '                                                <option value="Salta">Salta</option>\n' +
                    '                                                <option value="San Juan">San Juan</option>\n' +
                    '                                                <option value="San Luis">San Luis</option>\n' +
                    '                                                <option value="Santa Cruz">Santa Cruz</option>\n' +
                    '                                                <option value="Santa Fe">Santa Fe</option>\n' +
                    '                                                <option value="Santiago del Estero">Santiago del Estero</option>\n' +
                    '                                                <option value="Tierra del Fuego">Tierra del Fuego</option>\n' +
                    '                                                <option value="Tucuman">Tucuman</option>' +
                    '                                                </select>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="localidad_descarga[]">Localidad</label>\n' +
                    '                                                <select class="form-control" name="localidad_descarga[]" required="required"></select>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="observaciones_descarga[]">Observaciones</label>\n' +
                    '                                                <textarea class="form-control" name="observaciones_descarga[]"></textarea>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        <div class="col-12">\n' +
                    '                                                <div class="row">\n' +
                    '                                                    <div class="col-6">\n' +
                    '                                                        <div class="float-left">\n' +
                    '                                                            <button type="button" class="btn btn-outline-danger eliminarDireccion">REMOVER DIRECCION</button>\n' +
                    '                                                        </div>\n' +
                    '                                                    </div>\n' +
                    '\n' +
                    '                                                    <div class="col-6">\n' +
                    '                                                        <div class="float-right">\n' +
                    '                                                            <button type="button" class="btn btn-outline-success verDireccionEnMapa">VER MAPA</button>\n' +
                    '                                                        </div>\n' +
                    '                                                    </div>\n' +
                    '                                                </div>\n' +
                    '                                            </div>\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                            </div> ' +
                    '                        </div>');

                $('input[name="cantidad_direcciones_descarga"]').val(direccionesDescarga);

                cargarEventoBoton();
                cargarEventoProvincias();
                cargarCKEditor();
            });

            function cargarEventoBoton() {
                $('#direccionesDeCarga button.verDireccionEnMapa').unbind().click(function() {

                    var tipo = "carga";

                    // OBTENER DIRECCION
                    var direccion = $(this).parent().parent().parent().parent().parent().find('input[name="direccion_'+ tipo +'[]"]').val().replace(/ /g, '+');
                    var provincia = $(this).parent().parent().parent().parent().parent().find('select[name="provincia_'+ tipo +'[]"]').val().replace(/ /g, '+');
                    var localidad = $(this).parent().parent().parent().parent().parent().find('select[name="localidad_'+ tipo +'[]"]').val().replace(/ /g, '+');
                    var direccionFinal = direccion + "+" + localidad + "," + provincia;
                    // END OBTENER DIRECCION

                    $('#modalMapa .modal-body').empty();
                    $('#modalMapa .modal-body').append('<iframe\n' +
                        '                            width="100%"\n' +
                        '                            height="450"\n' +
                        '                            frameborder="0" style="border:0"\n' +
                        '                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyChLjx44r8oXAcJ4kuroTIEy29d8vv1NbM\n' +
                        '                            &q='+ direccionFinal +'" allowfullscreen>\n' +
                        '                    </iframe>');
                    $('#modalMapa').modal('show');
                });

                $('#direccionesDeDescarga button.verDireccionEnMapa').unbind().click(function() {

                    var tipo = "descarga";

                    // OBTENER DIRECCION
                    var direccion = $(this).parent().parent().parent().parent().parent().find('input[name="direccion_'+ tipo +'[]"]').val().replace(/ /g, '+');
                    var provincia = $(this).parent().parent().parent().parent().parent().find('select[name="provincia_'+ tipo +'[]"]').val().replace(/ /g, '+');
                    var localidad = $(this).parent().parent().parent().parent().parent().find('select[name="localidad_'+ tipo +'[]"]').val().replace(/ /g, '+');
                    var direccionFinal = direccion + "+" + localidad + "," + provincia;
                    // END OBTENER DIRECCION

                    $('#modalMapa .modal-body').empty();
                    $('#modalMapa .modal-body').append('<iframe\n' +
                        '                            width="100%"\n' +
                        '                            height="450"\n' +
                        '                            frameborder="0" style="border:0"\n' +
                        '                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyChLjx44r8oXAcJ4kuroTIEy29d8vv1NbM\n' +
                        '                            &q='+ direccionFinal +'" allowfullscreen>\n' +
                        '                    </iframe>');
                    $('#modalMapa').modal('show');
                });

                $('#direccionesDeCarga button.eliminarDireccion').unbind().click(function() {

                    if(confirm('Está seguro que desea remover esta dirección')) {
                        direccionesCarga--;
                        $('input[name="cantidad_direcciones_carga"]').val(direccionesCarga);

                        $(this).parent().parent().parent().parent().parent().parent().parent().parent().remove();
                    }
                });

                $('#direccionesDeDescarga button.eliminarDireccion').unbind().click(function() {

                    if(confirm('Está seguro que desea remover esta dirección')) {
                        direccionesDescarga--;
                        $('input[name="cantidad_direcciones_descarga"]').val(direccionesDescarga);

                        $(this).parent().parent().parent().parent().parent().parent().parent().parent().remove();
                    }
                });
            }

            function cargarEventoProvincias() {
                $('select[name="provincia_carga[]"]').unbind().change(function() {
                    provinciaSeleccionada('_carga', $(this));
                });

                $('select[name="provincia_descarga[]"]').unbind().change(function() {
                    provinciaSeleccionada('_descarga', $(this));
                });
            }

            // END DIRECCIONES

            // PROVINCIAS Y CIUDADES

            function provinciaSeleccionada(prefix, select) {

                if($(select).val() != '' && $(select).val() != null) {

                    $.get('/ajax/localidades/' + $(select).val(), function (data) {

                        $options = "";

                        $.each(data, function(index, localidad) {
                            $options += '<option value="' + localidad.nombre + '">' + localidad.nombre + '</option>';
                        });

                        // ACTUALIZAMOS LA LOCALIDAD
                        $localidad = $(select).parent().parent().parent().find('select[name="localidad' + prefix + '[]"]');
                        $localidad.attr('data-live-search', true);
                        $localidad.empty().append($options);
                        $localidad.selectpicker('refresh');
                    });
                }

            }

            function cargarLocalidadesParaDireccionesCargadas() {

                $('select[name="provincia_carga[]').each(function() {

                    $selectCarga = $(this);

                    if($selectCarga.val() != '' && $selectCarga.val() != null) {

                        $.get({
                            url: '/ajax/localidades/' + $selectCarga.val(),
                            async: false,
                        }, function (data) {

                            $options = "";

                            $.each(data, function(index, localidad) {
                                $options += '<option value="' + localidad.nombre + '">' + localidad.nombre + '</option>';
                            });

                            // ACTUALIZAMOS LA LOCALIDAD
                            $localidad = $selectCarga.parent().parent().parent().find('select[name="localidad_carga[]"]');
                            $localidad.attr('data-live-search', true);
                            $localidad.empty().append($options);
                            $localidad.selectpicker('refresh');

                            // SETEAMOS EL VALUE A LA LOCALIDAD CARGADA
                            var localidad_seleccionada = $localidad.attr('localidad-seleccionada');
                            if(localidad_seleccionada !== undefined && localidad_seleccionada !== false) {
                                $localidad.selectpicker('val', localidad_seleccionada);
                            }
                        });
                    }

                });

                $('select[name="provincia_descarga[]').each(function() {

                    $selectDescarga = $(this);

                    if($selectDescarga.val() != '' && $selectDescarga.val() != null) {

                        $.get({
                            url: '/ajax/localidades/' + $selectDescarga.val(),
                            async: false,
                        }, function (data) {

                            $options = "";

                            $.each(data, function(index, localidad) {
                                $options += '<option value="' + localidad.nombre + '">' + localidad.nombre + '</option>';
                            });

                            // ACTUALIZAMOS LA LOCALIDAD
                            $localidad = $selectDescarga.parent().parent().parent().find('select[name="localidad_descarga[]"]');
                            $localidad.attr('data-live-search', true);
                            $localidad.empty().append($options);
                            $localidad.selectpicker('refresh');

                            // SETEAMOS EL VALUE A LA LOCALIDAD CARGADA
                            var localidad_seleccionada = $localidad.attr('localidad-seleccionada');
                            if(localidad_seleccionada !== undefined && localidad_seleccionada !== false) {
                                $localidad.selectpicker('val', localidad_seleccionada);
                            }
                        });
                    }
                });
            }

            function submitDelFormulario(e) {
                estadoBotonSubmit(true);
                return true;
            }

            // END PROVINCIAS Y CIUDADES

            /** END EVENTOS **/

            /** AGREGAMOS LAS DIRECCIONES POR DEFECTO **/

            // $("#agregarDireccionDeCarga").click();
            // $("#agregarDireccionDeDescarga").click();

            /** END DIRECCIONES */

            /** CHEQUEAMOS COTIZACION AL PRINCIPIO **/

            obtenerValorAproximado();

            /** END CHEQUEAMOS COTIZACION **/

            /** CHEQUEAMOS EL EMBAJALE **/

            function setearAtributoDisabled(estado) {
                $('input[name="detallevisita_fecha_embalaje"]').attr('disabled', estado);
                $('input[name="detallevisita_hora_desde"]').attr('disabled', estado);
                $('input[name="detallevisita_hora_hasta"]').attr('disabled', estado);
            }

            function valorEmbalaje() {
                var embalaje = $('select[name="detallevisita_embalaje"]').val();

                if(embalaje == 1) {
                    setearAtributoDisabled(true);
                } else {
                    setearAtributoDisabled(false);
                }
            }

            valorEmbalaje();

            $('select[name="detallevisita_embalaje"]').change(function() {
                valorEmbalaje();
            });

            /** END CHEQUEAMOS EL EMBALAJE **/

            /** AGREGAMOS MASK DE TELEFONOS **/

            var cleaveInputHoraInicio = new Cleave($('#modalAgregarAdicional input[name="hora_inicio"]'), {
                time: true,
                timePattern: ['h', 'm']
            });

            var cleaveInputHoraFin = new Cleave($('#modalAgregarAdicional input[name="hora_fin"]'), {
                time: true,
                timePattern: ['h', 'm']
            });

            /** END AGREGAMOS MASK DE TELEFONOS **/

            /** CKEDITOR */

            function cargarCKEditor() {

                // CKEDITOR
                $("textarea").each(function() {

                    $(this).ckeditor({
                        customConfig: '/js/ckeditor/config.js'
                    });

                    // $(this).addClass('ckeditor');
                    // CKEDITOR.replaceClass('ckeditor');

                });
            }

            /** END CKEDITOR */

            /** AGREGAMOS EVENTOS DE BOTONES TABLA ADICIONALES */

            cargarTablaAdicionales();
            cargarSubmitModalAdicional();

            /** END AGREGAMOS EVENTOS DE BOTONES TABLA ADICIONALES */

            /** OTROS EVENTOS **/

            cargarAutocompletado();
            cargarCKEditor();
            cargarEventoBoton('carga');
            cargarEventoBoton('descarga');
            cargarEventoProvincias();
            cargarLocalidadesParaDireccionesCargadas();

            /** END OTROS EVENTOS **/

            /** IMPUESTOS **/

            function tipoImpuesto() {
                var valor = $('select[name="tipo_impuesto"]').val();

                if(valor === 0) {
                    $('input[name="otro_impuesto"]').attr('disabled', true);

                    setearImpuesto(0);

                } else {

                    if(valor !== 'otro') {
                        $('input[name="otro_impuesto"]').attr('disabled', true);
                        setearImpuesto(valor);

                    } else { // SE ESCRIBE EL VALOR

                        $('input[name="otro_impuesto"]').attr('disabled', false);
                        setearImpuesto($('input[name="otro_impuesto"]').val());
                    }
                }
            }

            function setearImpuesto(valor) {

                if (valor === '') {
                    valor = 0;
                }

                $('input[name="impuesto"]').val(valor);

                obtenerCotizacion();
            }

            function inicializarImpuesto() {
                var valor = Number($('input[name="impuesto"]').val());

                if(valor !== 21 && valor !== 37) {
                    $('input[name="otro_impuesto"]').val(valor);
                    $('select[name="tipo_impuesto"]').val('otro');
                } else {
                    $('select[name="tipo_impuesto"]').val(valor);
                }

                tipoImpuesto();
            }

            $('select[name="tipo_impuesto"]').change(tipoImpuesto);

            $('input[name="otro_impuesto"]').change(function() {

                var tipoImpuesto = $('select[name="tipo_impuesto"]').val();

                if(tipoImpuesto === 'otro') {
                    setearImpuesto($(this).val());
                }
            });

            // INICIALIZAMOS EL IMPUESTO
            inicializarImpuesto();


            /** END IMPUESTOS **/
        });

    </script>

@endsection