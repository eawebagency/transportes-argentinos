@extends('master', ['seccionActiva' => 'Presupuestos'])

@section('titulo', (!empty($cliente)) ? 'Crear Presupuesto para '.$cliente->nombre.' '.$cliente->apellido : 'Crear Presupuesto')

@section('contenido')

    @if ($errors->any())
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-warning"></i>
            </div>
            <div class="m-alert__text">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    @if(isset($success))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check-circle"></i>
            </div>
            <div class="m-alert__text">
                {{ $success }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif

    {{ Form::open(['class' => 'form', 'files' => true]) }}

    {{ Form::hidden('valor_final', 0) }}
    {{ Form::hidden('cliente_existente', empty($cliente) ? 0 : 1) }}

    <div class="row">

        @if(empty($cliente))

            <div class="col-12">

                <div class="m-portlet">

                    <div class="m-portlet__body">
                        <div class="m-accordion m-accordion--default" role="tablist">
                            <div class="m-accordion__item" style="border: none">
                                <div class="m-accordion__item-head collapsed" role="tab" id="clienteCollapseLarga" data-toggle="collapse" href="#clienteCollapse" aria-expanded="false" style="cursor: pointer">
                                    {{--<span class="m-accordion__item-title">Cliente</span>--}}
                                    <h3 class="m-accordion__item-title">Cliente</h3>
                                    <span class="m-accordion__item-mode"></span>
                                </div>

                                <div class="m-accordion__item-body collapse show" id="clienteCollapse">
                                    <div class="row mt-3">
                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_nombre', 'Nombre') }}
                                                {{ Form::text('cliente_nombre', null, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_apellido', 'Apellido') }}
                                                {{ Form::text('cliente_apellido', null, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_email', 'Email') }}
                                                {{ Form::email('cliente_email', null, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_telefono', 'Telefono') }}
                                                {{ Form::text('cliente_telefono', null, ['class' => 'form-control input-telefono', 'required' => 'required']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_wpp', 'Whatsapp?') }}
                                                {{ Form::select('cliente_wpp', ['1' => 'Si', '0' => 'No'], null, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_telefono_alternativo', 'Teléfono alternativo') }}
                                                {{ Form::text('cliente_telefono_alternativo', null, ['class' => 'form-control input-telefono']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_contacto', 'Contacto') }}
                                                {{ Form::select('cliente_contacto', $formas, null, ['class' => 'form-control', 'required' => 'required' ,'placeholder' => 'Seleccionar forma de contacto']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_estado', 'Estado') }}
                                                {{ Form::select('cliente_estado', $estadosCliente, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar estado']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__foot">
                        <div class="m-form__actions">

                        </div>
                    </div>

                </div>

                {{--<div class="m-portlet">--}}

                    {{--<div class="m-portlet__body">--}}

                        {{--<h3>Cliente</h3>--}}

                        {{--<div class="row justify-content-center align-items-center">--}}
                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_nombre', 'Nombre') }}--}}
                                    {{--{{ Form::text('cliente_nombre', null, ['class' => 'form-control']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_apellido', 'Apellido') }}--}}
                                    {{--{{ Form::text('cliente_apellido', null, ['class' => 'form-control']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_email', 'Email') }}--}}
                                    {{--{{ Form::email('cliente_email', null, ['class' => 'form-control']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_telefono', 'Telefono') }}--}}
                                    {{--{{ Form::text('cliente_telefono', null, ['class' => 'form-control input-telefono', 'required' => 'required']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_wpp', 'Whatsapp?') }}--}}
                                    {{--{{ Form::select('cliente_wpp', ['1' => 'Si', '0' => 'No'], null, ['class' => 'form-control']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_telefono_alternativo', 'Teléfono alternativo') }}--}}
                                    {{--{{ Form::text('cliente_telefono_alternativo', null, ['class' => 'form-control input-telefono']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_contacto', 'Contacto') }}--}}
                                    {{--{{ Form::select('cliente_contacto', $formas, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar forma de contacto']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}

                {{--</div>--}}

            </div>

        @else

            <div class="col-12">

                <div class="m-portlet">

                    <div class="m-portlet__body">
                        <div class="m-accordion m-accordion--default" role="tablist">
                            <div class="m-accordion__item" style="border: none">
                                <div class="m-accordion__item-head collapsed" role="tab" id="clienteCollapseLarga" data-toggle="collapse" href="#clienteCollapse" aria-expanded="false" style="cursor: pointer">
                                    {{--<span class="m-accordion__item-title">Cliente</span>--}}
                                    <h3 class="m-accordion__item-title">Cliente</h3>
                                    <span class="m-accordion__item-mode"></span>
                                </div>

                                <div class="m-accordion__item-body collapse" id="clienteCollapse">
                                    <div class="row mt-3">
                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_nombre', 'Nombre') }}
                                                {{ Form::text('cliente_nombre', $cliente->nombre, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_apellido', 'Apellido') }}
                                                {{ Form::text('cliente_apellido', $cliente->apellido, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_email', 'Email') }}
                                                {{ Form::email('cliente_email', $cliente->email, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_telefono', 'Telefono') }}
                                                {{ Form::text('cliente_telefono', $cliente->telefono, ['class' => 'form-control input-telefono']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_wpp', 'Whatsapp?') }}
                                                {{ Form::select('cliente_wpp', ['1' => 'Si', '0' => 'No'], $cliente->wpp, ['class' => 'form-control']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_telefono_alternativo', 'Teléfono alternativo') }}
                                                {{ Form::text('cliente_telefono_alternativo', $cliente->telefono_alternativo, ['class' => 'form-control input-telefono']) }}
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                {{ Form::label('cliente_contacto', 'Contacto') }}
                                                {{ Form::select('cliente_contacto', $formas, $cliente->contacto, ['class' => 'form-control', 'placeholder' => 'Seleccionar forma de contacto']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__foot">
                        <div class="m-form__actions">

                        </div>
                    </div>

                </div>

                {{--<div class="m-portlet">--}}

                    {{--<div class="m-portlet__body">--}}

                        {{--<h3>Cliente</h3>--}}

                        {{--<div class="row justify-content-center align-items-center">--}}
                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_nombre', 'Nombre') }}--}}
                                    {{--{{ Form::text('cliente_nombre', $cliente->nombre, ['class' => 'form-control']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_apellido', 'Apellido') }}--}}
                                    {{--{{ Form::text('cliente_apellido', $cliente->apellido, ['class' => 'form-control']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_email', 'Email') }}--}}
                                    {{--{{ Form::email('cliente_email', $cliente->email, ['class' => 'form-control']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_telefono', 'Telefono') }}--}}
                                    {{--{{ Form::text('cliente_telefono', $cliente->telefono, ['class' => 'form-control input-telefono']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_wpp', 'Whatsapp?') }}--}}
                                    {{--{{ Form::select('cliente_wpp', ['1' => 'Si', '0' => 'No'], $cliente->wpp, ['class' => 'form-control']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_telefono_alternativo', 'Teléfono alternativo') }}--}}
                                    {{--{{ Form::text('cliente_telefono_alternativo', $cliente->telefono_alternativo, ['class' => 'form-control input-telefono']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-12 col-sm-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('cliente_contacto', 'Contacto') }}--}}
                                    {{--{{ Form::select('cliente_contacto', $formas, $cliente->contacto, ['class' => 'form-control', 'placeholder' => 'Seleccionar forma de contacto']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}

                {{--</div>--}}

            </div>

        @endif

            <div class="col-12">

                <div class="m-portlet">

                    <div class="m-portlet__body">
                        <div class="m-accordion m-accordion--default" role="tablist">
                            <div class="m-accordion__item" style="border: none">
                                <div class="m-accordion__item-head collapsed mt-3" role="tab" id="agregarEstadoCollapseLarga" data-toggle="collapse" href="#agregarEstadoCollapse" aria-expanded="false" style="cursor: pointer">
                                    <h3 class="m-accordion__item-title">Estado</h3>
                                    <span class="m-accordion__item-mode"></span>
                                </div>

                                <div class="m-accordion__item-body collapse show" id="agregarEstadoCollapse">
                                    <div class="row mt-3 justify-content-center">
                                        <div class="col-12 col-sm-8">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        {{ Form::label('estado', 'Estado') }}
                                                        {{ Form::select('estado', $estados, null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar estado']) }}
                                                    </div>
                                                </div>

                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        {{ Form::label('fecha_estado', 'Fecha') }}
                                                        {{ Form::date('fecha_estado', null, ['class' => 'form-control']) }}
                                                    </div>
                                                </div>

                                                <div class="col-12 col-sm-3">
                                                    <div class="form-group">
                                                        {{ Form::label('estado_hora_desde', 'Desde') }}
                                                        {{ Form::text('estado_hora_desde', null, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ej.: 10:30)' ]) }}
                                                    </div>
                                                </div>

                                                <div class="col-12 col-sm-3">
                                                    <div class="form-group">
                                                        {{ Form::label('estado_hora_hasta', 'Hasta') }}
                                                        {{ Form::text('estado_hora_hasta', null, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ej.: 10:30)' ]) }}
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group">
                                                        {{ Form::label('estado_observaciones', 'Observaciones') }}
                                                        {{ Form::textarea('estado_observaciones', null, ['class' => 'form-control input-hora' ]) }}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="m-portlet__foot">
                        <div class="m-form__actions">

                        </div>
                    </div>

                </div>

                {{--<div class="m-portlet">--}}

                    {{--<div class="m-portlet__body">--}}

                        {{--<h3>Estado</h3>--}}

                        {{--<div class="row justify-content-center">--}}

                            {{--<div class="col-12 col-sm-8">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-12">--}}
                                        {{--<div class="form-group">--}}
                                            {{--{{ Form::label('estado', 'Estado') }}--}}
                                            {{--{{ Form::select('estado', $estados, null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar estado']) }}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-12 col-sm-6">--}}
                                        {{--<div class="form-group">--}}
                                            {{--{{ Form::label('fecha_estado', 'Fecha') }}--}}
                                            {{--{{ Form::date('fecha_estado', null, ['class' => 'form-control']) }}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-12 col-sm-3">--}}
                                        {{--<div class="form-group">--}}
                                            {{--{{ Form::label('estado_hora_desde', 'Desde') }}--}}
                                            {{--{{ Form::text('estado_hora_desde', null, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ej.: 10:30)' ]) }}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-12 col-sm-3">--}}
                                        {{--<div class="form-group">--}}
                                            {{--{{ Form::label('estado_hora_hasta', 'Hasta') }}--}}
                                            {{--{{ Form::text('estado_hora_hasta', null, ['class' => 'form-control input-hora', 'placeholder' => 'HH:MM (Ej.: 10:30)' ]) }}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-12">--}}
                                        {{--<div class="form-group">--}}
                                            {{--{{ Form::label('estado_observaciones', 'Observaciones') }}--}}
                                            {{--{{ Form::textarea('estado_observaciones', null, ['class' => 'form-control input-hora' ]) }}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}

                    {{--<div class="m-portlet__foot">--}}
                        {{--<div class="m-form__actions">--}}

                        {{--</div>--}}
                    {{--</div>--}}

                {{--</div>--}}
            </div>

        <div class="col-12 col-sm-6">
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <h3>Carga - <button type="button" class="btn btn-sm btn-accent" id="agregarDireccionDeCarga">Agregar dirección de carga</button></h3>

                    <div class="m-accordion m-accordion--default" id="direccionesDeCarga" role="tablist">

                        <?php $contadorCarga = 0; ?>

                        @for($i = 0; $i < Session::getOldInput('cantidad_direcciones_carga'); $i++)
                                <div class="m-accordion__item">
                                    <div class="m-accordion__item-head collapsed" role="tab" id="direccionDeCargaLarga-{{ $contadorCarga }}" data-toggle="collapse" href="#direccionDeCarga-{{ $contadorCarga }}" aria-expanded="false">
                                        <span class="m-accordion__item-title">Dirección {{ $contadorCarga + 1 }}</span>
                                        <span class="m-accordion__item-mode"></span>
                                    </div>
                                    <div class="m-accordion__item-body collapse show" id="direccionDeCarga-1" role="tabpanel" aria-labelledby="direccionDeCargaLarga-{{ $contadorCarga }}">
                                        <div class="m-accordion__item-content">
                                            <div class="row">
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="direccion_carga[]">Dirección</label>
                                                        <input type="text" class="form-control" name="direccion_carga[]" value="{{ Session::getOldInput('direccion_carga')[$i] }}">
                                                    </div>
                                                </div>

                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="piso_carga[]">Piso</label>
                                                        <input type="text" class="form-control" name="piso_carga[]" required="required" value="{{ Session::getOldInput('piso_carga')[$i] }}">
                                                    </div>
                                                </div>

                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="departamento_carga[]">Departamento</label>
                                                        <input type="text" class="form-control" name="departamento_carga[]" required="required" value="{{ Session::getOldInput('departamento_carga')[$i] }}">
                                                    </div>
                                                </div>

                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="entrecalles_carga[]">Entrecalles</label>
                                                        <input type="text" class="form-control" name="entrecalles_carga[]" value="{{ Session::getOldInput('entrecalles_carga')[$i] }}">
                                                    </div>
                                                </div>

                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="provincia_carga[]">Provincia</label>
                                                        <select name="provincia_carga[]" class="form-control" required="required">
                                                            <option value="">Selecciona una provincia</option>
                                                            <option value="Ciudad Autonoma de Buenos Aires" @if(Session::getOldInput('provincia_carga')[$i] == "Ciudad Autonoma de Buenos Aires") selected='selected' @endif>Ciudad Autonoma de Buenos Aires</option>
                                                            <option value="Buenos Aires" @if(Session::getOldInput('provincia_carga')[$i] == "Buenos Aires") selected='selected' @endif>Buenos Aires</option>
                                                            <option value="Catamarca" @if(Session::getOldInput('provincia_carga')[$i] == "Catamarca") selected='selected' @endif>Catamarca</option>
                                                            <option value="Chaco" @if(Session::getOldInput('provincia_carga')[$i] == "Chaco") selected='selected' @endif>Chaco</option>
                                                            <option value="Chubut" @if(Session::getOldInput('provincia_carga')[$i] == "Chubut") selected='selected' @endif>Chubut</option>
                                                            <option value="Cordoba" @if(Session::getOldInput('provincia_carga')[$i] == "Cordoba") selected='selected' @endif>Cordoba</option>
                                                            <option value="Corrientes" @if(Session::getOldInput('provincia_carga')[$i] == "Corrientes") selected='selected' @endif>Corrientes</option>
                                                            <option value="Entre Rios" @if(Session::getOldInput('provincia_carga')[$i] == "Entre Rios") selected='selected' @endif>Entre Rios</option>
                                                            <option value="Formosa" @if(Session::getOldInput('provincia_carga')[$i] == "Formosa") selected='selected' @endif>Formosa</option>
                                                            <option value="Jujuy" @if(Session::getOldInput('provincia_carga')[$i] == "Jujuy") selected='selected' @endif>Jujuy</option>
                                                            <option value="La Pampa" @if(Session::getOldInput('provincia_carga')[$i] == "La Pampa") selected='selected' @endif>La Pampa</option>
                                                            <option value="La Rioja" @if(Session::getOldInput('provincia_carga')[$i] == "La Rioja") selected='selected' @endif>La Rioja</option>
                                                            <option value="Mendoza" @if(Session::getOldInput('provincia_carga')[$i] == "Mendoza") selected='selected' @endif>Mendoza</option>
                                                            <option value="Misiones" @if(Session::getOldInput('provincia_carga')[$i] == "Misiones") selected='selected' @endif>Misiones</option>
                                                            <option value="Neuquen" @if(Session::getOldInput('provincia_carga')[$i] == "Neuquen") selected='selected' @endif>Neuquen</option>
                                                            <option value="Rio Negro" @if(Session::getOldInput('provincia_carga')[$i] == "Rio Negro") selected='selected' @endif>Rio Negro</option>
                                                            <option value="Salta" @if(Session::getOldInput('provincia_carga')[$i] == "Salta") selected='selected' @endif>Salta</option>
                                                            <option value="San Juan" @if(Session::getOldInput('provincia_carga')[$i] == "San Juan") selected='selected' @endif>San Juan</option>
                                                            <option value="San Luis" @if(Session::getOldInput('provincia_carga')[$i] == "San Luis") selected='selected' @endif>San Luis</option>
                                                            <option value="Santa Cruz" @if(Session::getOldInput('provincia_carga')[$i] == "Santa Cruz") selected='selected' @endif>Santa Cruz</option>
                                                            <option value="Santa Fe" @if(Session::getOldInput('provincia_carga')[$i] == "Santa Fe") selected='selected' @endif>Santa Fe</option>
                                                            <option value="Santiago del Estero" @if(Session::getOldInput('provincia_carga')[$i] == "Santiago del Estero") selected='selected' @endif>Santiago del Estero</option>
                                                            <option value="Tierra del Fuego" @if(Session::getOldInput('provincia_carga')[$i] == "Tierra del Fuego") selected='selected' @endif>Tierra del Fuego</option>
                                                            <option value="Tucuman" @if(Session::getOldInput('provincia_carga')[$i] == "Tucuman") selected='selected' @endif>Tucuman</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="localidad_carga[]">Localidad</label>
                                                        <select class="form-control" name="localidad_carga[]" required="required" localidad-seleccionada="{{ Session::getOldInput('localidad_carga')[$i] }}"></select>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="observaciones_carga[]">Observaciones</label>
                                                        <textarea class="form-control" name="observaciones_carga[]">{!! Session::getOldInput('observaciones_carga')[$i] !!}</textarea>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="float-left">
                                                                <button type="button" class="btn btn-outline-danger eliminarDireccion">REMOVER DIRECCION</button>
                                                            </div>
                                                        </div>

                                                        <div class="col-6">
                                                            <div class="float-right">
                                                                <button type="button" class="btn btn-outline-success verDireccionEnMapa">VER MAPA</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php $contadorCarga++; ?>
                        @endfor

                    </div>

                </div>

            </div>

        </div>

        <div class="col-12 col-sm-6">
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <h3>Descarga - <button type="button" class="btn btn-sm btn-accent" id="agregarDireccionDeDescarga">Agregar dirección de descarga</button></h3>

                    <div class="m-accordion m-accordion--default" id="direccionesDeDescarga" role="tablist">

                        <?php $contadorDescarga = 0; ?>

                            @for($i = 0; $i < Session::getOldInput('cantidad_direcciones_descarga'); $i++)

                            <div class="m-accordion__item">
                                <div class="m-accordion__item-head collapsed" role="tab" id="direccionDeDescargaLarga-{{ $contadorDescarga }}" data-toggle="collapse" href="#direccionDeDescarga-{{ $contadorDescarga }}" aria-expanded="false">
                                    <span class="m-accordion__item-title">Dirección {{ $contadorDescarga + 1 }}</span>
                                    <span class="m-accordion__item-mode"></span>
                                </div>
                                <div class="m-accordion__item-body collapse show" id="direccionDeDescarga-1" role="tabpanel" aria-labelledby="direccionDeDescargaLarga-{{ $contadorDescarga }}">
                                    <div class="m-accordion__item-content">
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="direccion_descarga[]">Dirección</label>
                                                    <input type="text" class="form-control" name="direccion_descarga[]" value="{{ Session::getOldInput('direccion_descarga')[$i] }}">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="piso_descarga[]">Piso</label>
                                                    <input type="text" class="form-control" name="piso_descarga[]" value="{{ Session::getOldInput('piso_descarga')[$i] }}" required="required">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="departamento_descarga[]">Departamento</label>
                                                    <input type="text" class="form-control" name="departamento_descarga[]" value="{{ Session::getOldInput('departamento_descarga')[$i] }}" required="required">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="entrecalles_descarga[]">Entrecalles</label>
                                                    <input type="text" class="form-control" name="entrecalles_descarga[]" value="{{ Session::getOldInput('entrecalles_descarga')[$i] }}">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="provincia_descarga[]">Provincia</label>
                                                    <select name="provincia_descarga[]" class="form-control" required="required">
                                                        <option value="">Selecciona una provincia</option>
                                                        <option value="Ciudad Autonoma de Buenos Aires" @if(Session::getOldInput('provincia_descarga')[$i] == "Ciudad Autonoma de Buenos Aires") selected='selected' @endif>Ciudad Autonoma de Buenos Aires</option>
                                                        <option value="Buenos Aires" @if(Session::getOldInput('provincia_descarga')[$i] == "Buenos Aires") selected='selected' @endif>Buenos Aires</option>
                                                        <option value="Catamarca" @if(Session::getOldInput('provincia_descarga')[$i] == "Catamarca") selected='selected' @endif>Catamarca</option>
                                                        <option value="Chaco" @if(Session::getOldInput('provincia_descarga')[$i] == "Chaco") selected='selected' @endif>Chaco</option>
                                                        <option value="Chubut" @if(Session::getOldInput('provincia_descarga')[$i] == "Chubut") selected='selected' @endif>Chubut</option>
                                                        <option value="Cordoba" @if(Session::getOldInput('provincia_descarga')[$i] == "Cordoba") selected='selected' @endif>Cordoba</option>
                                                        <option value="Corrientes" @if(Session::getOldInput('provincia_descarga')[$i] == "Corrientes") selected='selected' @endif>Corrientes</option>
                                                        <option value="Entre Rios" @if(Session::getOldInput('provincia_descarga')[$i] == "Entre Rios") selected='selected' @endif>Entre Rios</option>
                                                        <option value="Formosa" @if(Session::getOldInput('provincia_descarga')[$i] == "Formosa") selected='selected' @endif>Formosa</option>
                                                        <option value="Jujuy" @if(Session::getOldInput('provincia_descarga')[$i] == "Jujuy") selected='selected' @endif>Jujuy</option>
                                                        <option value="La Pampa" @if(Session::getOldInput('provincia_descarga')[$i] == "La Pampa") selected='selected' @endif>La Pampa</option>
                                                        <option value="La Rioja" @if(Session::getOldInput('provincia_descarga')[$i] == "La Rioja") selected='selected' @endif>La Rioja</option>
                                                        <option value="Mendoza" @if(Session::getOldInput('provincia_descarga')[$i] == "Mendoza") selected='selected' @endif>Mendoza</option>
                                                        <option value="Misiones" @if(Session::getOldInput('provincia_descarga')[$i] == "Misiones") selected='selected' @endif>Misiones</option>
                                                        <option value="Neuquen" @if(Session::getOldInput('provincia_descarga')[$i] == "Neuquen") selected='selected' @endif>Neuquen</option>
                                                        <option value="Rio Negro" @if(Session::getOldInput('provincia_descarga')[$i] == "Rio Negro") selected='selected' @endif>Rio Negro</option>
                                                        <option value="Salta" @if(Session::getOldInput('provincia_descarga')[$i] == "Salta") selected='selected' @endif>Salta</option>
                                                        <option value="San Juan" @if(Session::getOldInput('provincia_descarga')[$i] == "San Juan") selected='selected' @endif>San Juan</option>
                                                        <option value="San Luis" @if(Session::getOldInput('provincia_descarga')[$i] == "San Luis") selected='selected' @endif>San Luis</option>
                                                        <option value="Santa Cruz" @if(Session::getOldInput('provincia_descarga')[$i] == "Santa Cruz") selected='selected' @endif>Santa Cruz</option>
                                                        <option value="Santa Fe" @if(Session::getOldInput('provincia_descarga')[$i] == "Santa Fe") selected='selected' @endif>Santa Fe</option>
                                                        <option value="Santiago del Estero" @if(Session::getOldInput('provincia_descarga')[$i] == "Santiago del Estero") selected='selected' @endif>Santiago del Estero</option>
                                                        <option value="Tierra del Fuego" @if(Session::getOldInput('provincia_descarga')[$i] == "Tierra del Fuego") selected='selected' @endif>Tierra del Fuego</option>
                                                        <option value="Tucuman" @if(Session::getOldInput('provincia_descarga')[$i] == "Tucuman") selected='selected' @endif>Tucuman</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <label for="localidad_descarga[]">Localidad</label>
                                                    <select class="form-control" name="localidad_descarga[]" localidad-seleccionada="{{ Session::getOldInput('localidad_descarga')[$i] }}" required="required"></select>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="observaciones_descarga[]">Observaciones</label>
                                                    <textarea class="form-control" name="observaciones_descarga[]">{!! Session::getOldInput('observaciones_descarga')[$i] !!}</textarea>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="float-left">
                                                            <button type="button" class="btn btn-outline-danger eliminarDireccion">REMOVER DIRECCION</button>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="float-right">
                                                            <button type="button" class="btn btn-outline-success verDireccionEnMapa">VER MAPA</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php $contadorDescarga++; ?>

                        @endfor

                    </div>

                </div>

            </div>

        </div>

            {{ Form::hidden('cantidad_direcciones_carga', $contadorCarga) }}
            {{ Form::hidden('cantidad_direcciones_descarga', $contadorDescarga) }}

        <div class="col-12 col-sm-6">
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <h3>Detalles</h3>

                    <div class="row justify-content-center align-items-center">
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::label('servicio', 'Servicio') }}
                                {{ Form::select('servicio', $servicios, null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar servicio']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::label('vehiculo', 'Vehículo') }}
                                {{ Form::select('vehiculo', $vehiculos, null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccionar vehículo']) }}
                                <small id="informacionDelVehiculo"></small>
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::label('cotizacion', 'Valor aproximado') }}
                                {{ Form::text('cotizacion', null, ['class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>

                        {{--<div class="col-12 col-sm-4">--}}
                            {{--<div class="form-check">--}}
                                {{--{{ Form::checkbox('larga_distancia', true, null, ['class' => 'form-check-input']) }}--}}
                                {{--{{ Form::label('larga_distancia', 'Larga distancia', ['class' => 'form-check-label']) }}--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::checkbox('larga_distancia', true, null, ['class' => 'form-check-input']) }} {{ Form::label('kms', 'Kms a recorrer') }}
                                {{ Form::number('kms', null, ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::label('ambientes', 'Ambientes') }}
                                {{ Form::number('ambientes', null, ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                {{ Form::label('personal', 'Personal') }}
                                {{ Form::number('personal', null, ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('tipo_impuesto', 'Tipo de Impuesto') }}
                                {{ Form::select('tipo_impuesto', [
                                    0 => 'Ninguno',
                                    21 => '21%',
                                    'otro' => 'Otro'
                                ], 0, ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('otro_impuesto', 'Impuesto') }}
                                {{ Form::number('otro_impuesto', null, ['class' => 'form-control', 'disabled' => 'disabled', 'required' => 'required']) }}
                            </div>
                        </div>

                        {{ Form::hidden('impuesto', 0) }}

                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('fecha', 'Fecha de Inicio') }}
                                {{ Form::date('fecha', null, ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('hora', 'Hora de Inicio') }}
                                {{ Form::text('hora', null, ['class' => 'form-control input-hora', 'placeholder' => 'hh:mm (Ejemplo: 20:30)']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('fecha_fin', 'Fecha de Fin') }}
                                {{ Form::date('fecha_fin', null, ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                {{ Form::label('hora_fin', 'Hora de Fin') }}
                                {{ Form::text('hora_fin', null, ['class' => 'form-control input-hora', 'placeholder' => 'hh:mm (Ejemplo: 20:30)']) }}
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                {{ Form::label('vencimiento', 'Vencimiento') }}
                                {{ Form::date('vencimiento', \Carbon\Carbon::now()->addDays(20)->format('Y-m-d'), ['class' => 'form-control', 'required' => 'required']) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                @if(!empty($cliente))
                                    {{ Form::hidden('cliente', $cliente->id) }}
                                @endif
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                {{ Form::label('observaciones', 'Observaciones generales') }}
                                {{ Form::textarea('observaciones', null, ['class' => 'form-control']) }}
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="col-12 col-sm-6">
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <h3>Adicionales</h3>

                    <div class="mb-3" style="cursor: pointer" id="agregarAdicional">
                        <span class="m-badge m-badge--danger">+</span> Agregar nuevo adicional
                    </div>

                    <div id="otrosAdicionales">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Valor</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="col-12">
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <h3>Cotización</h3>


                    <div id="cotizacionCompleta">

                    </div>

                </div>

            </div>

        </div>

        <div class="col-12">
            <div class="m-portlet__foot">
                <div class="m-form__actions">
                    {{ Form::submit('Guardar', ['class' => 'col-2 btn btn-primary', 'style' => 'position: fixed;bottom: 0;margin-bottom: 1rem;float: right;left: 80%;z-index:99999']) }}
                </div>
            </div>
        </div>
    </div>

    <div id="adicionales-modals">

    </div>

    {{ Form::close() }}

    <div class="modal fade" id="modalMapa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Mapa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalAgregarAdicional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar adicional</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ Form::open() }}
                        <div class="row">
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {{ Form::label('nombre', 'Nombre') }}
                                    {{ Form::text('nombre', null, ['class' => 'form-control awesomplete', 'required' => 'required']) }}
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {{ Form::label('valor', 'Valor') }}
                                    {{ Form::text('valor', null, ['class' => 'form-control', 'required' => 'required']) }}
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {{ Form::label('fecha', 'Fecha') }}
                                    {{ Form::date('fecha', null, ['class' => 'form-control']) }}
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {{ Form::label('hora_inicio', 'Hora de Inicio') }}
                                    {{ Form::text('hora_inicio', null, ['class' => 'form-control']) }}
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {{ Form::label('hora_fin', 'Hora de Fin') }}
                                    {{ Form::text('hora_fin', null, ['class' => 'form-control']) }}
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {{ Form::label('personal', 'Personal') }}
                                    {{ Form::number('personal', null, ['class' => 'form-control']) }}
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {{ Form::label('duracion', 'Duración') }}
                                    {{ Form::text('duracion', null, ['class' => 'form-control']) }}
                                </div>
                            </div>

                            <div class="col-12 col-sm-8">
                                <div class="form-group">
                                    {{ Form::label('observaciones', 'Observaciones') }}
                                    {{ Form::textarea('observaciones', null, ['class' => 'form-control']) }}
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-actions">
                                    {{ Form::submit('GUARDAR', ['class' => 'btn btn-success']) }}
                                </div>
                            </div>

                        </div>
                    {{ Form::close() }}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('adminAssets/vendors/custom/bootstrap-notify/bootstrap-notify.min.js') }}" type="text/javascript"></script>

    <script>
        $(document).ready(function() {

            function cambiarKms(estado) {
                $('input[name="kms"]').attr('readonly', estado);
            }

            function cargarInformacionVehiculo(vehiculo) {

                $.get('/ajax/vehiculo/' + vehiculo, function(data) {

                    $("#informacionDelVehiculo").empty().append(data.descripcion);

                });
            }

            /** DATOS DEL PRESUPUESTO **/

            function obtenerServicio() {
                var servicio = $('select[name="servicio"]').val();

                if(servicio != null && servicio != "") {
                    return servicio;
                }

                return null;
            }

            function obtenerVehiculo() {
                var vehiculo = $('select[name="vehiculo"]').val();

                if(vehiculo != null && vehiculo != "") {

                    // if($('select[name="servicio"]').val() == 1 || $('select[name="servicio"]').val() == 2 || $('select[name="servicio"]').val() == 5 || $('select[name="servicio"]').val() == 6) {
                    //     cargarInformacionVehiculo(vehiculo);
                    // } else {
                    //     $("#informacionDelVehiculo").empty();
                    // }

                    cargarInformacionVehiculo(vehiculo);

                    return vehiculo;
                }

                return null;
            }

            function obtenerKilometros() {
                var kilometros = $('input[name="kms"]').val();

                if(kilometros != null && kilometros != "") {
                    return kilometros;
                }

                return 0;
            }

            function obtenerAdicionales() {
                var adicionales = [];
                $('input[name="adicionales[]"]:checked').each(function(i){
                    adicionales[i] = $(this).val();
                });

                return adicionales;
            }

            function obtenerOtrosAdicionales() {
                var otros_adicionales = [];
                $('input[name="adicionales_valor[]"]').each(function(i) {
                    otros_adicionales[i] = {nombre: $($('input[name="adicionales_nombre[]"]').toArray()[i]).val(), valor: $(this).val()};
                });

                return otros_adicionales;
            }

            function obtenerLargaDistancia() {

                var larga_distancia = 0;
                var estado = true;

                if ($('input[name="larga_distancia"]').is(':checked')) {
                    estado = false;
                    larga_distancia = 1;
                }

                cambiarKms(estado);

                return larga_distancia;

            }

            function obtenerValorAproximado_input() {
                var valor_aproximado = $('input[name="cotizacion"]').val();

                if(valor_aproximado != null && valor_aproximado != "") {
                    return valor_aproximado;
                }

                return 0;
            }

            function obtenerImpuesto() {

                var impuesto = $('input[name="impuesto"]').val();

                if(impuesto === '' || impuesto === null) {
                    impuesto = 0;
                }

                return impuesto;
            }

            /** END DATOS DEL PRESUPUESTO **/

            function obtenerCotizacion() {
                var servicio = obtenerServicio();
                var vehiculo = obtenerVehiculo();
                var kilometros = obtenerKilometros();
                var adicionales = obtenerAdicionales();
                var otros_adicionales = obtenerOtrosAdicionales();
                var larga_distancia = obtenerLargaDistancia();
                var valor_aproximado = obtenerValorAproximado_input();
                var impuesto = obtenerImpuesto();

                $.ajax({
                    method: "POST",
                    url: "/ajax/cotizar",
                    data: { servicio: servicio, vehiculo: vehiculo, kilometros: kilometros, adicionales: adicionales, otros_adicionales: otros_adicionales, larga_distancia: larga_distancia, valor_aproximado: valor_aproximado, impuesto: impuesto }
                }).done(function(data) {

                    $("#cotizacionCompleta").empty().append(data.cotizacion); // HTML DE LA COTIZACION
                    $('input[name="valor_final"]').val(data.valor_final); // PONEMOS EL VALOR FINAL

                }).fail(function() {

                    $.notify({
                        message: '<strong>¡Error!</strong> No se pudo obtener la cotización, revise la información'
                    },{
                        type: 'danger'
                    });

                });
            }

            function obtenerValorAproximado() {
                var servicio = obtenerServicio();
                var vehiculo = obtenerVehiculo();
                var kilometros = obtenerKilometros();
                var larga_distancia = obtenerLargaDistancia();

                $.ajax({
                    method: "POST",
                    url: "/ajax/valorAproximado",
                    data: { servicio: servicio, vehiculo: vehiculo, kilometros: kilometros, larga_distancia: larga_distancia }
                }).done(function(data) {

                    $('input[name="cotizacion"]').val(data.valor_aproximado); // PONEMOS EL VALOR APROXIMADO
                    obtenerCotizacion();

                }).fail(function() {

                    $.notify({
                        message: '<strong>¡Error!</strong> No se pudo obtener la cotización, revise la información'
                    },{
                        type: 'danger'
                    });

                });
            }

            /** EVENTOS **/

            $('input[name="adicionales[]"]').change(function() {
                obtenerCotizacion();
            });

            $('select[name="servicio"]').change(function() {
                obtenerValorAproximado();
            });

            $('select[name="vehiculo"]').change(function() {
                obtenerValorAproximado();
            });

            $('input[name="kms"]').on('input',function(e){
                obtenerValorAproximado();
            });

            $('input[name="larga_distancia"]').change(function(e){
                obtenerValorAproximado();
            });

            $('input[name="cotizacion"]').change(function(e){
                obtenerCotizacion();
            });

            // AGREGAR ADICIONAL

            var cantidadAdicionales = 0;

            $('#agregarAdicional').click(function() {

                $('#modalAgregarAdicional').modal('show');
            });

            $('#modalAgregarAdicional form').submit(function(e) {

                e.preventDefault();

                var nombre = $($(this).find('input[name="nombre"]')[0]).val();
                var valor = $($(this).find('input[name="valor"]')[0]).val();
                var fecha = $($(this).find('input[name="fecha"]')[0]).val();
                var hora_inicio = $($(this).find('input[name="hora_inicio"]')[0]).val();
                var hora_fin = $($(this).find('input[name="hora_fin"]')[0]).val();
                var personal = $($(this).find('input[name="personal"]')[0]).val();
                var duracion = $($(this).find('input[name="duracion"]')[0]).val();
                var observaciones = $($(this).find('textarea[name="observaciones"]')[0]).val();

                $('#adicionales-modals').append(
                    '<div class="modal fade" id="adicional-'+ cantidadAdicionales + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">\n' +
                    '        <div class="modal-dialog modal-lg" role="document">\n' +
                    '            <div class="modal-content">\n' +
                    '                <div class="modal-header">\n' +
                    '                    <h5 class="modal-title" id="exampleModalLabel">Adicional</h5>\n' +
                    '                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
                    '                        <span aria-hidden="true">×</span>\n' +
                    '                    </button>\n' +
                    '                </div>\n' +
                    '                <div class="modal-body">\n' +
                    '                    <div class="row">\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_nombre[]">Nombre</label>\n' +
                    '                                <input class="form-control awesomplete" name="adicionales_nombre[]" type="text" value="' + nombre + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_valor[]">Valor</label>\n' +
                    '                                <input class="form-control" name="adicionales_valor[]" type="text" value="' + valor + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_fecha[]">Fecha</label>\n' +
                    '                                <input class="form-control" name="adicionales_fecha[]" type="date" value="' + fecha + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_hora_inicio[]">Hora de Inicio</label>\n' +
                    '                                <input class="form-control" name="adicionales_hora_inicio[]" type="text" value="' + hora_inicio + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_hora_fin[]">Hora de Fin</label>\n' +
                    '                                <input class="form-control" name="adicionales_hora_fin[]" type="text" value="' + hora_fin + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_personal[]">Personal</label>\n' +
                    '                                <input class="form-control" name="adicionales_personal[]" type="number" value="' + personal + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-4">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_duracion[]">Duración</label>\n' +
                    '                                <input class="form-control" name="adicionales_duracion[]" type="text" value="' + duracion + '">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12 col-sm-8">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="adicionales_observaciones[]">Observaciones</label>\n' +
                    '                                <textarea class="form-control" name="adicionales_observaciones[]" cols="50" rows="10" id="adicionales_observaciones_' + cantidadAdicionales + '">' + observaciones + '</textarea>\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-12">\n' +
                    '                            <div class="form-actions">\n' +
                    '                                <button type="button" class="btn btn-success">GUARDAR</button>\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                    </div>\n' +
                    '                </div>\n' +
                    '                <div class="modal-footer">\n' +
                    '                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>\n' +
                    '                </div>\n' +
                    '            </div>\n' +
                    '        </div>\n' +
                    '    </div>'
                );

                var cleaveInputHoraInicio = new Cleave($('#adicional-'+ cantidadAdicionales + ' input[name="adicionales_hora_inicio[]"]'), {
                    time: true,
                    timePattern: ['h', 'm']
                });

                var cleaveInputHoraFin = new Cleave($('#adicional-'+ cantidadAdicionales + ' input[name="adicionales_hora_fin[]"]'), {
                    time: true,
                    timePattern: ['h', 'm']
                });

                cantidadAdicionales++;

                cargarTablaAdicionales();
                cargarSubmitModalAdicional();
                cargarCKEditor();
                cargarAutocompletado();

                $($(this).find('input[name="nombre"]')[0]).val('');
                $($(this).find('input[name="valor"]')[0]).val('');
                $($(this).find('input[name="fecha"]')[0]).val('');
                $($(this).find('input[name="hora_inicio"]')[0]).val('');
                $($(this).find('input[name="hora_fin"]')[0]).val('');
                $($(this).find('input[name="personal"]')[0]).val('');
                $($(this).find('input[name="duracion"]')[0]).val('');
                $($(this).find('textarea[name="observaciones"]')[0]).val('');

                $('#modalAgregarAdicional').modal('hide');

                $.notify({
                    message: '<strong>¡Adicional agregado con éxito!</strong>'
                },{
                    type: 'success'
                });

            });

            function cargarTablaAdicionales() {

                var tabla = $('#otrosAdicionales table').DataTable();
                tabla.clear().draw();

                $('#adicionales-modals .modal').each(function(index, modal) {
                    var id = $(this).attr('id').split('adicional-')[1];
                    var nombre = $($(modal).find('input[name="adicionales_nombre[]"]')[0]).val();
                    var valor = $($(modal).find('input[name="adicionales_valor[]"]')[0]).val();

                    tabla.row.add([
                        nombre,
                        valor,
                        '<button type="button" class="btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar" data-accion="editar" data-id="' + id + '">\n' +
                        '                                                <i class="la la-edit"></i>\n' +
                        '                                            </button>\n' +
                        '                                            <button type="button" class="btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" data-accion="eliminar" data-id="' + id + '">\n' +
                        '                                                <i class="la la-trash"></i>\n' +
                        '                                            </button>\n',
                    ]).draw( false );

                    cargarBotonesTablaAdicional();

                });

                obtenerCotizacion();
            }

            function cargarSubmitModalAdicional() {

                $('#adicionales-modals .modal .form-actions button').click(function() {

                    $(this).parent().parent().parent().parent().parent().parent().parent().modal('hide');

                    cargarTablaAdicionales();

                });

            }

            function cargarBotonesTablaAdicional() {
                $('#otrosAdicionales button').unbind().click(function() {

                    var accion = $(this).attr('data-accion');
                    var id = $(this).attr('data-id');

                    if(accion == "editar") {

                        $('#adicional-' + id).modal('show');

                    } else if(accion == "eliminar") {

                        if(confirm('Está seguro que desea eliminar este elemento?')) {
                            $('#adicional-' + id).remove();
                            cargarTablaAdicionales();
                        }

                    }
                });
            }

            function cargarAutocompletado() {

                $.ajax({
                    method: "GET",
                    url: "/items_adicionales/ver",
                }).done(function(data) {

                    var adicionalesNombre = [];

                    data.forEach(function(valor, index) {
                        adicionalesNombre.push(valor.nombre);
                    });

                    if($('input[name="adicionales_nombre[]"]').length) {
                        $.each($('input[name="adicionales_nombre[]"]'), function(indexInput, input) {
                            new Awesomplete($('input[name="adicionales_nombre[]"]')[indexInput], {
                                list: adicionalesNombre
                            });

                            $($('input[name="adicionales_nombre[]"]')[indexInput]).on('awesomplete-selectcomplete', function() {

                                var nombreAdicional = this.value;

                                data.forEach(function(valor, index) {

                                    if(valor.nombre == nombreAdicional) {
                                        CKEDITOR.instances['adicionales_observaciones_' + (indexInput)].setData(valor.descripcion);
                                    }
                                });
                            });
                        });
                    }

                    new Awesomplete('#modalAgregarAdicional input[name="nombre"]', {
                        list: adicionalesNombre
                    });

                    $('#modalAgregarAdicional input[name="nombre"]').on('awesomplete-selectcomplete', function() {
                        var nombreAdicional = this.value;

                        data.forEach(function(valor, index) {

                            if(valor.nombre == nombreAdicional) {
                                CKEDITOR.instances['observaciones'].setData(valor.descripcion);
                            }
                        });
                    });

                }).fail(function() {

                    $.notify({
                        message: '<strong>¡Error!</strong> No se pudo obtener los adicionales'
                    },{
                        type: 'danger'
                    });

                });
            }

            // END AGREGAR ADICIONAL

            // DIRECCIONES

            var direccionesCarga = $('input[name="cantidad_direcciones_carga"]').val();
            var direccionesDescarga = $('input[name="cantidad_direcciones_descarga"]').val();
            var primeraDireccionCarga = "show";
            var primeraDireccionDescarga = "show";

            $("#agregarDireccionDeCarga").click(function(data) {
                direccionesCarga++;

                $("#direccionesDeCarga").append('<div class="m-accordion__item itemDireccionDeCarga">\n' +
                    '                            <div class="m-accordion__item-head collapsed" role="tab" id="direccionDeCargaLarga-'+ direccionesCarga +'" data-toggle="collapse" href="#direccionDeCarga-'+ direccionesCarga +'" aria-expanded="false">\n' +
                    '                                <span class="m-accordion__item-title">Dirección '+ direccionesCarga +'</span>\n' +
                    '                                <span class="m-accordion__item-mode"></span>\n' +
                    '                            </div>\n' +
                    '                            <div class="m-accordion__item-body collapse ' + primeraDireccionCarga + '" id="direccionDeCarga-'+ direccionesCarga +'" role="tabpanel" aria-labelledby="direccionDeCargaLarga-'+ direccionesCarga +'">\n' +
                    '                                <div class="m-accordion__item-content">\n' +
                    '                                    <div class="row">\n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="direccion_carga[]">Dirección</label>\n' +
                    '                                                <input type="text" class="form-control" name="direccion_carga[]">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="piso_carga[]">Piso</label>\n' +
                    '                                                <input type="text" class="form-control" name="piso_carga[]" required="required">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="departamento_carga[]">Departamento</label>\n' +
                    '                                                <input type="text" class="form-control" name="departamento_carga[]" required="required">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="entrecalles_carga[]">Entrecalles</label>\n' +
                    '                                                <input type="text" class="form-control" name="entrecalles_carga[]">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="provincia_carga[]">Provincia</label>\n' +
                    '                                                <select name="provincia_carga[]" class="form-control" required="required">\n' +
                    '                                                <option selected="selected" value="">Selecciona una provincia</option>\n' +
                    '                                                <option value="Ciudad Autonoma de Buenos Aires">Ciudad Autonoma de Buenos Aires</option>\n' +
                    '                                                <option value="Buenos Aires">Buenos Aires</option>\n' +
                    '                                                <option value="Catamarca">Catamarca</option>\n' +
                    '                                                <option value="Chaco">Chaco</option>\n' +
                    '                                                <option value="Chubut">Chubut</option>\n' +
                    '                                                <option value="Cordoba">Cordoba</option>\n' +
                    '                                                <option value="Corrientes">Corrientes</option>\n' +
                    '                                                <option value="Entre Rios">Entre Rios</option>\n' +
                    '                                                <option value="Formosa">Formosa</option>\n' +
                    '                                                <option value="Jujuy">Jujuy</option>\n' +
                    '                                                <option value="La Pampa">La Pampa</option>\n' +
                    '                                                <option value="La Rioja">La Rioja</option>\n' +
                    '                                                <option value="Mendoza">Mendoza</option>\n' +
                    '                                                <option value="Misiones">Misiones</option>\n' +
                    '                                                <option value="Neuquen">Neuquen</option>\n' +
                    '                                                <option value="Rio Negro">Rio Negro</option>\n' +
                    '                                                <option value="Salta">Salta</option>\n' +
                    '                                                <option value="San Juan">San Juan</option>\n' +
                    '                                                <option value="San Luis">San Luis</option>\n' +
                    '                                                <option value="Santa Cruz">Santa Cruz</option>\n' +
                    '                                                <option value="Santa Fe">Santa Fe</option>\n' +
                    '                                                <option value="Santiago del Estero">Santiago del Estero</option>\n' +
                    '                                                <option value="Tierra del Fuego">Tierra del Fuego</option>\n' +
                    '                                                <option value="Tucuman">Tucuman</option>' +
                    '                                                </select>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="localidad_carga[]">Localidad</label>\n' +
                    '                                                <select class="form-control" name="localidad_carga[]" required="required"></select>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="observaciones_carga[]">Observaciones</label>\n' +
                    '                                                <textarea class="form-control" name="observaciones_carga[]"></textarea>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        <div class="col-12">\n' +
                    '                                             <div class="row">\n' +
                    '                                               <div class="col-6">\n' +
                    '                                                   <div class="float-left">\n' +
                    '                                                       <button type="button" class="btn btn-outline-danger eliminarDireccion">REMOVER DIRECCION</button>\n' +
                    '                                                    </div>\n' +
                    '                                               </div>\n' +
                    '\n' +
                    '                                               <div class="col-6">\n' +
                    '                                                   <div class="float-right">\n' +
                    '                                                       <button type="button" class="btn btn-outline-success verDireccionEnMapa">VER MAPA</button>\n' +
                    '                                                    </div>\n' +
                    '                                                </div>\n' +
                    '                                             </div>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                        </div>');

                primeraDireccionCarga = "";

                $('input[name="cantidad_direcciones_carga"]').val(direccionesCarga);

                cargarEventoBoton();
                cargarEventoProvincias();
                cargarCKEditor();
            });

            $("#agregarDireccionDeDescarga").click(function(data) {
                direccionesDescarga++;

                $("#direccionesDeDescarga").append('<div class="m-accordion__item itemDireccionDeDescarga">\n' +
                    '                            <div class="m-accordion__item-head collapsed" role="tab" id="direccionDeDescargaLarga-'+ direccionesDescarga +'" data-toggle="collapse" href="#direccionDeDescarga-'+ direccionesDescarga +'" aria-expanded="false">\n' +
                    '                                <span class="m-accordion__item-title">Dirección '+ direccionesDescarga +'</span>\n' +
                    '                                <span class="m-accordion__item-mode"></span>\n' +
                    '                            </div>\n' +
                    '                            <div class="m-accordion__item-body collapse ' + primeraDireccionDescarga + '" id="direccionDeDescarga-'+ direccionesDescarga +'" role="tabpanel" aria-labelledby="direccionDeDescargaLarga-'+ direccionesDescarga +'">\n' +
                    '                                <div class="m-accordion__item-content">\n' +
                    '                                    <div class="row">\n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="direccion_descarga[]">Dirección</label>\n' +
                    '                                                <input type="text" class="form-control" name="direccion_descarga[]">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="piso_descarga[]">Piso</label>\n' +
                    '                                                <input type="text" class="form-control" name="piso_descarga[]" required="required">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="departamento_descarga[]">Departamento</label>\n' +
                    '                                                <input type="text" class="form-control" name="departamento_descarga[]" required="required">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="entrecalles_descarga[]">Entrecalles</label>\n' +
                    '                                                <input type="text" class="form-control" name="entrecalles_descarga[]">\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="provincia_descarga[]" required="required">Provincia</label>\n' +
                    '                                                <select name="provincia_descarga[]" class="form-control">\n' +
                    '                                                <option selected="selected" value="">Selecciona una provincia</option>\n' +
                    '                                                <option value="Ciudad Autonoma de Buenos Aires">Ciudad Autonoma de Buenos Aires</option>\n' +
                    '                                                <option value="Buenos Aires">Buenos Aires</option>\n' +
                    '                                                <option value="Catamarca">Catamarca</option>\n' +
                    '                                                <option value="Chaco">Chaco</option>\n' +
                    '                                                <option value="Chubut">Chubut</option>\n' +
                    '                                                <option value="Cordoba">Cordoba</option>\n' +
                    '                                                <option value="Corrientes">Corrientes</option>\n' +
                    '                                                <option value="Entre Rios">Entre Rios</option>\n' +
                    '                                                <option value="Formosa">Formosa</option>\n' +
                    '                                                <option value="Jujuy">Jujuy</option>\n' +
                    '                                                <option value="La Pampa">La Pampa</option>\n' +
                    '                                                <option value="La Rioja">La Rioja</option>\n' +
                    '                                                <option value="Mendoza">Mendoza</option>\n' +
                    '                                                <option value="Misiones">Misiones</option>\n' +
                    '                                                <option value="Neuquen">Neuquen</option>\n' +
                    '                                                <option value="Rio Negro">Rio Negro</option>\n' +
                    '                                                <option value="Salta">Salta</option>\n' +
                    '                                                <option value="San Juan">San Juan</option>\n' +
                    '                                                <option value="San Luis">San Luis</option>\n' +
                    '                                                <option value="Santa Cruz">Santa Cruz</option>\n' +
                    '                                                <option value="Santa Fe">Santa Fe</option>\n' +
                    '                                                <option value="Santiago del Estero">Santiago del Estero</option>\n' +
                    '                                                <option value="Tierra del Fuego">Tierra del Fuego</option>\n' +
                    '                                                <option value="Tucuman">Tucuman</option>' +
                    '                                                </select>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12 col-sm-6">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="localidad_descarga[]" required="required">Localidad</label>\n' +
                    '                                                <select class="form-control" name="localidad_descarga[]"></select>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        \n' +
                    '                                        <div class="col-12">\n' +
                    '                                            <div class="form-group">\n' +
                    '                                                <label for="observaciones_descarga[]">Observaciones</label>\n' +
                    '                                                <textarea class="form-control" name="observaciones_descarga[]"></textarea>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        <div class="col-12">\n' +
                    '                                             <div class="row">\n' +
                    '                                               <div class="col-6">\n' +
                    '                                                   <div class="float-left">\n' +
                    '                                                       <button type="button" class="btn btn-outline-danger eliminarDireccion">REMOVER DIRECCION</button>\n' +
                    '                                                    </div>\n' +
                    '                                               </div>\n' +
                    '\n' +
                    '                                               <div class="col-6">\n' +
                    '                                                   <div class="float-right">\n' +
                    '                                                       <button type="button" class="btn btn-outline-success verDireccionEnMapa">VER MAPA</button>\n' +
                    '                                                    </div>\n' +
                    '                                                </div>\n' +
                    '                                             </div>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                            </div> ' +
                    '                        </div>');

                primeraDireccionDescarga = "";

                $('input[name="cantidad_direcciones_descarga"]').val(direccionesDescarga);

                cargarEventoBoton();
                cargarEventoProvincias();
                cargarCKEditor();
            });

            function cargarEventoBoton() {

                $('#direccionesDeCarga button.verDireccionEnMapa').unbind().click(function() {

                    var tipo = 'carga';

                    // OBTENER DIRECCION
                    var direccion = $(this).parent().parent().parent().parent().parent().find('input[name="direccion_'+ tipo +'[]"]').val().replace(/ /g, '+');
                    var provincia = $(this).parent().parent().parent().parent().parent().find('select[name="provincia_'+ tipo +'[]"]').val().replace(/ /g, '+');
                    var localidad = $(this).parent().parent().parent().parent().parent().find('select[name="localidad_'+ tipo +'[]"]').val().replace(/ /g, '+');
                    var direccionFinal = direccion + "+" + localidad + "," + provincia;
                    // END OBTENER DIRECCION

                    $('#modalMapa .modal-body').empty();
                    $('#modalMapa .modal-body').append('<iframe\n' +
                        '                            width="100%"\n' +
                        '                            height="450"\n' +
                        '                            frameborder="0" style="border:0"\n' +
                        '                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyChLjx44r8oXAcJ4kuroTIEy29d8vv1NbM\n' +
                        '                            &q='+ direccionFinal +'" allowfullscreen>\n' +
                        '                    </iframe>');
                    $('#modalMapa').modal('show');
                });

                $('#direccionesDeDescarga button.verDireccionEnMapa').unbind().click(function() {

                    var tipo = 'descarga';

                    // OBTENER DIRECCION
                    var direccion = $(this).parent().parent().parent().parent().parent().find('input[name="direccion_'+ tipo +'[]"]').val().replace(/ /g, '+');
                    var provincia = $(this).parent().parent().parent().parent().parent().find('select[name="provincia_'+ tipo +'[]"]').val().replace(/ /g, '+');
                    var localidad = $(this).parent().parent().parent().parent().parent().find('select[name="localidad_'+ tipo +'[]"]').val().replace(/ /g, '+');
                    var direccionFinal = direccion + "+" + localidad + "," + provincia;
                    // END OBTENER DIRECCION

                    $('#modalMapa .modal-body').empty();
                    $('#modalMapa .modal-body').append('<iframe\n' +
                        '                            width="100%"\n' +
                        '                            height="450"\n' +
                        '                            frameborder="0" style="border:0"\n' +
                        '                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyChLjx44r8oXAcJ4kuroTIEy29d8vv1NbM\n' +
                        '                            &q='+ direccionFinal +'" allowfullscreen>\n' +
                        '                    </iframe>');
                    $('#modalMapa').modal('show');
                });

                $('#direccionesDeCarga button.eliminarDireccion').unbind().click(function() {

                    if(confirm('Está seguro que desea remover esta dirección')) {
                        direccionesCarga--;
                        $('input[name="cantidad_direcciones_carga"]').val(direccionesCarga);

                        $(this).parent().parent().parent().parent().parent().parent().parent().parent().remove();
                    }
                });

                $('#direccionesDeDescarga button.eliminarDireccion').unbind().click(function() {

                    if(confirm('Está seguro que desea remover esta dirección')) {
                        direccionesDescarga--;
                        $('input[name="cantidad_direcciones_descarga"]').val(direccionesDescarga);

                        $(this).parent().parent().parent().parent().parent().parent().parent().parent().remove();
                    }
                });
            }

            function cargarEventoProvincias() {
                $('select[name="provincia_carga[]"]').unbind().change(function() {
                    provinciaSeleccionada('_carga', $(this));
                });

                $('select[name="provincia_descarga[]"]').unbind().change(function() {
                    provinciaSeleccionada('_descarga', $(this));
                });
            }

            // END DIRECCIONES

            // PROVINCIAS Y CIUDADES

            function provinciaSeleccionada(prefix, select) {

                if($(select).val() != '' && $(select).val() != null) {

                    $.get('/ajax/localidades/' + $(select).val(), function (data) {

                        $options = "";

                        $.each(data, function(index, localidad) {
                            $options += '<option value="' + localidad.nombre + '">' + localidad.nombre + '</option>';
                        });

                        // ACTUALIZAMOS LA LOCALIDAD
                        $localidad = $(select).parent().parent().parent().find('select[name="localidad' + prefix + '[]"]');
                        $localidad.attr('data-live-search', true);
                        $localidad.empty().append($options);
                        $localidad.selectpicker('refresh');
                    });
                }

            }

            function cargarLocalidadesParaDireccionesCargadas() {

                $('select[name="provincia_carga[]').each(function() {

                    $selectCarga = $(this);

                    if($selectCarga.val() != '' && $selectCarga.val() != null) {

                        $.get({
                            url: '/ajax/localidades/' + $selectCarga.val(),
                            async: false,
                        }, function (data) {

                            $options = "";

                            $.each(data, function(index, localidad) {
                                $options += '<option value="' + localidad.nombre + '">' + localidad.nombre + '</option>';
                            });

                            // ACTUALIZAMOS LA LOCALIDAD
                            $localidad = $selectCarga.parent().parent().parent().find('select[name="localidad_carga[]"]');
                            $localidad.attr('data-live-search', true);
                            $localidad.empty().append($options);
                            $localidad.selectpicker('refresh');

                            // SETEAMOS EL VALUE A LA LOCALIDAD CARGADA
                            var localidad_seleccionada = $localidad.attr('localidad-seleccionada');
                            if(localidad_seleccionada !== undefined && localidad_seleccionada !== false) {
                                $localidad.selectpicker('val', localidad_seleccionada);
                            }
                        });
                    }

                });

                $('select[name="provincia_descarga[]').each(function() {

                    $selectDescarga = $(this);

                    if($selectDescarga.val() != '' && $selectDescarga.val() != null) {

                        $.get({
                            url: '/ajax/localidades/' + $selectDescarga.val(),
                            async: false,
                        }, function (data) {

                            $options = "";

                            $.each(data, function(index, localidad) {
                                $options += '<option value="' + localidad.nombre + '">' + localidad.nombre + '</option>';
                            });

                            // ACTUALIZAMOS LA LOCALIDAD
                            $localidad = $selectDescarga.parent().parent().parent().find('select[name="localidad_descarga[]"]');
                            $localidad.attr('data-live-search', true);
                            $localidad.empty().append($options);
                            $localidad.selectpicker('refresh');

                            // SETEAMOS EL VALUE A LA LOCALIDAD CARGADA
                            var localidad_seleccionada = $localidad.attr('localidad-seleccionada');
                            if(localidad_seleccionada !== undefined && localidad_seleccionada !== false) {
                                $localidad.selectpicker('val', localidad_seleccionada);
                            }
                        });
                    }
                });
            }

            // END PROVINCIAS Y CIUDADES

            /** END EVENTOS **/

            /** AGREGAMOS LAS DIRECCIONES POR DEFECTO **/

            if(direccionesCarga == 0) {
                $("#agregarDireccionDeCarga").click();
            }

            if(direccionesDescarga == 0) {
                $("#agregarDireccionDeDescarga").click();
            }

            /** END DIRECCIONES */

            /** CHEQUEAMOS COTIZACION AL PRINCIPIO **/

            obtenerValorAproximado();

            /** END CHEQUEAMOS COTIZACION **/

            /** AGREGAMOS MASK DE TELEFONOS **/

            var cleaveInputHoraInicio = new Cleave($('#modalAgregarAdicional input[name="hora_inicio"]'), {
                time: true,
                timePattern: ['h', 'm']
            });

            var cleaveInputHoraFin = new Cleave($('#modalAgregarAdicional input[name="hora_fin"]'), {
                time: true,
                timePattern: ['h', 'm']
            });

            /** END AGREGAMOS MASK DE TELEFONOS **/

            /** CKEDITOR */

            function cargarCKEditor() {

                // CKEDITOR
                $("textarea").each(function() {

                    $(this).ckeditor({
                        customConfig: '/js/ckeditor/config.js'
                    });

                    // $(this).addClass('ckeditor');
                    // CKEDITOR.replaceClass('ckeditor');

                });
            }

            /** CKEDITOR */

            /** OTROS EVENTOS */

            cargarAutocompletado();
            cargarEventoProvincias();
            cargarCKEditor();
            cargarLocalidadesParaDireccionesCargadas();

            /** END OTROS EVENTOS */

            /** IMPUESTOS **/

            function tipoImpuesto() {
                var valor = $('select[name="tipo_impuesto"]').val();

                if(valor === 0) {
                    $('input[name="otro_impuesto"]').attr('disabled', true);

                    setearImpuesto(0);

                } else {

                    if(valor !== 'otro') {
                        $('input[name="otro_impuesto"]').attr('disabled', true);
                        setearImpuesto(valor);

                    } else { // SE ESCRIBE EL VALOR

                        $('input[name="otro_impuesto"]').attr('disabled', false);
                        setearImpuesto($('input[name="otro_impuesto"]').val());
                    }
                }
            }

            function setearImpuesto(valor) {

                if(valor === '') {
                    valor = 0;
                }

                $('input[name="impuesto"]').val(valor);

                obtenerCotizacion();
            }

            $('select[name="tipo_impuesto"]').change(tipoImpuesto);

            $('input[name="otro_impuesto"]').change(function() {

                var tipoImpuesto = $('select[name="tipo_impuesto"]').val();

                if(tipoImpuesto === 'otro') {
                    setearImpuesto($(this).val());
                }
            });


            /** END IMPUESTOS **/
        });

    </script>
@endsection