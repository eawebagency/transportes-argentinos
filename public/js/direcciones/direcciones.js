function direccionJS() {

    var direccionesCarga = 0;
    var direccionesDescarga = 0;
    var primeraDireccionCarga = "show";
    var primeraDireccionDescarga = "show";

    /**
     * AGREGAR DIRECCION DE CARGA
     * @param chequearPrimera bool (TRUE = VERIFICA SI ES LA PRIMERA DIRECCION / FALSE = NO HACE NADA)
     */
    this.agregarDireccionCarga = function(chequearPrimera) {

        if(!chequearPrimera) {
            this.primeraDireccionCarga = "";
        }

        this.direccionesCarga++;

        $("#direccionesDeCarga").append('<div class="m-accordion__item itemDireccionDeCarga">\n' +
            '                            <div class="m-accordion__item-head collapsed" role="tab" id="direccionDeCargaLarga-'+ direccionesCarga +'" data-toggle="collapse" href="#direccionDeCarga-'+ direccionesCarga +'" aria-expanded="false">\n' +
            '                                <span class="m-accordion__item-title">Dirección '+ direccionesCarga +'</span>\n' +
            '                                <span class="m-accordion__item-mode"></span>\n' +
            '                            </div>\n' +
            '                            <div class="m-accordion__item-body collapse ' + this.primeraDireccionCarga + '" id="direccionDeCarga-'+ direccionesCarga +'" role="tabpanel" aria-labelledby="direccionDeCargaLarga-'+ direccionesCarga +'">\n' +
            '                                <div class="m-accordion__item-content">\n' +
            '                                    <div class="row">\n' +
            '                                        <div class="col-12 col-sm-6">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="direccion_carga[]">Dirección</label>\n' +
            '                                                <input type="text" class="form-control" name="direccion_carga[]" required="required">\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        \n' +
            '                                        <div class="col-12 col-sm-6">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="piso_carga[]">Piso</label>\n' +
            '                                                <input type="text" class="form-control" name="piso_carga[]" required="required">\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        \n' +
            '                                        <div class="col-12 col-sm-6">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="departamento_carga[]">Departamento</label>\n' +
            '                                                <input type="text" class="form-control" name="departamento_carga[]" required="required">\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        \n' +
            '                                        <div class="col-12 col-sm-6">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="entrecalles_carga[]">Entrecalles</label>\n' +
            '                                                <input type="text" class="form-control" name="entrecalles_carga[]">\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        \n' +
            '                                        <div class="col-12 col-sm-6">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="provincia_carga[]">Provincia</label>\n' +
            '                                                <select name="provincia_carga[]" class="form-control" required="required">\n' +
            '                                                <option selected="selected" value="">Selecciona una provincia</option>\n' +
            '                                                <option value="Ciudad Autonoma de Buenos Aires">Ciudad Autonoma de Buenos Aires</option>\n' +
            '                                                <option value="Buenos Aires">Buenos Aires</option>\n' +
            '                                                <option value="Catamarca">Catamarca</option>\n' +
            '                                                <option value="Chaco">Chaco</option>\n' +
            '                                                <option value="Chubut">Chubut</option>\n' +
            '                                                <option value="Cordoba">Cordoba</option>\n' +
            '                                                <option value="Corrientes">Corrientes</option>\n' +
            '                                                <option value="Entre Rios">Entre Rios</option>\n' +
            '                                                <option value="Formosa">Formosa</option>\n' +
            '                                                <option value="Jujuy">Jujuy</option>\n' +
            '                                                <option value="La Pampa">La Pampa</option>\n' +
            '                                                <option value="La Rioja">La Rioja</option>\n' +
            '                                                <option value="Mendoza">Mendoza</option>\n' +
            '                                                <option value="Misiones">Misiones</option>\n' +
            '                                                <option value="Neuquen">Neuquen</option>\n' +
            '                                                <option value="Rio Negro">Rio Negro</option>\n' +
            '                                                <option value="Salta">Salta</option>\n' +
            '                                                <option value="San Juan">San Juan</option>\n' +
            '                                                <option value="San Luis">San Luis</option>\n' +
            '                                                <option value="Santa Cruz">Santa Cruz</option>\n' +
            '                                                <option value="Santa Fe">Santa Fe</option>\n' +
            '                                                <option value="Santiago del Estero">Santiago del Estero</option>\n' +
            '                                                <option value="Tierra del Fuego">Tierra del Fuego</option>\n' +
            '                                                <option value="Tucuman">Tucuman</option>' +
            '                                                </select>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        \n' +
            '                                        <div class="col-12 col-sm-6">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="localidad_carga[]">Localidad</label>\n' +
            '                                                <select class="form-control" name="localidad_carga[]" required="required"></select>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        \n' +
            '                                        <div class="col-12">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="observaciones_carga[]">Observaciones</label>\n' +
            '                                                <textarea class="form-control" name="observaciones_carga[]"></textarea>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        <div class="col-12">\n' +
            '                                             <div class="row">\n' +
            '                                                    <div class="col-6">\n' +
            '                                                        <div class="float-left">\n' +
            '                                                            <button type="button" class="btn btn-outline-danger eliminarDireccion">REMOVER DIRECCION</button>\n' +
            '                                                        </div>\n' +
            '                                                    </div>\n' +
            '\n' +
            '                                                    <div class="col-6">\n' +
            '                                                        <div class="float-right">\n' +
            '                                                            <button type="button" class="btn btn-outline-success verDireccionEnMapa">VER MAPA</button>\n' +
            '                                                        </div>\n' +
            '                                                    </div>\n' +
            '                                                </div>\n' +
            '                                            </div>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>');

        this.primeraDireccionCarga = "";

        $('input[name="cantidad_direcciones_carga"]').val(this.direccionesCarga);

        this.cargarEventoBoton();
        this.cargarEventoProvincias();
        this.cargarCKEditor();
    };

    /**
     * AGREGAR DIRECCION DE DESCARGA
     * @param chequearPrimera bool (TRUE = VERIFICA SI ES LA PRIMERA DIRECCION / FALSE = NO HACE NADA)
     */
    this.agregarDireccionDescarga = function(chequearPrimera) {

        if(!chequearPrimera) {
            this.primeraDireccionDescarga = "";
        }

        this.direccionesDescarga++;

        $("#direccionesDeDescarga").append('<div class="m-accordion__item itemDireccionDeDescarga">\n' +
            '                            <div class="m-accordion__item-head collapsed" role="tab" id="direccionDeDescargaLarga-'+ direccionesDescarga +'" data-toggle="collapse" href="#direccionDeDescarga-'+ direccionesDescarga +'" aria-expanded="false">\n' +
            '                                <span class="m-accordion__item-title">Dirección '+ direccionesDescarga +'</span>\n' +
            '                                <span class="m-accordion__item-mode"></span>\n' +
            '                            </div>\n' +
            '                            <div class="m-accordion__item-body collapse ' + this.primeraDireccionDescarga + '" id="direccionDeDescarga-'+ direccionesDescarga +'" role="tabpanel" aria-labelledby="direccionDeDescargaLarga-'+ direccionesDescarga +'">\n' +
            '                                <div class="m-accordion__item-content">\n' +
            '                                    <div class="row">\n' +
            '                                        <div class="col-12 col-sm-6">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="direccion_descarga[]">Dirección</label>\n' +
            '                                                <input type="text" class="form-control" name="direccion_descarga[]">\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        \n' +
            '                                        <div class="col-12 col-sm-6">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="piso_descarga[]">Piso</label>\n' +
            '                                                <input type="text" class="form-control" name="piso_descarga[]">\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        \n' +
            '                                        <div class="col-12 col-sm-6">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="departamento_descarga[]">Departamento</label>\n' +
            '                                                <input type="text" class="form-control" name="departamento_descarga[]">\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        \n' +
            '                                        <div class="col-12 col-sm-6">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="entrecalles_descarga[]">Entrecalles</label>\n' +
            '                                                <input type="text" class="form-control" name="entrecalles_descarga[]">\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        \n' +
            '                                        <div class="col-12 col-sm-6">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="provincia_descarga[]">Provincia</label>\n' +
            '                                                <select name="provincia_descarga[]" class="form-control" required="required">\n' +
            '                                                <option selected="selected" value="">Selecciona una provincia</option>\n' +
            '                                                <option value="Ciudad Autonoma de Buenos Aires">Ciudad Autonoma de Buenos Aires</option>\n' +
            '                                                <option value="Buenos Aires">Buenos Aires</option>\n' +
            '                                                <option value="Catamarca">Catamarca</option>\n' +
            '                                                <option value="Chaco">Chaco</option>\n' +
            '                                                <option value="Chubut">Chubut</option>\n' +
            '                                                <option value="Cordoba">Cordoba</option>\n' +
            '                                                <option value="Corrientes">Corrientes</option>\n' +
            '                                                <option value="Entre Rios">Entre Rios</option>\n' +
            '                                                <option value="Formosa">Formosa</option>\n' +
            '                                                <option value="Jujuy">Jujuy</option>\n' +
            '                                                <option value="La Pampa">La Pampa</option>\n' +
            '                                                <option value="La Rioja">La Rioja</option>\n' +
            '                                                <option value="Mendoza">Mendoza</option>\n' +
            '                                                <option value="Misiones">Misiones</option>\n' +
            '                                                <option value="Neuquen">Neuquen</option>\n' +
            '                                                <option value="Rio Negro">Rio Negro</option>\n' +
            '                                                <option value="Salta">Salta</option>\n' +
            '                                                <option value="San Juan">San Juan</option>\n' +
            '                                                <option value="San Luis">San Luis</option>\n' +
            '                                                <option value="Santa Cruz">Santa Cruz</option>\n' +
            '                                                <option value="Santa Fe">Santa Fe</option>\n' +
            '                                                <option value="Santiago del Estero">Santiago del Estero</option>\n' +
            '                                                <option value="Tierra del Fuego">Tierra del Fuego</option>\n' +
            '                                                <option value="Tucuman">Tucuman</option>' +
            '                                                </select>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        \n' +
            '                                        <div class="col-12 col-sm-6">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="localidad_descarga[]">Localidad</label>\n' +
            '                                                <select class="form-control" name="localidad_descarga[]"></select>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        \n' +
            '                                        <div class="col-12">\n' +
            '                                            <div class="form-group">\n' +
            '                                                <label for="observaciones_descarga[]">Observaciones</label>\n' +
            '                                                <textarea class="form-control" name="observaciones_descarga[]"></textarea>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                        <div class="col-12">\n' +
            '                                                <div class="row">\n' +
            '                                                    <div class="col-6">\n' +
            '                                                        <div class="float-left">\n' +
            '                                                            <button type="button" class="btn btn-outline-danger eliminarDireccion">REMOVER DIRECCION</button>\n' +
            '                                                        </div>\n' +
            '                                                    </div>\n' +
            '\n' +
            '                                                    <div class="col-6">\n' +
            '                                                        <div class="float-right">\n' +
            '                                                            <button type="button" class="btn btn-outline-success verDireccionEnMapa">VER MAPA</button>\n' +
            '                                                        </div>\n' +
            '                                                    </div>\n' +
            '                                                </div>\n' +
            '                                            </div>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                            </div> ' +
            '                        </div>');

        this.primeraDireccionDescarga = "";

        $('input[name="cantidad_direcciones_descarga"]').val(this.direccionesDescarga);

        this.cargarEventoBoton();
        this.cargarEventoProvincias();
        this.cargarCKEditor();
    };

    /**
     * CARGAR EVENTO DE LOS BOTONES
     * -> VER DIRECCION EN MAPA
     * -> ELIMINAR DIRECCION
     */
    this.cargarEventoBoton = function() {
        $('#direccionesDeCarga button.verDireccionEnMapa').unbind().click(function() {

            var tipo = "carga";

            // OBTENER DIRECCION
            var direccion = $(this).parent().parent().parent().parent().parent().find('input[name="direccion_'+ tipo +'[]"]').val().replace(/ /g, '+');
            var provincia = $(this).parent().parent().parent().parent().parent().find('select[name="provincia_'+ tipo +'[]"]').val().replace(/ /g, '+');
            var localidad = $(this).parent().parent().parent().parent().parent().find('select[name="localidad_'+ tipo +'[]"]').val().replace(/ /g, '+');
            var direccionFinal = direccion + "+" + localidad + "," + provincia;
            // END OBTENER DIRECCION

            $('#modalMapa .modal-body').empty();
            $('#modalMapa .modal-body').append('<iframe\n' +
                '                            width="100%"\n' +
                '                            height="450"\n' +
                '                            frameborder="0" style="border:0"\n' +
                '                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyChLjx44r8oXAcJ4kuroTIEy29d8vv1NbM\n' +
                '                            &q='+ direccionFinal +'" allowfullscreen>\n' +
                '                    </iframe>');
            $('#modalMapa').modal('show');
        });

        $('#direccionesDeDescarga button.verDireccionEnMapa').unbind().click(function() {

            var tipo = "descarga";

            // OBTENER DIRECCION
            var direccion = $(this).parent().parent().parent().parent().parent().find('input[name="direccion_'+ tipo +'[]"]').val().replace(/ /g, '+');
            var provincia = $(this).parent().parent().parent().parent().parent().find('select[name="provincia_'+ tipo +'[]"]').val().replace(/ /g, '+');
            var localidad = $(this).parent().parent().parent().parent().parent().find('select[name="localidad_'+ tipo +'[]"]').val().replace(/ /g, '+');
            var direccionFinal = direccion + "+" + localidad + "," + provincia;
            // END OBTENER DIRECCION

            $('#modalMapa .modal-body').empty();
            $('#modalMapa .modal-body').append('<iframe\n' +
                '                            width="100%"\n' +
                '                            height="450"\n' +
                '                            frameborder="0" style="border:0"\n' +
                '                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyChLjx44r8oXAcJ4kuroTIEy29d8vv1NbM\n' +
                '                            &q='+ direccionFinal +'" allowfullscreen>\n' +
                '                    </iframe>');
            $('#modalMapa').modal('show');
        });

        $('#direccionesDeCarga button.eliminarDireccion').unbind().click({clase: this}, function(clase) {

            if(confirm('Está seguro que desea remover esta dirección')) {
                clase.direccionesCarga--;
                $('input[name="cantidad_direcciones_carga"]').val(clase.direccionesCarga);

                $(this).parent().parent().parent().parent().parent().parent().parent().parent().remove();
            }
        });

        $('#direccionesDeDescarga button.eliminarDireccion').unbind().click({clase: this}, function(clase) {

            if(confirm('Está seguro que desea remover esta dirección')) {
                clase.direccionesDescarga--;
                $('input[name="cantidad_direcciones_descarga"]').val(clase.direccionesDescarga);

                $(this).parent().parent().parent().parent().parent().parent().parent().parent().remove();
            }
        });
    };

    this.cargarCKEditor = function() {

        $('textarea[name="observaciones_carga[]"]').each(function() {
            $(this).ckeditor({
                customConfig: '/js/ckeditor/config.js'
            });
        });

        $('textarea[name="observaciones_descarga[]"]').each(function() {
            $(this).ckeditor({
                customConfig: '/js/ckeditor/config.js'
            });
        });

    };

    this.cargarEventoProvincias = function() {

        $('select[name="provincia_carga[]"]').unbind().change({clase: this}, function(clase) {
            clase.provinciaSeleccionada('_carga', $(this));
        });

        $('select[name="provincia_descarga[]"]').unbind().change({clase: this}, function(clase) {
            clase.provinciaSeleccionada('_descarga', $(this));
        });
    };

    this.provinciaSeleccionada = function(prefix, select) {

        if($(select).val() != '' && $(select).val() != null) {

            $.get('/ajax/localidades/' + $(select).val(), function (data) {

                $options = "";

                $.each(data, function(index, localidad) {
                    $options += '<option value="' + localidad.nombre + '">' + localidad.nombre + '</option>';
                });

                // ACTUALIZAMOS LA LOCALIDAD
                $localidad = $(select).parent().parent().parent().find('select[name="localidad' + prefix + '[]"]');
                $localidad.attr('data-live-search', true);
                $localidad.empty().append($options);
                $localidad.selectpicker('refresh');
            });
        }

    };

    this.cargarLocalidadesParaDireccionesCargadas = function() {
        $('select[name="provincia_carga[]').each(function() {

            $selectCarga = $(this);

            if($selectCarga.val() != '' && $selectCarga.val() != null) {

                $.get({
                    url: '/ajax/localidades/' + $selectCarga.val(),
                    async: false,
                }, function (data) {

                    $options = "";

                    $.each(data, function(index, localidad) {
                        $options += '<option value="' + localidad.nombre + '">' + localidad.nombre + '</option>';
                    });

                    // ACTUALIZAMOS LA LOCALIDAD
                    $localidad = $selectCarga.parent().parent().parent().find('select[name="localidad_carga[]"]');
                    $localidad.attr('data-live-search', true);
                    $localidad.empty().append($options);
                    $localidad.selectpicker('refresh');

                    // SETEAMOS EL VALUE A LA LOCALIDAD CARGADA
                    var localidad_seleccionada = $localidad.attr('localidad-seleccionada');
                    if(localidad_seleccionada !== undefined && localidad_seleccionada !== false) {
                        $localidad.selectpicker('val', localidad_seleccionada);
                    }
                });
            }

        });

        $('select[name="provincia_descarga[]').each(function() {

            $selectDescarga = $(this);

            if($selectDescarga.val() != '' && $selectDescarga.val() != null) {

                $.get({
                    url: '/ajax/localidades/' + $selectDescarga.val(),
                    async: false,
                }, function (data) {

                    $options = "";

                    $.each(data, function(index, localidad) {
                        $options += '<option value="' + localidad.nombre + '">' + localidad.nombre + '</option>';
                    });

                    // ACTUALIZAMOS LA LOCALIDAD
                    $localidad = $selectDescarga.parent().parent().parent().find('select[name="localidad_descarga[]"]');
                    $localidad.attr('data-live-search', true);
                    $localidad.empty().append($options);
                    $localidad.selectpicker('refresh');

                    // SETEAMOS EL VALUE A LA LOCALIDAD CARGADA
                    var localidad_seleccionada = $localidad.attr('localidad-seleccionada');
                    if(localidad_seleccionada !== undefined && localidad_seleccionada !== false) {
                        $localidad.selectpicker('val', localidad_seleccionada);
                    }
                });
            }
        });
    };
}