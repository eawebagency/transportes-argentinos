<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visita extends Model
{
    public function detalle() {
        return $this->hasOne('App\DetalleVisita', 'visita')->first();
    }

    public function observaciones() {
        return $this->hasMany('App\ObservacionVisita', 'visita')->get();
    }

    public function presupuesto() {
        return $this->belongsTo('App\Presupuesto', 'presupuesto')->first();
    }

    public function visitador() {
        return $this->belongsTo('App\User', 'visitador')->first();
    }
}
