<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OlvidarContrasenia extends Mailable
{
    use Queueable, SerializesModels;

    protected $usuario;
    protected $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $usuario, $token)
    {
        $this->usuario = $usuario;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Cambiar contraseña')
            ->view('mails.olvidarContrasenia')
            ->with([
                'usuario' => $this->usuario,
                'token' => $this->token,
            ]);
    }
}
