<?php

namespace App\Mail;

use App\Presupuesto;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnviarMaterialDeEmbalaje extends Mailable
{
    use Queueable, SerializesModels;

    protected $presupuesto;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Presupuesto $presupuesto)
    {
        $this->presupuesto = $presupuesto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('#'.$this->presupuesto->id.' - Material de embalaje')
            ->view('mails.enviarMaterialDeEmbalaje')
            ->attach('detallesDeVisita/Detalle_de_visita_-_Presupuesto_#'.$this->presupuesto->id.'.pdf', ['as' => 'Detalle de visita - Presupuesto #'.$this->presupuesto->id.'.pdf', 'mime' => 'application/pdf'])
            ->attach('presupuestos/Presupuesto_#'.$this->presupuesto->id.'.pdf', ['as' => 'Presupuesto #'.$this->presupuesto->id.'.pdf', 'mime' => 'application/pdf'])
            ->with([
                'presupuesto' => $this->presupuesto,
            ]);
    }
}
