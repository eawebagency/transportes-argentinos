<?php

namespace App\Providers;

use App\Notificacion;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // VARIABLES AL MASTER LAYOUT

        view()->composer('master', function($view)
        {
            $view->with('notificacion', new Notificacion);
            $view->with('usuario', Auth::user());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
