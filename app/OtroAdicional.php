<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtroAdicional extends Model
{
    public function presupuesto() {
        return $this->belongsTo('App\Presupuesto', 'presupuesto')->first();
    }
}
