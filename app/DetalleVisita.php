<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleVisita extends Model
{
    protected $fillable = ['embalaje', 'percheros_moviles', 'cajas', 'horas', 'visita'];

    public function visita() {
        return $this->belongsTo('App\Visita', 'visita')->first();
    }
}
