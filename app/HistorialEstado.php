<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialEstado extends Model
{
    public function usuario() {
        return $this->belongsTo('App\User', 'usuario')->first();
    }

    public function estado() {
        return $this->belongsTo('App\EstadoPresupuesto', 'estado')->first();
    }

    public function presupuesto() {
        return $this->belongsTo('App\Presupuesto', 'presupuesto')->first();
    }
}
