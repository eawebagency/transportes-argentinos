<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{

    /** FUNCIONES AUXILIARES */

    public function adjuntoEsPdf() {
        $nombres = explode('.', $this->comprobante);
        end($nombres);
        $ultimaKey = key($nombres);

        if($nombres[$ultimaKey] == 'pdf') {
            return true;
        }

        return false;
    }

    /** END FUNCIONES AUXILIARES */

    public function forma() {
        return $this->belongsTo('App\FormaPago', 'forma')->first();
    }

    public function usuario() {
        return $this->belongsTo('App\User', 'usuario')->first();
    }
}
