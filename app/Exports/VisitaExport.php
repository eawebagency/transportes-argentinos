<?php

namespace App\Exports;

use App\Visita;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class VisitaExport implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize, WithEvents
{
    public function query()
    {
        return Visita::query()
            ->where('fecha', Carbon::now()->format('Y-m-d'));
    }

    public function map($visita): array
    {
        return [
            $visita->visitador()->nombre.' '.$visita->visitador()->apellido,
            $visita->presupuesto()->cargas()[0]->localidad,
            Carbon::parse($visita->hora_inicio)->format('H:i').' - '.Carbon::parse($visita->hora_fin)->format('H:i'),
            $visita->presupuesto()->cliente()->nombre.' '.$visita->presupuesto()->cliente()->apellido,
            $visita->presupuesto()->cliente()->telefono,
            $visita->presupuesto()->cargas()[0]->direccion,
            $visita->presupuesto()->cargas()[0]->entrecalles,
            '$'.$visita->presupuesto()->valor_aproximado,
            html_entity_decode(strip_tags($visita->observaciones)),
        ];
    }

    public function headings(): array
    {
        return [
            'Nombre del operador',
            'Localidad',
            'Franja horaria',
            'Nombre y apellido del cliente',
            'Teléfono',
            'Dirección',
            'Entre calles',
            'Cotización inicial',
            'Observaciones',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()
                    ->setSize(14)
                    ->setBold('bold');
            },
        ];
    }
}
