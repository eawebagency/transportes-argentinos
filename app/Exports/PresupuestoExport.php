<?php

namespace App\Exports;

use App\Visita;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class PresupuestoExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithEvents
{

    protected $presupuestos;

    public function __construct($presupuestos)
    {
        $this->presupuestos = $presupuestos;
    }

    public function collection()
    {
        return $this->presupuestos;
    }

    public function map($presupuesto): array
    {
        return [
            Carbon::parse($presupuesto->created_at)->format('d/m/Y'),
            $presupuesto->id,
            $presupuesto->cliente()->nombre." ".$presupuesto->cliente()->apellido,
            $presupuesto->cliente()->contacto()->nombre,
            $presupuesto->vendedor()->nombre." ".$presupuesto->vendedor()->apellido,
            $presupuesto->cotizacion,
        ];
    }

    public function headings(): array
    {
        return [
            'Fecha',
            'Nro. de presupuesto',
            'Cliente',
            'Forma de contacto',
            'Vendedor',
            'Valor final del presupuesto',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()
                    ->setSize(14)
                    ->setBold('bold');
            },
        ];
    }
}
