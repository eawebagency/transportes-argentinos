<?php

namespace App\Rules;

use App\Cliente;
use Illuminate\Contracts\Validation\Rule;

class ClienteExistente implements Rule
{
    protected $nombre;
    protected $apellido;
    protected $email;
    protected $wpp;
    protected $telefono_alternativo;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($nombre, $apellido, $email, $wpp, $telefono_alternativo)
    {
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->email = $email;
        $this->wpp = $wpp;
        $this->telefono_alternativo = $telefono_alternativo;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $cliente = Cliente::where('nombre', $this->nombre)
            ->where('apellido', $this->apellido)
            ->where('email', $this->email)
            ->where('telefono', $value)
            ->where('wpp', $this->wpp)
            ->where('telefono_alternativo', $this->telefono_alternativo)
            ->first();

        if(!empty($cliente)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El cliente ya se encuentra registrado';
    }
}
