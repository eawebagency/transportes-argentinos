<?php

namespace App\Http\Controllers;

use App\Direccion;
use App\Presupuesto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DireccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($presupuesto)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {
            $controlador = new PresupuestoController();
            return $controlador->edit($pres->id);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $presupuesto)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {
            return view('direcciones.crear')->with([
                'presupuesto' => $presupuesto,
                'carga' => $request->input('carga'),
            ]);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $presupuesto)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {
            $rules = [
                'direccion' => 'required|string',
                'departamento' => 'required|string',
                'piso' => 'required|string',
                'entrecalles' => 'nullable|string',
                'provincia' => 'required|string',
                'localidad' => 'required|string',
                'observaciones' => 'nullable|string',
                'carga' => 'required|boolean',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'integer' => 'El campo :attribute tiene que ser un entero',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
                'boolean' => 'El campo :attribute no contiene un valor válido',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->create($request, $presupuesto)->withErrors($validator);
            }

            $direccion = new Direccion;
            $direccion->direccion = $request->input('direccion');
            $direccion->departamento = $request->input('departamento');
            $direccion->piso = $request->input('piso');
            $direccion->entrecalles = $request->input('entrecalles');
            $direccion->provincia = $request->input('provincia');
            $direccion->localidad = $request->input('localidad');
            $direccion->observaciones = $request->input('observaciones');
            $direccion->carga = $request->input('carga');
            $direccion->presupuesto = $presupuesto;
            $direccion->save();

            return $this->create($request, $presupuesto)->with(['success' => 'Dirección agregada con éxito']);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Direccion  $direccion
     * @return \Illuminate\Http\Response
     */
    public function show(Direccion $direccion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Direccion  $direccion
     * @return \Illuminate\Http\Response
     */
    public function edit($presupuesto, $id)
    {
        $direccion = Direccion::find($id);

        if(!empty($direccion)) {
            return view('direcciones.editarIndividual')->with([
                'elemento' => $direccion,
            ]);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Direccion  $direccion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $presupuesto, $id)
    {
        $direccion = Direccion::find($id);

        if(!empty($direccion)) {
            $rules = [
                'direccion' => 'required|string',
                'departamento' => 'required|string',
                'piso' => 'required|string',
                'entrecalles' => 'nullable|string',
                'provincia' => 'required|string',
                'localidad' => 'required|string',
                'observaciones' => 'nullable|string',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'integer' => 'El campo :attribute tiene que ser un entero',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->edit($presupuesto, $id)->withErrors($validator);
            }

            $direccion->direccion = $request->input('direccion');
            $direccion->departamento = $request->input('departamento');
            $direccion->piso = $request->input('piso');
            $direccion->entrecalles = $request->input('entrecalles');
            $direccion->provincia = $request->input('provincia');
            $direccion->localidad = $request->input('localidad');
            $direccion->observaciones = $request->input('observaciones');
            $direccion->save();

            return $this->edit($presupuesto, $id)->with(['success' => 'Dirección editada con éxito']);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Direccion  $direccion
     * @return \Illuminate\Http\Response
     */
    public function destroy($presupuesto, $id)
    {
        $direccion = Direccion::find($id);

        if(!empty($direccion)) {
            if($direccion->delete()) {
                return $this->index($presupuesto)->with(['success' => 'Dirección eliminada con éxito']);
            }
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
