<?php

namespace App\Http\Controllers;

use App\Exports\VisitaExport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function visitasDelDia() {
        return Excel::download(new VisitaExport, 'visitas_del_dia'.Carbon::now()->format('d-m-Y').'.xlsx');
    }


}
