<?php

namespace App\Http\Controllers;

use App\Servicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicios = Servicio::all();

        return view('servicios.editar')->with([
            'elementos' => $servicios,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('servicios.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|string|unique:servicios,nombre',
//            'valor' => 'required|numeric',
            'basesycondiciones' => 'nullable|string',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'numeric' => 'El campo :attribute tiene que ser un número válido',
            'unique' => 'El :attribute ingresado ya se encuentra registrado',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->create()->withErrors($validator);
        }

        $servicio = new Servicio;
        $servicio->nombre = $request->input('nombre');
        $servicio->valor = 0;
        $servicio->basesycondiciones = $request->input('basesycondiciones');
        $servicio->save();

        return $this->create()->with(['success' => 'Servicio creado con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Servicio  $servicio
     * @return \Illuminate\Http\Response
     */
    public function show(Servicio $servicio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Servicio  $servicio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio = Servicio::find($id);

        if(!empty($servicio)) {
            return view('servicios.editarIndividual')->with([
                'elemento' => $servicio,
            ]);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Servicio  $servicio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $servicio = Servicio::find($id);

        if(!empty($servicio)) {
            $rules = [
                'nombre' => 'required|string',
//                'valor' => 'required|numeric',
                'basesycondiciones' => 'nullable|string',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'numeric' => 'El campo :attribute tiene que ser un número válido',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->edit($id)->withErrors($validator);
            }

            $servicio->nombre = $request->input('nombre');
//            $servicio->valor = $request->input('valor');
            $servicio->basesycondiciones = $request->input('basesycondiciones');
            $servicio->save();

            return $this->index()->with(['success' => 'Servicio editado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Servicio  $servicio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $servicio = Servicio::find($id);

        if(!empty($servicio)) {
            if($servicio->delete()) {
                return $this->index()->with(['success' => 'El servicio fue eliminado con éxito']);
            }
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    public function desactivar($id)
    {
        $servicio = Servicio::find($id);

        if(!empty($servicio)) {
            $servicio->activo = false;
            $servicio->save();

            return $this->index()->with(['success' => 'El servicio fue desactivado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
