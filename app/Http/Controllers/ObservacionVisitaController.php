<?php

namespace App\Http\Controllers;

use App\ObservacionVisita;
use App\Visita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ObservacionVisitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($visita)
    {
        $vis = Visita::find($visita);

        if(!empty($vis)) {
            $controlador = new VisitaController();
            return $controlador->edit($vis->presupuesto, $vis->id);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($visita)
    {
        $vis = Visita::find($visita);

        if(!empty($vis)) {
            return view('observaciones_visita.crear')->with([
                'visita' => $vis,
            ]);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $visita)
    {
        $vis = Visita::find($visita);

        if(!empty($vis)) {
            $rules = [
                'nombre' => 'required|string',
                'descripcion' => 'required|string',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'numeric' => 'El campo :attribute tiene que ser un número válido',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {

                $request->flash();

                return $this->create($visita)->withErrors($validator);
            }

            $observacion = new ObservacionVisita;
            $observacion->nombre = $request->input('nombre');
            $observacion->descripcion = $request->input('descripcion');
            $observacion->visita = $visita;
            $observacion->save();

            return $this->create($visita)->with(['success' => 'La observación fue creada con éxito']);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ObservacionVisita  $observacionVisita
     * @return \Illuminate\Http\Response
     */
    public function show(ObservacionVisita $observacionVisita)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ObservacionVisita  $observacionVisita
     * @return \Illuminate\Http\Response
     */
    public function edit($visita, $id)
    {
        $vis = Visita::find($visita);

        if(!empty($vis)) {

            $observacion = ObservacionVisita::find($id);

            if (!empty($observacion)) {
                return view('observaciones_visita.editarIndividual')->with([
                    'elemento' => $observacion,
                    'visita' => $vis,
                ]);
            }

            return $this->index($visita)->withErrors(['existe' => 'El elemento solicitado no existe']);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ObservacionVisita  $observacionVisita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $visita, $id)
    {
        $vis = Visita::find($visita);

        if(!empty($vis)) {

            $observacion = ObservacionVisita::find($id);

            if (!empty($observacion)) {
                $rules = [
                    'nombre' => 'required|string',
                    'descripcion' => 'required|string',
                ];

                $messages = [
                    'required' => 'El campo :attribute es requerido',
                    'numeric' => 'El campo :attribute tiene que ser un número válido',
                    'unique' => 'El :attribute ingresado ya se encuentra registrado',
                ];

                $validator = Validator::make($request->all(), $rules, $messages);

                if ($validator->fails()) {

                    return $this->edit($visita, $id)->withErrors($validator);
                }

                $observacion->nombre = $request->input('nombre');
                $observacion->descripcion = $request->input('descripcion');
                $observacion->save();

                return $this->edit($visita, $id)->with(['success' => 'La observación fue editada con éxito']);
            }

            return $this->index($visita)->withErrors(['existe' => 'El elemento solicitado no existe']);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ObservacionVisita  $observacionVisita
     * @return \Illuminate\Http\Response
     */
    public function destroy($visita, $id)
    {
        $vis = Visita::find($visita);

        if(!empty($vis)) {

            $observacion = ObservacionVisita::find($id);

            if (!empty($observacion)) {
                if ($observacion->delete()) {
                    return $this->index($visita)->with(['success' => 'La observación fue eliminada con éxito']);
                }
            }

            return $this->index($visita)->withErrors(['existe' => 'El elemento solicitado no existe']);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
