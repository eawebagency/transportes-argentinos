<?php

namespace App\Http\Controllers;

use App\HistorialEstado;
use App\Presupuesto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HistorialEstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($presupuesto)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {
            $controlador = new PresupuestoController();
            return $controlador->edit($presupuesto);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HistorialEstado  $historialEstado
     * @return \Illuminate\Http\Response
     */
    public function show(HistorialEstado $historialEstado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HistorialEstado  $historialEstado
     * @return \Illuminate\Http\Response
     */
    public function edit($presupuesto, $id)
    {
        $historial = HistorialEstado::find($id);

        if(!empty($historial)) {
            return view('historial.editarIndividual')->with([
                'elemento' => $historial,
            ]);
        }

        return $this->index($presupuesto)->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HistorialEstado  $historialEstado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $presupuesto, $id)
    {
        $historial = HistorialEstado::find($id);

        if(!empty($historial)) {
            $rules = [
                'fecha' => 'nullable|date',
                'desde' => 'nullable|date_format:"H:i"',
                'hasta' => 'nullable|date_format:"H:i"',
                'observaciones' => 'nullable|string',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'numeric' => 'El campo :attribute tiene que ser un número válido',
                'exists' => 'El campo :attribute no se encuentra registrado',
                'date_format' => 'El campo :attribute no respeta el formato dado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->edit($presupuesto, $id)->withErrors($validator);
            }

            $historial->fecha = $request->input('fecha');
            $historial->desde = $request->input('desde');
            $historial->hasta = $request->input('hasta');
            $historial->observaciones = $request->input('observaciones');

            if($historial->estado == 9) {
                $presupuestoM = Presupuesto::find($presupuesto);
                $presupuestoM->enviarMaterial = false;
                $presupuestoM->save();
            }

            if($historial->estado == 2) {
                $presupuestoM = Presupuesto::find($presupuesto);
                $presupuestoM->enviarDetalle = false;
                $presupuestoM->save();
            }

            $historial->save();

            return $this->index($presupuesto)->with(['success' => 'El historial fue editado con éxito']);
        }

        return $this->index($presupuesto)->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HistorialEstado  $historialEstado
     * @return \Illuminate\Http\Response
     */
    public function destroy($presupuesto, $id)
    {
        $historial = HistorialEstado::find($id);

        if(!empty($historial)) {

            $contadorHistorial = HistorialEstado::where('presupuesto', $presupuesto)
                ->get();

            if(count($contadorHistorial) > 1) {
                if ($historial->delete()) {

                    $historialAnterior = HistorialEstado::where('presupuesto', $presupuesto)
                        ->orderBy('id', 'desc')
                        ->first();

                    if (!empty($historialAnterior)) {
                        $presupuestoActual = Presupuesto::find($presupuesto);
                        $presupuestoActual->estado = $historialAnterior->estado;
                        $presupuestoActual->save();
                    }

                    return $this->index($presupuesto)->with(['success' => 'El historial fue editado con éxito']);
                }
            }

            return $this->index($presupuesto)->withErrors(['ultimo' => 'No se permite eliminar el último historial de estado. Edite el actual de ser necesario']);
        }

        return $this->index($presupuesto)->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
