<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class LocalidadController extends Controller
{
    /** HELPER FUNCTIONS */

    public function obtenerLocalidadesPorProvincia($provincia) {

        $localidades = DB::table('localidades')
            ->select('nombre')
            ->where('provincia', $provincia)
            ->get()
            ->toArray();

        return Response::json($localidades);
    }

    public function obtenerProvinciasSelect() {

        $devolver = [];
        $provincias = DB::table('localidades')
            ->selectRaw('provincia, SUM(nombre)')
            ->groupBy('provincia')
            ->get();

        foreach($provincias as $provincia) {
            $devolver[$provincia->provincia] = $provincia->provincia;
        }

        return $devolver;
    }

    /** END HELPER FUNCTIONS */

    public function index()
    {
        $localidades = DB::table('localidades')->get();

        return view('localidades.editar')->with([
            'elementos' => $localidades,
        ]);
    }

    public function create()
    {
        return view('localidades.crear')->with([
            'provincias' => $this->obtenerProvinciasSelect(),
        ]);
    }

    public function store(Request $request)
    {
        $rules = [
            'provincia' => 'required|string|exists:localidades,provincia',
            'nombre' => 'required|string',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'numeric' => 'El campo :attribute tiene que ser un número válido',
            'unique' => 'El :attribute ingresado ya se encuentra registrado',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->create()->withErrors($validator);
        }

        DB::table('localidades')
            ->insert([
                'provincia' => $request->input('provincia'),
                'nombre' => $request->input('nombre'),
            ]);

        return $this->create()->with(['success' => 'Localidad creada con éxito']);
    }

    public function show()
    {
        //
    }

    public function edit($id)
    {
        $localidad = DB::table('localidades')->where('id', $id)->first();

        if(!empty($localidad)) {
            return view('localidades.editarIndividual')->with([
                'elemento' => $localidad,
                'provincias' => $this->obtenerProvinciasSelect(),
            ]);
        }

        return $this->index()->withErrors(['existe' => 'La localidad no existe']);
    }

    public function update(Request $request, $id)
    {
        $localidad = DB::table('localidades')->where('id', $id)->first();

        if(!empty($localidad)) {
            $rules = [
                'provincia' => 'required|string|exists:localidades,provincia',
                'nombre' => 'required|string',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'numeric' => 'El campo :attribute tiene que ser un número válido',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->create()->withErrors($validator);
            }

            DB::table('localidades')
                ->where('id', $id)
                ->update([
                    'provincia' => $request->input('provincia'),
                    'nombre' => $request->input('nombre'),
                ]);

            return $this->edit($id)->with(['success' => 'Localidad editada con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    public function destroy($id)
    {
        $localidad = DB::table('localidades')->where('id', $id)->first();

        if(!empty($localidad)) {
            DB::table('localidades')
                ->where('id', $id)
                ->delete();

            return $this->index()->with(['success' => 'La localidad fue eliminada con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'La localidad no existe']);
    }
}
