<?php

namespace App\Http\Controllers;

use App\RolUsuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = RolUsuario::all();

        return view()->with([
            'elementos' => $roles
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|string|unique:rol_usuarios,nombre',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'unique' => 'El :attribute ingresado ya se encuentra registrado',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->create()->withErrors($validator);
        }

        $rol = new RolUsuario;
        $rol->nombre = $request->input('nombre');
        $rol->save();

        return $this->create()->with(['success' => 'Rol creado con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RolUsuario  $rolUsuario
     * @return \Illuminate\Http\Response
     */
    public function show(RolUsuario $rolUsuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RolUsuario  $rolUsuario
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rol = RolUsuario::find($id);

        if(!empty($rol)) {

            return view()->with([
                'elemento' => $rol,
            ]);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RolUsuario  $rolUsuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rol = RolUsuario::find($id);

        if(!empty($rol)) {
            $rules = [
                'nombre' => 'required|string|',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->edit($id)->withErrors($validator);
            }

            $rol->nombre = $request->input('nombre');
            $rol->save();

            return $this->edit($id)->with(['success' => 'Rol editado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RolUsuario  $rolUsuario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rol = RolUsuario::find($id);

        if(!empty($rol)) {

            if($rol->delete()) {
                return $this->index()->with(['success' => 'El rol ha sido eliminado con éxito']);
            }
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
