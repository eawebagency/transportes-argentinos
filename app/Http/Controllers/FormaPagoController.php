<?php

namespace App\Http\Controllers;

use App\FormaPago;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FormaPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formas = FormaPago::all();

        return view('formas_pago.editar')->with([
            'elementos' => $formas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('formas_pago.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|string|unique:forma_pagos,nombre',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'numeric' => 'El campo :attribute tiene que ser un número válido',
            'unique' => 'El :attribute ingresado ya se encuentra registrado',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->create()->withErrors($validator);
        }

        $forma = new FormaPago;
        $forma->nombre = $request->input('nombre');
        $forma->save();

        return $this->create()->with(['success' => 'Forma de pago creado con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FormaPago  $formaPago
     * @return \Illuminate\Http\Response
     */
    public function show(FormaPago $formaPago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FormaPago  $formaPago
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $forma = FormaPago::find($id);

        if(!empty($forma)) {
            return view('formas_pago.editarIndividual')->with([
                'elemento' => $forma,
            ]);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FormaPago  $formaPago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $forma = FormaPago::find($id);

        if(!empty($forma)) {
            $rules = [
                'nombre' => 'required|string',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'numeric' => 'El campo :attribute tiene que ser un número válido',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->edit($id)->withErrors($validator);
            }

            $forma->nombre = $request->input('nombre');
            $forma->save();

            return $this->index()->with(['success' => 'Forma de pago creado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FormaPago  $formaPago
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $forma = FormaPago::find($id);

        if(!empty($forma)) {
            if($forma->delete()) {
                return $this->index()->with(['success' => 'Forma de pago eliminada con éxito']);
            }
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
