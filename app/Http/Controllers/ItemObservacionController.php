<?php

namespace App\Http\Controllers;

use App\ItemObservacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ItemObservacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = ItemObservacion::all();

        return view('items_observaciones.editar')->with([
            'elementos' => $items,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('items_observaciones.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|string|unique:item_observacions,nombre',
            'descripcion' => 'required|string',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'unique' => 'El campo :attribute ingresado ya se encuentra registrado',
            'mimes' => 'El campo :attribute sólo acepta formato PNG',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->create()->withErrors($validator);
        }

        $item = new ItemObservacion();
        $item->nombre = $request->input('nombre');
        $item->descripcion = $request->input('descripcion');
        $item->save();

        return $this->create()->with(['success' => 'Item creado con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemObservacion  $itemObservacion
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $items = ItemObservacion::all();

        return Response::json($items);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemObservacion  $itemObservacion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = ItemObservacion::find($id);

        if(!empty($item)) {
            return view('items_observaciones.editarIndividual')->with([
                'elemento' => $item,
            ]);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemObservacion  $itemObservacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = ItemObservacion::find($id);

        if(!empty($item)) {
            $rules = [
                'nombre' => 'required|string',
                'descripcion' => 'required|string',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'unique' => 'El campo :attribute ingresado ya se encuentra registrado',
                'mimes' => 'El campo :attribute sólo acepta formato PNG',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->edit($id)->withErrors($validator);
            }

            $item->nombre = $request->input('nombre');
            $item->descripcion = $request->input('descripcion');
            $item->save();

            return $this->edit($id)->with(['success' => 'Item creado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemObservacion  $itemObservacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = ItemObservacion::find($id);

        if(!empty($item)) {
            if($item->delete()) {
                return $this->index()->with(['success' => 'El item ha sido eliminado con éxito']);
            }
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
