<?php

namespace App\Http\Controllers;

use App\Adicional;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdicionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adicionales = Adicional::all();

        return view('adicionales.editar')->with([
            'elementos' => $adicionales
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adicionales.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|string|unique:adicionals,nombre',
            'valor' => 'required|numeric',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'numeric' => 'El campo :attribute tiene que ser un número válido',
            'unique' => 'El :attribute ingresado ya se encuentra registrado',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->create()->withErrors($validator);
        }

        $adicional = new Adicional;
        $adicional->nombre = $request->input('nombre');
        $adicional->valor = $request->input('valor');
        $adicional->save();

        return $this->create()->with(['success' => 'Adicional creado con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Adicional  $adicional
     * @return \Illuminate\Http\Response
     */
    public function show(Adicional $adicional)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Adicional  $adicional
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adicional = Adicional::find($id);

        if(!empty($adicional)) {
            return view('adicionales.editarIndividual')->with([
                'elemento' => $adicional,
            ]);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Adicional  $adicional
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adicional = Adicional::find($id);

        if(!empty($adicional)) {
            $rules = [
                'nombre' => 'required|string',
                'valor' => 'required|numeric',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'numeric' => 'El campo :attribute tiene que ser un número válido',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                $request->flash();

                return $this->edit($id)->withErrors($validator);
            }

            $adicional->nombre = $request->input('nombre');
            $adicional->valor = $request->input('valor');
            $adicional->save();

            return $this->index()->with(['success' => 'Adicional creado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Adicional  $adicional
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $adicional = Adicional::find($id);

        if(!empty($adicional)) {
            if($adicional->delete()) {
                return $this->index()->with(['success' => 'Adicional eliminado con éxito']);
            }
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
