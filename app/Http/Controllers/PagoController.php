<?php

namespace App\Http\Controllers;

use App\FormaPago;
use App\Pago;
use App\Presupuesto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PagoController extends Controller
{

    public function obtenerFormasSelect() {
        $devolver = [];

        $formas = FormaPago::all();

        foreach($formas as $forma) {
            $devolver[$forma->id] = $forma->nombre;
        }

        return $devolver;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($presupuesto)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {
            $controlador = new PresupuestoController();
            return $controlador->edit($presupuesto);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($presupuesto)
    {
        $formas = $this->obtenerFormasSelect();
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {
            return view('pagos.crear')->with([
                'formas' => $formas,
                'presupuesto' => $presupuesto,
            ]);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $presupuesto)
    {
        $rules = [
            'forma' => 'required|integer|exists:forma_pagos,id',
            'monto' => 'required|numeric',
            'fecha' => 'required|date',
            'pagorecibido' => 'required|boolean',
            'comprobante' => 'nullable|file',
            'observaciones' => 'nullable|string',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'numeric' => 'El campo :attribute tiene que ser un número válido',
            'exists' => 'El campo :attribute no se encuentra registrado',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->create($presupuesto)->withErrors($validator);
        }

        $pago = new Pago;
        $pago->forma = $request->input('forma');
        $pago->monto = $request->input('monto');
        $pago->fecha = $request->input('fecha');
        $pago->pagorecibido = $request->input('pagorecibido');
        $pago->observaciones = $request->input('observaciones');
        if(!empty($request->file('comprobante'))) {
            $file = $request->file('comprobante');
            $file->move('comprobantes/pagos/', $file->getClientOriginalName());
            $pago->comprobante = 'comprobantes/pagos/'.$file->getClientOriginalName();
        }
        $pago->usuario = Auth::id();
        $pago->presupuesto = $presupuesto;
        $pago->save();

        return $this->index($presupuesto)->with(['success' => 'El pago fue creado con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function show(Pago $pago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function edit($presupuesto, $id)
    {
        $formas = $this->obtenerFormasSelect();
        $pago = Pago::find($id);

        if(!empty($pago)) {
            return view('pagos.editarIndividual')->with([
                'elemento' => $pago,
                'formas' => $formas,
            ]);
        }

        return $this->index($presupuesto)->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $presupuesto, $id)
    {
        $pago = Pago::find($id);

        if(!empty($pago)) {
            $rules = [
                'forma' => 'required|integer|exists:forma_pagos,id',
                'monto' => 'required|numeric',
                'fecha' => 'required|date',
                'pagorecibido' => 'required|boolean',
                'comprobante' => 'nullable|file',
                'observaciones' => 'nullable|string',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'numeric' => 'El campo :attribute tiene que ser un número válido',
                'exists' => 'El campo :attribute no se encuentra registrado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->edit($presupuesto, $id)->withErrors($validator);
            }

            $pago->forma = $request->input('forma');
            $pago->monto = $request->input('monto');
            $pago->fecha = $request->input('fecha');
            $pago->pagorecibido = $request->input('pagorecibido');
            $pago->observaciones = $request->input('observaciones');
            if(!empty($request->file('comprobante'))) {
                $file = $request->file('comprobante');
                $file->move('comprobantes/pagos/', $file->getClientOriginalName());
                $pago->comprobante = 'comprobantes/pagos/'.$file->getClientOriginalName();
            }
            $pago->save();

            return $this->edit($presupuesto, $id)->with(['success' => 'El pago fue editado con éxito']);
        }

        return $this->index($presupuesto)->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function destroy($presupuesto ,$id)
    {
        $pago = Pago::find($id);

        if(!empty($pago)) {
            if($pago->delete()) {
                return $this->index($presupuesto)->with(['success' => 'El pago fue eliminado con éxito']);
            }
        }

        return $this->index($presupuesto)->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
