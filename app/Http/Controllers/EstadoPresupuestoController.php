<?php

namespace App\Http\Controllers;

use App\EstadoPresupuesto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EstadoPresupuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados = EstadoPresupuesto::all();

        return view('estados_presupuesto.editar')->with([
            'elementos' => $estados,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estados_presupuesto.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|string|unique:estado_presupuestos,nombre',
            'color' => 'required|string',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'numeric' => 'El campo :attribute tiene que ser un número válido',
            'unique' => 'El :attribute ingresado ya se encuentra registrado',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->create()->withErrors($validator);
        }

        $estado = new EstadoPresupuesto;
        $estado->nombre = $request->input('nombre');
        $estado->color = $request->input('color');
        $estado->save();

        return $this->create()->with(['success' => 'Estado creado con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EstadoPresupuesto  $estadoPresupuesto
     * @return \Illuminate\Http\Response
     */
    public function show(EstadoPresupuesto $estadoPresupuesto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EstadoPresupuesto  $estadoPresupuesto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estado = EstadoPresupuesto::find($id);

        if(!empty($estado)) {
            return view('estados_presupuesto.editarIndividual')->with([
                'elemento' => $estado,
            ]);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EstadoPresupuesto  $estadoPresupuesto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estado = EstadoPresupuesto::find($id);

        if(!empty($estado)) {
            $rules = [
                'nombre' => 'required|string',
                'color' => 'required|string',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'numeric' => 'El campo :attribute tiene que ser un número válido',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->edit($id)->withErrors($validator);
            }

            $estado->nombre = $request->input('nombre');
            $estado->color = $request->input('color');
            $estado->save();

            return $this->index()->with(['success' => 'Estado editado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoPresupuesto  $estadoPresupuesto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estado = EstadoPresupuesto::find($id);

        if(!empty($estado)) {
            if($estado->delete()) {
                return $this->index()->with(['success' => 'El estado ha sido eliminado con éxito']);
            }
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
