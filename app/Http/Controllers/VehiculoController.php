<?php

namespace App\Http\Controllers;

use App\Vehiculo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class VehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehiculos = Vehiculo::all();

        return view('vehiculos.editar')->with([
            'elementos' => $vehiculos,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vehiculos.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'patente' => 'required|string|unique:vehiculos,patente',
            'descripcion' => 'nullable|string',
            'valor' => 'required|numeric',
            'valor_mudanza' => 'required|numeric',
            'valor_km' => 'required|numeric',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'numeric' => 'El campo :attribute tiene que ser un número válido',
            'unique' => 'El :attribute ingresado ya se encuentra registrado',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->create()->withErrors($validator);
        }

        $vehiculo = new Vehiculo;
        $vehiculo->patente = $request->input('patente');
        $vehiculo->descripcion = $request->input('descripcion');
        $vehiculo->valor = $request->input('valor');
        $vehiculo->valor_mudanza = $request->input('valor_mudanza');
        $vehiculo->valor_km = $request->input('valor_km');
        $vehiculo->save();

        return $this->create()->with(['success' => 'Vehículo creado con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehiculo  $vehiculo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehiculo = Vehiculo::find($id);

        if(!empty($vehiculo)) {
            return Response::json($vehiculo);
        }

        return null;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehiculo  $vehiculo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehiculo = Vehiculo::find($id);

        if(!empty($vehiculo)) {
            return view('vehiculos.editarIndividual')->with([
                'elemento' => $vehiculo,
            ]);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehiculo  $vehiculo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vehiculo = Vehiculo::find($id);

        if(!empty($vehiculo)) {
            $rules = [
                'patente' => 'required|string',
                'descripcion' => 'nullable|string',
                'valor' => 'required|numeric',
                'valor_mudanza' => 'required|numeric',
                'valor_km' => 'required|numeric',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'numeric' => 'El campo :attribute tiene que ser un número válido',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->edit($id)->withErrors($validator);
            }

            $vehiculo->patente = $request->input('patente');
            $vehiculo->descripcion = $request->input('descripcion');
            $vehiculo->valor = $request->input('valor');
            $vehiculo->valor_mudanza = $request->input('valor_mudanza');
            $vehiculo->valor_km = $request->input('valor_km');
            $vehiculo->save();

            return $this->index()->with(['success' => 'Vehículo editado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento seleccionado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehiculo  $vehiculo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vehiculo = Vehiculo::find($id);

        if(!empty($vehiculo)) {
            if($vehiculo->delete()) {
                return $this->index()->with(['success' => 'El vehículo fue eliminado con éxito']);
            }
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    public function desactivar($id)
    {
        $vehiculo = Vehiculo::find($id);

        if(!empty($vehiculo)) {
            $vehiculo->activo = false;
            $vehiculo->save();

            return $this->index()->with(['success' => 'El vehículo fue eliminado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
