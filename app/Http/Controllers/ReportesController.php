<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\EstadoPresupuesto;
use App\Exports\PresupuestoExport;
use App\Presupuesto;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ReportesController extends Controller
{

    /** HELPER FUNCTIONS */

    public function obtenerEstadosSelect() {
        $devolver = [];
        $estados = EstadoPresupuesto::all();

        foreach($estados as $estado) {
            $devolver[$estado->id] = $estado->nombre;
        }

        return $devolver;
    }

    public function obtenerVendedoresSelect() {
        $devolver = [];
        $vendedores = User::where('rol', 2)
            ->get();

        foreach($vendedores as $vendedor) {
            $devolver[$vendedor->id] = $vendedor->nombre." ".$vendedor->apellido;
        }

        return $devolver;
    }

    public function obtenerUsuariosSelect() {
        $devolver = [];
        $vendedores = User::all();

        foreach($vendedores as $vendedor) {
            $devolver[$vendedor->id] = $vendedor->nombre." ".$vendedor->apellido;
        }

        return $devolver;
    }

    /** END HELPER FUNCTIONS */

    public function vendedores() {

//        $vendedores = User::where('rol', 2)
//            ->get();
        $vendedores = User::all();

        return view('reportes.vendedores')->with([
            'elementos' => $vendedores,
        ]);
    }

    public function vendedor($id) {
        $vendedor = User::find($id);

        if(!empty($vendedor)) {
            return view('reportes.vendedor')->with([
                'elemento' => $vendedor,
            ]);
        }

        return $this->vendedores()->withErrors(['existe' => 'El usuario solicitado no existe']);
    }

    public function general() {
        $contactosTotalesDelMes = Cliente::whereRaw('MONTH(created_at) = MONTH(NOW())')
            ->get()
            ->count();

        return view('reportes.general')->with([
            'contactosTotalesDelMes' => $contactosTotalesDelMes,
        ]);
    }

    public function formasDeContacto() {

        $desde = Carbon::now()->format('Y-m-').'01';
        $hasta = Carbon::now()->format('Y-m-d');

        $contactos = DB::table('forma_contactos')
            ->selectRaw('forma_contactos.nombre as nombre,
            (SELECT COUNT(*) FROM presupuestos JOIN clientes ON presupuestos.cliente = clientes.id WHERE clientes.contacto = forma_contactos.id) as cantidad')
        ->get();

        return view('reportes.formasContacto')->with([
            'elementos' => $contactos,
            'desde' => $desde,
            'hasta' => $hasta,
            'confirmados' => 0,
        ]);
    }

    public function buscarFormasDeContacto(Request $request) {

        $rules = [
            'desde' => 'nullable|date',
            'hasta' => 'nullable|date',
            'confirmados' => 'required|boolean',
        ];

        $messages = [
            'date' => 'El campo :attribute tiene que ser una fecha válida',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->formasDeContacto()->withErrors($validator);
        }

        $desde = Carbon::now()->format('Y-m-').'01';
        $hasta = Carbon::now()->format('Y-m-d');
        $confirmados = $request->input('confirmados');
        $wheresAdicionales = "";

        if($request->input('desde')) {
            $wheresAdicionales .= ' AND DATE(presupuestos.created_at) >= "'.$request->input('desde').'"';
            $desde = Carbon::parse($request->input('desde'))->format('Y-m-').'01';
        }

        if($request->input('hasta')) {
            $wheresAdicionales .= ' AND DATE(presupuestos.created_at) <= "'.$request->input('hasta').'"';
            $hasta = Carbon::parse($request->input('hasta'))->format('Y-m-d');
        }

        if($confirmados) {
            $wheresAdicionales .= ' AND EXISTS(SELECT * FROM historial_estados WHERE historial_estados.presupuesto = presupuestos.id AND historial_estados.estado = 7)';
        }

        $contactos = DB::table('forma_contactos')
            ->selectRaw('forma_contactos.nombre as nombre,
            (SELECT COUNT(*) FROM presupuestos 
            JOIN clientes ON presupuestos.cliente = clientes.id WHERE clientes.contacto = forma_contactos.id '.$wheresAdicionales.') as cantidad')
        ->get();

//        $contactos = DB::table('historial_estados')
//            ->selectRaw('forma_contactos.nombre as nombre,
//            (SELECT COUNT(*) FROM presupuestos JOIN clientes ON presupuestos.cliente = clientes.id WHERE presupuestos.estado = 7 AND clientes.contacto = forma_contactos.id '.$wheresAdicionales.') as cantidad')
//            ->join('presupuestos', 'historial_estados.presupuesto', '=', 'presupuestos.id')
//            ->get();

        return view('reportes.formasContacto')->with([
            'elementos' => $contactos,
            'desde' => $desde,
            'hasta' => $hasta,
            'confirmados' => $confirmados,
        ]);
    }

    public function presupuestosExcel(Request $request) {

        $rules = [
            'desde' => 'nullable|date',
            'hasta' => 'nullable|date',
            'estado' => 'nullable|integer|exists:estado_presupuestos,id',
            'vendedor' => 'nullable|integer|exists:users,id'
        ];

        $messages = [
            'date' => 'El campo :attribute tiene que ser una fecha válida',
            'exists' => 'El campo :attribute posee un valor no existente',
            'integer' => 'El campo :attribute tiene que ser un entero'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return view('reportes.presupuestosExcel')->withErrors($validator);
        }

        $presupuestosDB = DB::table('presupuestos')
            ->select('presupuestos.id');

        if($request->input('desde') != '') {
            $fecha = Carbon::parse($request->input('desde'))->format('Y-m-d');
            $presupuestosDB->whereRaw('DATE(created_at) >= "'.$fecha.'"');
        }

        if($request->input('hasta') != '') {
            $fecha = Carbon::parse($request->input('hasta'))->format('Y-m-d');
            $presupuestosDB->whereRaw('DATE(created_at) <= "'.$fecha.'"');
        }

        if($request->input('estado') != '') {

            $estado = $request->input('estado');

            $presupuestosDB->whereExists(function($query) use ($estado) {
                $query->select(DB::raw(1))
                    ->from('historial_estados')
                    ->whereRaw('historial_estados.presupuesto = presupuestos.id AND historial_estados.estado = '.$estado);
            });
        }

        if($request->input('vendedor') != '') {
            $presupuestosDB->where('vendedor', $request->input('vendedor'));
        }

        $presupuestosDB = $presupuestosDB->get();

        $presupuestos = [];
        foreach($presupuestosDB as $presupuestoDB) {
            $presupuestos[] = Presupuesto::find($presupuestoDB->id);
        }

        $presupuestos = collect($presupuestos);

        $nombreArchivo = str_replace('/', '-', str_replace('.', '-', Hash::make(Carbon::now()->format('Y-m-d H:i:s'))));
        Excel::store(new PresupuestoExport($presupuestos), 'excelsPresupuestos/'.$nombreArchivo.'.xlsx', 'public', \Maatwebsite\Excel\Excel::XLSX);

        return view('reportes.presupuestosExcel')->with([
            'elementos' => $presupuestos,
            'estados' => $this->obtenerEstadosSelect(),
            'vendedores' => $this->obtenerUsuariosSelect(),
            'nombreArchivo' => $nombreArchivo,
        ]);
    }

    public function descargarPresupuestoExcel($nombreArchivo) {
        return response()->download(storage_path('app/public/excelsPresupuestos/'.$nombreArchivo.'.xlsx'), 'Presupuestos.xlsx');
    }

    /** AJAX */

    public function vendedorPresupuestoAjax($id) {

        $meses = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
        $devolver = new \stdClass();
        $vendedor = User::find($id);

        if(!empty($vendedor)) {

            // GENERADOS
            $devolver->generados = new \stdClass();
            $valoresGenerados = [];
            foreach($meses as $mes) {
                $presupuestos = DB::table('presupuestos')
                    ->selectRaw('count(*) as cantidad')
                    ->whereRaw('vendedor = '.$id.' AND MONTH(created_at) = '.$mes)
                    ->first();

                $valoresGenerados[] = $presupuestos->cantidad;
            }
            $devolver->generados->valores = $valoresGenerados;

            // CONFIRMADOS
            $devolver->confirmados = new \stdClass();
            $valoresConfirmados = [];
            foreach($meses as $mes) {
                $presupuestos = DB::table('presupuestos')
                    ->selectRaw('count(*) as cantidad')
                    ->whereRaw('vendedor = '.$id.' AND MONTH(created_at) = '.$mes.' AND estado = 7')
                    ->first();

                $valoresConfirmados[] = $presupuestos->cantidad;
            }
            $devolver->confirmados->valores = $valoresConfirmados;
        }

        return Response::json($devolver);
    }

    public function vendedorProspectosAjax($id) {

        $meses = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
        $devolver = new \stdClass();
        $vendedor = User::find($id);

        if(!empty($vendedor)) {

            // PROSPECTOS
            $devolver->prospectos = new \stdClass();
            $valoresProspectos = [];
            foreach($meses as $mes) {
                $presupuestos = DB::table('clientes')
                    ->selectRaw('count(*) as cantidad')
                    ->whereRaw('usuario = '.$id.' AND prospecto = 1 AND MONTH(created_at) = '.$mes)
                    ->first();

                $valoresProspectos[] = $presupuestos->cantidad;
            }
            $devolver->prospectos->valores = $valoresProspectos;

            // CLIENTES
            $devolver->clientes = new \stdClass();
            $valoresClientes = [];
            foreach($meses as $mes) {
                $presupuestos = DB::table('clientes')
                    ->selectRaw('count(*) as cantidad')
                    ->whereRaw('usuario = '.$id.' AND prospecto = 0 AND MONTH(created_at) = '.$mes)
                    ->first();

                $valoresClientes[] = $presupuestos->cantidad;
            }
            $devolver->clientes->valores = $valoresClientes;
        }

        return Response::json($devolver);
    }

    public function prospectos() {
        $meses = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
        $devolver = new \stdClass();

        // PROSPECTOS
        $devolver->prospectos = new \stdClass();
        $valoresProspectos = [];
        foreach($meses as $mes) {
            $presupuestos = DB::table('clientes')
                ->selectRaw('count(*) as cantidad')
                ->whereRaw('prospecto = 1 AND MONTH(created_at) = '.$mes)
                ->first();

            $valoresProspectos[] = $presupuestos->cantidad;
        }
        $devolver->prospectos->valores = $valoresProspectos;

        // CLIENTES
        $devolver->clientes = new \stdClass();
        $valoresClientes = [];
        foreach($meses as $mes) {
            $presupuestos = DB::table('clientes')
                ->selectRaw('count(*) as cantidad')
                ->whereRaw('prospecto = 0 AND MONTH(created_at) = '.$mes)
                ->first();

            $valoresClientes[] = $presupuestos->cantidad;
        }
        $devolver->clientes->valores = $valoresClientes;

        return Response::json($devolver);
    }

    public function presupuestos() {
        $meses = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
        $devolver = new \stdClass();

        // GENERADOS
        $devolver->generados = new \stdClass();
        $valoresGenerados = [];
        foreach($meses as $mes) {
            $presupuestos = DB::table('presupuestos')
                ->selectRaw('count(*) as cantidad')
                ->whereRaw('MONTH(created_at) = '.$mes)
                ->first();

            $valoresGenerados[] = $presupuestos->cantidad;
        }
        $devolver->generados->valores = $valoresGenerados;

        // CONFIRMADOS
        $devolver->confirmados = new \stdClass();
        $valoresConfirmados = [];
        foreach($meses as $mes) {
            $presupuestos = DB::table('presupuestos')
                ->selectRaw('count(*) as cantidad')
                ->whereRaw('MONTH(created_at) = '.$mes.' AND estado = 7')
                ->first();

            $valoresConfirmados[] = $presupuestos->cantidad;
        }
        $devolver->confirmados->valores = $valoresConfirmados;

        // CONTACTOS
        $devolver->contactos = new \stdClass();
        $valoresContactos = [];
        foreach($meses as $mes) {
            $contactosTotalesDelMes = Cliente::whereRaw('MONTH(created_at) = '.$mes)
                ->get()
                ->count();

            $valoresContactos[] = $contactosTotalesDelMes;
        }
        $devolver->contactos->valores = $valoresContactos;

        return Response::json($devolver);
    }

    /** END AJAX */

}
