<?php

namespace App\Http\Controllers\Views;

use App\Cliente;
use App\HistorialEstado;
use App\Mail\OlvidarContrasenia;
use App\OtroAdicional;
use App\Presupuesto;
use App\User;
use App\Visita;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PanelController extends Controller
{
    /** FUNCIONES AUXILIARES */

    public function obtenerConsultasDelMes() {
        $mes = Carbon::now()->format('n');

        $clientes = Cliente::whereRaw('MONTH(created_at) = '.$mes)
            ->get()
            ->count();

        return $clientes;
    }

    public function obtenerConsultasDelMesUsuario() {
        $mes = Carbon::now()->format('n');

        $clientes = Cliente::whereRaw('MONTH(created_at) = '.$mes)
            ->where('usuario', Auth::id())
            ->get()
            ->count();

        return $clientes;
    }

    public function obtenerPresupuestosEnviados() {
        $mes = Carbon::now()->format('n');

        $presupuestos = Presupuesto::whereRaw('MONTH(created_at) = '.$mes)
            ->get()
            ->count();

        return $presupuestos;
    }

    public function obtenerPresupuestosEnviadosUsuario() {
        $mes = Carbon::now()->format('n');

        $presupuestos = Presupuesto::whereRaw('MONTH(created_at) = '.$mes)
            ->where('vendedor', Auth::id())
            ->get()
            ->count();

        return $presupuestos;
    }

    public function obtenerVisitasConfirmadas() {
        $mes = Carbon::now()->format('n');
        $visitas = 0;

//        $visitas = Visita::whereRaw('MONTH(created_at) = '.$mes)
//            ->get()
//            ->count();

        $presupuestos = Presupuesto::where('estado', 2)
            ->get();

        foreach($presupuestos as $presupuesto) {
            $historial = HistorialEstado::where('presupuesto', $presupuesto->id)
                ->where('estado', 2)
                ->whereRaw('MONTH(fecha) = '.$mes)
                ->orderBy('id', 'desc')
                ->first();

            if(!empty($historial)) {
                $visitas++;
            }
        }

        return $visitas;
    }

    public function obtenerVisitasConfirmadasUsuario() {
        $mes = Carbon::now()->format('n');

        $visitas = 0;
        $presupuestos = Presupuesto::where('estado', 2)
            ->where('vendedor', Auth::id())
            ->get();

        foreach($presupuestos as $presupuesto) {
            $historial = HistorialEstado::where('presupuesto', $presupuesto->id)
                ->where('estado', 2)
                ->whereRaw('MONTH(fecha) = '.$mes)
                ->orderBy('id', 'desc')
                ->first();

            if(!empty($historial)) {
                $visitas++;
            }
        }

//        $visitas = DB::table('visitas')
//            ->join('presupuestos', 'visitas.presupuesto', '=', 'presupuestos.id')
//            ->whereRaw('MONTH(visitas.created_at) = '.$mes)
//            ->where('presupuestos.vendedor', Auth::id())
//            ->get()
//            ->count();

        return $visitas;
    }

    public function obtenerMudanzasConfirmadas() {
        $mes = Carbon::now()->format('n');

        $mudanzas = HistorialEstado::whereRaw('MONTH(created_at) = '.$mes)
            ->where('estado', 7)
            ->get()
            ->count();

        return $mudanzas;
    }

    public function obtenerMudanzasConfirmadasUsuario() {
        $mes = Carbon::now()->format('n');

        $mudanzas = DB::table('historial_estados')
            ->join('presupuestos', 'historial_estados.presupuesto', '=', 'presupuestos.id')
            ->whereRaw('MONTH(historial_estados.created_at) = '.$mes)
            ->where('historial_estados.estado', 7)
            ->where('presupuestos.vendedor', Auth::id())
            ->get()
            ->count();

        return $mudanzas;
    }

    public function prospectos() {
        $clientes = Cliente::where('prospecto', true)
            ->orderBy('created_at', 'asc')
            ->get();

        return $clientes;
    }

    public function visitasDeHoy() {
        $visitas = [];
        $presupuestos = Presupuesto::where('estado', 2)
            ->get();

        foreach($presupuestos as $presupuesto) {
            $historial = HistorialEstado::where('presupuesto', $presupuesto->id)
                ->where('estado', 2)
                ->whereRaw('fecha = NOW()')
                ->orderBy('id', 'desc')
                ->first();

            if(!empty($historial)) {
                $visitas[] = $historial;
            }
        }

        return $visitas;
    }

    public function mudanzasDeHoy() {

        $presupuestos = Presupuesto::where('estado', 7)
            ->whereRaw('fecha = NOW()')
            ->get();

        return $presupuestos;
    }

    public function mudanzasDeManana() {

        $presupuestos = Presupuesto::where('estado', 7)
            ->where(function($query) {
                $query->select(DB::raw(1))
                    ->from('presupuestos')
                    ->whereRaw('DATEDIFF(fecha, NOW()) = 1')
                    ->orWhereRaw('DAYOFWEEK(NOW()) = 6 AND DAYOFWEEK(fecha) = 2');
            })
            ->get();

        return $presupuestos;
    }

    public function entregaDeEmbalajeHoy() {

        $embalajes = [];
        $presupuestos = Presupuesto::where('estado', 9)
            ->get();

        foreach($presupuestos as $presupuesto) {

            if($presupuesto->visita()->detalle()->embalaje == 0) {
                $historial = HistorialEstado::where('presupuesto', $presupuesto->id)
                    ->where('estado', 9)
                    ->whereRaw('fecha = NOW()')
                    ->orderBy('id', 'desc')
                    ->first();

                if (!empty($historial)) {
                    $embalajes[] = $presupuesto;
                }
            }
        }

//        $presupuestos = DB::table('detalle_visitas')
//            ->join('visitas', 'detalle_visitas.visita', '=', 'visitas.id')
//            ->join('presupuestos', 'visitas.presupuesto', '=', 'presupuestos.id')
//            ->where('detalle_visitas.embalaje', 0)
//            ->whereRaw('detalle_visitas.fecha = NOW()')
//            ->get();

        return $embalajes;
    }

    public function adicionalesDeHoy() {

        $adicionales = OtroAdicional::whereRaw('fecha = NOW()')
            ->get();

        return $adicionales;
    }

    /** END FUNCIONES AUXILIARES */

    public function indexAdministrador() {

        return view('indexAdministrador')->with([
            'llamadosAProspectos' => $this->prospectos(),
            'mudanzasDeHoy' => $this->mudanzasDeHoy(),
            'visitasDeHoy' => $this->visitasDeHoy(),

            'consultasDelMes' => $this->obtenerConsultasDelMes(),
            'presupuestosEnviados' => $this->obtenerPresupuestosEnviados(),
            'visitasConfirmadas' => $this->obtenerVisitasConfirmadas(),
            'mudanzasConfirmadas' => $this->obtenerMudanzasConfirmadas(),
        ]);

    }

    public function indexUsuario() {

        return view('indexUsuario')->with([
            'llamadosAProspectos' => $this->prospectos(),
            'mudanzasDeManana' => $this->mudanzasDeManana(),
            'mudanzasDeHoy' => $this->mudanzasDeHoy(),
            'visitasDeHoy' => $this->visitasDeHoy(),
            'entregaDeEmbalajeHoy' => $this->entregaDeEmbalajeHoy(),
            'adicionalesDeHoy' => $this->adicionalesDeHoy(),

            'consultasDelMes' => $this->obtenerConsultasDelMes(),
            'presupuestosEnviados' => $this->obtenerPresupuestosEnviados(),
            'visitasConfirmadas' => $this->obtenerVisitasConfirmadas(),
            'mudanzasConfirmadas' => $this->obtenerMudanzasConfirmadas(),
            'consultasDelMesUsuario' => $this->obtenerConsultasDelMesUsuario(),
            'presupuestosEnviadosUsuario' => $this->obtenerPresupuestosEnviadosUsuario(),
            'visitasConfirmadasUsuario' => $this->obtenerVisitasConfirmadasUsuario(),
            'mudanzasConfirmadasUsuario' => $this->obtenerMudanzasConfirmadasUsuario(),
        ]);

    }

    public function index() {

        if(Auth::user()->rol == 1) {
            return $this->indexAdministrador();
        }

        return $this->indexUsuario();
    }

    public function login() {
        return view('login');
    }

    public function loguear(Request $request) {

        $rules = [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|string',
            'recordarme' => 'nullable|boolean',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'email' => 'El campo :attribute tiene que ser un email válido',
            'exists' => 'El email ingresado no se encuentra registrado',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            return $this->login()->withErrors($validator);
        }

        if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $request->input('recordarme'))) {
            return redirect('/');
        }

        return $this->login()->withErrors(['error' => 'La contraseña es erronea.']);
    }

    public function cerrarSesion() {
        Auth::logout();
        return redirect('/');
    }

    public function olvidarContrasenia(Request $request) {

        $rules = [
            'email' => 'required|email|exists:users,email',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'email' => 'El campo :attribute tiene que ser un email válido',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            return $this->login()->withErrors($validator);
        }

        $usuario = User::where('email', $request->input('email'))->first();

        $peticionAnterior = DB::table('password_resets')->where('email', $usuario->email)->first();

        if(empty($peticionAnterior)) {
            $token = Hash::make($usuario->id . rand(0, 99));
            DB::table('password_resets')
                ->insert([
                    'email' => $usuario->email,
                    'token' => $token,
                    'created_at' => 'NOW()',
                ]);

            Mail::to($request->input('email'))->send(new OlvidarContrasenia($usuario, $token));

            return $this->login()->with(['success' => 'Por favor verifica tu cuenta de mail para poder cambiar tu contraseña']);
        }

        return $this->login()->withErrors(['existe' => 'Usted ya tiene una petición para cambiar la contraseña en curso']);

    }

    public function cambiarContrasenia($token) {

        $peticion = DB::table('password_resets')->where('token', $token)->get();

        if(!empty($peticion)) {
            return view('cambiarContrasenia');
        }

        return redirect('/')->withErrors(['existe' => 'Token inválido']);

    }

    public function cambioContrasenia(Request $request, $token) {

        $peticion = DB::table('password_resets')->where('token', $token)->get();

        if(!empty($peticion)) {

            $rules = [
                'password' => 'required|string|confirmed',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'confirmed' => 'Las contraseñas no coinciden',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {
                return $this->cambiarContrasenia($token)->withErrors($validator);
            }

            $usuario = User::where('email', $peticion->email)->first();
            $usuario->password = Hash::make($request->input('password'));
            $usuario->save();

            DB::table('password_resets')->where('token', $token)->delete();

            return $this->cambiarContrasenia($token)->with(['success' => 'La contraseña fue actualizada con éxito']);
        }

        return redirect('/')->withErrors(['existe' => 'Token inválido']);

    }

}
