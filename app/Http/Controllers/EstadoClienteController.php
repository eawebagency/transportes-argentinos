<?php

namespace App\Http\Controllers;

use App\EstadoCliente;
use Illuminate\Http\Request;

class EstadoClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EstadoCliente  $estadoCliente
     * @return \Illuminate\Http\Response
     */
    public function show(EstadoCliente $estadoCliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EstadoCliente  $estadoCliente
     * @return \Illuminate\Http\Response
     */
    public function edit(EstadoCliente $estadoCliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EstadoCliente  $estadoCliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstadoCliente $estadoCliente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoCliente  $estadoCliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstadoCliente $estadoCliente)
    {
        //
    }
}
