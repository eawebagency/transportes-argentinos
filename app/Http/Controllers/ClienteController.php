<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\EstadoCliente;
use App\FormaContacto;
use App\Rules\ClienteExistente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ClienteController extends Controller
{
    public function obtenerFormasContactoSelect() {
        $devolver = array();

        $formas = FormaContacto::all();

        foreach($formas as $forma) {
            $devolver[$forma->id] = $forma->nombre;
        }

        return $devolver;
    }

    public function obtenerEstadosSelect() {
        $devolver = [];

        $estados = EstadoCliente::all();

        foreach($estados as $estado) {
            $devolver[$estado->id] = $estado->nombre;
        }

        return $devolver;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::orderBy('created_at', 'desc')
            ->get();

        return view('clientes.editar')->with([
            'elementos' => $clientes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados = $this->obtenerEstadosSelect();
        $formas = $this->obtenerFormasContactoSelect();

        return view('clientes.crear')->with([
            'formas' => $formas,
            'estados' => $estados,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'nullable|string',
            'apellido' => 'nullable|string',
            'email' => 'nullable|email',
            'telefono' => ['required', 'string', new ClienteExistente($request->input('nombre'), $request->input('apellido'), $request->input('email'), $request->input('wpp'), $request->input('telefono_alternativo'))],
            'wpp' => 'nullable|boolean',
            'telefono_alternativo' => 'nullable|string',
            'contacto' => 'required|exists:forma_contactos,id',
            'estado' => 'required|exists:estado_clientes,id',
            'observaciones' => 'nullable|string',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'boolean' => 'El campo :attribute tiene que ser un booleano',
            'unique' => 'El :attribute ingresado ya se encuentra registrado',
            'required_without' => 'El campo :attribute sólo es aceptado si :attribute es nulo',
            'integer' => 'El campo :attribute tiene que ser un entero',
            'exists' => 'El valor ingresado en el campo :attribute no existe',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->create()->withErrors($validator);
        }

        $cliente = new Cliente;
        $cliente->nombre = ucwords($request->input('nombre'));
        $cliente->apellido = ucwords($request->input('apellido'));
        $cliente->email = $request->input('email');
        $cliente->telefono = $request->input('telefono');
        $cliente->wpp = $request->input('wpp');
        $cliente->telefono_alternativo = $request->input('telefono_alternativo');
        $cliente->usuario = Auth::id();
        $cliente->ultimoUsuario = Auth::id();
        $cliente->contacto = $request->input('contacto');
        $cliente->estado = $request->input('estado');
        $cliente->observaciones = $request->input('observaciones');
        $cliente->prospecto = true;
        $cliente->save();

        return $this->index()->with(['success' => 'Cliente creado con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estados = $this->obtenerEstadosSelect();
        $formas = $this->obtenerFormasContactoSelect();
        $cliente = Cliente::find($id);

        if(!empty($cliente)) {
            return view('clientes.editarIndividual')->with([
                'formas' => $formas,
                'estados' => $estados,
                'elemento' => $cliente,
            ]);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = Cliente::find($id);

        if(!empty($cliente)) {
            $rules = [
                'nombre' => 'nullable|string',
                'apellido' => 'nullable|string',
                'email' => 'nullable|email',
                'telefono' => 'required|string',
                'wpp' => 'nullable|boolean',
                'telefono_alternativo' => 'nullable|string',
                'contacto' => 'required|exists:forma_contactos,id',
                'estado' => 'required|exists:estado_clientes,id',
                'observaciones' => 'nullable|string',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'boolean' => 'El campo :attribute tiene que ser un booleano',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
                'required_without' => 'El campo :attribute sólo es aceptado si :required_without es nulo',
                'integer' => 'El campo :attribute tiene que ser un entero',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->edit($id)->withErrors($validator);
            }

            $cliente->nombre = ucwords($request->input('nombre'));
            $cliente->apellido = ucwords($request->input('apellido'));
            $cliente->email = $request->input('email');
            $cliente->telefono = $request->input('telefono');
            $cliente->wpp = $request->input('wpp');
            $cliente->telefono_alternativo = $request->input('telefono_alternativo');
            $cliente->contacto = $request->input('contacto');
            $cliente->estado = $request->input('estado');
            $cliente->observaciones = $request->input('observaciones');
            $cliente->ultimoUsuario = Auth::id();
            $cliente->save();

            return $this->edit($id)->with(['success' => 'Cliente editado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento seleccionado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::find($id);

        if(!empty($cliente)) {
            if($cliente->delete()) {
                return $this->index()->with(['success' => 'El cliente ha sido eliminado con éxito']);
            }
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
