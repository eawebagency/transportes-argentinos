<?php

namespace App\Http\Controllers;

use App\Adicional;
use App\OtroAdicional;
use App\Presupuesto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OtroAdicionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($presupuesto)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {

            $presupuestoController = new PresupuestoController();
            return $presupuestoController->edit($presupuesto);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($presupuesto)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {
            return view('otros_adicionales.crear')->with([
                'presupuesto' => $presupuesto,
            ]);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $presupuesto)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {
            $rules = [
                'nombre' => 'required|string',
                'valor' => 'required|numeric',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'numeric' => 'El campo :attribute tiene que ser un número válido',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                $request->flash();

                return $this->create($presupuesto)->withErrors($validator);
            }

            $adicional = new OtroAdicional;
            $adicional->nombre = $request->input('nombre');
            $adicional->valor = $request->input('valor');
            $adicional->presupuesto = $presupuesto;
            $adicional->save();

            return $this->create($presupuesto)->with(['success' => 'El adicional fue creado con éxito']);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OtroAdicional  $otroAdicional
     * @return \Illuminate\Http\Response
     */
    public function show(OtroAdicional $otroAdicional)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OtroAdicional  $otroAdicional
     * @return \Illuminate\Http\Response
     */
    public function edit($presupuesto, $id)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {

            $adicional = OtroAdicional::find($id);

            if(!empty($adicional)) {
                return view('otros_adicionales.editarIndividual')->with([
                    'elemento' => $adicional,
                ]);
            }

            return $this->index($presupuesto)->withErrors(['existe' => 'El elemento solicitado no existe']);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OtroAdicional  $otroAdicional
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $presupuesto, $id)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {

            $adicional = OtroAdicional::find($id);

            if(!empty($adicional)) {
                $rules = [
                    'nombre' => 'required|string',
                    'valor' => 'required|numeric',
                ];

                $messages = [
                    'required' => 'El campo :attribute es requerido',
                    'numeric' => 'El campo :attribute tiene que ser un número válido',
                    'unique' => 'El :attribute ingresado ya se encuentra registrado',
                ];

                $validator = Validator::make($request->all(), $rules, $messages);

                if($validator->fails()) {

                    return $this->edit($presupuesto, $id)->withErrors($validator);
                }

                $adicional->nombre = $request->input('nombre');
                $adicional->valor = $request->input('valor');
                $adicional->save();

                return $this->edit($presupuesto, $id)->with(['success' => 'El adicional fue editado con éxito']);
            }

            return $this->index($presupuesto)->withErrors(['existe' => 'El elemento solicitado no existe']);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OtroAdicional  $otroAdicional
     * @return \Illuminate\Http\Response
     */
    public function destroy($presupuesto, $id)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {

            $adicional = OtroAdicional::find($id);

            if(!empty($adicional)) {
                if($adicional->delete()) {
                    return $this->index($presupuesto)->with(['success' => 'El adicional fue eliminado con éxito']);
                }
            }

            return $this->index($presupuesto)->withErrors(['existe' => 'El elemento solicitado no existe']);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
