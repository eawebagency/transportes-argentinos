<?php

namespace App\Http\Controllers;

use App\Adicional;
use App\Cliente;
use App\DetalleVisita;
use App\Direccion;
use App\EstadoCliente;
use App\EstadoPresupuesto;
use App\Evento;
use App\FormaContacto;
use App\HistorialEstado;
use App\OtroAdicional;
use App\Presupuesto;
use App\Rules\EsVisitador;
use App\Servicio;
use App\User;
use App\Vehiculo;
use App\Visita;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class PresupuestoController extends Controller
{
    public function obtenerEstadosSelect() {
        $devolver = [];
        $estados = EstadoPresupuesto::all();

        foreach($estados as $estado) {
            $devolver[$estado->id] = $estado->nombre;
        }

        return $devolver;
    }

    public function obtenerVehiculosSelect() {
        $devolver = [];
        $vehiculos = Vehiculo::where('activo', 1)
            ->orderBy('orden', 'asc')->get();

        foreach($vehiculos as $vehiculo) {
            $devolver[$vehiculo->id] = $vehiculo->patente;
        }

        return $devolver;
    }

    public function obtenerServiciosSelect() {
        $devolver = [];
        $servicios = Servicio::where('activo', 1)
            ->orderBy('nombre', 'asc')->get();

        foreach($servicios as $servicio) {
            $devolver[$servicio->id] = $servicio->nombre;
        }

        return $devolver;
    }

    public function obtenerClientesSelect() {
        $devolver = [];
        $clientes = Cliente::all();

        foreach($clientes as $cliente) {
            $devolver[$cliente->id] = $cliente->nombre." ".$cliente->apellido;
        }

        return $devolver;
    }

    public function obtenerFormasContactoSelect() {
        $devolver = array();

        $formas = FormaContacto::all();

        foreach($formas as $forma) {
            $devolver[$forma->id] = $forma->nombre;
        }

        return $devolver;
    }

    public function obtenerVisitadoresSelect() {
        $devolver = array();

        $visitadores = User::where('rol', 3)->get();

        foreach($visitadores as $visitador) {
            $devolver[$visitador->id] = $visitador->nombre." ".$visitador->apellido;
        }

        return $devolver;
    }

    public function obtenerEstadosClientesSelect() {
        $devolver = [];

        $estados = EstadoCliente::all();

        foreach($estados as $estado) {
            $devolver[$estado->id] = $estado->nombre;
        }

        return $devolver;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fechaHoy = Carbon::now();

        $presupuestos = Presupuesto::where('created_at', '>=', $fechaHoy->subMonths(3)->format('Y-m-d 00:00:00'))
            ->orderBy('created_at', 'desc')
            ->get();

        return view('presupuestos.editar')->with([
            'elementos' => $presupuestos,
        ]);
    }

    public function buscar(Request $request) {
        $rules = [
            'desde' => 'nullable|date',
            'hasta' => 'nullable|date',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'date' => 'El campo :attribute tiene que ser una fecha válida',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            $request->flash();
            return $this->index()->withErrors($validator);
        }

        $presupuestosQuery = Presupuesto::query();

        if(!empty($request->input('desde'))) {
            $fecha = Carbon::parse($request->input('desde'));
            $presupuestosQuery->where('created_at', '>=', $fecha->format('Y-m-d 00:00:00'));
        }

        if(!empty($request->input('hasta'))) {
            $fecha = Carbon::parse($request->input('hasta'));
            $presupuestosQuery->where('created_at', '<=', $fecha->format('Y-m-d 00:00:00'));
        }

        $presupuestos = $presupuestosQuery->orderBy('created_at', 'desc')
            ->get();

        return view('presupuestos.editar')->with([
            'elementos' => $presupuestos,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rules = [
            'cliente' => 'nullable|integer|exists:clientes,id',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'exists' => 'El :attribute ingresado no se encuentra',
            'integer' => 'El campo :attribute tiene que ser un entero',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            return redirect()->back()->withErrors($validator);
        }

        $vehiculos = $this->obtenerVehiculosSelect();
        $servicios = $this->obtenerServiciosSelect();
        $clientes = $this->obtenerClientesSelect();
        $adicionales = Adicional::all();
        $formas = $this->obtenerFormasContactoSelect();
        $estados = $this->obtenerEstadosSelect();

        return view('presupuestos.crear')->with([
            'vehiculos' => $vehiculos,
            'servicios' => $servicios,
            'clientes' => $clientes,
            'adicionales' => $adicionales,
            'cliente' => Cliente::find($request->input('cliente')),
            'formas' => $formas,
            'estados' => $estados,
            'estadosCliente' => $this->obtenerEstadosClientesSelect(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'cliente_existente' => 'required|boolean',
            'cliente_nombre' => 'nullable|string',
            'cliente_apellido' => 'nullable|string',
            'cliente_email' => 'nullable|email',
            'cliente_telefono' => 'required|string',
            'cliente_wpp' => 'nullable|boolean',
            'cliente_telefono_alternativo' => 'nullable|string',
            'cliente_contacto' => 'required|exists:forma_contactos,id',
            'cliente_observaciones' => 'nullable|string',
            'cliente_estado' => 'nullable|integer|exists:estado_clientes,id',
            'estado' => 'required|exists:estado_presupuestos,id',
            'fecha_estado' => 'nullable|date',
            'estado_hora_desde' => 'nullable|date_format:"H:i"',
            'estado_hora_hasta' => 'nullable|date_format:"H:i"',
            'estado_observaciones' => 'nullable|string',
            'cantidad_direcciones_carga' => 'required|integer',
            'cantidad_direcciones_descarga' => 'required|integer',
            'direccion_carga' => 'nullable|array',
            'direccion_carga.*' => 'nullable|string',
            'departamento_carga' => 'required|array',
            'departamento_carga.*' => 'required|string',
            'piso_carga' => 'required|array',
            'piso_carga.*' => 'required|string',
            'entrecalles_carga' => 'nullable|array',
            'entrecalles_carga.*' => 'nullable|string',
            'provincia_carga' => 'required|array',
            'provincia_carga.*' => 'required|string',
            'localidad_carga' => 'required|array',
            'localidad_carga.*' => 'required|string',
            'observaciones_carga' => 'nullable|array',
            'observaciones_carga.*' => 'nullable|string',
            'direccion_descarga' => 'nullable|array',
            'departamento_descarga' => 'nullable|array',
            'departamento_descarga.*' => 'required|string',
            'piso_descarga' => 'nullable|array',
            'piso_descarga.*' => 'required|string',
            'entrecalles_descarga' => 'nullable|array',
            'provincia_descarga' => 'nullable|array',
            'provincia_descarga.*' => 'required|string',
            'localidad_descarga' => 'nullable|array',
            'localidad_descarga.*' => 'required|string',
            'observaciones_descarga' => 'nullable|array',
            'observaciones_descarga.*' => 'nullable|string',
            'fecha' => 'nullable|date',
            'fecha_fin' => 'nullable|date',
            'hora' => 'nullable|date_format:"H:i"',
            'hora_fin' => 'nullable|date_format:"H:i"',
            'ambientes' => 'nullable|integer',
            'vehiculo' => 'required|exists:vehiculos,id',
            'servicio' => 'required|exists:servicios,id',
            'larga_distancia' => 'nullable|boolean',
            'kms' => 'nullable|numeric',
            'personal' => 'nullable|integer',
            'cotizacion' => 'required|numeric',
            'valor_final' => 'required|numeric',
            'vencimiento' => 'required|date',
            'observaciones' => 'nullable|string',
            'cliente' => 'required_if:cliente_existente,==,1|nullable|exists:clientes,id',
            'adicionales_nombre' => 'nullable|array',
            'adicionales_nombre.*' => 'required|string',
            'adicionales_valor' => 'nullable|array',
            'adicionales_valor.*' => 'required|numeric',
            'adicionales_fecha' => 'nullable|array',
            'adicionales_fecha.*' => 'nullable|date',
            'adicionales_hora_inicio' => 'nullable|array',
            'adicionales_hora_inicio.*' => 'nullable|date_format:"H:i"',
            'adicionales_hora_fin' => 'nullable|array',
            'adicionales_hora_fin.*' => 'nullable|date_format:"H:i"',
            'adicionales_personal' => 'nullable|array',
            'adicionales_personal.*' => 'nullable|integer',
            'adicionales_duracion' => 'nullable|array',
            'adicionales_duracion.*' => 'nullable|integer',
            'adicionales_observaciones' => 'nullable|array',
            'adicionales_observaciones.*' => 'nullable|string',
            'impuesto' => 'required|numeric',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'numeric' => 'El campo :attribute tiene que ser un número válido',
            'exists' => 'El :attribute ingresado no se encuentra',
            'integer' => 'El campo :attribute tiene que ser un entero',
            'date' => 'El campo :attribute tiene que ser una fecha válida',
            'date_format' => 'El campo :attribute tiene que cumplir el formato dado',
            'required_if' => 'El campo :attribute es obligatorio',
            'departamento_descarga.*.required' => 'En una dirección de descarga, falta completar el campo departamento',
            'piso_descarga.*.required' => 'En una dirección de descarga, falta completar el campo piso',
            'provincia_descarga.*.required' => 'En una dirección de descarga, falta completar el campo provincia',
            'localidad_descarga.*.required' => 'En una dirección de descarga, falta completar el campo localidad',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            $request->flash();

            return $this->create($request)->withErrors($validator);
        }

        if ($request->input('cliente_existente')) {
            $cliente = Cliente::find($request->input('cliente'));

            $cliente->nombre = ucwords($request->input('cliente_nombre'));
            $cliente->apellido = ucwords($request->input('cliente_apellido'));
            $cliente->email = $request->input('cliente_email');
            $cliente->telefono = $request->input('cliente_telefono');
            $cliente->wpp = $request->input('cliente_wpp');
            $cliente->telefono_alternativo = $request->input('cliente_telefono_alternativo');
            $cliente->ultimoUsuario = Auth::id();
            $cliente->contacto = $request->input('cliente_contacto');
            $cliente->observaciones = $request->input('cliente_observaciones');
            $cliente->estado = $request->input('cliente_estado');
            $cliente->save();
        } else {

            // CREAMOS EL CLIENTE
            $cliente = new Cliente;
            $cliente->nombre = ucwords($request->input('cliente_nombre'));
            $cliente->apellido = ucwords($request->input('cliente_apellido'));
            $cliente->email = $request->input('cliente_email');
            $cliente->telefono = $request->input('cliente_telefono');
            $cliente->wpp = $request->input('cliente_wpp');
            $cliente->telefono_alternativo = $request->input('cliente_telefono_alternativo');
            $cliente->usuario = Auth::id();
            $cliente->ultimoUsuario = Auth::id();
            $cliente->contacto = $request->input('cliente_contacto');
            $cliente->observaciones = $request->input('cliente_observaciones');
            $cliente->prospecto = false;
            $cliente->estado = $request->input('cliente_estado');
            $cliente->save();
        }

        // PRESUPUESTO
        $presupuesto = new Presupuesto;
        $presupuesto->fecha = $request->input('fecha');
        $presupuesto->fecha_fin = $request->input('fecha_fin');
        $presupuesto->hora = $request->input('hora');
        $presupuesto->hora_fin = $request->input('hora_fin');
        $presupuesto->ambientes = $request->input('ambientes');
        $presupuesto->vehiculo = $request->input('vehiculo');
        $presupuesto->servicio = $request->input('servicio');
        $presupuesto->larga_distancia = (empty($request->input('larga_distancia'))) ? false : $request->input('larga_distancia');
        $presupuesto->kms = $request->input('kms');
        $presupuesto->personal = $request->input('personal');
        $presupuesto->valor_aproximado = $request->input('cotizacion');
        $presupuesto->cotizacion = $request->input('valor_final');
        $presupuesto->vencimiento = $request->input('vencimiento');
        $presupuesto->cliente = $cliente->id;
        $presupuesto->observaciones = $request->input('observaciones');
        $presupuesto->estado = $request->input('estado');
        $presupuesto->vendedor = Auth::id();
        $presupuesto->enviarDetalle = false;
        $presupuesto->enviarPresupuesto = false;
        $presupuesto->enviarMaterial = false;
        $presupuesto->enviarMudanza = false;
        $presupuesto->impuesto = $request->input('impuesto');
        $presupuesto->setearTotal();
        $presupuesto->save();

        // CREAMOS LA VISITA Y EL DETALLE
        $visita = new Visita;
        $visita->presupuesto = $presupuesto->id;
        $visita->save();
        $detalle = DetalleVisita::create(['embalaje' => 0, 'percheros_moviles' => 0, 'cajas' => 0, 'horas' => 0, 'visita' => $visita->id]);

        // HISTORIAL ESTADOS
        $historialEstado = new HistorialEstado;
        $historialEstado->presupuesto = $presupuesto->id;
        $historialEstado->estado = $request->input('estado');
        $historialEstado->usuario = Auth::id();
        $historialEstado->fecha = $request->input('fecha_estado');
        $historialEstado->desde = $request->input('estado_hora_desde');
        $historialEstado->hasta = $request->input('estado_hora_hasta');
        $historialEstado->observaciones = $request->input('estado_observaciones');
        $historialEstado->save();

        // ADICIONALES
//        if(!empty($request->input('adicionales'))) {
//            foreach ($request->input('adicionales') as $adicional) {
//                DB::table('presupuesto_adicionals')
//                    ->insert([
//                        'presupuesto' => $presupuesto->id,
//                        'adicional' => $adicional,
//                    ]);
//            }
//        }

        // OTROS ADICIONALES
        if(!empty($request->input('adicionales_nombre'))) {

            for($i = 0; $i < count($request->input('adicionales_nombre')); $i++) {

                if($request->input('adicionales_nombre')[$i] != '' && $request->input('adicionales_valor')[$i] != '') {
                    $otroadicional = new OtroAdicional;
                    $otroadicional->nombre = $request->input('adicionales_nombre')[$i];
                    $otroadicional->valor = $request->input('adicionales_valor')[$i];
                    $otroadicional->fecha = $request->input('adicionales_fecha')[$i];
                    $otroadicional->hora_inicio = $request->input('adicionales_hora_inicio')[$i];
                    $otroadicional->hora_fin = $request->input('adicionales_hora_fin')[$i];
                    $otroadicional->personal = $request->input('adicionales_personal')[$i];
                    $otroadicional->duracion = $request->input('adicionales_duracion')[$i];
                    $otroadicional->observaciones = $request->input('adicionales_observaciones')[$i];
                    $otroadicional->presupuesto = $presupuesto->id;
                    $otroadicional->save();
                }
            }
        }

        // DIRECCIONES CARGA
        for($i = 0; $i < $request->input('cantidad_direcciones_carga'); $i++) {
            $direccion = new Direccion;
            $direccion->direccion = ucwords($request->input('direccion_carga')[$i]);
            $direccion->departamento = $request->input('departamento_carga')[$i];
            $direccion->piso = $request->input('piso_carga')[$i];
            $direccion->entrecalles = ucwords($request->input('entrecalles_carga')[$i]);
            $direccion->provincia = $request->input('provincia_carga')[$i];
            $direccion->localidad = $request->input('localidad_carga')[$i];
            $direccion->observaciones = $request->input('observaciones_carga')[$i];
            $direccion->carga = true;
            $direccion->presupuesto = $presupuesto->id;
            $direccion->save();
        }

        // DIRECCIONES DESCARGA
        for($i = 0; $i < $request->input('cantidad_direcciones_descarga'); $i++) {

            if(isset($request->input('departamento_descarga')[$i]) && isset($request->input('piso_descarga')[$i]) && isset($request->input('provincia_descarga')[$i]) && isset($request->input('localidad_descarga')[$i])) {

                if ($request->input('departamento_descarga')[$i] != '' || $request->input('piso_descarga')[$i] != '' || $request->input('provincia_descarga')[$i] != '' || $request->input('localidad_descarga')[$i] != '') {

                    $direccion = new Direccion;
                    $direccion->direccion = ucwords($request->input('direccion_descarga')[$i]);
                    $direccion->departamento = $request->input('departamento_descarga')[$i];
                    $direccion->piso = $request->input('piso_descarga')[$i];
                    $direccion->entrecalles = ucwords($request->input('entrecalles_descarga')[$i]);
                    $direccion->provincia = $request->input('provincia_descarga')[$i];
                    $direccion->localidad = $request->input('localidad_descarga')[$i];
                    $direccion->observaciones = $request->input('observaciones_descarga')[$i];
                    $direccion->carga = false;
                    $direccion->presupuesto = $presupuesto->id;
                    $direccion->save();

                }

            }
        }

        // CAMBIAMOS EL CLIENTE A QUE NO SEA UN PROSPECTO
        $cliente = Cliente::find($cliente->id);
        $cliente->prospecto = false;
        $cliente->save();

        return $this->edit($presupuesto->id)->with(['success' => 'El presupuesto ha sido creado con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function show(Presupuesto $presupuesto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehiculos = $this->obtenerVehiculosSelect();
        $servicios = $this->obtenerServiciosSelect();
        $clientes = $this->obtenerClientesSelect();
        $estados = $this->obtenerEstadosSelect();
        $formas = $this->obtenerFormasContactoSelect();
        $visitadores = $this->obtenerVisitadoresSelect();

        $adicionales = Adicional::all();

        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {
            return view('presupuestos.editarIndividual')->with([
                'elemento' => $presupuesto,
                'vehiculos' => $vehiculos,
                'servicios' => $servicios,
                'clientes' => $clientes,
                'adicionales' => $adicionales,
                'estados' => $estados,
                'formas' => $formas,
                'visitadores' => $visitadores,
                'estadosCliente' => $this->obtenerEstadosClientesSelect(),
            ]);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {
            $rules = [
                'cliente_nombre' => 'nullable|string',
                'cliente_apellido' => 'nullable|string',
                'cliente_email' => 'nullable|email',
                'cliente_telefono' => 'required|string',
                'cliente_wpp' => 'nullable|boolean',
                'cliente_telefono_alternativo' => 'nullable|string',
                'cliente_contacto' => 'required|exists:forma_contactos,id',
                'cliente_observaciones' => 'nullable|string',
                'cliente_estado' => 'nullable|integer|exists:estado_clientes,id',
                'fecha' => 'required_if:estado,7|nullable|date',
                'fecha_fin' => 'required_if:estado,7|nullable|date',
                'hora' => 'required_if:estado,7|nullable|date_format:"H:i"',
                'hora_fin' => 'required_if:estado,7|nullable|date_format:"H:i"',
                'ambientes' => 'nullable|integer',
                'vehiculo' => 'required|exists:vehiculos,id',
                'servicio' => 'required|exists:servicios,id',
                'larga_distancia' => 'nullable|boolean',
                'kms' => 'nullable|numeric',
                'personal' => 'nullable|integer',
                'cotizacion' => 'required|numeric',
                'valor_final' => 'required|numeric',
                'vencimiento' => 'required|date',
                'observaciones' => 'nullable|string',
                'estado' => 'required|exists:estado_presupuestos,id',
                'adicionales' => 'nullable|array',
                'adicionales.*' => 'required|integer|exists:adicionals,id',
                'otros_adicionales_nombre' => 'nullable|array',
                'otros_adicionales_nombre.*' => 'nullable|string',
                'otros_adicionales_valor' => 'nullable|array',
                'otros_adicionales_valor.*' => 'nullable|numeric',
                'fecha_estado' => 'nullable|date',
                'estado_hora_desde' => 'nullable|date_format:"H:i"',
                'estado_hora_hasta' => 'nullable|date_format:"H:i"',
                'estado_observaciones' => 'nullable|string',

                'cantidad_direcciones_carga' => 'required|integer',
                'cantidad_direcciones_descarga' => 'required|integer',
                'direccion_carga' => 'nullable|array',
                'direccion_carga.*' => 'nullable|string',
                'departamento_carga' => 'required|array',
                'departamento_carga.*' => 'required|string',
                'piso_carga' => 'required|array',
                'piso_carga.*' => 'required|string',
                'entrecalles_carga' => 'nullable|array',
                'entrecalles_carga.*' => 'nullable|string',
                'provincia_carga' => 'required|array',
                'provincia_carga.*' => 'required|string',
                'localidad_carga' => 'required|array',
                'localidad_carga.*' => 'required|string',
                'observaciones_carga' => 'nullable|array',
                'observaciones_carga.*' => 'nullable|string',
                'direccion_descarga' => 'nullable|array',
                'departamento_descarga' => 'nullable|array',
                'departamento_descarga.*' => 'required|string',
                'piso_descarga' => 'nullable|array',
                'piso_descarga.*' => 'required|string',
                'entrecalles_descarga' => 'nullable|array',
                'provincia_descarga' => 'nullable|array',
                'provincia_descarga.*' => 'required|string',
                'localidad_descarga' => 'nullable|array',
                'localidad_descarga.*' => 'required|string',
                'observaciones_descarga' => 'nullable|array',
                'observaciones_descarga.*' => 'nullable|string',

//                'visita_fecha' => 'required_with_all:visita_hora_inicio,visita_hora_fin|nullable|date',
//                'visita_hora_inicio' => 'required_with_all:visita_fecha,visita_hora_fin|nullable|date_format:"H:i"',
//                'visita_hora_fin' => 'required_with_all:visita_hora_inicio,visita_fecha|nullable|date_format:"H:i"',
//                'visita_observaciones' => 'nullable|string',
//                'visita_observaciones_carga' => 'nullable|string',
//                'visita_observaciones_descarga' => 'nullable|string',

                'visita_visitador' => ['required', 'exists:users,id', new EsVisitador()],
                'adicionales_nombre' => 'nullable|array',
                'adicionales_nombre.*' => 'required|string',
                'adicionales_valor' => 'nullable|array',
                'adicionales_valor.*' => 'required|numeric',
                'adicionales_fecha' => 'nullable|array',
                'adicionales_fecha.*' => 'nullable|date',
                'adicionales_hora_inicio' => 'nullable|array',
                'adicionales_hora_inicio.*' => 'nullable|date_format:"H:i"',
                'adicionales_hora_fin' => 'nullable|array',
                'adicionales_hora_fin.*' => 'nullable|date_format:"H:i"',
                'adicionales_personal' => 'nullable|array',
                'adicionales_personal.*' => 'nullable|integer',
                'adicionales_duracion' => 'nullable|array',
                'adicionales_duracion.*' => 'nullable|integer',
                'adicionales_observaciones' => 'nullable|array',
                'adicionales_observaciones.*' => 'nullable|string',

                'impuesto' => 'required|numeric',
            ];

            if(!empty($presupuesto->visita())) {
                $rules['detallevisita_embalaje'] = 'required|boolean';
//                $rules['detallevisita_percheros_moviles'] = 'required|integer';
                $rules['detallevisita_cajas'] = 'required|integer';
                $rules['detallevisita_horas'] = 'required|integer';
            }

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'numeric' => 'El campo :attribute tiene que ser un número válido',
                'exists' => 'El :attribute ingresado no se encuentra',
                'integer' => 'El campo :attribute tiene que ser un entero',
                'date' => 'El campo :attribute tiene que ser una fecha válida',
                'date_format' => 'El campo :attribute tiene que cumplir el formato dado',
                'required_if' => 'El campo :attribute es requerido para el estado seleccionado',
                'required_with_all' => 'El campo :attribute es requerido si :values están completados',
                'fecha.required_if' => 'La fecha de inicio es requerida si el estado es Confirmado',
                'fecha_fin.required_if' => 'La fecha de fin es requerida si el estado es Confirmado',
                'hora.required_if' => 'La hora de inicio es requerida si el estado es Confirmado',
                'hora_fin.required_if' => 'La hora de fin es requerida si el estado es Confirmado',
                'departamento_descarga.*.required' => 'En una dirección de descarga, falta completar el campo departamento',
                'piso_descarga.*.required' => 'En una dirección de descarga, falta completar el campo piso',
                'provincia_descarga.*.required' => 'En una dirección de descarga, falta completar el campo provincia',
                'localidad_descarga.*.required' => 'En una dirección de descarga, falta completar el campo localidad',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                $request->flash();

                return $this->edit($id)->withErrors($validator);
            }

            // CLIENTE
            $cliente = $presupuesto->cliente();
            $cliente->nombre = ucwords($request->input('cliente_nombre'));
            $cliente->apellido = ucwords($request->input('cliente_apellido'));
            $cliente->email = $request->input('cliente_email');
            $cliente->telefono = $request->input('cliente_telefono');
            $cliente->ultimoUsuario = Auth::id();
            $cliente->wpp = $request->input('cliente_wpp');
            $cliente->telefono_alternativo = $request->input('cliente_telefono_alternativo');
            $cliente->contacto = $request->input('cliente_contacto');
            $cliente->observaciones = $request->input('cliente_observaciones');
            $cliente->estado = $request->input('cliente_estado');
            $cliente->save();

            // HISTORIAL ESTADO
            if($presupuesto->estado != $request->input('estado')) { // SI EL ESTADO DE ANTES ES DIFERENTE (OSEA QUE CAMBIO), ACTUALIZAMOS EL HISTORIAL
                $historialEstado = new HistorialEstado;
                $historialEstado->presupuesto = $presupuesto->id;
                $historialEstado->estado = $request->input('estado');
                $historialEstado->usuario = Auth::id();
                $historialEstado->fecha = $request->input('fecha_estado');
                $historialEstado->desde = $request->input('estado_hora_desde');
                $historialEstado->hasta = $request->input('estado_hora_hasta');
                $historialEstado->observaciones = $request->input('estado_observaciones');
                $historialEstado->save();

                // SI SE AGENDA UNA NUEVA VISITA, SE PONE ENVIAR DETALLE A FALSE
                if($request->input('estado') == 2) {
                    $presupuesto->enviarDetalle = false;
                }
            } else if($request->input('fecha_estado') != '' && $request->input('estado_hora_desde') != '' && $request->input('estado_hora_hasta')) {
                return $this->edit($id)->withErrors(['existe' => 'No puede agregar un nuevo estado igual al actual']);
            }

            // PRESUPUESTO
            if($presupuesto->fecha != $request->input('fecha')) {
                $presupuesto->enviarMudanza = false;
            }
            $presupuesto->fecha = $request->input('fecha');
            $presupuesto->fecha_fin = $request->input('fecha_fin');
            $presupuesto->hora = $request->input('hora');
            $presupuesto->hora_fin = $request->input('hora_fin');
            $presupuesto->ambientes = $request->input('ambientes');
            $presupuesto->vehiculo = $request->input('vehiculo');
            $presupuesto->servicio = $request->input('servicio');
            $presupuesto->larga_distancia = (empty($request->input('larga_distancia'))) ? false : $request->input('larga_distancia');
            $presupuesto->kms = $request->input('kms');
            $presupuesto->personal = $request->input('personal');
            $presupuesto->valor_aproximado = $request->input('cotizacion');
            $presupuesto->cotizacion = $request->input('valor_final');
            $presupuesto->vencimiento = $request->input('vencimiento');
            $presupuesto->estado = $request->input('estado');
            $presupuesto->observaciones = $request->input('observaciones');
            $presupuesto->fecha_estado = $request->input('fecha_estado');
            $presupuesto->hora_desde = $request->input('hora_desde');
            $presupuesto->hora_hasta = $request->input('hora_hasta');
            $presupuesto->impuesto = $request->input('impuesto');
            $presupuesto->setearTotal();
            $presupuesto->save();

            // VISITA
            $visita = $presupuesto->visita();
//            if(!empty($request->input('visita_fecha')) && !empty($request->input('visita_hora_inicio')) && !empty($request->input('visita_hora_fin'))) {

//                $visita->fecha = $request->input('visita_fecha');
//                $visita->hora_inicio = $request->input('visita_hora_inicio');
//                $visita->hora_fin = $request->input('visita_hora_fin');
//                $visita->observaciones = $request->input('visita_observaciones');
//                $visita->observaciones_carga = $request->input('visita_observaciones_carga');
//                $visita->observaciones_descarga = $request->input('visita_observaciones_descarga');
                $visita->visitador = $request->input('visita_visitador');
                $visita->save();

            //}

            $detalle = DetalleVisita::find($visita->id);
            $detalle->embalaje = $request->input('detallevisita_embalaje');
            $detalle->percheros_moviles = 0;
            $detalle->cajas = $request->input('detallevisita_cajas');
            $detalle->horas = $request->input('detallevisita_horas');
//            $detalle->fecha = $request->input('detallevisita_fecha_embalaje');
//            $detalle->hora_desde = $request->input('detallevisita_hora_desde');
//            $detalle->hora_hasta = $request->input('detallevisita_hora_hasta');
            $detalle->save();


//            DB::table('presupuesto_adicionals')->where('presupuesto', $presupuesto->id)->delete();
//            if(!empty($request->input('adicionales'))) {
//                foreach ($request->input('adicionales') as $adicional) {
//                    DB::table('presupuesto_adicionals')
//                        ->insert([
//                            'presupuesto' => $presupuesto->id,
//                            'adicional' => $adicional,
//                        ]);
//                }
//            }

            // OTROS ADICIONALES
            OtroAdicional::where('presupuesto', $presupuesto->id)->delete();
            if(!empty($request->input('adicionales_nombre'))) {

                for($i = 0; $i < count($request->input('adicionales_nombre')); $i++) {

                    if($request->input('adicionales_nombre')[$i] != '' && $request->input('adicionales_valor')[$i] != '') {
                        $otroadicional = new OtroAdicional;
                        $otroadicional->nombre = $request->input('adicionales_nombre')[$i];
                        $otroadicional->valor = $request->input('adicionales_valor')[$i];
                        $otroadicional->fecha = $request->input('adicionales_fecha')[$i];
                        $otroadicional->hora_inicio = $request->input('adicionales_hora_inicio')[$i];
                        $otroadicional->hora_fin = $request->input('adicionales_hora_fin')[$i];
                        $otroadicional->personal = $request->input('adicionales_personal')[$i];
                        $otroadicional->duracion = $request->input('adicionales_duracion')[$i];
                        $otroadicional->observaciones = $request->input('adicionales_observaciones')[$i];
                        $otroadicional->presupuesto = $presupuesto->id;
                        $otroadicional->save();
                    }
                }
            }

            Direccion::where('presupuesto', $presupuesto->id)
                ->delete(); //

            // DIRECCIONES CARGA
            for($i = 0; $i < $request->input('cantidad_direcciones_carga'); $i++) {
                $direccion = new Direccion;
                $direccion->direccion = ucwords($request->input('direccion_carga')[$i]);
                $direccion->departamento = $request->input('departamento_carga')[$i];
                $direccion->piso = $request->input('piso_carga')[$i];
                $direccion->entrecalles = ucwords($request->input('entrecalles_carga')[$i]);
                $direccion->provincia = $request->input('provincia_carga')[$i];
                $direccion->localidad = $request->input('localidad_carga')[$i];
                $direccion->observaciones = $request->input('observaciones_carga')[$i];
                $direccion->carga = true;
                $direccion->presupuesto = $presupuesto->id;
                $direccion->save();
            }

            // DIRECCIONES DESCARGA
            for($i = 0; $i < $request->input('cantidad_direcciones_descarga'); $i++) {

                if (isset($request->input('departamento_descarga')[$i]) && isset($request->input('piso_descarga')[$i]) && isset($request->input('provincia_descarga')[$i]) && isset($request->input('localidad_descarga')[$i])) {

                    if ($request->input('departamento_descarga')[$i] != '' || $request->input('piso_descarga')[$i] != '' || $request->input('provincia_descarga')[$i] != '' || $request->input('localidad_descarga')[$i] != '') {
                        $direccion = new Direccion;
                        $direccion->direccion = ucwords($request->input('direccion_descarga')[$i]);
                        $direccion->departamento = $request->input('departamento_descarga')[$i];
                        $direccion->piso = $request->input('piso_descarga')[$i];
                        $direccion->entrecalles = ucwords($request->input('entrecalles_descarga')[$i]);
                        $direccion->provincia = $request->input('provincia_descarga')[$i];
                        $direccion->localidad = $request->input('localidad_descarga')[$i];
                        $direccion->observaciones = $request->input('observaciones_descarga')[$i];
                        $direccion->carga = false;
                        $direccion->presupuesto = $presupuesto->id;
                        $direccion->save();
                    }

                }
            }

            return $this->edit($id)->with(['success' => 'El presupuesto ha sido editado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {
            if($presupuesto->delete()) {
                return $this->index()->with(['success' => 'El presupuesto ha sido eliminado con éxito']);
            }
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    public function finalizados() {
        $presupuestos = Presupuesto::where('estado', 8)->get();

        return view('presupuestos.editar')->with([
            'elementos' => $presupuestos,
        ]);
    }

    public function cotizar(Request $request) {
        $rules = [

//            'ambientes' => 'required|integer',
            'vehiculo' => 'nullable|exists:vehiculos,id',
            'servicio' => 'nullable|exists:servicios,id',
            'kilometros' => 'required|numeric',
//            'personal' => 'required|integer',
            'adicionales' => 'nullable|array',
            'adicionales.*' => 'nullable|integer|exists:adicionals,id',
            'otros_adicionales' => 'nullable|array',
            'otros_adicionales.*' => 'nullable|array',
            'larga_distancia' => 'required|boolean',
            'valor_aproximado' => 'required|numeric',
            'impuesto' => 'required|numeric',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'numeric' => 'El campo :attribute tiene que ser un número válido',
            'exists' => 'El :attribute ingresado no se encuentra',
            'integer' => 'El campo :attribute tiene que ser un entero',
            'date' => 'El campo :attribute tiene que ser una fecha válida',
            'date_format' => 'El campo :attribute tiene que cumplir el formato dado',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            return abort(500);
        }

        $cotizacion_valor = 0;

        /** COTIZACION
         * */

        $cotizacion = '<ul class="list-unstyled">';

        if(!empty($request->input('servicio'))) {
            $servicio = Servicio::find($request->input('servicio'));
//            $cotizacion_valor += $servicio->valor;

            $cotizacion .= '<li><strong>Servicio:</strong> '.$servicio->nombre.'</li>';
        }

        if(!empty($request->input('vehiculo'))) {
            $valor_vehiculo = 0;
            $vehiculo = Vehiculo::find($request->input('vehiculo'));
//
//            if(!empty($request->input('servicio'))) {
//                switch ($request->input('servicio')) {
//                    case 1: // FLETE
//                        $valor_vehiculo += $vehiculo->valor;
//                        break;
//
//                    case 2: // MUDANZA FAMILIAR
//                        // NO HACEMOS NADA PARA EL VALOR APROXIMADO
//                        break;
//                }
//            }
//
//            if($request->input('larga_distancia')) {
//                $kms = $request->input('kilometros');
//                $valor_vehiculo += $kms * $vehiculo->valor_km;
//            }

//            $cotizacion_valor += $valor_vehiculo;
//            $cotizacion_valor += $request->input('valor_aproximado');

            // CHEQUEAMOS EL VALOR OBTENIDO ACÁ CON EL QUE VIENE DEL INPUT

            $cotizacion_valor += $request->input('valor_aproximado');

            $servicio_larga_distancia = ($request->input('larga_distancia')) ? "(Servicio larga distancia - ".$request->input('kilometros')." kms x $".$vehiculo->valor_km.")" : "";

            $cotizacion .= '<li><strong>Vehículo:</strong> '.$vehiculo->patente.' - $'.$request->input('valor_aproximado').' '.$servicio_larga_distancia.'</li>';
        }

        if(!empty($request->input('adicionales'))) {

            $cotizacion .= '<li><strong>Adicionales</strong></li>';
            $cotizacion .= '<ul>';

            foreach ($request->input('adicionales') as $adicional) {
                $adicionalM = Adicional::find($adicional);

                $cotizacion .= '<li><strong>'.$adicionalM->nombre.':</strong> $'.$adicionalM->valor.'</li>';
                $cotizacion_valor += $adicionalM->valor;
            }

            $cotizacion .= '</ul>';
        }

        if(!empty($request->input('otros_adicionales'))) {

            $cotizacion .= '<li><strong>Otros adicionales</strong></li>';
            $cotizacion .= '<ul>';

            foreach ($request->input('otros_adicionales') as $otro_adicional) {

                $cotizacion .= '<li><strong>'.$otro_adicional['nombre'].':</strong> $'.$otro_adicional['valor'].'</li>';

                $cotizacion_valor += $otro_adicional['valor'];
            }

            $cotizacion .= '</ul>';
        }

        $cotizacion .= '</ul>';

        if($request->input('impuesto') != 0) {

            $impuesto = ($request->input('impuesto') * $cotizacion_valor) / 100;

            $cotizacion .= '<h6><strong>Impuesto:</strong> '.$request->input('impuesto').'% - $'.$impuesto;
            $cotizacion .= '<h5><strong>Presupuesto SIN IVA:</strong> $'.$cotizacion_valor;
            $cotizacion_valor = $cotizacion_valor + $impuesto;
        }

        $cotizacion .= '<h5><strong>Presupuesto:</strong> $'.$cotizacion_valor;

        return Response::json([
            'cotizacion' => $cotizacion,
            'valor_final' => $cotizacion_valor,
        ]);
    }

    public function valorAproximado(Request $request) {

        $rules = [
            'vehiculo' => 'nullable|exists:vehiculos,id',
            'servicio' => 'nullable|exists:servicios,id',
            'kilometros' => 'required|numeric',
            'larga_distancia' => 'required|boolean',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'numeric' => 'El campo :attribute tiene que ser un número válido',
            'exists' => 'El :attribute ingresado no se encuentra',
            'integer' => 'El campo :attribute tiene que ser un entero',
            'date' => 'El campo :attribute tiene que ser una fecha válida',
            'date_format' => 'El campo :attribute tiene que cumplir el formato dado',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            return abort(500);
        }

        $valor_aproximado = null;

        /** VALOR APROXIMADO
         * */

        if(!empty($request->input('servicio')) && !empty($request->input('vehiculo'))) {

            $vehiculo = Vehiculo::find($request->input('vehiculo'));

            switch ($request->input('servicio')) {
                case 1: // MUDANZA FAMILIAR
                    $valor_aproximado = $vehiculo->valor_mudanza;
                    break;

                case 2: // MUDANZA OFICINA
                    $valor_aproximado = $vehiculo->valor_mudanza;
                    break;

                case 3: // Flete
                    $valor_aproximado = $vehiculo->valor;
                    break;

                case 4: // Flete de Larga Distancia
                    $valor_aproximado = $vehiculo->valor;
                    break;

                case 5: // Mudanza Familiar Larga Distancia
                    $valor_aproximado = $vehiculo->valor_mudanza;
                    break;

                case 6: // DESEMBALAJE PREMIUM
                    $valor_aproximado = $vehiculo->valor;
                    break;
            }

            if($request->input('larga_distancia')) {
                $kms = $request->input('kilometros');
                $valor_aproximado += $kms * $vehiculo->valor_km;
            }
        }

        return Response::json([
            'valor_aproximado' => $valor_aproximado,
        ]);
    }
}
