<?php

namespace App\Http\Controllers;

use App\DetalleVisita;
use App\HistorialEstado;
use App\Presupuesto;
use App\Visita;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ListadoController extends Controller
{
    public function mudanzas() {

        $presupuestos = Presupuesto::whereRaw('fecha >= '.Carbon::now()->format('Y-m-d'))
            ->where('estado', 7)
            ->get();

//        $presupuestos = Presupuesto::where('fecha', '>=', Carbon::now()->format('Y-m-d'))
//            ->where('estado', 7)
//            ->get();

        return view('listados.mudanzas')->with([
            'elementos' => $presupuestos,
        ]);
    }

    public function visitas() { // MATERIAL DE EMBALAJE + VISITAS
        $visitas = [];

//        $historiales = HistorialEstado::where('estado', 2)
//            ->where('fecha', '>', Carbon::now()->format('Y-m-d'))
//            ->orderBy('id', 'desc')
//            ->get();

        $historiales = HistorialEstado::where(function($query) {
            $query->select(DB::raw(1))
                ->from('historial_estados')
                ->where('estado', 2) // PENDIENTE DE VISITAR
                ->orWhere('estado', 9) // ENVIAR MATERIAL DE EMBALAJE
                ->orWhere('estado', 11); // RETIRO DE SEÑA
        })
            ->where('fecha', '>=', Carbon::now()->format('Y-m-d'))
            ->orderBy('id', 'desc')
            ->get();

        foreach($historiales as $historial) {
            $presupuesto = Presupuesto::find($historial->presupuesto);

            switch($historial->estado) {
                case 2:
                    if($presupuesto->enviarDetalle == 0) {
                        $visitas[] = $historial;
                    }
                    break;

                case 9:
                    if($presupuesto->enviarMaterial == 0) {
                        $visitas[] = $historial;
                    }
                    break;

                case 11:
                    if($presupuesto->enviarRetiroDeSenia == 0) {
                        $visitas[] = $historial;
                    }
                    break;
            }
        }

//        $presupuestos = Presupuesto::where('estado', 2)
//            ->get();
//
//        foreach($presupuestos as $presupuesto) {
//            $historial = HistorialEstado::where('presupuesto', $presupuesto->id)
//                ->where('estado', 2)
//                ->where('fecha', '>', Carbon::now()->format('Y-m-d'))
//                ->orderBy('id', 'desc')
//                ->first();
//
//            if(!empty($historial)) {
//                $visitas[] = $historial;
//            }
//        }

        return view('listados.visitas')->with([
            'elementos' => $visitas,
        ]);
    }

//    public function entrega() {
//        $visitas = [];
//
//        $historiales = HistorialEstado::where('estado', 9)
//            ->where('fecha', '>', Carbon::now()->format('Y-m-d'))
//            ->get();
//
//        foreach($historiales as $historial) {
//
//            $visitas[] = $historial;
//        }
//
//        return view('listados.entrega')->with([
//            'elementos' => $visitas,
//        ]);
//    }
}
