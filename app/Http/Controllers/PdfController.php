<?php

namespace App\Http\Controllers;

use App\EstadoPresupuesto;
use App\HistorialEstado;
use App\Mail\EnviarDetalleDeVisita;
use App\Mail\EnviarMaterialDeEmbalaje;
use App\Mail\EnviarMudanza;
use App\Mail\EnviarPresupuesto;
use App\Mail\EnviarResumenDeServicio;
use App\Mail\EnviarRetiroDeSenia;
use App\Presupuesto;
use App\Visita;
use Carbon\Carbon;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PdfController extends Controller
{

    /** SE SETEA UNA VARIABLE AUXILIAR DE LA INSTANCIA DEL CONTROLADOR DEL DOMPDF */

    protected $domPdf;

    public function __construct()
    {
        $this->domPdf = new DomPdfController();
    }

    /** END DOMPDF */

    // FUNCIONES DE VISTAS

    public function presupuestoVista($id) {
        $presupuesto = Presupuesto::find($id);
        $ultimaVisita = HistorialEstado::where('presupuesto', $id)
            ->where('estado', 2)
            ->orderBy('id', 'desc')
            ->first();

        return view('pdf.presupuesto')->with([
            'presupuesto' => $presupuesto,
            'ultimaVisita' => $ultimaVisita,
        ]);
    }

    public function resumenDeServicioVista($id) {
        $presupuesto = Presupuesto::find($id);
        return view('pdf.resumenDeServicio')->with([
            'presupuesto' => $presupuesto,
        ]);
    }

    public function resumenDeServicioConObservacionesVista($id) {
        $presupuesto = Presupuesto::find($id);
        return view('pdf.resumenDeServicioConObservaciones')->with([
            'presupuesto' => $presupuesto,
        ]);
    }

    public function visitasDelDiaVista($fecha)
    {
        $visitas = [];

//        $presupuestos = Presupuesto::where('estado', 2)
//            ->get();
//
//        foreach ($presupuestos as $presupuesto) {
//            $historial = HistorialEstado::where('presupuesto', $presupuesto->id)
//                ->where('estado', 2)
//                ->where('fecha', Carbon::now()->format('Y-m-d'))
//                ->first();
//
//            $visitas[] = $historial;
//        }
//

        $historiales = HistorialEstado::where(function($query) {
            $query->where('estado', 2)
                ->orWhere('estado', 9)
                ->orWhere('estado', 11);
        })
            ->where('fecha', $fecha)
            ->orderBy('desde', 'asc')
            ->get();

        $visitas = $historiales;

        return view('pdf.visitasDelDia')->with([
            'visitas' => $visitas,
            'fecha' => $fecha,
        ]);
    }

   public function detalleDeVisitaVista($id) {
       $presupuesto = Presupuesto::find($id);
       return view('pdf.detalleDeVisita')->with([
           'presupuesto' => $presupuesto,
       ]);
   }

   public function planillaDeVisitaVista($id) {
       $presupuesto = Presupuesto::find($id);
       return view('pdf.planillaDeVisita')->with([
           'presupuesto' => $presupuesto,
       ]);
   }

   public function planillaDeVisitaPorEstadoVista($estado, $id) {

       $estado = EstadoPresupuesto::find($estado);

       $presupuesto = Presupuesto::find($id);
       return view('pdf.planillaDeVisitaPorEstado')->with([
           'presupuesto' => $presupuesto,
           'estado' => $estado,
       ]);

   }

    // END FUNCIONES DE VISITAS

    // FUNCIONES PARA DESCARGAR PDF

    public function presupuesto($id) {
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {
            $nombreArchivo = 'Presupuesto #'.$id.'.pdf';
            $pathArchivo = 'presupuestos/'.str_replace(' ', '_', $nombreArchivo);

            $salida = exec("wkhtmltopdf --footer-right \"Pagina [page] de [toPage]\" ".url("/pdf/vista/presupuesto/".$id)." ".$pathArchivo);

            if(!file_exists( public_path() .'/'.$pathArchivo)) {
                return $this->domPdf->presupuesto($presupuesto->id);
            }

            $headers = array(
                'Content-Type: application/pdf',
            );

            return Response::download(public_path().'/'.$pathArchivo, $nombreArchivo, $headers);
        }

        return abort(404);
    }

    public function resumenDeServicio($id) {
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {
            $nombreArchivo = 'Resumen de servicio - Presupuesto #'.$id.'.pdf';
            $pathArchivo = 'resumenesDeServicio/'.str_replace(' ', '_', $nombreArchivo);

            $salida = exec("wkhtmltopdf -T 30mm -B 5mm --footer-right \"Pagina [page] de [toPage]\" --header-html ".url("/pdf/header/".$id)." --header-spacing 9 ".url("/pdf/vista/resumenDeServicio/".$id)." ".$pathArchivo);

            if(!file_exists( public_path() .'/'.$pathArchivo)) {
                return $this->domPdf->resumenDeServicio($presupuesto->id);
            }

            $headers = array(
                'Content-Type: application/pdf',
            );

            return Response::download(public_path().'/'.$pathArchivo, $nombreArchivo, $headers);
        }

        return abort(404);
    }

    public function visitasDelDia(\Illuminate\Http\Request $request) {

        $rules = [
            'fecha' => 'required|date',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'numeric' => 'El campo :attribute tiene que ser un número válido',
            'unique' => 'El :attribute ingresado ya se encuentra registrado',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if(!$validator->fails()) {

            $nombreArchivo = 'visitas-del-dia-' . $request->input('fecha') . '.pdf';
            $pathArchivo = 'visitasDelDia/' . str_replace(' ', '_', $nombreArchivo);

            $salida = exec("wkhtmltopdf -O landscape " . url( "/pdf/vista/visitasDelDia/".$request->input('fecha'))." " . $pathArchivo);

            if(!file_exists( public_path() .'/'.$pathArchivo)) {
                return $this->visitasDelDiaVista($request->input('fecha'));
            }

            $headers = array(
                'Content-Type: application/pdf',
            );

            return Response::download(public_path() . '/' . $pathArchivo, $nombreArchivo, $headers);
        }
    }

    public function detalleDeVisita($id) {
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {
            $nombreArchivo = 'Detalle de Visita - Presupuesto #'.$id.'.pdf';
            $pathArchivo = 'detallesDeVisita/'.str_replace(' ', '_', $nombreArchivo);

            $salida = exec("wkhtmltopdf --footer-right \"Pagina [page] de [toPage]\" ".url("/pdf/vista/detalleDeVisita/".$id)." ".$pathArchivo);

            if(!file_exists( public_path() .'/'.$pathArchivo)) {
                return $this->domPdf->detalleDeVisita($id);
            }

            $headers = array(
                'Content-Type: application/pdf',
            );

            return Response::download(public_path().'/'.$pathArchivo, $nombreArchivo, $headers);
        }

        return abort(404);
    }

    // END FUNCIONES PARA DESCARGAR

    // FUNCIONES ENVIAR

    public function enviarPresupuesto($id) {

        $controladorPresupuestos = new PresupuestoController;
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {

            if($presupuesto->cliente()->email != '') {

                // PONEMOS ENVIAR PRESUPUESTO EN TRUE
                $presupuesto->enviarPresupuesto = true;
                $presupuesto->save();

                // GENERAMOS EL PDF
                $nombreArchivo = 'Presupuesto #' . $id . '.pdf';
                $pathArchivo = 'presupuestos/' . str_replace(' ', '_', $nombreArchivo);
                $salida = exec("wkhtmltopdf --footer-right \"Pagina [page] de [toPage]\" " . url( "/pdf/vista/presupuesto/" . $id) . " " . $pathArchivo);

                if(!file_exists( public_path() .'/'.$pathArchivo)) {
                    return $this->domPdf->enviarPresupuesto($presupuesto->id);
                }

                // ENVIAMOS MAIL
                Mail::to([$presupuesto->cliente()->email])
                    ->bcc(['elisa@eawebagency.com', 'transportesargentinos@hotmail.com'])
                    ->send(new EnviarPresupuesto($presupuesto));

                // RETORNAMOS LA VISTA
                return $controladorPresupuestos->index()->with(['success' => 'El presupuesto se ha enviado con éxito']);
            }

            return $controladorPresupuestos->index()->withErrors(['existe' => 'El cliente tiene que tener un mail asignado']);
        }

        return $controladorPresupuestos->index()->withErrors(['existe' => 'El presupuesto no existe']);
    }

    public function enviarResumenDeServicio($id) {
        $controladorListado = new ListadoController;
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {

            if($presupuesto->cliente()->email != '') {
                // PONEMOS ENVIAR DETALLE EN TRUE
                $presupuesto->enviarDetalle = true;
                $presupuesto->save();

                // GENERAMOS EL PDF
                $nombreArchivo = 'Resumen de servicio - Presupuesto #' . $id . '.pdf';
                $pathArchivo = 'resumenesDeServicio/' . str_replace(' ', '_', $nombreArchivo);
                $salida = exec("wkhtmltopdf -T 30mm -B 5mm --footer-right \"Pagina [page] de [toPage]\" --header-html " . url( "/pdf/header/" . $id) . " --header-spacing 9 " . url( "/pdf/vista/resumenDeServicio/" . $id) . " " . $pathArchivo);

                if(!file_exists( public_path() .'/'.$pathArchivo)) {
                    return $this->domPdf->enviarResumenDeServicio($presupuesto->id);
                }

                // ENVIAMOS MAIL
                Mail::to([$presupuesto->cliente()->email])
                    ->bcc(['elisa@eawebagency.com', 'transportesargentinos@hotmail.com'])
                    ->send(new EnviarResumenDeServicio($presupuesto));

                // RETORNAMOS LA VISTA
                return $controladorListado->visitas()->with(['success' => 'El resumen de servicio se ha enviado con éxito']);
            }

            return $controladorListado->index()->withErrors(['existe' => 'El cliente tiene que tener un mail asignado']);
        }

        return $controladorListado->visitas()->withErrors(['existe' => 'El presupuesto no existe']);
    }

    public function enviarDetalleDeVisita($id) {
        $controladorListado = new ListadoController;
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {

            if($presupuesto->cliente()->email != '') {
                // PONEMOS ENVIAR DETALLE EN TRUE
                $presupuesto->enviarDetalle = true;
                $presupuesto->save();

                // GENERAMOS EL PDF
                $nombreArchivo = 'Detalle de visita - Presupuesto #' . $id . '.pdf';
                $pathArchivo = 'detallesDeVisita/' . str_replace(' ', '_', $nombreArchivo);
                $salida = exec("wkhtmltopdf --footer-right \"Pagina [page] de [toPage]\" " . url( "/pdf/vista/detalleDeVisita/" . $id) . " " . $pathArchivo);

                if(!file_exists( public_path() .'/'.$pathArchivo)) {
                    return $this->domPdf->enviarDetalleDeVisita($presupuesto->id);
                }

                // GENERAMOS EL PDF
                $nombreArchivo = 'Presupuesto #' . $id . '.pdf';
                $pathArchivo = 'presupuestos/' . str_replace(' ', '_', $nombreArchivo);
                $salida = exec("wkhtmltopdf --footer-right \"Pagina [page] de [toPage]\" " . url( "/pdf/vista/presupuesto/" . $id) . " " . $pathArchivo);

                if(!file_exists( public_path() .'/'.$pathArchivo)) {
                    return $this->domPdf->enviarDetalleDeVisita($presupuesto->id);
                }

                // ENVIAMOS MAIL
                Mail::to([$presupuesto->cliente()->email])
                    ->bcc(['elisa@eawebagency.com', 'transportesargentinos@hotmail.com'])
                    ->send(new EnviarDetalleDeVisita($presupuesto));

                // RETORNAMOS LA VISTA
                return $controladorListado->visitas()->with(['success' => 'El detalle de la visita se ha enviado con éxito']);
            }

            return $controladorListado->visitas()->withErrors(['existe' => 'El cliente tiene que tener un mail válido']);
        }

        return $controladorListado->visitas()->withErrors(['existe' => 'El presupuesto no existe']);
    }

    public function enviarMaterialDeEmbalaje($id) {
        $controladorLllamado = new LlamadosController;
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {

            if($presupuesto->cliente()->email != '') {
                // PONEMOS ENVIAR MATERIAL EN TRUE
                $presupuesto->enviarMaterial = true;
                $presupuesto->save();

                // GENERAMOS EL PDF
                $nombreArchivo = 'Detalle de visita - Presupuesto #' . $id . '.pdf';
                $pathArchivo = 'detallesDeVisita/' . str_replace(' ', '_', $nombreArchivo);
                $salida = exec("wkhtmltopdf --footer-right \"Pagina [page] de [toPage]\" " . url( "/pdf/vista/detalleDeVisita/" . $id) . " " . $pathArchivo);

                if(!file_exists( public_path() .'/'.$pathArchivo)) {
                    return $this->domPdf->enviarMaterialDeEmbalaje($presupuesto->id);
                }

                // GENERAMOS EL PDF
                $nombreArchivo = 'Presupuesto #' . $id . '.pdf';
                $pathArchivo = 'presupuestos/' . str_replace(' ', '_', $nombreArchivo);
                $salida = exec("wkhtmltopdf --footer-right \"Pagina [page] de [toPage]\" " . url( "/pdf/vista/presupuesto/" . $id) . " " . $pathArchivo);

                if(!file_exists( public_path() .'/'.$pathArchivo)) {
                    return $this->domPdf->enviarMaterialDeEmbalaje($presupuesto->id);
                }

                // ENVIAMOS MAIL
                Mail::to([$presupuesto->cliente()->email])
                    ->bcc(['elisa@eawebagency.com', 'transportesargentinos@hotmail.com'])
                    ->send(new EnviarMaterialDeEmbalaje($presupuesto));

                // RETORNAMOS LA VISTA
                return $controladorLllamado->visitas()->with(['success' => 'El material de embalaje se ha enviado con éxito']);
            }

            return $controladorLllamado->mudanzas()->withErrors(['existe' => 'El cliente tiene que tener un mail asignado']);
        }

        return $controladorLllamado->visitas()->withErrors(['existe' => 'El presupuesto no existe']);
    }

    public function enviarMudanza($id) {
        $controladorLllamado = new LlamadosController;
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {

            if($presupuesto->cliente()->email != '') {
                // PONEMOS ENVIAR MUDANZA EN TRUE
                $presupuesto->enviarMudanza = true;
                $presupuesto->save();

                // GENERAMOS EL PDF
                $nombreArchivo = 'Detalle de visita - Presupuesto #' . $id . '.pdf';
                $pathArchivo = 'detallesDeVisita/' . str_replace(' ', '_', $nombreArchivo);
                $salida = exec("wkhtmltopdf --footer-right \"Pagina [page] de [toPage]\" " . url( "/pdf/vista/detalleDeVisita/" . $id) . " " . $pathArchivo);

                if(!file_exists( public_path() .'/'.$pathArchivo)) {
                    return $this->domPdf->enviarMudanza($presupuesto->id);
                }

                // GENERAMOS EL PDF
                $nombreArchivo = 'Presupuesto #' . $id . '.pdf';
                $pathArchivo = 'presupuestos/' . str_replace(' ', '_', $nombreArchivo);
                $salida = exec("wkhtmltopdf --footer-right \"Pagina [page] de [toPage]\" " . url( "/pdf/vista/presupuesto/" . $id) . " " . $pathArchivo);

                if(!file_exists( public_path() .'/'.$pathArchivo)) {
                    return $this->domPdf->enviarMudanza($presupuesto->id);
                }

                // ENVIAMOS MAIL
                Mail::to([$presupuesto->cliente()->email])
                    ->bcc(['elisa@eawebagency.com', 'transportesargentinos@hotmail.com'])
                    ->send(new EnviarMudanza($presupuesto));

                // RETORNAMOS LA VISTA
                return $controladorLllamado->mudanzas()->with(['success' => 'La mudanza se ha enviado con éxito']);
            }

            return $controladorLllamado->mudanzas()->withErrors(['existe' => 'El cliente tiene que tener un mail asignado']);
        }

        return $controladorLllamado->mudanzas()->withErrors(['existe' => 'El presupuesto no existe']);
    }

    public function enviarRetiroDeSenia($id) {
        $controladorLllamado = new LlamadosController;
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {

            if($presupuesto->cliente()->email != '') {

                // PONEMOS ENVIAR RETIRO DE SEÑA EN TRUE
                $presupuesto->enviarRetiroDeSenia = true;
                $presupuesto->save();

                // GENERAMOS EL PDF
                $nombreArchivo = 'Detalle de visita - Presupuesto #' . $id . '.pdf';
                $pathArchivo = 'detallesDeVisita/' . str_replace(' ', '_', $nombreArchivo);
                $salida = exec("wkhtmltopdf --footer-right \"Pagina [page] de [toPage]\" " . url( "/pdf/vista/detalleDeVisita/" . $id) . " " . $pathArchivo);

                if(!file_exists( public_path() .'/'.$pathArchivo)) {
                    return $this->domPdf->enviarRetiroDeSenia($presupuesto->id);
                }

                // GENERAMOS EL PDF
                $nombreArchivo = 'Presupuesto #' . $id . '.pdf';
                $pathArchivo = 'presupuestos/' . str_replace(' ', '_', $nombreArchivo);
                $salida = exec("wkhtmltopdf --footer-right \"Pagina [page] de [toPage]\" " . url( "/pdf/vista/presupuesto/" . $id) . " " . $pathArchivo);

                if(!file_exists( public_path() .'/'.$pathArchivo)) {
                    return $this->domPdf->enviarRetiroDeSenia($presupuesto->id);
                }

                // ENVIAMOS MAIL
                Mail::to([$presupuesto->cliente()->email])
                    ->bcc(['elisa@eawebagency.com', 'transportesargentinos@hotmail.com'])
                    ->send(new EnviarRetiroDeSenia($presupuesto));

                // RETORNAMOS LA VISTA
                return $controladorLllamado->visitas()->with(['success' => 'El retiro de seña se ha enviado con éxito']);
            }

            return $controladorLllamado->visitas()->withErrors(['existe' => 'El cliente tiene que tener un mail asignado']);
        }

        return $controladorLllamado->visitas()->withErrors(['existe' => 'El presupuesto no existe']);
    }

    // END FUNCIONES ENVIAR

    // FUNCIONES PARA VER

    public function presupuestoVer($id) {
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {
            $nombreArchivo = 'Presupuesto_'.$id.'.pdf';
            $pathArchivo = 'presupuestos/'.str_replace(' ', '_', $nombreArchivo);

            $salida = exec("wkhtmltopdf --footer-right \"Pagina [page] de [toPage]\" ".url("/pdf/vista/presupuesto/".$id)." ".$pathArchivo);

            if(!file_exists( public_path() .'/'.$pathArchivo)) {
                return $this->domPdf->presupuestoVer($presupuesto->id);
            }

            return Response::redirectTo(url('/'.$pathArchivo));
        }

        return abort(404);
    }

    public function resumenDeServicioVer($id) {
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {
            $nombreArchivo = 'Resumen de servicio - Presupuesto_'.$id.'.pdf';
            $pathArchivo = 'resumenesDeServicio/'.str_replace(' ', '_', $nombreArchivo);

            $salida = exec("wkhtmltopdf -T 30mm -B 5mm --footer-right \"Pagina [page] de [toPage]\" --header-html ".url("/pdf/header/".$id)." --header-spacing 9 ".url("/pdf/vista/resumenDeServicio/".$id)." ".$pathArchivo);

            if(!file_exists( public_path() .'/'.$pathArchivo)) {
                return $this->domPdf->resumenDeServicioVer($presupuesto->id);
            }

            return Response::redirectTo(url('/'.$pathArchivo));
        }

        return abort(404);
    }

    public function resumenDeServicioConObservacionesVer($id) {
        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {
            $nombreArchivo = 'Resumen de servicio - Presupuesto_'.$id.'.pdf';
            $pathArchivo = 'resumenesDeServicioConObservaciones/'.str_replace(' ', '_', $nombreArchivo);

            $salida = exec("wkhtmltopdf -T 30mm -B 5mm --footer-right \"Pagina [page] de [toPage]\" --header-html ".url("/pdf/header/".$id)." --header-spacing 9 ".url("/pdf/vista/resumenDeServicioConObservaciones/".$id)." ".$pathArchivo);

            if(!file_exists( public_path() .'/'.$pathArchivo)) {
                return $this->domPdf->resumenDeServicioConObservacionesVer($id);
            }

            return Response::redirectTo(url('/'.$pathArchivo));
        }

        return abort(404);
    }

    public function planillaDeVisitaVer($id) {

        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {
            $nombreArchivo = 'Planilla de visita - Presupuesto_'.$id.'.pdf';
            $pathArchivo = 'planillasDeVisita/'.str_replace(' ', '_', $nombreArchivo);

            $salida = exec("wkhtmltopdf -T 30mm -B 5mm --footer-right \"Pagina [page] de [toPage]\" --header-html ".url("/pdf/headerPlanilla/".$id)." --header-spacing 9 ".url("/pdf/vista/planillaDeVisita/".$id)." ".$pathArchivo);

            if(!file_exists( public_path() .'/'.$pathArchivo)) {
                return $this->planillaDeVisitaVista($id);
            }
            
            return Response::redirectTo(url('/'.$pathArchivo));
        }

        return abort(404);
    }

    public function planillaDeVisitaPorEstadoVer($estado, $id) {

        $presupuesto = Presupuesto::find($id);

        if(!empty($presupuesto)) {
            $nombreArchivo = 'Planilla de visita - Presupuesto_'.$id.'.pdf';
            $pathArchivo = 'planillasDeVisita/'.str_replace(' ', '_', $nombreArchivo);

            $salida = exec("wkhtmltopdf -T 30mm -B 5mm --footer-right \"Pagina [page] de [toPage]\" --header-html ".url("/pdf/headerPlanilla/".$id)." --header-spacing 9 ".url("/pdf/vista/planillaDeVisitaPorEstado/".$estado."/".$id)." ".$pathArchivo);

            if(!file_exists( public_path() .'/'.$pathArchivo)) {
                return $this->planillaDeVisitaPorEstadoVista($estado, $id);
            }

            return Response::redirectTo(url('/'.$pathArchivo));
        }

        return abort(404);
    }

    // END FUNCIONES VER

    // HEADERS

    public function header($id) {
        return view('pdf.header')->with([
            'id' => $id
        ]);
    }

    public function headerPlanilla($id) {

        $presupuesto = Presupuesto::find($id);

        return view('pdf.headerPlanilla')->with([
            'presupuesto' => $presupuesto
        ]);
    }

    // END HEADERS
}
