<?php

namespace App\Http\Controllers;

use App\DetalleVisita;
use App\Visita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DetalleVisitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DetalleVisita  $detalleVisita
     * @return \Illuminate\Http\Response
     */
    public function show(DetalleVisita $detalleVisita)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DetalleVisita  $detalleVisita
     * @return \Illuminate\Http\Response
     */
    public function edit($visita)
    {
        $visita = Visita::find($visita);

        if(!empty($visita)) {

            $detalle = DetalleVisita::find($visita);

            return view()->with([
                'elemento' => $detalle
            ]);
        }

        return redirect()->back()->withErrors(['existe' => 'La visita no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetalleVisita  $detalleVisita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $visita)
    {
        $visita = Visita::find($visita);

        if(!empty($visita)) {

            $detalle = DetalleVisita::find($visita);

            $rules = [
                'embalaje' => 'required|boolean',
                'percheros_moviles' => 'required|integer',
                'cajas' => 'required|integer',
                'horas' => 'required|integer',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'integer' => 'El campo :attribute tiene que ser un entero',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                $request->flash();

                return $this->edit($visita)->withErrors($validator);
            }

            $detalle->embalaje = $request->input('embalaje');
            $detalle->percheros_moviles = $request->input('percheros_moviles');
            $detalle->cajas = $request->input('cajas');
            $detalle->horas = $request->input('horas');
            $detalle->save();
        }

        return redirect()->back()->withErrors(['existe' => 'La visita no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetalleVisita  $detalleVisita
     * @return \Illuminate\Http\Response
     */
    public function destroy($visita)
    {
        //
    }
}
