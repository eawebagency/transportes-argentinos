<?php

namespace App\Http\Controllers;

use App\DetalleVisita;
use App\Evento;
use App\Presupuesto;
use App\Rules\EsVisitador;
use App\User;
use App\Visita;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VisitaController extends Controller
{

    public function obtenerVisitadoresSelect() {
        $devolver = array();

        $visitadores = User::where('rol', 3)->get();

        foreach($visitadores as $visitador) {
            $devolver[$visitador->id] = $visitador->nombre." ".$visitador->apellido;
        }

        return $devolver;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($presupuesto)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {
            $controlador = new PresupuestoController();
            return $controlador->edit($presupuesto);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($presupuesto)
    {
        $visitadores = $this->obtenerVisitadoresSelect();
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {
            return view('visitas.crear')->with([
                'visitadores' => $visitadores,
                'presupuesto' => $presupuesto,
            ]);
        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $presupuesto)
    {
        $pres = Presupuesto::find($presupuesto);

        if(!empty($pres)) {

            $rules = [
                'fecha' => 'required|date',
                'hora_inicio' => 'required|date_format:"H:i"',
                'hora_fin' => 'required|date_format:"H:i"',
                'observaciones' => 'nullable|string',
                'observaciones_carga' => 'nullable|string',
                'observaciones_descarga' => 'nullable|string',
                'visitador' => ['required', 'exists:users,id', new EsVisitador()],
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'integer' => 'El campo :attribute tiene que ser un entero',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {

                $request->flash();

                return $this->create($presupuesto)->withErrors($validator);
            }

            $visita = new Visita;
            $visita->fecha = $request->input('fecha');
            $visita->hora_inicio = $request->input('hora_inicio');
            $visita->hora_fin = $request->input('hora_fin');
            $visita->observaciones = $request->input('observaciones');
            $visita->observaciones_carga = $request->input('observaciones_carga');
            $visita->observaciones_descarga = $request->input('observaciones_descarga');
            $visita->visitador = $request->input('visitador');
            $visita->presupuesto = $presupuesto;
            $visita->save();

            // UPDATE ESTADO DEL PRESUPUESTO SI ESTÁ EN PENDIENTE DE LLAMAR

            if($pres->estado == 1) {
                $pres->estado = 2;
                $pres->save();
            }

            // SE CREA DETALLE DE VISITA

            $detalle = DetalleVisita::create(['embalaje' => 0, 'percheros_moviles' => 0, 'cajas' => 0, 'horas' => 0, 'visita' => $visita->id]);

            // SE CREA EVENTO

//            $evento = new Evento;
//            $evento->titulo = "Visita";
//            $evento->descripcion = '
//            <ul>
//                <li><strong>Nro. de presupuesto:</strong> '.$visita->presupuesto.'</li>
//                <li><strong>Franja horaria:</strong> '.$visita->hora_inicio.' - '.$visita->hora_fin.'</li>
//                <li>Direcciones de carga</li>';
//            foreach($pres->cargas() as $carga) {
//                $evento->descripcion .= '
//                        <ul>
//                        <li><strong>Direccion:</strong> '.$carga->direccion.'</li>
//                        <li><strong>Localidad:</strong> '.$carga->localidad.'</li>
//                        </ul>';
//            }
//            $evento->descripcion .= '
//                <li><strong>Nombre:</strong> '.$visita->presupuesto()->cliente()->nombre.' '.$visita->presupuesto()->cliente()->apellido.'</li>
//                <li><strong>Observaciones carga:</strong> <p>'.$visita->observaciones_carga.'</p></li>
//                <li><strong>Observaciones descarga:</strong> <p>'.$visita->observaciones_descarga.'</p></li>
//            </ul>
//            ';
//            $evento->fecha_inicio = Carbon::parse($visita->fecha)->format('Y-m-d');
//            $evento->fecha_fin = Carbon::parse($visita->fecha)->format('Y-m-d');
//            $evento->hora_inicio = Carbon::parse($visita->hora_inicio)->format('H:i:s');
//            $evento->hora_fin = Carbon::parse($visita->hora_fin)->format('H:i:s');
//            $evento->color = "#93ffa7";
//            $evento->save();

            return $this->index($presupuesto)->with(['success' => 'Visita creada con éxito']);

        }

        return redirect()->back()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visita  $visita
     * @return \Illuminate\Http\Response
     */
    public function show(Visita $visita)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visita  $visita
     * @return \Illuminate\Http\Response
     */
    public function edit($presupuesto, $id)
    {
        $visita = Visita::find($id);

        if(!empty($visita)) {

            $presupuestoController = new PresupuestoController();

            return $presupuestoController->edit($visita->presupuesto);

        }

        return $this->index($presupuesto)->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visita  $visita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $presupuesto, $id)
    {
        $visita = Visita::find($id);

        if(!empty($visita)) {

            $rules = [
                'fecha' => 'required|date',
                'hora_inicio' => 'required|date_format:"H:i"',
                'hora_fin' => 'required|date_format:"H:i"',
                'observaciones' => 'nullable|string',
                'observaciones_carga' => 'nullable|string',
                'observaciones_descarga' => 'nullable|string',
                'visitador' => ['required', 'exists:users,id', new EsVisitador()],
                'embalaje' => 'required|boolean',
                'percheros_moviles' => 'required|integer',
                'cajas' => 'required|integer',
                'horas' => 'required|integer',
                'fecha_embalaje' => 'required_if:embalaje,==,0|nullable|date',
                'hora_desde' => 'required_if:embalaje,==,0|nullable|date_format:"H:i"',
                'hora_hasta' => 'required_if:embalaje,==,0|nullable|date_format:"H:i"',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'integer' => 'El campo :attribute tiene que ser un entero',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
                'required_if' => 'El campo :attribute es obligatorio si no se envió el embalaje',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {

                $request->flash();

                return $this->edit($presupuesto, $id)->withErrors($validator);
            }

            $visita->fecha = $request->input('fecha');
            $visita->hora_inicio = $request->input('hora_inicio');
            $visita->hora_fin = $request->input('hora_fin');
            $visita->observaciones = $request->input('observaciones');
            $visita->observaciones_carga = $request->input('observaciones_carga');
            $visita->observaciones_descarga = $request->input('observaciones_descarga');
            $visita->visitador = $request->input('visitador');
            $visita->save();

            // EDITAMOS EL DETALLE

            $detalle = DetalleVisita::find($id);
            $detalle->embalaje = $request->input('embalaje');
            $detalle->percheros_moviles = $request->input('percheros_moviles');
            $detalle->cajas = $request->input('cajas');
            $detalle->horas = $request->input('horas');
            $detalle->fecha = $request->input('fecha_embalaje');
            $detalle->hora_desde = $request->input('hora_desde');
            $detalle->hora_hasta = $request->input('hora_hasta');
            $detalle->save();

            return $this->index($presupuesto)->with(['success' => 'Visita editada con éxito']);
        }

        return $this->index($presupuesto)->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visita  $visita
     * @return \Illuminate\Http\Response
     */
    public function destroy($presupuesto, $id)
    {
        $visita = Visita::find($id);

        if(!empty($visita)) {

            if($visita->delete()) {
                return $this->index($presupuesto)->with(['success' => 'La visita fue eliminada con éxito']);
            }
        }

        return $this->index($presupuesto)->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
