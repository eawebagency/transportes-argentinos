<?php

namespace App\Http\Controllers;

use App\RolUsuario;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function obtenerRolesSelect() {
        $devolver = [];

        $roles = RolUsuario::all();

        foreach($roles as $rol) {
            $devolver[$rol->id] = $rol->nombre;
        }

        return $devolver;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();

        return view('usuarios.editar')->with([
            'elementos' => $usuarios,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->obtenerRolesSelect();

        return view('usuarios.crear')->with([
            'roles' => $roles,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'email' => 'required|email',
            'telefono' => 'required|string',
            'password' => 'required|confirmed',
            'rol' => 'required|integer|exists:rol_usuarios,id',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'integer' => 'El campo :attribute tiene que ser un número válido',
            'unique' => 'El :attribute ingresado ya se encuentra registrado',
            'exists' => 'El valor no existe',
            'email' => 'Tiene que ingresar un mail válido',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->create()->withErrors($validator);
        }

        $user = new User;
        $user->password = Hash::make($request->input('password'));
        $user->nombre = $request->input('nombre');
        $user->apellido = $request->input('apellido');
        $user->email = $request->input('email');
        $user->telefono = $request->input('telefono');
        $user->rol = $request->input('rol');
        $user->save();

        return $this->create()->with(['success' => 'Usuario creado con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = $this->obtenerRolesSelect();
        $usuario = User::find($id);

        if(!empty($usuario)) {
            return view('usuarios.editarIndividual')->with([
                'elemento' => $usuario,
                'roles' => $roles,
            ]);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = User::find($id);

        if(!empty($usuario)) {

            $rules = [
                'nombre' => 'required|string',
                'apellido' => 'required|string',
                'email' => 'required|email',
                'telefono' => 'required|string',
                'password' => 'nullable|confirmed',
                'rol' => 'required|integer|exists:rol_usuarios,id',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'integer' => 'El campo :attribute tiene que ser un número válido',
                'unique' => 'El :attribute ingresado ya se encuentra registrado',
                'exists' => 'El valor no existe',
                'email' => 'Tiene que ingresar un mail válido',
                'confirmed' => 'Las contraseñas no coinciden',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                return $this->edit($id)->withErrors($validator);
            }

            if(!empty($request->input('password'))) {
                $usuario->password = Hash::make($request->input('password'));
            }

            $usuario->nombre = $request->input('nombre');
            $usuario->apellido = $request->input('apellido');
            $usuario->email = $request->input('email');
            $usuario->telefono = $request->input('telefono');
            $usuario->rol = $request->input('rol');
            $usuario->save();

            return $this->edit($id)->with(['success' => 'Usuario editado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::find($id);

        if(!empty($usuario)) {

            if($usuario->delete()) {
                return $this->index()->with(['success' => 'El usuario fue eliminado con éxito']);
            }
        }

        return $this->index()->withErrors(['existe' => 'El elemento solicitado no existe']);
    }
}
