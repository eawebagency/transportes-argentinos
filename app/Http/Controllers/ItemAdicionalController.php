<?php

namespace App\Http\Controllers;

use App\Helpers\ImagenPdf;
use App\ItemAdicional;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ItemAdicionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = ItemAdicional::all();

        return view('items_adicionales.editar')->with([
            'elementos' => $items,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('items_adicionales.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|string|unique:item_adicionals,nombre',
            'descripcion' => 'required|string',
            'imagen' => 'required|image|mimes:png',
        ];

        $messages = [
            'required' => 'El campo :attribute es requerido',
            'unique' => 'El campo :attribute ingresado ya se encuentra registrado',
            'mimes' => 'El campo :attribute sólo acepta formato PNG',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            $request->flash();

            return $this->create()->withErrors($validator);
        }

        $item = new ItemAdicional();
        $item->nombre = $request->input('nombre');
        $item->descripcion = $request->input('descripcion');
        if(!empty($request->file('imagen'))) {
            $file = $request->file('imagen');
            $file->move('img/pdf/', ImagenPdf::generarNombre($item->nombre).'.png');
        }
        $item->save();

        return $this->create()->with(['success' => 'Item creado con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemAdicional  $itemAdicional
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $items = ItemAdicional::all();

        return Response::json($items);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemAdicional  $itemAdicional
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = ItemAdicional::find($id);

        if(!empty($item)) {
            return view('items_adicionales.editarIndividual')->with([
                'elemento' => $item,
            ]);
        }

        return $this->index()->withErrors(['existe' => 'El item solicitado no existe']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemAdicional  $itemAdicional
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = ItemAdicional::find($id);

        if(!empty($item)) {
            $rules = [
                'nombre' => 'required|string',
                'descripcion' => 'required|string',
                'imagen' => 'nullable|image|mimes:png',
            ];

            $messages = [
                'required' => 'El campo :attribute es requerido',
                'unique' => 'El campo :attribute ingresado ya se encuentra registrado',
                'mimes' => 'El campo :attribute sólo acepta formato PNG',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {
                return $this->edit($id)->withErrors($validator);
            }

            $item->nombre = $request->input('nombre');
            $item->descripcion = $request->input('descripcion');
            if(!empty($request->file('imagen'))) {
                $file = $request->file('imagen');
                $file->move('img/pdf/', ImagenPdf::generarNombre($item->nombre).'.png');
            }
            $item->save();

            return $this->edit($id)->with(['success' => 'Item creado con éxito']);
        }

        return $this->index()->withErrors(['existe' => 'El item solicitado no existe']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemAdicional  $itemAdicional
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = ItemAdicional::find($id);

        if(!empty($item)) {

            if($item->delete()) {
                return $this->index()->with(['success' => 'El item ha sido eliminado con éxito']);
            }
        }

        return $this->index()->withErrors(['existe' => 'El item solicitado no existe']);
    }
}
