<?php

namespace App\Http\Controllers;

use App\DetalleVisita;
use App\Evento;
use App\HistorialEstado;
use App\Presupuesto;
use App\Visita;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;

class EventoController extends Controller
{

    /** FUNCIONES EVENTOS */

    // FUNCION PARA LIMPIAR HISTORIALES REPETIDOS CON EL MISMO PRESUPUESTO
    public function limpiarHistorialesRepetidosPorEstado($historiales) {
        $presupuestos = [];
        $devolver = [];

        foreach($historiales as $historial) {
            if(!in_array($historial->presupuesto, $presupuestos)) {
                $presupuestos[] = $historial->presupuesto;
                $devolver[] = $historial;
            }
        }

        return $devolver;
    }

    // FUNCION QUE EVALUA SI UN HISTORIAL ESTA ANTES QUE UN RECHAZO
    public function limpiarHistorialesAntesDeRechazos($historiales) {
        $devolver = [];

        foreach($historiales as $historial) {
            $rechazo = HistorialEstado::where('presupuesto', $historial->presupuesto)
                ->where('estado', 3)
                ->orderBy('id', 'desc')
                ->first();

            if(!empty($rechazo)) {
                if($rechazo->id > $historial->id) {
                    continue;
                }
            }

            $devolver[] = $historial;
        }

        return $devolver;
    }

    /** END FUNCIONES EVENTOS */

    public function obtenerEventosVisitas() {

        $devolver = [];
        $historiales = HistorialEstado::where('estado', 2)
            ->orderBy('id', 'desc')
            ->get();
        $historiales = $this->limpiarHistorialesRepetidosPorEstado($historiales);
        $historiales = $this->limpiarHistorialesAntesDeRechazos($historiales);

        foreach($historiales as $historial) {

            $presupuesto = $historial->presupuesto();

            $evento = new Evento;
            $evento->titulo = $presupuesto->id." - ".$presupuesto->cargas()[0]->localidad;
            $evento->descripcion = '
            <ul>
                <li><strong>Nro. de presupuesto:</strong> '.$historial->presupuesto.'</li>
                <li><strong>Nombre del cliente:</strong> '.$presupuesto->cliente()->nombre.' '.$presupuesto->cliente()->apellido.'</li>
                <li><strong>Fecha y Franja horaria:</strong> '.Carbon::parse($historial->fecha)->format('d/m/Y').' ('.$historial->desde.' - '.$historial->hasta.')</li>
                <li>Direcciones de carga</li>';
                    foreach($presupuesto->cargas() as $direccion_carga) {
                        $evento->descripcion .= '
                                <ul>
                                    <li><strong>Dirección:</strong> '.$direccion_carga->direccion.' (Piso: '.$direccion_carga->piso.' - Departamento: '.$direccion_carga->departamento.'), '.$direccion_carga->localidad.'</li>
                                    <li><strong>Entrecalles:</strong> '.$direccion_carga->entrecalles.'</li>
                                    <li><strong>Observaciones:</strong> '.$direccion_carga->observaciones.'</li>
                                </ul>';
                    }
            $evento->descripcion .= '
                <li><strong>Observaciones:</strong> <p>'.$presupuesto->observaciones.'</p></li>
            </ul>
            
            <div class="mt-3">
                <a href="'.url("presupuestos/editar/".$historial->presupuesto).'" target="_blank" class="btn btn-info">
                    Ver presupuesto
                </a>
                <a href="'.url("pdf/presupuesto/".$historial->presupuesto).'" target="_blank" class="btn btn-info">
                    Descargar PDF
                </a>
                <a href="'.url("pdf/ver/presupuesto/".$historial->presupuesto).'" target="_blank" class="btn btn-info">
                    Ver PDF
                </a>
            </div>
            ';
            $evento->fecha_inicio = Carbon::parse($historial->fecha)->format('Y-m-d');
            $evento->fecha_fin = Carbon::parse($historial->fecha)->format('Y-m-d');
            $evento->hora_inicio = Carbon::parse($historial->desde)->format('H:i:s');
            $evento->hora_fin = Carbon::parse($historial->hasta)->format('H:i:s');
            $evento->color = "#93ffa7";
            $evento->colorTexto = "black";
            $evento->orden = 2;

            $devolver[] = $evento;
        }

        return $devolver;
    }

    public function obtenerEventosPresupuestosConfirmados() {

        $devolver = [];
        $historiales = HistorialEstado::where('estado', 7)
//            ->groupBy('presupuesto')
            ->orderBy('id', 'desc')
            ->get();
        $historiales = $this->limpiarHistorialesRepetidosPorEstado($historiales);
        $historiales = $this->limpiarHistorialesAntesDeRechazos($historiales);

        foreach($historiales as $historial) {

            $presupuesto = $historial->presupuesto();

            $evento = new Evento;
            $evento->titulo = $presupuesto->id." - ".$presupuesto->vehiculo()->patente;
            $evento->descripcion = '
            <ul>
                <li><strong>Nro. de presupuesto:</strong> ' . $presupuesto->id . '</li>
                <li><strong>Nombre:</strong> ' . $presupuesto->cliente()->nombre . ' ' . $presupuesto->cliente()->apellido . '</li>
                <li><strong>Fecha y Hora del servicio:</strong> ' . Carbon::parse($presupuesto->fecha.' '.$presupuesto->hora)->format('d/m/Y H:i') . ' - ' . Carbon::parse($presupuesto->fecha_fin.' '.$presupuesto->hora_fin)->format('d/m/Y H:i') . '</li>
                <li><strong>Direcciones de carga:</strong>';
                    foreach($presupuesto->cargas() as $direccion_carga) {
                        $evento->descripcion .= '<ul>
                                                    <li><strong>Dirección:</strong> '.$direccion_carga->direccion.' (Piso: '.$direccion_carga->piso.' - Departamento: '.$direccion_carga->departamento.'), '.$direccion_carga->localidad.'</li>
                                                    <li><strong>Entrecalles:</strong> '.$direccion_carga->entrecalles.'</li>
                                                    <li><strong>Observaciones:</strong> '.$direccion_carga->observaciones.'</li>
                                                 </ul>';
                    }
            $evento->descripcion .= '</ul>
                </li>
                <li><strong>Direcciones de descarga:</strong>';
                    foreach($presupuesto->descargas() as $direccion_descarga) {
                        $evento->descripcion .= '<ul>
                                                    <li><strong>Dirección:</strong> '.$direccion_descarga->direccion.' (Piso: '.$direccion_descarga->piso.' - Departamento: '.$direccion_descarga->departamento.'), '.$direccion_descarga->localidad.'</li>
                                                    <li><strong>Entrecalles:</strong> '.$direccion_descarga->entrecalles.'</li>
                                                    <li><strong>Observaciones:</strong> '.$direccion_descarga->observaciones.'</li>
                                                 </ul>';
                    }
            $evento->descripcion .= '</ul>
                </li>
                <li><strong>Observaciones:</strong> ' . $presupuesto->observaciones . '</li>
            </ul>
            
            <div class="mt-3">
                <a href="'.url("presupuestos/editar/".$presupuesto->id).'" target="_blank" class="btn btn-info">
                    Ver presupuesto
                </a>
                <a href="'.url("pdf/presupuesto/".$presupuesto->id).'" target="_blank" class="btn btn-info">
                    Descargar PDF
                </a>
                <a href="'.url("pdf/ver/presupuesto/".$presupuesto->id).'" target="_blank" class="btn btn-info">
                    Ver PDF
                </a>
            </div>
            ';

            $evento->fecha_inicio = Carbon::parse($presupuesto->fecha)->format('Y-m-d');
            $evento->fecha_fin = Carbon::parse($presupuesto->fecha_fin)->format('Y-m-d');
            $evento->hora_inicio = Carbon::parse($presupuesto->hora)->format('H:i:s');
            $evento->hora_fin = Carbon::parse($presupuesto->hora_fin)->format('H:i:s');
            $evento->color = "red";
            $evento->colorTexto = "black";
            $evento->orden = 1;

            $devolver[] = $evento;
        }

        return $devolver;
    }

    public function obtenerEventosLlamadasPendientes()
    {

        $devolver = [];

        $presupuestosDB = Presupuesto::where('estado', 1)
            ->orderBy('created_at', 'asc')
            ->get();

        foreach($presupuestosDB as $presupuesto) {
            $historial = HistorialEstado::where('presupuesto', $presupuesto->id)
                ->where('estado', $presupuesto->estado)
                ->orderBy('id', 'desc')
                ->first();

            if(!empty($historial)) {

                $evento = new Evento;
                $evento->titulo = $presupuesto->id;
                $evento->descripcion = '
            <ul>
                <li><strong>Nro. de presupuesto:</strong> ' . $presupuesto->id . '</li>
                <li><strong>Nombre:</strong> ' . $presupuesto->cliente()->nombre . ' ' . $presupuesto->cliente()->apellido . '</li>
                <li><strong>Fecha y Hora del servicio:</strong> ' . ((!empty($presupuesto->fecha)) ? Carbon::parse($presupuesto->fecha.' '.$presupuesto->hora)->format('d/m/Y H:i') : "" ) . ' - ' . ((!empty($presupuesto->fecha_fin)) ? Carbon::parse($presupuesto->fecha_fin.' '.$presupuesto->hora_fin)->format('d/m/Y H:i') : "" ) . '</li>
                <li><strong>Fecha y Franja horaria para llamar:</strong> ' . ((!empty($historial->fecha)) ? Carbon::parse($historial->fecha.' '.$historial->desde)->format('d/m/Y'). ' ('.Carbon::parse($historial->desde)->format('H:i').' - '.Carbon::parse($historial->hasta)->format('H:i').')' : "" ) . '</li>
                <li><strong>Observaciones:</strong> ' . $presupuesto->observaciones . '</li>
            </ul>
            
            <div class="mt-3">
                <a href="'.url("presupuestos/editar/".$presupuesto->id).'" target="_blank" class="btn btn-info">
                    Ver presupuesto
                </a>
            </div>
            ';

                $evento->fecha_inicio = Carbon::parse($historial->fecha)->format('Y-m-d');
                $evento->fecha_fin = Carbon::parse($historial->fecha)->format('Y-m-d');
                $evento->hora_inicio = Carbon::parse($historial->desde)->format('H:i:s');
                $evento->hora_fin = Carbon::parse($historial->hasta)->format('H:i:s');
                $evento->color = "#99ddff";
                $evento->colorTexto = "black";
                $evento->orden = 5;

                $devolver[] = $evento;

            }
        }

//        $historiales = HistorialEstado::where('estado', 1)
//            ->orderBy('id', 'desc')
//            ->get();
////        $historiales = $this->limpiarHistorialesRepetidosPorEstado($historiales);
//        $historiales = $this->limpiarHistorialesAntesDeRechazos($historiales);
//
//        foreach($historiales as $historial) {
//
//            $presupuesto = $historial->presupuesto();
//
//            $evento = new Evento;
//            $evento->titulo = $presupuesto->id;
//            $evento->descripcion = '
//            <ul>
//                <li><strong>Nro. de presupuesto:</strong> ' . $presupuesto->id . '</li>
//                <li><strong>Nombre:</strong> ' . $presupuesto->cliente()->nombre . ' ' . $presupuesto->cliente()->apellido . '</li>
//                <li><strong>Fecha y Hora del servicio:</strong> ' . ((!empty($presupuesto->fecha)) ? Carbon::parse($presupuesto->fecha.' '.$presupuesto->hora)->format('d/m/Y H:i') : "" ) . ' - ' . ((!empty($presupuesto->fecha_fin)) ? Carbon::parse($presupuesto->fecha_fin.' '.$presupuesto->hora_fin)->format('d/m/Y H:i') : "" ) . '</li>
//                <li><strong>Fecha y Franja horaria para llamar:</strong> ' . ((!empty($historial->fecha)) ? Carbon::parse($historial->fecha.' '.$historial->desde)->format('d/m/Y'). ' ('.Carbon::parse($historial->desde)->format('H:i').' - '.Carbon::parse($historial->hasta)->format('H:i').')' : "" ) . '</li>
//                <li><strong>Observaciones:</strong> ' . $presupuesto->observaciones . '</li>
//            </ul>
//
//            <div class="mt-3">
//                <a href="'.url("presupuestos/editar/".$presupuesto->id).'" target="_blank" class="btn btn-info">
//                    Ver presupuesto
//                </a>
//            </div>
//            ';
//
//            $evento->fecha_inicio = Carbon::parse($historial->fecha)->format('Y-m-d');
//            $evento->fecha_fin = Carbon::parse($historial->fecha)->format('Y-m-d');
//            $evento->hora_inicio = Carbon::parse($historial->desde)->format('H:i:s');
//            $evento->hora_fin = Carbon::parse($historial->hasta)->format('H:i:s');
//            $evento->color = "#99ddff";
//            $evento->colorTexto = "black";
//            $evento->orden = 5;
//
//            $devolver[] = $evento;
//        }

        return $devolver;
    }

    public function obtenerEventosPendienteDeConfirmar() {

        $devolver = [];
        $presupuestos = Presupuesto::where('estado', 5)->get();

        foreach($presupuestos as $presupuesto) {

            $historial = HistorialEstado::where('estado', 5)
                ->where('presupuesto', $presupuesto->id)
                ->orderBy('id', 'desc')
                ->first();

            $evento = new Evento;
            $evento->titulo = $presupuesto->id;
            $evento->descripcion = '
            <ul>
                <li><strong>Nro. de presupuesto:</strong> ' . $presupuesto->id . '</li>
                <li><strong>Nombre:</strong> ' . $presupuesto->cliente()->nombre . ' ' . $presupuesto->cliente()->apellido . '</li>
                <li><strong>Fecha y Hora del servicio:</strong> ' . Carbon::parse($presupuesto->fecha.' '.$presupuesto->hora)->format('d/m/Y H:i') . ' - ' . Carbon::parse($presupuesto->fecha_fin.' '.$presupuesto->hora_fin)->format('d/m/Y H:i') . '</li>
                <li><strong>Direcciones de carga:</strong>';
                    foreach($presupuesto->cargas() as $direccion_carga) {
                        $evento->descripcion .= '<ul>
                                                    <li><strong>Dirección:</strong> '.$direccion_carga->direccion.' (Piso: '.$direccion_carga->piso.' - Departamento: '.$direccion_carga->departamento.'), '.$direccion_carga->localidad.'</li>
                                                    <li><strong>Entrecalles:</strong> '.$direccion_carga->entrecalles.'</li>
                                                    <li><strong>Observaciones:</strong> '.$direccion_carga->observaciones.'</li>
                                                 </ul>';
                    }
            $evento->descripcion .= '</ul>
                </li>
                <li><strong>Direcciones de descarga:</strong>';
                    foreach($presupuesto->descargas() as $direccion_descarga) {
                        $evento->descripcion .= '<ul>
                                                    <li><strong>Dirección:</strong> '.$direccion_descarga->direccion.' (Piso: '.$direccion_descarga->piso.' - Departamento: '.$direccion_descarga->departamento.'), '.$direccion_descarga->localidad.'</li>
                                                    <li><strong>Entrecalles:</strong> '.$direccion_descarga->entrecalles.'</li>
                                                    <li><strong>Observaciones:</strong> '.$direccion_descarga->observaciones.'</li>
                                                 </ul>';
                    }
            $evento->descripcion .= '</ul>
                </li>
                
                <li><strong>Observaciones:</strong> ' . $presupuesto->observaciones . '</li>
            </ul>
            
            <div class="mt-3">
                <a href="'.url("presupuestos/editar/".$presupuesto->id).'" target="_blank" class="btn btn-info">
                    Ver presupuesto
                </a>
            </div>
            ';

            $evento->fecha_inicio = Carbon::parse($historial->fecha)->format('Y-m-d');
            $evento->fecha_fin = Carbon::parse($historial->fecha)->format('Y-m-d');
            $evento->hora_inicio = Carbon::parse($historial->desde)->format('H:i:s');
            $evento->hora_fin = Carbon::parse($historial->hasta)->format('H:i:s');
            $evento->color = "#1d00ff";
            $evento->colorTexto = "#FFF";
            $evento->orden = 4;

            $devolver[] = $evento;
        }

        return $devolver;
    }

    public function obtenerEventosEnviarMaterialDeEmbalaje() {

        $devolver = [];

        $historiales = HistorialEstado::where('estado', 9)
//            ->groupBy('presupuesto')
            ->orderBy('id', 'desc')
            ->get();
        $historiales = $this->limpiarHistorialesRepetidosPorEstado($historiales);
        $historiales = $this->limpiarHistorialesAntesDeRechazos($historiales);

        foreach($historiales as $historial) {

            $presupuesto = $historial->presupuesto();

            $evento = new Evento;
            $evento->titulo = $presupuesto->id." - ".$presupuesto->cargas()[0]->localidad;
            $evento->descripcion = '
            <ul>
                <li><strong>Nro. de presupuesto:</strong> ' . $presupuesto->id . '</li>
                <li><strong>Nombre:</strong> ' . $presupuesto->cliente()->nombre . ' ' . $presupuesto->cliente()->apellido . '</li>
                <li><strong>Fecha y Hora:</strong> ' . Carbon::parse($historial->fecha)->format('d/m/Y').' ('.Carbon::parse($historial->desde)->format('H:i').' - '.Carbon::parse($historial->hasta)->format('H:i').')</li>
                <li><strong>Direcciones de carga:</strong>';
            foreach($presupuesto->cargas() as $direccion_carga) {
                $evento->descripcion .= '<ul>
                                                    <li><strong>Dirección:</strong> '.$direccion_carga->direccion.' (Piso: '.$direccion_carga->piso.' - Departamento: '.$direccion_carga->departamento.'), '.$direccion_carga->localidad.'</li>
                                                    <li><strong>Entrecalles:</strong> '.$direccion_carga->entrecalles.'</li>
                                                    <li><strong>Observaciones:</strong> '.$direccion_carga->observaciones.'</li>
                                                 </ul>';
            }
            $evento->descripcion .= '</ul>
                </li>
                <li><strong>Direcciones de descarga:</strong>';
            foreach($presupuesto->descargas() as $direccion_descarga) {
                $evento->descripcion .= '<ul>
                                                    <li><strong>Dirección:</strong> '.$direccion_descarga->direccion.' (Piso: '.$direccion_descarga->piso.' - Departamento: '.$direccion_descarga->departamento.'), '.$direccion_descarga->localidad.'</li>
                                                    <li><strong>Entrecalles:</strong> '.$direccion_descarga->entrecalles.'</li>
                                                    <li><strong>Observaciones:</strong> '.$direccion_descarga->observaciones.'</li>
                                                 </ul>';
            }
            $evento->descripcion .= '</ul>
                </li>
                
                <li><strong>Observaciones:</strong> ' . $presupuesto->observaciones . '</li>
            </ul>
            
            <div class="mt-3">
                <a href="'.url("presupuestos/editar/".$presupuesto->id).'" target="_blank" class="btn btn-info">
                    Ver presupuesto
                </a>
            </div>
            ';

            $evento->fecha_inicio = Carbon::parse($historial->fecha)->format('Y-m-d');
            $evento->fecha_fin = Carbon::parse($historial->fecha)->format('Y-m-d');
            $evento->hora_inicio = Carbon::parse($historial->desde)->format('H:i:s');
            $evento->hora_fin = Carbon::parse($historial->hasta)->format('H:i:s');
            $evento->color = "#b37505";
            $evento->colorTexto = "black";
            $evento->orden = 3;

            $devolver[] = $evento;
        }

        return $devolver;
    }

    public function obtenerEventosVencimientos() {

        $devolver = [];

        return $devolver;
    }

    public function obtenerEventosAdicionales() {

        $devolver = [];

        $historiales = HistorialEstado::where('estado', 7)
            ->orderBy('id', 'desc')
            ->get();
        $historiales = $this->limpiarHistorialesRepetidosPorEstado($historiales);
        $historiales = $this->limpiarHistorialesAntesDeRechazos($historiales);

        foreach($historiales as $historial) {

            $presupuesto = $historial->presupuesto();
            $adicionales = $presupuesto->otros_adicionales();

            foreach($adicionales as $adicional) {
                $evento = new Evento;
                $evento->titulo = $presupuesto->id." - ".$adicional->nombre;
                $evento->descripcion = '
            <ul>
                <li><strong>Nro. de presupuesto:</strong> ' . $presupuesto->id . '</li>
                <li><strong>Nombre:</strong> ' . $presupuesto->cliente()->nombre . ' ' . $presupuesto->cliente()->apellido . '</li>
                <li><strong>Fecha y Hora:</strong> ' . Carbon::parse($adicional->fecha)->format('d/m/Y') . ' (' . Carbon::parse($adicional->hora_inicio)->format('H:i') . ' - ' . Carbon::parse($adicional->hora_fin)->format('H:i') . ')</li>
                <li><strong>Direcciones de carga:</strong>';
                foreach ($presupuesto->cargas() as $direccion_carga) {
                    $evento->descripcion .= '<ul>
                                                    <li><strong>Dirección:</strong> ' . $direccion_carga->direccion . ' (Piso: ' . $direccion_carga->piso . ' - Departamento: ' . $direccion_carga->departamento . '), ' . $direccion_carga->localidad . '</li>
                                                    <li><strong>Entrecalles:</strong> ' . $direccion_carga->entrecalles . '</li>
                                                    <li><strong>Observaciones:</strong> ' . $direccion_carga->observaciones . '</li>
                                                 </ul>';
                }
                $evento->descripcion .= '</ul>
                </li>
                <li><strong>Direcciones de descarga:</strong>';
                foreach ($presupuesto->descargas() as $direccion_descarga) {
                    $evento->descripcion .= '<ul>
                                                    <li><strong>Dirección:</strong> ' . $direccion_descarga->direccion . ' (Piso: ' . $direccion_descarga->piso . ' - Departamento: ' . $direccion_descarga->departamento . '), ' . $direccion_descarga->localidad . '</li>
                                                    <li><strong>Entrecalles:</strong> ' . $direccion_descarga->entrecalles . '</li>
                                                    <li><strong>Observaciones:</strong> ' . $direccion_descarga->observaciones . '</li>
                                                 </ul>';
                }
                $evento->descripcion .= '</ul>
                </li>
                
                <li><strong>Observaciones:</strong> ' . $adicional->observaciones . '</li>
            </ul>
            
            <div class="mt-3">
                <a href="' . url("presupuestos/editar/" . $presupuesto->id) . '" target="_blank" class="btn btn-info">
                    Ver presupuesto
                </a>
            </div>
            ';

                $evento->fecha_inicio = Carbon::parse($adicional->fecha)->format('Y-m-d');
                $evento->fecha_fin = Carbon::parse($adicional->fecha)->format('Y-m-d');
                $evento->hora_inicio = Carbon::parse($adicional->hora_inicio)->format('H:i:s');
                $evento->hora_fin = Carbon::parse($adicional->hora_fin)->format('H:i:s');
                $evento->color = "#b37505";
                $evento->colorTexto = "black";
                $evento->orden = 6;

                $devolver[] = $evento;
            }
        }

        return $devolver;
    }

    /** END FUNCIONES EVENTOS */

    public function obtenerEventos() {
        $visitas = $this->obtenerEventosVisitas(); // VISITAS
        $presupuestosConfirmados = $this->obtenerEventosPresupuestosConfirmados(); // MUDANZAS
        $llamadosPendientes = $this->obtenerEventosLlamadasPendientes(); // LLAMADOS PENDIENTES (ESTADO PENDIENTE DE LLAMAR)
        $pendienteDeConfirmar = $this->obtenerEventosPendienteDeConfirmar(); // PENDIENTE DE CONFIRMAR (ESTADO PENDIENTE DE CONFIRMAR)
        $materialEmbalaje = $this->obtenerEventosEnviarMaterialDeEmbalaje(); // ENVIAR MATERIAL DE EMBALAJE
        $vencimientos = $this->obtenerEventosVencimientos(); // VENCIMIENTOS
        $adicionales = $this->obtenerEventosAdicionales(); // ADICIONALES DE PRESUPUESTOS CONFIRMADOS

        $eventos = array_merge($visitas, $presupuestosConfirmados, $llamadosPendientes, $pendienteDeConfirmar, $materialEmbalaje, $vencimientos, $adicionales);

        return $eventos;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventos = $this->obtenerEventos();

        return Response::json($eventos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Evento  $evento
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $eventos = $this->obtenerEventos();

        return view('agenda.editar')->with([
            'elementos' => $eventos,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Evento  $evento
     * @return \Illuminate\Http\Response
     */
    public function edit(Evento $evento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Evento  $evento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Evento $evento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Evento  $evento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Evento $evento)
    {
        //
    }
}
