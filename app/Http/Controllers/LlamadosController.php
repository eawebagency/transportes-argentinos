<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\HistorialEstado;
use App\Presupuesto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LlamadosController extends Controller
{

    /** HELPER FUNCTIONS */

    public function parsearPresupuestosDeDB($presupuestos) {
        $devolver = [];

        foreach($presupuestos as $presupuesto) {
            $devolver[] = Presupuesto::find($presupuesto->id);
        }

        return $devolver;

    }

    /** END HELPER FUNCTIONS */

    public function prospectos() {
        $clientes = Cliente::orderBy('created_at', 'desc')
            ->get();

        return view('llamados.prospectos')->with([
            'elementos' => $clientes,
        ]);
    }

    public function visitas() { // MATERIAL DE EMBALAJE + VISITAS + RETIRO DE SEÑA

        $presupuestos = [];

        $historiales = [];
        $historialesArray = DB::table('historial_estados as h1')
            ->select('h1.id', 'h1.estado', 'h1.presupuesto')
            ->where(function($query) {
//                $query->select(DB::raw(1))
//                    ->from('historial_estados')
                    $query->where('h1.estado', 2)
                    ->orWhere('h1.estado', 9)
                    ->orWhere('h1.estado', 11);
            })
            ->where('fecha', '<=', Carbon::now()->format('Y-m-d'))
            ->whereNotExists(function($query) {
                $query->select(DB::raw(1))
                    ->from('historial_estados as h2')
                    ->whereRaw('h1.presupuesto = h2.presupuesto AND h1.estado = h2.estado AND h2.id > h1.id');
            })
            ->orderBy('h1.id', 'desc')
            ->get();

        foreach($historialesArray as $historial) {
            $historiales[] = HistorialEstado::find($historial->id);
        }

//        $historiales = HistorialEstado::where(function($query) {
//            $query->select(DB::raw(1))
//                ->from('historial_estados')
//                ->where('estado', 2)
//                ->orWhere('estado', 9);
//        })
//            ->where('fecha', '<=', Carbon::now()->format('Y-m-d'))
//            ->orderBy('id', 'desc')
//            ->get();

        foreach($historiales as $historial) {
            $presupuesto = Presupuesto::find($historial->presupuesto);

            switch($historial->estado) {
                case 2:
                    if($presupuesto->enviarDetalle == 0) {
                        $presupuestos[] = $historial;
                    }
                    break;

                case 9:
                    if($presupuesto->enviarMaterial == 0) {
                        $presupuestos[] = $historial;
                    }
                    break;

                case 11:
                    if($presupuesto->enviarRetiroDeSenia == 0) {
                        $presupuestos[] = $historial;
                    }
                    break;
            }
        }

//        $historiales = HistorialEstado::where('estado', 2)
//            ->where('fecha', '<=', Carbon::now()->format('Y-m-d'))
//            ->orderBy('id', 'desc')
//            ->get();
//
//        foreach($historiales as $historial) {
//            $presupuesto = Presupuesto::find($historial->presupuesto);
//
//            if($presupuesto->enviarDetalle == 0) {
//                $presupuestos[] = $presupuesto;
//            }
//        }

//        $presupuestosSinFiltrar = Presupuesto::where('estado', 2)
//            ->where('enviarDetalle', 0)
//            ->orderBy('created_at', 'asc')
//            ->get();
//
//        foreach($presupuestosSinFiltrar as $presupuestoSinFiltrar) {
//            $historial = HistorialEstado::where('presupuesto', $presupuestoSinFiltrar->id)
//                ->where('estado', 2)
//                ->orderBy('id', 'desc')
//                ->first();
//
//            if(!empty($historial)) {
//                $fechaVisita = Carbon::parse($historial->fecha);
//                $fechaHoy = Carbon::now();
//
//                if($fechaVisita->lte($fechaHoy)) {
//                    $presupuestos[] = $presupuestoSinFiltrar;
//                }
//            }
//
//        }

//        $presupuestos = Presupuesto::where('estado', 2)
//            ->orWhere('estado', 9)
//            ->orderBy('created_at', 'asc')
//            ->get();

        return view('llamados.visitas')->with([
            'elementos' => $presupuestos,
        ]);
    }

//    public function material() {
//
//        $presupuestos = [];
//
//        $historiales = HistorialEstado::where('estado', 9)
//            ->where('fecha', '<=', Carbon::now()->format('Y-m-d'))
//            ->orderBy('id', 'desc')
//            ->get();
//
//        foreach($historiales as $historial) {
//            $presupuesto = Presupuesto::find($historial->presupuesto);
//
//            if($presupuesto->enviarMaterial == 0) {
//                $presupuestos[] = $presupuesto;
//            }
//        }
//
////        $presupuestosSinFiltrar = Presupuesto::where('estado', 9)
////            ->where('enviarMaterial', 0)
////            ->orderBy('created_at', 'asc')
////            ->get();
////
////        foreach($presupuestosSinFiltrar as $presupuestoSinFiltrar) {
////            $historial = HistorialEstado::where('presupuesto', $presupuestoSinFiltrar->id)
////                ->where('estado', 9)
////                ->orderBy('id', 'desc')
////                ->first();
////
////            if(!empty($historial)) {
////                $fechaVisita = Carbon::parse($historial->fecha);
////                $fechaHoy = Carbon::now();
////
////                if($fechaVisita->lte($fechaHoy)) {
////                    $presupuestos[] = $presupuestoSinFiltrar;
////                }
////            }
////
////        }
//
//        return view('llamados.material')->with([
//            'elementos' => $presupuestos,
//        ]);
//    }

    public function mudanzas() {

//        $presupuestos = Presupuesto::where('estado', 7)
//            ->where('enviarMudanza', 0)
//            ->where(function($query) {
//                $query->select(DB::raw(1))
//                    ->from('presupuestos')
//                    ->whereRaw('DATEDIFF(fecha, NOW()) = 1')
//                    ->orWhereRaw('DAYOFWEEK(NOW()) = 6 AND DAYOFWEEK(fecha) = 2');
//            })
//            ->get();

//        $presupuestos = [];
//        $historiales = HistorialEstado::where('estado', 7)
//            ->get();
//
//        foreach($historiales as $historial) {
//            $presupuesto = Presupuesto::find($historial->presupuesto);
//
//            $fechaHoy = Carbon::now();
//            $fechaMudanza = Carbon::parse($presupuesto->fecha);
//
//            if($presupuesto->enviarMudanza == 0 && ( ($fechaHoy->diffInDays($fechaMudanza) == 1) || ($fechaHoy->dayOfWeek == 5 && $fechaMudanza->dayOfWeek == 1) ) && $fechaMudanza->gt($fechaHoy)) {
//
//                $presupuestos[] = $presupuesto;
//            }
//        }

        $presupuestos = [];
        $historiales = DB::table('historial_estados')
            ->join('presupuestos', 'historial_estados.presupuesto', '=', 'presupuestos.id')
            ->select('presupuestos.id as presupuesto')
            ->distinct()
            ->where('historial_estados.estado', 7)
            ->where('presupuestos.fecha', '>=', Carbon::now()->format('Y-m-d'))
            ->where(function ($query) {
                $query->whereRaw('DATEDIFF(presupuestos.fecha, NOW()) = 1 OR DATEDIFF(presupuestos.fecha, NOW()) = 2')
                    ->orWhereRaw('(DAYOFWEEK(NOW()) = 6 AND DAYOFWEEK(presupuestos.fecha) = 2 AND DATEDIFF(presupuestos.fecha, NOW()) = 3) or (DAYOFWEEK(NOW()) = 7 AND DAYOFWEEK(presupuestos.fecha) = 2 AND DATEDIFF(presupuestos.fecha, NOW()) = 2)');
            })
            ->where('presupuestos.enviarMudanza', 0)
            ->get();
        foreach($historiales as $historial) {
            $presupuesto = Presupuesto::find($historial->presupuesto);

            $presupuestos[] = $presupuesto;
        }

        return view('llamados.mudanzas')->with([
            'elementos' => $presupuestos,
        ]);
    }

    public function confirmar() { // PENDIENTES DE LLAMAR Y LLAMADOS CON PRIORIDAD

        $historiales = HistorialEstado::where(function($query2) {
                    $query2->where('estado', 1)
                        ->orWhere('estado', 5)
                        ->orWhere('estado', 10);
                })
            ->where('fecha', '<=', Carbon::now()->format('Y-m-d'))
            ->whereNotExists(function($query) {
                $query->select(DB::raw(1))
                    ->from('presupuestos')
                    ->where('presupuestos.estado', 3)
                    ->whereRaw('presupuestos.id = historial_estados.presupuesto');
            })
            ->orderBy('fecha', 'desc')
            ->get();

        $presupuestos = $historiales;

        /** V1 */
//        $presupuestos = Presupuesto::whereExists(function($query) {
//            $query->select(DB::raw(1))
//                ->from('historial_estados')
//                ->where(function($query2) {
//                    $query2->where('historial_estados.estado', 1)
//                        ->orWhere('historial_estados.estado', 5)
//                        ->orWhere('historial_estados.estado', 10);
//                })
//                ->where('historial_estados.fecha', '<=', Carbon::now()->format('Y-m-d'))
//                ->whereRaw('historial_estados.presupuesto = presupuestos.id');
//        })
//            ->orderBy('created_at', 'asc')
//            ->get();

        /** V2 */

//        $presupuestosDB = Presupuesto::where('estado', 1)
//            ->orWhere('estado', 5)
//            ->orWhere('estado', 10)
//            ->orderBy('created_at', 'asc')
//            ->get();
//
//        foreach($presupuestosDB as $presupuesto) {
//            $historial = HistorialEstado::where('presupuesto', $presupuesto->id)
//                ->where('estado', $presupuesto->estado)
//                ->where('fecha', '<=', Carbon::now()->format('Y-m-d'))
//                ->orderBy('id', 'desc')
//                ->first();
//
//            if(!empty($historial)) {
//                $presupuestos[] = $historial;
//            }
//        }

        // ORDEN SEGÚN FECHA Y HORA
//        for($i = 0; $i < count($presupuestos); $i++) {
//            for($j = 0; $j < count($presupuestos) - 1; $j++) {
//
//                $fecha1 = Carbon::parse($presupuestos[$j]->fecha);
//                $fecha2 = Carbon::parse($presupuestos[$j+1]->fecha);
//
//                if($fecha1->gt($fecha2)) {
//                    $auxiliar = $presupuestos[$j];
//                    $presupuestos[$j] = $presupuestos[$j+1];
//                    $presupuestos[$j+1] = $auxiliar;
//                } else if($fecha1->eq($fecha2)) {
//
//                    $hora1 = Carbon::parse($presupuestos[$j]->desde);
//                    $hora2 = Carbon::parse($presupuestos[$j+1]->desde);
//
//                    if($hora1->gt($hora2)) {
//                        $auxiliar = $presupuestos[$j];
//                        $presupuestos[$j] = $presupuestos[$j+1];
//                        $presupuestos[$j+1] = $auxiliar;
//                    }
//                }
//            }
//        }

//        $presupuestosDB = DB::table('presupuestos')
//            ->select('he1.id')
//            ->join('historial_estados as he1', 'presupuestos.id', '=', 'he1.presupuesto')
//            ->where(function($query) {
//                $query->where('presupuestos.estado', 1)
//                    ->orWhere('presupuestos.estado', 5);
//            })
//            ->where('he1.fecha', '<=', Carbon::now()->format('Y-m-d'))
//            ->whereNotExists(function($query) {
//                $query->select(DB::raw(1))
//                    ->from('historial_estados as he2')
//                    ->whereRaw('he2.presupuesto = he1.presupuesto AND he2.estado = he1.estado AND he2.id > he1.id');
//            })
//            ->orderBy('he1.fecha', 'asc')
//            ->get();
//        $presupuestosDB = DB::table('historial_estados as he1')
//            ->select('he1.id')
//            ->join('presupuestos', 'he1.presupuesto', '=', 'presupuestos.id')
//            ->where(function($query) {
//                $query->where('presupuestos.estado', 1)
//                    ->orWhere('presupuestos.estado', 5);
//            })
//            ->where('he1.fecha', '<=', Carbon::now()->format('Y-m-d'))
//            ->whereNotExists(function($query) {
//                $query->select(DB::raw('*'))
//                    ->from('historial_estados as he2')
//                    ->whereRaw('he2.presupuesto = he1.presupuesto AND he2.estado = he1.estado AND he2.fecha <= '.Carbon::now()->format('Y-m-d').' AND he2.id > he1.id');
//            })
//            ->orderBy('he1.fecha', 'asc')
//            ->get();
////            ->toSql();
//        dd($presupuestosDB);
//
//        foreach($presupuestosDB as $presupuesto) {
//            $historial = HistorialEstado::find($presupuesto->id);
//
//            $presupuestos[] = $historial;
//        }

        return view('llamados.confirmar')->with([
            'elementos' => $presupuestos,
        ]);
    }
}
