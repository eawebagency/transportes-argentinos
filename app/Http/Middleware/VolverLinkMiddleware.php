<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class VolverLinkMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $urlsExcluidas = [
            '/items_adicionales/ver',
            '/items_observaciones/ver',
        ];

        $path = $request->getRequestUri();

        if($request->getMethod() == 'GET') {

            if(!in_array($path, $urlsExcluidas)) {

                $pathArray = explode('/', $path);

                if ($pathArray[count($pathArray) - 2] != 'editar') {
                    Session::put('urlActual', $path);
                }

                Session::put('urlPrevia', Session::get('urlActual'));
            }
        }

        return $next($request);
    }
}
