<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    public function usuario() {
        return $this->belongsTo('App\User', 'usuario')->first();
    }

    public function ultimoUsuario() {
        return $this->belongsTo('App\User', 'ultimoUsuario')->first();
    }

    public function contacto() {
        return $this->belongsTo('App\FormaContacto', 'contacto')->first();
    }

    public function estado() {
        return $this->belongsTo('App\EstadoCliente', 'estado')->first();
    }
}
