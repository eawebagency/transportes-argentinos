<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Presupuesto extends Model
{

    public function cargas() {
        return Direccion::where('presupuesto', $this->id)->where('carga', true)->get();
    }

    public function descargas() {
        return Direccion::where('presupuesto', $this->id)->where('carga', false)->get();
    }

    public function vehiculo() {
        return $this->belongsTo('App\Vehiculo', 'vehiculo')->first();
    }

    public function servicio() {
        return $this->belongsTo('App\Servicio', 'servicio')->first();
    }

    public function estado() {
        return $this->belongsTo('App\EstadoPresupuesto', 'estado')->first();
    }

//    public function visitas() {
//        return $this->hasMany('App\Visita', 'presupuesto')->get();
//    }

    public function visita() {
        return $this->hasOne('App\Visita', 'presupuesto')->first();
    }

    public function adicionales() {
        return $this->belongsToMany('App\Adicional', 'presupuesto_adicionals', 'presupuesto', 'adicional')->get();
    }

    public function otros_adicionales() {
        return $this->hasMany('App\OtroAdicional', 'presupuesto')->get();
    }

    public function formasDePago() {
        return $this->belongsToMany('App\FormaPago', 'presupuesto_pagos', 'presupuesto', 'forma')->get();
    }

    public function pagos() {
        return $this->hasMany('App\Pago', 'presupuesto')->get();
    }

    public function vendedor() { // OPERADOR
        return $this->belongsTo('App\User', 'vendedor')->first();
    }

    public function cliente() {
        return $this->belongsTo('App\Cliente', 'cliente')->first();
    }

    public function historialEstados() {
        return $this->hasMany('App\HistorialEstado', 'presupuesto')->orderBy('id', 'desc')->get();
    }

    /** OTHER FUNCTIONS **/

    public function verificarPago() {
        $pagado = 0;

        foreach($this->pagos() as $pago) {
            $pagado += $pago->monto;
        }

        if($pagado >= $this->cotizacion) {
            // SI EL MONTO PAGADO SUPERA O IGUALA LA COTIZACION, PASAMOS EL PRESUPUESTO A FINALIZADO
            $this->estado = 8;
            $this->save();
        }

        return false;

    }

    public function pagado() {
        $pagado = 0;

        foreach($this->pagos() as $pago) {
            $pagado += $pago->monto;
        }

        return $pagado;
    }

    public function faltaPagar() {
        $faltaPagar = $this->cotizacion;

        foreach($this->pagos() as $pago) {
            $faltaPagar -= $pago->monto;
        }

        return $faltaPagar;
    }

    public function existeAdicional($adicional) {
        return !empty(DB::table('presupuesto_adicionals')->where('presupuesto', $this->id)->where('adicional', $adicional)->first());
    }

    public function setearTotal() {

        // EN ESTE MÉTODO SE OBTIENE EL VALOR TOTAL SIN IMPUESTOS

        $cotizacion_valor = 0;


        if(!empty($this->vehiculo())) {
            $cotizacion_valor += $this->valor_aproximado;
        }

        if(!empty($this->adicionales())) {

            foreach ($this->adicionales() as $adicional) {
                $adicionalM = Adicional::find($adicional);

                $cotizacion_valor += $adicionalM->valor;
            }
        }

        if(!empty($this->otros_adicionales())) {

            foreach ($this->otros_adicionales() as $otro_adicional) {

                $cotizacion_valor += $otro_adicional['valor'];
            }
        }

        $this->total = $cotizacion_valor;
    }

    public function ultimaVisita() {
        $visita = HistorialEstado::where(function($query) {
            $query->where('estado', 2)
                ->orWhere('estado', 9);
        })
            ->where('presupuesto', $this->id)
            ->orderBy('id', 'desc')
            ->first();

        return $visita;
    }

    public function ultimaVisitaPorEstado($id) {
        $visita = HistorialEstado::where('estado', $id)
            ->where('presupuesto', $this->id)
            ->orderBy('id', 'desc')
            ->first();

        return $visita;
    }

}
